<?php
add_action("wp_head","olwslider");
function olwslider(){
echo '<script type="text/javascript" src="'.CHILD_URL.'/js/jquery.sticky.js"></script>';
}
add_action('genesis_before_content_sidebar_wrap', 'header_giaovien');
function header_giaovien(){
?>
<div id="highlight">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="header_right">
					<h1 class="page-header">Giáo viên toán: Nguyễn Thị Thu Hoài</h1>
					<div class="desc">Bạn muốn Kinh Doanh Online nhưng Không có Vốn Trong tay? Khoá học giúp bạn xây dựng hệ thống Khởi nghiệp kinh doanh tự động từ 2 bàn tay trắng</div>
					<div class="teacher">
						<span>Chủ đề:</span>
						<span class="title">
							<a class="xanhthan" href="#">Bộ môn toán</a>
						</span>
					</div>
					<div class="star star_yellow">
						<ul class="item-list" style="color: yellow;">
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
						</ul>
						<span class="title">122 đánh giá - 1652 học viên </span>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div id="sidebar-right">
					<div class="content" id="video-youtube">
						<div class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
							<i class="fa fa-youtube-play fa-4" aria-hidden="true"></i>
							<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/montoan.png" alt="">
						</div>
						<!-- Modal -->
						<div class="modal fade" id="myModal" role="dialog">
							<div class="modal-dialog">
								
								<!-- Modal content-->
								<div class="modal-content">
									<!-- <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Video trailer</h4>
									</div> -->
									<div class="modal-body">
										<iframe width="600" height="336" src="https://www.youtube.com/embed/pW7pvUIXoDs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div id="countdown">
						<div id='tiles'></div>
						<div class="labels">
							<li>Ngày</li>
							<li>Giờ</li>
							<li>Phút</li>
							<li>Giây</li>
						</div>
					</div>
					<div id="block-15" class="block-cart">
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<span class="old-price">200,000<sup>đ</sup></span>
							<span class="discount">-50%</span>
						</div>
						<div class="link link1">
							<a href="#" title="">Đăng ký học</a>
						</div>
						<div class="desc">
							<ul class="item-list">
								<li class="fa fa-play-circle" aria-hidden="true">&nbsp;<b>64</b>bài giảng</li><br>
								<li class="fa fa-clock-o" aria-hidden="true">&nbsp;06 giờ 18 phút</li>
								<li class="fa fa-sign-in" aria-hidden="true">&nbsp; Mua 1 gói học trọn đời</li>
								<li class="fa fa-mobile" aria-hidden="true">&nbsp;truy cập thiết bị di động</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(window).bind('scroll', function () {
if ($(window).scrollTop() > 300) {
$('#video-youtube').addClass('fixed');
} else {
$('#video-youtube').removeClass('fixed');
}
});
</script>
<script type="text/javascript">
	var target_date = new Date().getTime() + (1000*3600*48); // set the countdown date
var days, hours, minutes, seconds; // variables for time units
var countdown = document.getElementById("tiles"); // get tag element
getCountdown();
setInterval(function () { getCountdown(); }, 1000);
function getCountdown(){
	// find the amount of "seconds" between now and target
	var current_date = new Date().getTime();
	var seconds_left = (target_date - current_date) / 1000;
	days = pad( parseInt(seconds_left / 86400) );
	seconds_left = seconds_left % 86400;
		
	hours = pad( parseInt(seconds_left / 3600) );
	seconds_left = seconds_left % 3600;
		
	minutes = pad( parseInt(seconds_left / 60) );
	seconds = pad( parseInt( seconds_left % 60 ) );
	// format countdown string + set tag value
	countdown.innerHTML = "<span>" + days + "</span><span>" + hours + "</span><span>" + minutes + "</span><span>" + seconds + "</span>";
}
function pad(n) {
	return (n < 10 ? '0' : '') + n;
}
</script>
<!-- <script>
	$(window).load(function(){
		$("#highlight").sticky({ topSpacing: 0 });
	});
</script> -->
<?php
}
add_action('genesis_loop','header_content');
function header_content(){
?>
<div class="">
	<div id="main-content">
		<div id="block-6" class="block learning" style="margin-top: 5px;">
			<h2 class="block-title">Bạn sẽ học được gì?</h2>
			<div class="content">
				<div class="row">
					<div class="col-sm-6">
						<div class="title">Thiết kế hình ảnh chuyên nghiệp mà không cần dùng đến các phần mềm Photoshop,inllustrator...</div>
					</div>
					<div class="col-sm-6">
						<div class="title">Ứng dụng đa lĩnh vực, từ học tập, công việc, cuộc sống...</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="title">Ghi điểm với khán giả ngồi xem với những hình ảnh, font chữ bắt mắt</div>
					</div>
					<div class="col-sm-6">
						<div class="title">Truyền đạt ý tưởng một cách ngắn gọn súc tích mà không tốn nhiều thời gian chuẩn bị</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="title">
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="block-7" class="block learning2">
			<h2 class="block-title">Đối tượng đào tạo</h2>
			<div class="content">
				<ul class="item-list">
					<li>Dành cho tất cả những ai sử dụng thường xuyên Power Point để phục vụ thuyết trình, đặc biệt là Sinh Viên và Người đi làm</li>
					<li>Độ tuổi : 19 - 40 tuổi</li>
				</ul>
			</div>
		</div>
		
		<div id="block-9" class="block learning3">
			<h2 class="block-title">Giới thiệu khóa học</h2>
			<div class="content">
				<p>Bạn đã bao giờ dành thời gian, tâm huyết, hì hụi chuẩn bị một bài thuyết trình tuyệt với những slide ấn tượng nhất cho khán giả của bạn? Và rồi nhận được những phản hồi đại loai: "Phong cách tốt nhưng slide của bạn tôi chẳng nhớ được gì !"</p>
				<p>Bạn cảm thấy thật thất vọng, chán nản?</p>
				<p>Đừng lo lắng, vì đã có Unica.</p>
				<p>Khóa học Thiết kế Power Point - của Chuyên gia giảng dạy Visual Thinking Trần Quang Vũ sẽ giúp bạn làm "chinh phục thị giác khán giả.</p>
				<p>Bạn không cần thành thạo những công cụ trong phần mềm Power Ponit. Bạn không cần phải là một nhà thiết kế chuyên nghiệp. Bạn cũng không cần tốn quá nhiều thời gian để thiết kế một slide dễ dàng đi vào lòng người.&nbsp;</p>
			</div>
		</div>
		
		<div id="block-11" class="block detail-project">
			<h2 class="block-title">Nội dung khóa học</h2>
			<div class="content">
				<button class="accordion">Phần 1: Nhập môn thiết kế Power Point</button>
				<div class="panel">
					<div class="col">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-5 col-md-8">
									<div class="title">
										<a href="#">
											<i class="fa fa-play-circle" aria-hidden="true"></i> Bài 1: Làm quen với thiết kế
										</a>
									</div>
								</div>
								<div class="col-xs-4 col-md-2">
									<div class="link">&nbsp;<a class="btn-preview" href="javascript:void(0)" onclick="preview_freetrial(658);">Học thử</a>
								</div>
							</div>
							<div class="col-xs-3 col-md-2"><div class="time">00:03:12</div></div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-5 col-md-8">
								<div class="title">
									<a href="#"><i class="fa fa-play-circle" aria-hidden="true"></i> Bài 2: Chuẩn bị</a>
								</div>
							</div>
							<div class="col-xs-4 col-md-2">
								<div class="link">&nbsp;<a class="btn-preview" href="javascript:void(0)" onclick="preview_freetrial(657);">Học thử</a></div>
							</div>
							<div class="col-xs-3 col-md-2"><div class="time">00:01:48</div></div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-5 col-md-8">
								<div class="title">
									<a href="#"><i class="fa fa-play-circle" aria-hidden="true"></i> Bài 3: Tầm quan trọng của Layout</a>
								</div>
							</div>
							<div class="col-xs-4 col-md-2">
								<div class="link">&nbsp;
									<a class="btn-preview" href="javascript:void(0)" onclick="preview_freetrial(723);">Học thử</a>
								</div>
							</div>
							<div class="col-xs-3 col-md-2">
								<div class="time">00:11:16</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<button class="accordion">Phần 2: Hướng dẫn thực hành chi tiết</button>
			<div class="panel">
				<div class="col">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-5 col-md-8">
								<div class="title">
									<a href="#">
										<i class="fa fa-play-circle" aria-hidden="true"></i> Bài 1: Làm quen với thiết kế
									</a>
								</div>
							</div>
							<div class="col-xs-4 col-md-2">
								<div class="link">&nbsp;<a class="btn-preview" href="javascript:void(0)" onclick="preview_freetrial(658);">Học thử</a>
							</div>
						</div>
						<div class="col-xs-3 col-md-2"><div class="time">00:03:12</div></div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-5 col-md-8">
							<div class="title">
								<a href="#"><i class="fa fa-play-circle" aria-hidden="true"></i> Bài 2: Chuẩn bị</a>
							</div>
						</div>
						<div class="col-xs-4 col-md-2">
							<div class="link">&nbsp;<a class="btn-preview" href="javascript:void(0)" onclick="preview_freetrial(657);">Học thử</a></div>
						</div>
						<div class="col-xs-3 col-md-2"><div class="time">00:01:48</div></div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-5 col-md-8">
							<div class="title">
								<a href="#"><i class="fa fa-play-circle" aria-hidden="true"></i> Bài 3: Tầm quan trọng của Layout</a>
							</div>
						</div>
						<div class="col-xs-4 col-md-2">
							<div class="link">&nbsp;
								<a class="btn-preview" href="javascript:void(0)" onclick="preview_freetrial(723);">Học thử</a>
							</div>
						</div>
						<div class="col-xs-3 col-md-2">
							<div class="time">00:11:16</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<button class="accordion">Phần 3: Hoàn thiện sản phẩm</button>
		<div class="panel">
			<div class="col">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-5 col-md-8">
							<div class="title">
								<a href="#">
									<i class="fa fa-play-circle" aria-hidden="true"></i> Bài 1: Làm quen với thiết kế
								</a>
							</div>
						</div>
						<div class="col-xs-4 col-md-2">
							<div class="link">&nbsp;<a class="btn-preview" href="javascript:void(0)" onclick="preview_freetrial(658);">Học thử</a>
						</div>
					</div>
					<div class="col-xs-3 col-md-2"><div class="time">00:03:12</div></div>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-5 col-md-8">
						<div class="title">
							<a href="#"><i class="fa fa-play-circle" aria-hidden="true"></i> Bài 2: Chuẩn bị</a>
						</div>
					</div>
					<div class="col-xs-4 col-md-2">
						<div class="link">&nbsp;<a class="btn-preview" href="javascript:void(0)" onclick="preview_freetrial(657);">Học thử</a></div>
					</div>
					<div class="col-xs-3 col-md-2"><div class="time">00:01:48</div></div>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-5 col-md-8">
						<div class="title">
							<a href="#"><i class="fa fa-play-circle" aria-hidden="true"></i> Bài 3: Tầm quan trọng của Layout</a>
						</div>
					</div>
					<div class="col-xs-4 col-md-2">
						<div class="link">&nbsp;
							<a class="btn-preview" href="javascript:void(0)" onclick="preview_freetrial(723);">Học thử</a>
						</div>
					</div>
					<div class="col-xs-3 col-md-2">
						<div class="time">00:11:16</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;
		for (i = 0; i < acc.length; i++) {
			acc[i].addEventListener("click", function() {
				this.classList.toggle("active");
			var panel = this.nextElementSibling;
			if (panel.style.maxHeight){
				panel.style.maxHeight = null;
			} else {
				panel.style.maxHeight = panel.scrollHeight + "px";
			}
		});
		}
	</script>
</div>
</div>
<div id="block-12" class="block giangvien">
<h2 class="block-title">Giảng viên</h2>
<div class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="images">
				<center><img async="" style="border-radius: 50% 50%;" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/giaovienhoai.jpg"></center>
			</div>
		</div>
		<div class="col-md-9">
			<div class="title">
				<center><a style="color:#333;font-weight:bold;font-size: 20px;" href="#">Nguyễn Thị Thu Hoài</a></center>
			</div>
			<div class="desc article noidung">
				<p>Giảng viên Nguyễn Thị Thu Hoài hiện đang là chuyên gia giảng dạy trong lĩnh vực Visual Thinking - Tư duy hình ảnh.</p>
				<p>Anh đã có 4 năm kinh nghiệm trong việc nghiên cứu và ứng dụng Visual Thinking vào học tập, công việc và cuộc sống.</p>
				<p>Hiện tại, anh đang là Trainer , đã từng giảng dạy tại Vietfuture, Prepo, Topica... và nhiều đối tác khác.</p>
				<p>Từ thực tế công việc diễn thuyết của mình, anh Vũ hiểu rằng một trong những yếu tố làm nên thành công tại các buổi đào tạo là việc tương tác giữa giảng viên và học viên bằng những câu hỏi và hiệu ứng do Slide mang lại.&nbsp;</p>
				<p>Anh hiểu được một slide thu phục lòng người cần phải làm gì, và cần đưa thông tin như thế nào cho khán giả dễ theo dõi.</p>
				<p>&nbsp;</p>
			</div>
			<div class="more"><!-- <a href="#">Xem tiếp</a> --></div>
		</div>
	</div>
</div>
</div>
<div id="block-13" class="block danhgia">
<h2 class="block-title">Học viên đánh giá</h2>
<div class="content">
	<?php comments_template(); ?>
</div>
</div>
</div>
</div>
<?php
}
genesis();