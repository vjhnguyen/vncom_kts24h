<?php
add_action( 'wp_head', 'add_my_style' );
function add_my_style() {
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/nguyenthang-monhoa.css">';
}
add_action('genesis_loop','content_hoahoc');
function content_hoahoc()
{
?>
<div class="container">
	<div class="row">
		<div class="img_bg">
		</div>
		<div>
			<img class="img_mon" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/images-thang/a.png"/>
		</div>
		<div class="ten_mon">
			<h1>MÔN TOÁN</h1>
			<h5>SAMSET EDUCATION</h5>
		</div>
		<div class="phu"></div>
		<div class="header_content">
			<div class="row main">
				<div class="col-md-12 title_mon">
					<div class="row logo_content">
						<div class="col-md-6 ctdt">
							<h2>CHƯƠNG TRÌNH HỌC</h2>
						</div>
						<div class="col-md-6 mon">
							<h2>MÔN TOÁN</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 mota">
							<h5>Học trực tuyến bởi Công ty CP giáo dục và đào tạo SamSet</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ndmota">
					<p>
						Năm 2018, môn Toán tiếp tục được tổ chức thi dưới hình thức trắc nghiệm. Nội dung kiến thức thi môn Toán trong THPT Quốc gia năm nay yêu cầu thí sinh phải nắm vững ở cả 2 năm lớp 11 và lớp 12. Do đó, lượng kiến thức khá nặng.<br>
						Hiểu được những gì học sinh cần nắm vững, kenhtuyensin24h kết hợp với Samset Education khởi động Chương trình hỗ trợ luyện thi THPT Quốc Gia môn Toán, được chia làm 3 giai đoạn tương ứng với 3 khóa học Khởi Động – Tăng Tốc và Về Đích.
					</p>
				</div>
			</div>
		</div>
		<div class="ctts">
			<h3> LỰA CHỌN KHÓA HỌC</h3>
			<div class="ctts_left">
				<button class="tablinks" onclick="openCity(event, 'khoaKD')" id="defaultOpen">Khóa Khởi Động</button><br></br>
				<button class="tablinks" onclick="openCity(event, 'khoaCT')">Khóa Tăng Tốc</button><br></br>
				<button class="tablinks" onclick="openCity(event, 'khoa')">Khóa Về Đích</button>
			</div>
		</div>
		<div id="khoaKD" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA KHỞI ĐỘNG</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>Khóa Khởi Động được thiết kế phù hợp cho các bạn mất gốc môn Toán, các bạn có học lực trung bình khá. Khóa học sẽ tập trung cho các bạn học sinh ôn luyện các kiến thức nền tảng, cần thiết có trong kỳ thi. Đồng thời, học tới đâu, các dạng bài tập ví dụ minh họa cũng được đưa vào kết hợp với phương pháp giải tới đó, giúp học sinh hiểu và nắm bắt ngay kiến thức và các dạng bài tập đi kèm. </p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>Cập nhật...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>Cập nhật...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Học sinh sẽ lấy lại được kiến thức nền tảng.<br>
								- Hệ thống hóa lại khối kiến thức đồ sộ một cách ngắn gọn, dễ hiểu, dễ nhớ <br>
								- Học sinh sẽ giải được các bài toán cơ bản về hàm số, lượng giác, logarit, tích phân, số phức, hình học không gian và hình tọa độ không gian trong đề thi THPT Quốc gia chính thức.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>Cập nhật...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>Cập nhật...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoaCT" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA TĂNG TỐC</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Tăng Tốc môn Toán được thiết kế phù hợp cho các bạn học sinh đã nắm được các kiến thức cơ bản trong chương trình thi THPT nhưng chưa có kinh nghiệm làm bài.<br>
								- Đúng như tên gọi, nếu như Khóa Khởi Động là bước đệm cung cấp cho học sinh hành trang kiến thức, thì ở Khóa Tăng Tốc, học sinh sẽ tập trung vận dụng hết những kiến thức đó vào giải các dạng bài tập và luyện đề, giúp học sinh nắm vững các phương pháp, kỹ năng để sẵn sàng đương đầu với mọi thiên biến vạn hóa của đề thi.
							</p>
						</div>
						<div class="col-md-5">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Sau khóa học, học sinh sẽ tự tin làm được tất cả các bài tập ở độ khó tương đương với đề chính thức, kể cả các dạng bài ở những chuyên đề thường khiến các bạn học sinh e ngại như:  hình học không gian, hình học tọa độ, loga, tích phân… <br>
								- Như vậy, với cấu trúc đề thi Toán bao gồm 50% câu dễ, 30% câu hỏi ở mức độ khá, và 15-20% là câu khó, qua khóa học, học sinh có thể làm được tối thiểu 5 điểm đề thi.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoa" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA VỀ ĐÍCH</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Về Đích môn Toán được thiết kế với mục đích tổng ôn và hệ thống hóa kiến thức trọng tâm cho các bạn học sinh đã nắm vững kiến thức và đã có tương đối kinh nghiệm luyện đề. <br>
								- Đúng như tên gọi, Khóa Về Đích môn Toán giúp học sinh hoàn thiện những bước chạy nước rút cuối cùng, khắc phục những lỗi sai thường gặp, thuần thục các phương pháp, kỹ năng và mẹo làm bài, với tiêu chí: đọc đề - chỉ việc thay số và bấm kết quả.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Với các dạng bài tập và kiến thức trọng tâm, sát nhất với cấu trúc đề thi của bộ, học sinh sẽ được rèn luyện nhuần nhuyễn phương pháp, mẹo và kỹ năng phân bổ thời gian làm đề; được chỉ ra các lỗi sai và cách khắc phục; đảm bảo cho các bạn có thể ghi điểm chắc chắn những câu dễ trong thời gian ngắn nhất, dành thời gian còn lại cho những câu khó hơn để nâng cao mức điểm. <br>
								- Với khóa Về Đích, học sinh có thể đạt được tối thiểu 6 điểm cho bài thi của mình.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function openCity(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tab");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
<?php
}
genesis();