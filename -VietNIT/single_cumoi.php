<?php

global $post;
$categories = get_the_category( $post->ID);
$cat = $categories[0]->cat_ID;	

if(has_post_format( 'aside' )){
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action('genesis_loop','main_content' );
//add_action('genesis_after_loop', 'tag');
add_action('genesis_after_loop','line');
add_action( 'genesis_after_loop', 'genesis_get_comments_template' );
add_action("genesis_after_content_sidebar_wrap","tinlienquan");
add_action("genesis_after_content_sidebar_wrap","nhomnganh");
add_action('wp_head','position2');
}
elseif($cat==3 || $cat==232 || $cat==203 || $cat==870 || $cat==869 || $cat==868 || $cat==7901 || $cat==8479 || $cat==7893){
add_filter( 'the_content', 'prefix_insert_post_ads' );
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action("genesis_loop","tintuc");
add_action('genesis_after_loop','line');
//add_action( 'genesis_loop', 'genesis_get_comments_template' );
add_action("genesis_after_content_sidebar_wrap","cungchuyenmuc");
add_action('wp_head','position1');
}
elseif($cat==84 || $cat==76 || $cat==85){
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action('genesis_loop','main_content_lienthong' );
//add_action('genesis_after_loop', 'tag');
add_action('genesis_after_loop','line');
add_action( 'genesis_after_loop', 'genesis_get_comments_template' );
add_action("genesis_after_content_sidebar_wrap","tinlienquan");
add_action("genesis_after_content_sidebar_wrap","cungchuyenmucvb2");
add_action('genesis_before_content_sidebar_wrap',"opendom" );
add_action('genesis_after_content_sidebar_wrap',"closedom" );
add_action('wp_head','position2');

function opendom(){
	?>
	<div id="lienthong">
	<?php
}
function closedom(){
	?>
	</div>
	<?php
}
}
else{
remove_action( 'genesis_loop', 'genesis_do_loop' );	
add_action("genesis_loop","tintuc");
add_action('wp_head','position1');
}
add_action("wp_head","olwslider");
function olwslider(){
	echo '<script src="'.CHILD_URL.'/js/olw/owl.carousel.js"></script>';	
	echo '<link type="text/css" href="'.CHILD_URL.'/css/olw/owl.carousel.css" rel="stylesheet" />';
	echo '<link type="text/css" href="'.CHILD_URL.'/css/olw/owl.theme.css" rel="stylesheet" />';
	?>
	<script type="text/javascript">
		$(document).ready(function() {
 
			  var owl = $(".slcontent");

			  owl.owlCarousel({
			      items : 4, //10 items above 1000px browser width
			      itemsDesktop : [1000,5], //5 items between 1000px and 901px
			      itemsDesktopSmall : [900,3], // betweem 900px and 601px
			      itemsTablet: [600,2], //2 items between 600 and 0
			      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
			  });
			 
			  // Custom Navigation Events
			  $(".next").click(function(){
			    owl.trigger('owl.next');
			  })
			  $(".prev").click(function(){
			    owl.trigger('owl.prev');
			  })
			});
	</script>	    
	<?php
}
function line(){
	?>
	<div class="linearea"></div>
	<?php
}
function position1(){
	?>
	<script>
    $(function(){
	    var elementPosition = $('.linearea').offset().top;
	    var elementPosition2 = $('#caia-post-list-9').offset().top;
    	console.log(elementPosition-250);
    	console.log(elementPosition2-200);
    	$(window).on('scroll', function(){
                if( $(window).scrollTop()>elementPosition2-200 && $(window).scrollTop() < elementPosition-650 ){
                    $('#caia-post-list-9').addClass('fixed');
                    $('#caia-post-list-9').removeClass('fixedp');
                }
                else if($(window).scrollTop()>elementPosition-650){
                    $('#caia-post-list-9').removeClass('fixed');
                    $('#caia-post-list-9').addClass('fixedp');
                }
                else{
	                $('#caia-post-list-9').removeClass('fixedp');
	                $('#caia-post-list-9').removeClass('fixed');	
                }
            });
            $(window).on('scroll', function(){
                if( $(window).scrollTop()>1670 && $(window).scrollTop()< elementPosition-400){
                    $('#text-23').addClass('fixed2');
                    $('#text-23').removeClass('fixed2p');
                }
                else if($(window).scrollTop() > elementPosition-400){
                    $('#text-23').removeClass('fixed2');
                    $('#text-23').addClass('fixed2p');
                }
                else{
                	$('#text-23').removeClass('fixed2');
                	$('#text-23').removeClass('fixed2p');
                }
            });
	});
    		
    </script>
	<?php
}

function position2(){
	?>
	<script>
    $(function(){
	    var elementPosition = $('#nhomnganh').offset().top;
    	console.log(elementPosition);    	    	
            $(window).on('scroll', function(){
                if( $(window).scrollTop()>1600 && $(window).scrollTop()< elementPosition-500){
                	$('#text-23').removeClass('fixedtb');
                    $('#text-23').addClass('fixed2');
                }
                else if($(window).scrollTop()>elementPosition-500){
                    $('#text-23').removeClass('fixed2');
                    $('#text-23').addClass('fixedtb');
                }
                else{
                	$('#text-23').removeClass('fixed2');
                	$('#text-23').removeClass('fixedtb');
                }          
            });
	});
    		
            
    </script>
	<?php
}

function main_content(){
	?>
	<div id="main-info">
		<div class="wrap-info">
		<?php 
			while(have_posts()):the_post();
			$hinhanhtruong= types_render_field( "hinh-anh-truong", array( "alt" => "hình ảnh trường", "class" => "img-responsive", "proportional" => "true" ) );
			$kihieu=types_render_field( "ki-hieu-truong", array( ) );
			$loaihinhdt=types_render_field( "loai-hinh-dao-tao", array( ) );
			$diachi=types_render_field( "dia-chi", array( ) );
			$web=types_render_field( "website", array( ) );
			$phone=types_render_field( "dien-thoai", array( ) );
			?>
			<div class="top-info">
					<div class="hinh-anh-truong">
						<?php echo $hinhanhtruong; ?>
					</div>
					<div class="desc-info">
						<h1 class="info-title entry-title updated"><?php the_title(); ?></h1>
						<span class="vcard author"><span class="fn" style="display: none;"><?php the_author(); ?></span></span>
						<p>Loại hình đào tạo: <?php echo $loaihinhdt; ?></p>
						<p>Địa chỉ: <?php echo $diachi; ?></p>
						<p>Website: <?php echo $web; ?></p>
						<p>Điện thoại: <?php echo $phone; ?></p>
					</div>
			</div><!--end top-info -->
			<div class="under-info">
				<div class="block-gd">
					<div class="kh-pic">
							<div class="image-thumbnail">
							<?php									
									if ( has_post_thumbnail() ) {
									    the_post_thumbnail("full");
									}
									else {
									    echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) 
									        . '/images/khong-logo.png" />';
									}
									?>
							</div>
							<div class="kihieu text-center">
								<p>Mã trường</p>
								<p class="stylekh"><?php echo $kihieu; ?></p>
							</div>
					</div> <!-- hinh anh logo -->
					<div class="info-dc">
							<div class="dc-wrap">								
								<?php
									$orig_post = $post;
									global $post;
									$tags = wp_get_post_tags($post->ID);
									if ($tags) {
									$tag_ids = array();
									foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
									$args=array(
									'tag__in' => $tag_ids,
									'post__not_in' => array($post->ID),
									'posts_per_page'=>5, // Number of related posts that will be shown.
									'tax_query' => array(
										array(
													'taxonomy' => 'muc_diem_chuan',
													'field' => 'id',
													'terms' => '1852'
												)
											),
									'post_type' => 'diem_chuan',
									'orderby' => 'date',
									'order' =>'ASC',
									'caller_get_posts'=>1
									);
									$my_query = new wp_query( $args );
									if( $my_query->have_posts() ):
									?>
									<div class="dc-title"><h4>Thông tin điểm chuẩn</h4></div>
									<ul class="ul-dc">
									<?php
									while( $my_query->have_posts()):$my_query->the_post(); ?>
								  <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								  <?php endwhile; wp_reset_postdata(); ?>
								  	</ul>
								<?php  endif; }?>									
							</div>
						</div><!-- diem chuan -->
						<div class="info-th">
							<div class="dc-wrap">								
								<?php
									$orig_post = $post;
									global $post;
									$tags = wp_get_post_tags($post->ID);
									if ($tags) {
									$tag_ids = array();
									foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
									$args=array(
									'tag__in' => $tag_ids,
									'post__not_in' => array($post->ID),
									'posts_per_page'=>20, // Number of related posts that will be shown.
									'tax_query' => array(
										array(
													'taxonomy' => 'tim_hieu_nganh_nghe',
													'field' => 'id',
													'terms' => '2345'
												)
											),
									'post_type' => 'tim_hieu_nganh_nghe',
									'caller_get_posts'=>1
									);
									$my_query = new wp_query( $args );
									if( $my_query->have_posts() ):
									?>
									<div class="dc-title"><h4>Tìm hiểu ngành nghề</h4></div>		
									<ul class="ul-th">
									<?php
									while( $my_query->have_posts()):$my_query->the_post(); ?>
								  <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								  <?php endwhile; wp_reset_postdata(); ?>
								 	</ul>
								<?php  endif; }?>
							</div>
						</div> <!--tim hieu nganh nghe -->
						<div class="dtkhac">
							<div class="dc-wrap">								
								<div class="dc-title"><h4>Loại hình đào tạo khác</h4></div>
								<div class="dtk">
									<div class="ul-khac">
									<?php
									$orig_post = $post;
									global $post;
									$tags = wp_get_post_tags($post->ID);
									if ($tags) {
									$tag_ids = array();
									foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
									$args=array(
									'tag__in' => $tag_ids,
									'post__not_in' => array($post->ID),
									'posts_per_page'=>10, // Number of related posts that will be shown.
									'cat'=>84,
									'caller_get_posts'=>1
									);
									$my_query = new wp_query( $args );
									if( $my_query->have_posts() ):
									?>
									<?php
									while( $my_query->have_posts()):$my_query->the_post(); ?>
								  	<div class="postkhac">
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full',array('class'=>'alignleft')); ?></a>
										<h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
									</div>
								  <?php endwhile; wp_reset_postdata(); ?>
								  <?php  endif; } ?> <!-- end lien thong -->

								  <?php
									$orig_post = $post;
									global $post;
									$tags = wp_get_post_tags($post->ID);
									if ($tags) {
									$tag_ids = array();
									foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
									$args=array(
									'tag__in' => $tag_ids,
									'post__not_in' => array($post->ID),
									'posts_per_page'=>10, // Number of related posts that will be shown.
									'cat'=>85,
									'caller_get_posts'=>1
									);
									$my_query = new wp_query( $args );
									if( $my_query->have_posts() ):
									?>
									<?php
									while( $my_query->have_posts()):$my_query->the_post(); ?>
								  	<div class="postkhac">
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full',array('class'=>'alignleft')); ?></a>
										<h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
									</div>
								  <?php endwhile; wp_reset_postdata(); ?>
								  <?php  endif; } ?> <!-- end cao hoc -->

								  <?php
									$orig_post = $post;
									global $post;
									$tags = wp_get_post_tags($post->ID);
									if ($tags) {
									$tag_ids = array();
									foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
									$args=array(
									'tag__in' => $tag_ids,
									'post__not_in' => array($post->ID),
									'posts_per_page'=>10, // Number of related posts that will be shown.
									'cat'=>76,
									'caller_get_posts'=>1
									);
									$my_query = new wp_query( $args );
									if( $my_query->have_posts() ):
									?>
									<?php
									while( $my_query->have_posts()):$my_query->the_post(); ?>
								  	<div class="postkhac">
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full',array('class'=>'alignleft')); ?></a>
										<h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
									</div>
								  <?php endwhile; wp_reset_postdata(); ?>
								  <?php  endif; } ?> <!-- end van bang 2 -->
								  	</div>
								</div>
							</div>
						</div> <!--end dao tao khac -->
						<?php
						// if(is_single('21273')){ ?>
						<div class="clear"></div>
						<div class="bk-video">
						    <?php if ( is_active_sidebar( 'bk-video' ) && !dynamic_sidebar('bk-video') ) : endif; ?>
						</div>
						<?php // } ?>
				</div> <!--end block gd -->
				<div class="block-nd entry-content">
					<?php the_content(); ?>
				</div>

				    
			</div><!--end under-info -->
		<?php endwhile; wp_reset_postdata(); ?>
		</div>	<!--end wrap-info -->
	</div><!--end main-info -->
	<?php
}//end main content

function main_content_lienthong(){
	?>
	<div id="main-info">
		<div class="wrap-info">
		<?php 
			while(have_posts()):the_post();
			$hinhanhtruong= types_render_field( "hinh-anh-truong", array( "alt" => "hình ảnh trường", "class" => "img-responsive", "proportional" => "true" ) );
			$kihieu=types_render_field( "ki-hieu-truong", array( ) );
			$loaihinhdt=types_render_field( "loai-hinh-dao-tao", array( ) );
			$diachi=types_render_field( "dia-chi", array( ) );
			$web=types_render_field( "website", array( ) );
			$phone=types_render_field( "dien-thoai", array( ) );
			?>
			<div class="top-info">
					<div class="hinh-anh-truong">
						<?php echo $hinhanhtruong; ?>
					</div>
					<div class="desc-info">
						<h1 class="info-title entry-title updated"><?php the_title(); ?></h1>
						<span class="vcard author"><span class="fn" style="display: none;"><?php the_author(); ?></span></span>
						<p>Loại hình đào tạo: <?php echo $loaihinhdt; ?></p>
						<p>Địa chỉ: <?php echo $diachi; ?></p>
						<p>Website: <?php echo $web; ?></p>
						<p>Điện thoại: <?php echo $phone; ?></p>
					</div>
			</div><!--end top-info -->
			<div class="under-info">
				<div class="block-gd">
					<div class="kh-pic">
							<div class="image-thumbnail">
							<?php									
									if ( has_post_thumbnail() ) {
									    the_post_thumbnail("full");
									}
									else {
									    echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) 
									        . '/images/khong-logo.png" />';
									}
									?>
							</div>
							<div class="kihieu text-center">
								<p>Mã trường</p>
								<p class="stylekh"><?php echo $kihieu; ?></p>
							</div>
					</div> <!-- hinh anh logo -->
				</div> <!--end block gd -->
				<div class="block-nd entry-content">
					<?php the_content(); ?>
				</div>
			</div><!--end under-info -->

		<?php endwhile; wp_reset_postdata(); ?>
		</div>	<!--end wrap-info -->
	</div><!--end main-info -->
	<?php
}
//end main content lienthong

// bai viet cung tag với mục tin tuc hien thi trong cac bai thong bao
function tinlienquan(){
	?>	
			<?php
				
				global $post;
				$tags = wp_get_post_tags($post->ID);
				if ($tags) {
				$tag_ids = array();				
				foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
				$args=array(
				'tag__in' => $tag_ids,
				'post__not_in' => array($post->ID),
				'posts_per_page'=>20, // Number of related posts that will be shown.
				'cat' => 3,
				'caller_get_posts'=>1
				);
				$my_query = new wp_query( $args );
				if( $my_query->have_posts() ):
				?>
				<div id="boxNewest" class="slwrap clearfix">
					<b class="sltitle">Tin liên quan</b>
					<ul class="slcontent">
					<?php
					while( $my_query->have_posts()):$my_query->the_post(); ?>
					  <li class="item">
						<a class="avatar" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>				
						<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					  </li>
				  	<?php endwhile; wp_reset_postdata(); ?>
				  	</ul>
				  	<p class="slprev"><a class="prev">Prev</a></p>
					<p class="slnext"><a class="next">Next</a></p>						
				</div>
			<?php  endif; }	?>
<?php
}

// bai viet cung nhom nganh vung mien hien thị trong cac bai thong bao
function nhomnganh(){
	?>
	<div id="nhomnganh">
		<?php
			$cat = get_the_category( get_the_ID() );
			$t = $cat[0];
			$terms1 = get_the_terms( get_the_ID(), 'khu_vuc' );
			$terms2 = get_the_terms( get_the_ID(), 'trinh_do' );
			$terms3 = get_the_terms( get_the_ID(), 'nhom_nganh' );
			$terms[0]->term_id;
			global $post;
			$args=array(						
						'post__not_in' => array($post->ID),
						'posts_per_page'=>7, // Number of related posts that will be shown.
						'cat'=> $t->term_id,
						 'tax_query' => array(
						        array(
						            'taxonomy' => 'khu_vuc', 
						            'terms' =>$terms1[0]->term_id,
						            'field' => 'id'
						        ),
						        array(
						        	'taxonomy' =>'trinh_do',
						        	'terms' => $terms2[0]->term_id,
						        	'field' => 'id'
						        	),
						        array(
						        	'taxonomy' =>'nhom_nganh',
						        	'terms' => $terms3[0]->term_id,
						        	'field' => 'id'
						        	),
						    ),
						'caller_get_posts'=>1
						);
			$my_query = new wp_query( $args );
					if( $my_query->have_posts() ):
							?>
							<b class="sltitle">Các trường khác tuyển sinh cùng nhóm ngành</b>
							<ul>
							<?php
							while( $my_query->have_posts()):$my_query->the_post(); ?>
								<li>
									<a class="imgnc" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
										<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
								</li>
							<?php endwhile; wp_reset_postdata(); ?>
							</ul>
			<?php  endif; 
		
		// end if đại học
?>
	</div>
<?php
}
add_action('genesis_after_post', 'tag');
function tag() {
echo '<div class="t" >' ;
 the_tags('Tag: ' , ','); 
 echo '</div>' ;
};


//template tin tuc 
function tintuc(){
	?>
	<div id="tin-tuc">
		<div class="wrap-news">
			<div class="main-news">
			<?php 
				while(have_posts()):the_post();
			?>
				<h1 class="entry-title updated"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php the_title(); ?></h1>				
				<span class="vcard author"><span class="fn" style="display: none;"><?php the_author(); ?></span></span>
				<div class="breadrum">
				<?php the_breadcrumb(); ?>
					<div class="socialbutton">					
						<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
						
						<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink(); ?>">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

						<!-- Đặt thẻ này vào phần đầu hoặc ngay trước thẻ đóng phần nội dung của bạn. -->
						<script src="https://apis.google.com/js/platform.js" async defer>
						  {lang: 'vi'}
						</script>

						<!-- Đặt thẻ này vào nơi bạn muốn nút chia sẻ kết xuất. -->
						<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="entry-content">
				<?php the_content(); ?>
				</div>
				<div class="likebox">
				<div class="liketext">Bạn thích bài viết này ? </div>
				<div class="likebutton">
					<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>										
					<!-- Đặt thẻ này vào nơi bạn muốn Nút +1 kết xuất. -->
					<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
				</div>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
			</div>			
			<?php comments_template(); ?>		
		</div>
		<div class="news-sidebar sidebar widget-area">
				<?php if(is_active_sidebar('sidebar-alt') && !dynamic_sidebar('sidebar-alt')):endif; ?>
		</div>
	</div>	
	<?php
}
//Chèn bài liên quan vào giữa nội dung 
function prefix_insert_post_ads( $content ) {
		$post1=types_render_field( "post-1", array( ) );
		$link1=types_render_field( "link-post-1", array( ) );
		$post2=types_render_field( "post-2", array( ) );
		$link2=types_render_field( "link-post-2", array( ) );
		$post3=types_render_field( "post-3", array( ) );
		$link3=types_render_field( "link-post-3", array( ) );
		if(isset($post1) && !empty($link1)){
		$related_posts= '
		<div id="ct-morelink">
		<p><span><a class="star" href="'.$link1.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post1.'</a></span></p>
		<p><span><a class="star" href="'.$link2.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post2.'</a></span></p>
		<p><span><a class="star" href="'.$link3.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post3.'</a></span></p>
		</div>
		';	
		}
		else{
			$related_posts="";
		}
        
 
        if ( is_single() ) {
                return count_paragraph( $related_posts, 1, $content );
        }
 
        return $content;
}

// hien thi bai viet cung chuyen muc trong post tin tuc
function cungchuyenmuc(){
	?>	
			<?php
				global $post;
				$categories = get_the_category( $post->ID);
				$cat = $categories[0]->cat_ID;

				$args=array(				
				'post__not_in' => array($post->ID),
				'posts_per_page'=>4, // Number of related posts that will be shown.
				'cat' => $cat,
				'caller_get_posts'=>1
				);
				$my_query = new wp_query( $args );
				if( $my_query->have_posts() ):
				?>
				<div id="boxNewest" class="slwrap clearfix">
					<b class="sltitle">Bài viết cùng chuyên mục</b>
					<ul class="slcontent">
					<?php
					while( $my_query->have_posts()):$my_query->the_post(); ?>
					  <li class="item">
						<a class="avatar" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>				
						<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					  </li>
				  	<?php endwhile; wp_reset_postdata(); ?>
				  	</ul>
				  	<p class="slprev"><a class="prev">Prev</a></p>
					<p class="slnext"><a class="next">Next</a></p>
				</div>				
			<?php  endif; ?>
<?php
}

// hien thi bai viet cung chuyen muc trong van bang 2
function cungchuyenmucvb2(){
	?>	
	<div id="nhomnganh">
		<?php
			global $post;
				$categories = get_the_category( $post->ID);
				$cat = $categories[0]->cat_ID;

				$args=array(				
				'post__not_in' => array($post->ID),
				'posts_per_page'=>7, // Number of related posts that will be shown.
				'cat' => $cat,
				'caller_get_posts'=>1
				);
			$my_query = new wp_query( $args );
					if( $my_query->have_posts() ):
							?>
							<b class="sltitle">Bài viết cùng chuyên mục</b>
							<ul>
							<?php
							while( $my_query->have_posts()):$my_query->the_post(); ?>
								<li>
									<a class="imgnc" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
										<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
								</li>
							<?php endwhile; wp_reset_postdata(); ?>
							</ul>
			<?php  endif; 
		
		// end if đại học
?>
	</div>
<?php
}


genesis();