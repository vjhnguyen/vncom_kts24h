<?php
/* Template Name:giao-vien-toan */

add_action( 'wp_head', 'add_my_style' );
function add_my_style() {
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/giaovien.css">';
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/font-awesome.min.css">';
}
add_action('genesis_loop','content');
function content(){
?>
<div class="col">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="home_slide">
				<?php echo do_shortcode('[rev_slider alias="toan_hoc"]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="main">
			<div class="col-md-5 col-sm-12 col-xs-12">
				<div class="sideBar-infor">
					<div class="video-youtube">
						<iframe width="438px" height="300px" src="https://www.youtube.com/embed/7b7svA00fs4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
					<div class="sidebar-course">
						<h2> Chi tiết khóa học</h2>
						<form>
							<ul>
								<li><span>Giới thiệu khóa học</span>
								
							</li>
							<ul >
								<li><input type="checkbox" name="" value="">Bài:1</li>
							</ul>
							<li><span>Cơ bản</span>
						</li>
						<ul>
							<li><input type="checkbox" name="" value="">Bài:1</li>
							<li><input type="checkbox" name="" value="">Bài:2</li>
							<li><input type="checkbox" name="" value="">Bài:3</li>
							<li><input type="checkbox" name="" value="">Bài:4</li>
							<li><input type="checkbox" name="" value="">Bài:5</li>
							<li><input type="checkbox" name="" value="">Bài:6</li>
							<li><input type="checkbox" name="" value="">Bài:7</li>
							<li><input type="checkbox" name="" value="">Bài:8</li>
						</ul>
					</ul>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-sm-12 col-xs-12">
		<div class="main-content">
			<h2>SẤM SÉT EDUCATION</h2>
			<h1>KHÓA KHỞI ĐỘNG MÔN TOÁN 2018</h1>
			<p>Thầy Trần Ngọc Quang Huy, với 4 năm kinh nghiệm ôn thi THPT Quốc Gia môn Toán, thầy vô cùng tâm lý, hiểu được những khó khăn của học sinh và luôn mong muốn giảng dạy theo phương pháp sao cho học sinh dễ hiểu, dễ nhớ nhất. Do đó, các bài giảng của thầy vô cùng cô đọng, xúc tích, không tốn quá nhiều thời gian nhưng mang lại hiệu quả cao.  </p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Xem thêm...</button>
			<div id="demo" class="collapse see-more">
				<p class="item-course">
					Khi đến với khóa Khởi Động của thầy, dựa trên cấu trúc đề thi năm 2018 bao gồm 80% là kiến thức lớp 12, và 20% còn lại sẽ là kiến thức lớp 11, khóa học sẽ bao gồm 18 bài giảng chia thành 18 chuyên đề, trong đó 15 buổi học chương trình Toán 12, và 3 buổi để ôn tập Toán 11.
				</p>
				<p class="item-course">
					Mỗi bài giảng sẽ kéo dài 45’, trong đó phần lý thuyết sẽ được cô đọng xúc tích. Sau đó là các bài tập ví dụ điển hình. Tuy nhiên, thầy sẽ chú trọng vào dấu hiệu nhận biết các dạng bài, định hướng tư duy cho học sinh, và mẹo làm bài để khi gặp các dạng bài khác nhau trong một đề tổng hợp, học sinh có thể biết ngay cách xử lý dữ liệu và áp dụng những phương pháp giải phù hợp.
				</p>
			</div>
		</div>
		<div class="more">
			<h2>KNOW MORE</h2>
			<div class="row">
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="image-left">
		<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/toan2.png" alt="">
	</div>
	<div class="timeline">
		<div class="contain left">
			<div class="content">
				<h2>Câu nói ưa thích</h2>
				<p class="has-item"><i class="fas fa-quote-left"></i>
					Câu nói ưa thích: Không có bí mật của thành công. Thành công là kết quả của sự chuẩn bị, nỗ lực chăm chỉ và học từ các thất bại trước đó.<i class="fas fa-quote-right"></i>
				</p>
			</div>
		</div>
		<div class="contain right">
			<div class="content">
				<h2>Câu chuyện của Thầy</h2>
				<p>Với kiến thức từ việc tham gia đội tuyển Toán thời Cấp 2 cùng thành tích học tập khá cao, thầy thi đỗ vào THPT chuyên Lam Sơn một cách rất dễ dàng. Vì vậy mà suốt 2 năm lớp 10 và 11 thầy đã rất xao nhãng học tập, vì thầy cho rằng mình đã quá xuất sắc rồi. Hậu quả là thầy rớt hẳn xuống vị trí cuối lớp </p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo1">XEM THÊM ...</button>
				<div id="demo1" class="collapse">
					<p>
						Thầy đã rất hoang mang và nghi ngờ về năng lực của chính bản thân mình, và cũng rất lo lắng về kì thi đại học đã tới gần kề. May mắn thay, đúng thời điểm đó, thầy giáo của thầy đã nói một câu rằng: “Không có việc gì là quá khó khăn, miễn là em đủ quyết tâm và tìm ra được một phương pháp học thực sự phù hợp, em sẽ đạt được một kết quả ngoài mong đợi”. Cũng vì câu nói này, thầy đã đặt tất cả quyết tâm và nỗ lực vào việc học tập. Thầy chăm chú nghe giảng trên lớp, về nhà thì tìm thêm những mẹo giải toán nhanh, những cách giải ngắn gọn cho mỗi bài toán. Thầy cũng tìm tòi thật nhiều bài tập để luyện tập nhằm nắm vững hơn về mỗi chuyên đề. Với phương pháp học mới này, thầy đã tiến bộ rõ rệt trong vòng 4 tháng. Và kết quả là Thầy đã thi đỗ vào Đại học Ngoại Thương chuyên ngành Kinh tế đối ngoại, với kết quả ngoài mong đợi.
					</p>
				</div>
			</div>
		</div>
		<div class="contain left">
			<div class="content">
				<h2>Phương pháp giảng dạy</h2>
				<p>- Biến hóa công thức, cấu trúc khô khan thành những câu chữ biết nói trong cuộc sống để học sinh không chỉ nhớ lâu mà còn cảm thấy Toán vô cùng thú vị.</p>
				<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo2">XEM THÊM...</button>
				<div id="demo2" class="collapse">
					<p>
						+ Về lý thuyết, thầy luôn dạy một cách ngắn gọn, dễ hiểu, nhiều ví dụ, liên hệ đến những vấn đề thực tế để các em dễ dàng hình dung và hiểu được vấn đề.
						+ Về bài tập, thầy không chỉ hướng dẫn giải riêng bài toán đó mà hướng dẫn học sinh cách suy luận để học sinh có thể áp dụng để tự giải quyết những bài toán khó hơn. Mỗi buổi học đều có bài tập tự luyện hoặc đề kiểm tra với đáp án đầy đủ để các em hiểu sâu hơn về bài học.
						+ Ngoài ra, thầy cũng thường xuyên chia sẻ những kinh nghiệm học tập, thi cử và giải đáp những thắc mắc của học sinh trực tiếp trong buổi học.
					</p>
				</div>
			</div>
		</div>
		<div class="contain right">
			<div class="content">
				<h2>Câu chuyện của cô</h2>
				<p>Xuất phát điểm là một học sinh chuyên Toán của trường THPT Chuyên Lam Sơn, cô yêu thích các môn học tự nhiên như Toán, Lí, Hóa và cảm thấy Tiếng Anh vô cùng kinh khủng với vô vàn từ mới phải nhớ, vô vàn cấu trúc phải học. Đến tận năm lớp 12, Tiếng Anh vẫn là nỗi ác mộng mỗi khi đến lớp của cô và động lực học duy nhất của cô thời điểm đó là… học để qua tốt nghiệp! </p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3">XEM THÊM ...</button>
				<div id="demo3" class="collapse">
					<p>
						Cho đến khi cô có cơ hội tiếp xúc với một phương pháp học tập thú vị thông qua một buổi hội thảo do trường tổ chức. Lúc đó, cô đã nhận ra rằng Tiếng anh không hề khó như mình nghĩ mà chỉ vì phương pháp học tập của mình chưa đúng. Với niềm tin đó, cô đã dốc hết quyết tâm để học tiếng anh cô đã tìm tòi thật nhiều mẹo hay để biến những từ mới và cấu trúc khô khan trở nên gần gũi trong cuộc sống hằng ngày. Dần dần, nỗi sợ môn Tiếng Anh đã hoàn toàn được xua tan và thay vào đó là niềm yêu thích, sự háo hức chờ đợi mỗi khi đến tiết học mới. Và chỉ sau 4 tháng với phương pháp “Học đâu, nhớ đó” cô đã tự tin tham dự kì thi THPT Quốc gia với khối thi A1: Toán, Lý, Anh và đậu vào Đại học Ngoại thương - chuyên ngành Kinh tế đối ngoại, với số điểm môn ngoại ngữ nằm ngoài mong đợi.<br>
						Cô đã rút ra câu châm ngôn yêu thích của mình khi cô thử phương pháp học mới và nhận ra rằng mình có thể đạt được những điều mà bản thân chưa bao giờ dám nghĩ đến.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<?php
}
genesis();