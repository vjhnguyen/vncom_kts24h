<?php
  remove_action( 'genesis_loop', 'genesis_do_loop' );
 add_action( 'genesis_loop', 'thanhvu' );


function thanhvu()
{?>

                    <div class="tintuc-page">
						<?php  while(have_posts()) : the_post();
							$thumbnail = genesis_get_image( 
								array(
									'size' => 'vertical-hd',
									'attr' => array(
										'alt' => caia_get_image_attr( 'alt' ),
										'title' => caia_get_image_attr( 'title' ),
										'class' => 'alignleft'
									)
								)
							);
						?>
                        <div class="widget-tintuc">
                            
                            <?php 
							if ( ! empty( $thumbnail ) ) {
								printf( '<a href="%s" title="%s">%s</a>', get_permalink(), get_the_title(), $thumbnail );
							}							
							?>
							<h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a>
							<p class="time"><?php the_time('d') ?>/<?php the_time('m') ?>/<?php the_time('Y') ?></p>
							</h3>
                            <p><?php the_content_rss('', false,'', 50);?></p>
                           
							
                        </div>
						<?php endwhile; wp_reset_postdata();?>
			<div class="paging">
				<div class="navigation">
					<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
					<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
					<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
					<?php } ?>
				</div>
            </div>
                    </div><!--end tintuc-->
<?php } ?>
