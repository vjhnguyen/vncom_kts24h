<?php
/* Template Name:giao-vien-van */
add_action( 'wp_head', 'add_my_style' );
function add_my_style() {
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/giaovien.css">';
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/font-awesome.min.css">';
}
add_action('genesis_loop','content');
function content(){
?>
<div class="col">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="home_slide">
				<?php echo do_shortcode('[rev_slider alias="van_hoc"]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="main">
			<div class="col-md-5 col-sm-12 col-xs-12">
				<div class="sideBar-infor">
					<div class="video-youtube">
						<iframe width="438px" height="300px" src="https://www.youtube.com/embed/2fyia5ZR1qo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
					<div class="sidebar-course">
						<h2> Chi tiết khóa học</h2>
						<form>
							<ul>
								<li><span>Giới thiệu khóa học</span>
								
							</li>
							<ul >
								<li><input type="checkbox" name="" value="">Bài:1</li>
							</ul>
							<li><span>Cơ bản</span>
						</li>
						<ul>
							<li><input type="checkbox" name="" value="">Bài:1</li>
							<li><input type="checkbox" name="" value="">Bài:2</li>
							<li><input type="checkbox" name="" value="">Bài:3</li>
							<li><input type="checkbox" name="" value="">Bài:4</li>
							<li><input type="checkbox" name="" value="">Bài:5</li>
							<li><input type="checkbox" name="" value="">Bài:6</li>
							<li><input type="checkbox" name="" value="">Bài:7</li>
							<li><input type="checkbox" name="" value="">Bài:8</li>
						</ul>
					</ul>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-sm-12 col-xs-12">
		<div class="main-content">
			<h2>SẤM SÉT EDUCATION</h2>
			<h1>KHÓA KHỞI ĐỘNG MÔN NGỮ VĂN 2018</h1>
			<p>Cô Trần Hải Yến, luôn tâm niệm rằng: “Với cô, quan trọng không phải chúng ta học được bao nhiêu mà quan trọng là chúng ta tập trung vào điều gì. Sự tập trung sẽ khiến bài văn trở nên trọng tâm, sáng rõ như một điểm sáng toả ra ánh hào quang. Người ta sẽ tập trung vào điểm sáng đó còn những ánh hào quang bên cạnh chỉ là thứ tô điểm mà thôi.” </p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Xem thêm...</button>
			<div id="demo" class="collapse see-more">
				Do đó,  khi đến với Khóa Khởi Động môn Ngữ văn của cô Yến, các em sẽ được tập trung vào hệ thống luận điểm chính và những điểm sáng của tác phẩm để các em có thể ghi điểm mà không cần tốn quá nhiều công sức và thời gian.  <br>
				<p class="item-course">
					Xây dựng kiến thức nền tảng, biết cách xây dựng hệ thống luận điểm mạch lạc, rõ ràng cho bài viết là mục tiêu của khóa Khởi Động môn Ngữ Văn. Do đó, các bài học sẽ được thiết kế và giảng dạy một cách xúc tích, ngắn gọn trong 45 phút học.
				</p>
			</div>
		</div>
		<div class="more">
			<h2>KNOW MORE</h2>
			<div class="row">
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="image-left">
		<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/van2.png" alt="">
	</div>
	<div class="timeline">
		<div class="contain left">
			<div class="content">
				<h2>Câu nói ưa thích</h2>
				<p class="has-item"><i class="fas fa-quote-left"></i>
					Câu nói ưa thích: “Tin rằng bạn có thể làm một điều gì đó đồng nghĩa với việc bạn đã đi được nửa đường đến đó” – Theodore Roosevelt<i class="fas fa-quote-right"></i>
				</p>
			</div>
		</div>
		<div class="contain right">
			<div class="content">
				<h2>Câu chuyện của cô</h2>
				<p>Sau nhiều năm đi dạy, cô nhận thấy, nhiều bạn rất sợ môn Văn, cho rằng chỉ những bạn có năng khiếu mới học tốt, và luôn cảm thấy Văn rất nặng nề, rườm rà. </p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo1">XEM THÊM ...</button>
				<div id="demo1" class="collapse">
					<p>
						Khi học lớp 12, các bạn cùng trang lứa chuẩn bị cho kì thi tốt nghiệp và đại học phía  trước thì cô toàn tâm toàn ý chuẩn bị cho kì thi học sinh giỏi quốc gia môn Văn. Nhưng may mắn đã không mỉm cười với cô, năm đó cô không được giải, đồng nghĩa với việc chỉ còn 5 tháng để ôn đủ 3 môn đại học và thêm 2 môn tốt nghiệp. Lúc đó cô từng viết lên tờ giấy hai chữ “Sư phạm” và gạch nát nó đi bởi sự tuyệt vọng, tưởng chừng cánh cửa Đại học đã khép lại. Nhưng với ước mơ của mình, cô đã bắt đầu ôn luyện một cách khoa học, thoải mái, không nặng về kiến thức. Kết quả kì thi đó cô đỗ tốt nghiệp loại Giỏi và đạt số điểm cao nhất khi thi vào lớp Chất lượng cao K64 – Khoa Ngữ văn. Những thành tích trên không phải kết quả của sự nỗ lực quên mình mà là kết quả của những ngày tháng dám ước mơ và tìm ra cách học phù hợp. Cô cũng như các em, cũng bắt đầu từ con số 0 khi chỉ còn 5 tháng. Và cô sẽ truyền dạy lại toàn bộ kinh nghiệm mình có được cho các em trong khoá học này, để chúng ta cùng là người chiến thắng.<br>
						Cô đã rút ra câu châm ngôn yêu thích của mình khi cô thử phương pháp học mới và nhận ra rằng mình có thể đạt được những điều mà bản thân chưa bao giờ dám nghĩ đến.
					</p>
				</div>
			</div>
		</div>
		<div class="contain left">
			<div class="content">
				<h2>Phương pháp giảng dạy</h2>
				<p>- Biến hóa từ vựng, cấu trúc khô khan thành những câu chữ biết nói trong cuộc sống để học sinh không chỉ nhớ lâu mà còn cảm thấy Môn Văn vô cùng thú vị.</p>
				<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo2">XEM THÊM...</button>
				<div id="demo2" class="collapse">
					<p>
						Xây dựng kiến thức nền tảng, biết cách xây dựng hệ thống luận điểm mạch lạc, rõ ràng cho bài viết là mục tiêu của khóa Khởi Động môn Ngữ Văn. Do đó, các bài học sẽ được thiết kế và giảng dạy một cách xúc tích, ngắn gọn trong 45 phút học.
					</p>
				</div>
			</div>
		</div>
		<div class="contain right">
			<div class="content">
				<h2>Câu chuyện của cô</h2>
				<p>Xuất phát điểm là một học sinh chuyên Toán của trường THPT Chuyên Lam Sơn, cô yêu thích các môn học tự nhiên như Toán, Lí, Hóa và cảm thấy Tiếng Anh vô cùng kinh khủng với vô vàn từ mới phải nhớ, vô vàn cấu trúc phải học. Đến tận năm lớp 12, Tiếng Anh vẫn là nỗi ác mộng mỗi khi đến lớp của cô và động lực học duy nhất của cô thời điểm đó là… học để qua tốt nghiệp! </p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3">XEM THÊM ...</button>
				<div id="demo3" class="collapse">
					<p>
						Cho đến khi cô có cơ hội tiếp xúc với một phương pháp học tập thú vị thông qua một buổi hội thảo do trường tổ chức. Lúc đó, cô đã nhận ra rằng Tiếng anh không hề khó như mình nghĩ mà chỉ vì phương pháp học tập của mình chưa đúng. Với niềm tin đó, cô đã dốc hết quyết tâm để học tiếng anh cô đã tìm tòi thật nhiều mẹo hay để biến những từ mới và cấu trúc khô khan trở nên gần gũi trong cuộc sống hằng ngày. Dần dần, nỗi sợ môn Tiếng Anh đã hoàn toàn được xua tan và thay vào đó là niềm yêu thích, sự háo hức chờ đợi mỗi khi đến tiết học mới. Và chỉ sau 4 tháng với phương pháp “Học đâu, nhớ đó” cô đã tự tin tham dự kì thi THPT Quốc gia với khối thi A1: Toán, Lý, Anh và đậu vào Đại học Ngoại thương - chuyên ngành Kinh tế đối ngoại, với số điểm môn ngoại ngữ nằm ngoài mong đợi.<br>
						Cô đã rút ra câu châm ngôn yêu thích của mình khi cô thử phương pháp học mới và nhận ra rằng mình có thể đạt được những điều mà bản thân chưa bao giờ dám nghĩ đến.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<?php
}
genesis();