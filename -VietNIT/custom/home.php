<?php
add_action('genesis_before_content_sidebar_wrap','quangcao',1);
//add_action( 'genesis_before_content_sidebar_wrap', 'metro_menu', 2);
function quangcao(){
	if(is_active_sidebar("ads") && !dynamic_sidebar("ads")):endif;
}

function metro_menu(){
	?>
	<div id="metro_menu">
		<div class="tracuu"><a href="#" alt="">Tra cứu điểm chuẩn ĐH - CĐ toàn quốc năm 2015</a></div>
		<div id="ttc">
			<div class="thuvien"><a href="#" alt="">Thư viện đề thi</a></div>
			<div class="tin"><a href="http://kenhtuyensinh24h.vn/tin-tuc/" alt="Tin tuyển sinh">Tin Tuyển sinh</a></div>
			<div class="chitieu"><a href="#" alt="">Chỉ tiêu tuyển sinh 2015</a></div>
		</div>
	</div>
	<?php
}
// quảng cáo trong home mobile
add_action('genesis_after_header','before_featured');
function before_featured(){
	?>
	<div class="adver_top_home">
		<?php if(is_active_sidebar('adv_home_moblie') && !dynamic_sidebar('adv_home_moblie')):endif; ?>			
	</div>
	<?php
}

add_action( 'genesis_loop', 'featured', 3);
//genesis_loop
//genesis_before_content_sidebar_wrap
function featured() {
    $featureds = new WP_Query( 'cat=3&showposts=1' );
	?>
	<div class="therain">
		<div  class="lof_slidecontent">
			<div class="lof-main-outer">
				<?php
				$args= array(
					'cat'=>'3',
					'showposts' => 1
					);
				$featuredPost = new WP_Query($args);							
				while($featuredPost->have_posts()) : $featuredPost->the_post();?>
						<a class="slide" href="<?php the_permalink();?>">
							<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
						</a>
						<div class="lof-main-item-desc">
							<h3 class="ginuaday"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
							<p><?php the_content_rss('', false,'', 36);?></p>
						</div>
				<?php endwhile; wp_reset_postdata();?>							
				<div id="ct-morelink">
					<?php 
					$args= array(
						'cat'=>'3',
						'showposts' => 2,
						'offset' =>1
						);
					$featuredPost = new WP_Query($args);	
					while($featuredPost->have_posts()) : $featuredPost->the_post();?>
					<p><span>
						<a class="star" href="<?php the_permalink(); ?>"><i class="fa fa-hand-o-right" aria-hidden="true"></i><?php the_title(); ?></a>
					</span></p>
					<?php endwhile; wp_reset_postdata();?>
				</div>
				<!-- Mobile -->
				<ul id="ct-mobile">
					<?php 
					$args= array(
						'cat'=>'3',
						'showposts' => 3,
						'offset' =>1
						);
					$featuredPost = new WP_Query($args);
					while($featuredPost->have_posts()) : $featuredPost->the_post();?>
						<li>
							<a href="<?php the_permalink(); ?>" title=""><h3 class="star-mobile">
								<?php the_title(); ?>
							</h3>
							</a>
							<a class="slide-mobile" href="<?php the_permalink();?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(120,120));} ;?>
							</a>
							<p><?php the_content_rss('', false,'', 25);?></p>
						</li>
					<?php endwhile; wp_reset_postdata();?>
				</ul>
				<!-- END -->
			</div>
			<div class="lof_navigator_outer">
				<div class="icon-event fon3">
					<a href="#" title="Tin tức - Sự kiện">Tin tức - Sự kiện</a>
				</div>
				<ul class="lof_navigator">
					<?php
					$args = array(
							'post_type' =>'post',
							'posts_per_page' => 10,
							'cat'=>'3',
							'offset'=>4
						);
					$featuredPost = new WP_Query($args);							
					while($featuredPost->have_posts()) : $featuredPost->the_post();
					$thumbnail = genesis_get_image( 
						array(
							'size' => 'vertical-hds',
							'attr' => array(
								'alt' => caia_get_image_attr( 'alt' ),
								'title' => caia_get_image_attr( 'title' ),
								'class' => 'alignleft'
							)
						)
					);
					?>
						<li>
							<a class="atitle" href="<?php the_permalink();?>"><?php the_title();?></a>
						</li>
					<?php endwhile; wp_reset_postdata();?>
				</ul>
			</div>
		</div>
	</div><!--End The Rain-->
<?php 
}

add_action('genesis_loop','daihoc' );
function daihoc(){
	?>
	<!----------------------------------------------------------- MOBILE ---------------------------------------------------------------------->
	<div id="mobile-centent">
		<!-- Tin tức 1 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/van-bang-2-sau-dai-hoc/" title="Văn bằng 2">
					Văn bằng 2
				</a>
				<a href="https://kenhtuyensinh24h.vn/van-bang-2-sau-dai-hoc/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=76&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=76&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 2 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-tuc/" title="Văn bằng 2">
					Giáo dục và đào tạo
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-tuc/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=3&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=3&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 3 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/thi-thpt-quoc-gia/" title="Văn bằng 2">
					Thi THPT quốc gia
				</a>
				<a href="https://kenhtuyensinh24h.vn/thi-thpt-quoc-gia/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=7889&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=7889&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 4 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/nhip-song-tre/" title="Văn bằng 2">
					Nhịp sống trẻ
				</a>
				<a href="https://kenhtuyensinh24h.vn/nhip-song-tre/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=203&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=203&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 5 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/lien-thong-dai-hoc-2/" title="Văn bằng 2">
					Tin hệ liên thông
				</a>
				<a href="https://kenhtuyensinh24h.vn/lien-thong-dai-hoc-2/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=8498&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=8498&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 6 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/cac-khoi-thi/" title="Văn bằng 2">
					Khối thi
				</a>
				<a href="https://kenhtuyensinh24h.vn/cac-khoi-thi/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=7893&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=7893&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 7 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Văn bằng 2">
					Thủ tục hồ sơ nhập học
				</a>
				<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=7904&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=7904&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 8 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/xet-tuyen-dot-2-he-dai-hoc/" title="Văn bằng 2">
					Nguyện vọng bổ sung
				</a>
				<a href="https://kenhtuyensinh24h.vn/xet-tuyen-dot-2-he-dai-hoc/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=7901&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=7901&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		
	</div>
	
	<!-------------------------------------------------------------------- END MOBILE ---------------------------------------------------------------------------------->
	
	<div class="clear"></div>
	<!-- Đại học -->
	<div id="home-block-news">
		<div class="news news1">
			<div role="tabpanel">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs responsive hidden-xs hidden-sm" role="tablist">
					<li role="presentation" class="active">
						<a href="#daihoc" aria-controls="daihoc" role="tab" data-toggle="tab">Đại học</a>
					</li>
					<li role="presentation">
						<a href="#caodang" role="tab" data-toggle="tab">Cao Đẳng</a>
					</li>
					<li role="presentation">
						<a href="#trungcap" role="tab" data-toggle="tab">Trung Cấp</a>
					</li>
					<li role="presentation">
						<a href="#vanbang2" role="tab" data-toggle="tab">Văn Bằng 2</a>
					</li>
					<li role="presentation">
						<a href="#caohoc" role="tab" data-toggle="tab">Cao Học</a>
					</li>
					<li role="presentation">
						<a href="#lienthong" role="tab" data-toggle="tab">Liên Thông</a>
					</li>			
				</ul>
						<!-- tab mobile -->
				<ul class="nav nav-tabs responsive visible-xs visible-sm" role="tablist">
					<li role="presentation" class="custom-drop dropdown active">
					<h4 class="title-tab">Đại học</h4>
					<button class="dropdown-toggle custom-toggle" type="button" id="myTabDrop1" data-toggle="dropdown">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i></button>
						<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
							<li role="presentation" class="active">
							<a href="#daihoc" aria-controls="daihoc" role="tab" data-toggle="tab">Đại học</a>
							</li>
							<li role="presentation">
								<a href="#caodang" role="tab" data-toggle="tab">Cao Đẳng</a>
							</li>
							<li role="presentation">
								<a href="#trungcap" role="tab" data-toggle="tab">Trung cấp</a>
							</li>
							<li role="presentation">
								<a href="#vanbang2" role="tab" data-toggle="tab">Văn Bằng 2</a>
							</li>
							<li role="presentation">
								<a href="#caohoc" role="tab" data-toggle="tab">Cao Học</a>
							</li>
							<li role="presentation">
								<a href="#lienthong" role="tab" data-toggle="tab">Liên Thông</a>
							</li>
						</ul> 
					</li>
				</ul>
				
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active fade in" id="daihoc">			
						<div class="itemblock">
							<div class="item">
								<a class="item-img" href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-tp-ha-noi/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/hanoi.jpg" /></a>
								<a class="aname" href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-tp-ha-noi/">Danh sách các trường Đại Học và Học viện khu vực Hà Nội</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-tp-hcm/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/tphcm.jpg" /></a>
								<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-tp-hcm/" class="aname">Danh sách các trường Đại Học và Học viện khu vực TP.HCM</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-bac/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/mienbac.jpg" /></a>
								<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-bac/" class="aname">Danh sách các trường Đại Học và Học viện khu vực Miền Bắc</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-trung/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/mientrung.jpg" /></a>
								<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-trung/" class="aname">Danh sách các trường Đại Học và Học viện khu vực Miền Trung</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a class="item-img" href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-nam/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/miennam.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-nam/" class="aname">Danh sách các trường Đại Học và Học viện khu vực Miền Nam</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a class="item-img" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-bo-doi-cong-an/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/bodoi.jpg" /></a>
								<a href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-bo-doi-cong-an/" class="aname">Danh sách các trường Khối Bộ đội - Công an</a>
							</div>
						</div>
					</div>
					<!-- tab dai hoc -->
					<div role="tabpanel" class="tab-pane fade" id="caodang">
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-ha-noi/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-ha-noi/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực Hà Nội</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-hcm/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-hcm/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực TP.HCM</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-bac/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-bac/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực Miền Bắc</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a class="item-img" href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-trung/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-trung/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực Miền Trung</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-nam/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-nam/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực Miền Nam</a>
							</div>
						</div>
					</div>
					<!-- end tab cao dang -->
					<div role="tabpanel" class="tab-pane fade in" id="trungcap">
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-tp-ha-noi/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-tp-ha-noi/" class="aname">Danh sách các trường Trung Cấp khu vực Hà Nội</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-hcm/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-tp-hcm/" class="aname">Danh sách các trường Trung Cấp khu vực TP.HCM</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-bac/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-bac/" class="aname">Danh sách các trường Trung Cấp khu vực Miền Bắc</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-trung/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-trung/" class="aname">Danh sách các trường Trung Cấp khu vực Miền Trung</a>
							</div>
						</div>
						<div class="itemblock">
							<div class="item">
								<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-nam/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
								<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-nam/" class="aname">Danh sách các trường Trung Cấp khu vực Miền Nam</a>
							</div>
						</div>			
					</div>
					<!-- trung cap -->
					<div role="tabpanel" class="tab-pane fade in" id="vanbang2">
						<?php 
							$args=new WP_Query('cat=76&showposts=6');
							if(have_posts()):
							while($args->have_posts()):$args->the_post();
						?>
							<div class="itemblock">
								<div class="item">
									<a href="<?php the_permalink(); ?>" class='item-img'>
										<?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?>
									</a>
									<a href="<?php the_permalink(); ?>" class='aname'><?php echo short_title('...', 10); ?></a>
								</div>
							</div>
						<?php endwhile; wp_reset_postdata(); ?>
						<?php else: _e('Chưa có dữ liệu nào'); ?>
						<?php endif; ?>
					</div>
					<!-- end van bang 2 -->
					<div role="tabpanel" class="tab-pane fade in" id="caohoc">			
						<?php 
							$args=new WP_Query('cat=85&showposts=6');
							if(have_posts()):
							while($args->have_posts()):$args->the_post();
						?>
							<div class="itemblock">
								<div class="item">
									<a href="<?php the_permalink(); ?>" class='item-img'><?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?></a>
									<a href="<?php the_permalink(); ?>" class='aname'><?php echo short_title('...', 10); ?></a>
								</div>
							</div>
						<?php endwhile; wp_reset_postdata(); ?>
						<?php else: _e('Chưa có dữ liệu nào'); ?>
						<?php endif; ?>
					</div>
					<!-- end cao hoc -->
					<div role="tabpanel" class="tab-pane fade in" id="lienthong">
						<?php 
							$args=new WP_Query('cat=84&showposts=6');
							if(have_posts()):
							while($args->have_posts()):$args->the_post();
						?>
							<div class="itemblock">
								<div class="item">
									<a href="<?php the_permalink(); ?>" class='item-img'><?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?></a>
									<a href="<?php the_permalink(); ?>" class='aname'><?php echo short_title('...', 10); ?></a>
								</div>
							</div>
						<?php endwhile; wp_reset_postdata(); ?>
						<?php else: _e('Chưa có dữ liệu nào'); ?>
						<?php endif; ?>
					</div>
					<!-- end lien thong --> 
				</div><!--end tab-content --> 
			</div> <!--end tabpanel --> 
	</div> <!-- end #news1 -->
	
	<div class="clear"></div>
	<div class="news group-area-home">
		<div class="namekn"><h4>Tuyển sinh theo khối ngành</h4></div>
		
		<div class="quang-cao">
			<div class="content-quangcao">
				<div class="viewgt">
					<?php if(is_active_sidebar('banner_adv_home') && !dynamic_sidebar('banner_adv_home')):endif; ?>
				</div>
			</div>
		</div>
		
		<div class="kn1 kn">
			<div class="gitem kinhte">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-kinh-te/">
					<i class="fa fa-money" aria-hidden="true"></i>		
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Kinh Tế</p>
						</div>
				</a>
			</div>
			<div class="gitem kythuat">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-ky-thuat/">
					<i class="fa fa-cogs" aria-hidden="true"></i>
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Kỹ Thuật</p>
						</div>
				</a>
			</div>
			<div class="gitem vanhoa">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-van-hoa-nghe-thuat/">
					<i class="vanhoa"></i>
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Văn Hóa</p>
						</div>
				</a>
			</div>
			<div class="gitem supham">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-su-pham/">
					<i class="fa fa-graduation-cap" aria-hidden="true"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Sư Phạm</p>
					</div>
				</a>
			</div>
			<div class="gitem yduoc">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-y-duoc/">
				<i class="yduoc"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Y Dược</p>
					</div>
				</a>
			</div>
			<div class="gitem congan">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-bo-doi-cong-an/">
				<i class="congan"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Công An</p>
					</div>
				</a>
			</div>
		</div>
	</div> <!--end group-area-home-->
	<div class="clear"></div>
	
	</div> <!--end home block news-->
<?php
}

/**
 * Tin tức dưới cùng
 * SANG MINI thêm 21/11/2017
 */
add_action('genesis_loop','new_tintuc' );
function new_tintuc(){
	?>
	
	<!------- DESKTOP -------->
	<div id="desktop-centent">
		<!-- Tin tức 1 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/tin-huong-nghiep/" title="TIN HƯỚNG NGHIỆP">
					<span class="icon-block-title"></span>
					<span>TIN HƯỚNG NGHIỆP</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-huong-nghiep/" title="TIN HƯỚNG NGHIỆP" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php 
							$args = new WP_Query('cat=232&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
						<a class="caia-block-img" href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-0">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php echo short_title('...', 11); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						
							<ul>
								<?php 
								$args = new WP_Query('cat=232&showposts=3&offset=1');
								while($args->have_posts()):$args->the_post();
								?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div> <!-- end older_posts -->
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->
				
			</div><!-- end .block-wrap -->
		</div>
		
		<!--------------->
		<!-- Tin tức 2 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="ĐIỂM CHUẨN">
					<span class="icon-block-title"></span>
					<span>ĐIỂM CHUẨN</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="ĐIỂM CHUẨN" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php 
							$args = new WP_Query('cat=7894&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
						<a class="caia-block-img" href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-1">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						
							<ul>
								<?php 
								$args = new WP_Query('cat=7894&showposts=3&offset=1');
								while($args->have_posts()):$args->the_post();
								?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div> <!-- end older_posts -->
						<div class="clear"></div>
					</div><!-- end 1 post -->
				</div><!-- end .main-posts -->
				
			</div><!-- end .block-wrap -->
		</div>
		
		<!--------------->
		<!-- Tin tức 3 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/tin-cong-nghe/" title="TIN CÔNG NGHỆ">
					<span class="icon-block-title"></span>
					<span>TIN CÔNG NGHỆ</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-cong-nghe/" title="TIN CÔNG NGHỆ" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php 
							$args = new WP_Query('cat=204&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
						<a class="caia-block-img" href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-2">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						
							<ul>
								<?php 
								$args = new WP_Query('cat=204&showposts=3&offset=1');
								while($args->have_posts()):$args->the_post();
								?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div> <!-- end older_posts -->
						<div class="clear"></div>
					</div><!-- end 1 post -->
				</div><!-- end .main-posts -->
				
			</div><!-- end .block-wrap -->
		</div>
		
		<!--------------->
		<!-- Tin tức 4 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/tin-tuc-he-dai-hoc/" title="TIN HỆ ĐẠI HỌC">
					<span class="icon-block-title"></span>
					<span>TIN HỆ ĐẠI HỌC</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-tuc-he-dai-hoc/" title="TIN HỆ ĐẠI HỌC" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php 
							$args = new WP_Query('cat=870&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
						<a class="caia-block-img" href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-3">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						
							<ul>
								<?php 
								$args = new WP_Query('cat=870&showposts=3&offset=1');
								while($args->have_posts()):$args->the_post();
								?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div> <!-- end older_posts -->
						<div class="clear"></div>
					</div><!-- end 1 post -->
				</div><!-- end .main-posts -->
				
			</div><!-- end .block-wrap -->
		</div>
		
		<!--------------->
		<!-- Tin tức 5 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="TIN HỆ TRUNG CẤP">
					<span class="icon-block-title"></span>
					<span>TIN HỆ TRUNG CẤP</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="TIN HỆ TRUNG CẤP" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php 
							$args = new WP_Query('cat=868&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
						<a class="caia-block-img" href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-4">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
						<?php endwhile; wp_reset_postdata(); ?>
							<ul>
								<?php
								$args = new WP_Query('cat=868&showposts=3&offset=1');
								while($args->have_posts()):$args->the_post();
								?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div> <!-- end older_posts -->
						
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->
			</div><!-- end .block-wrap -->
		</div>
	</div>
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!-------------------------------------------------------------------------- Mobile -------------------------------------------------------------------------------->
	<div id="mobile-centent">
		<!-- Tin tức 1 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-huong-nghiep/" title="TIN HƯỚNG NGHIỆP">
					TIN HƯỚNG NGHIỆP
				</a>
				<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Thủ tục, hồ sơ nhập học" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php
							$args = new WP_Query('cat=232&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=232&showposts=5&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 2 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="ĐIỂM CHUẨN">
					ĐIỂM CHUẨN
				</a>
				<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="Điểm chuẩn" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=7894&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->
				</div><!-- end .main-posts -->
				
				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=7894&showposts=5&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 3 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-cong-nghiep/" title="TIN CÔNG NGHIỆP">
					TIN CÔNG NGHIỆP
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-cong-nghiep/" title="Tin công nghiệp" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=204&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=204&showposts=5&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 4 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-he-dai-hoc/" title="TIN HỆ ĐẠI HỌC">
					TIN HỆ ĐẠI HỌC
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-he-dai-hoc/" title="Tin hệ đại học" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=870&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=870&showposts=5&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 5 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="TIN HỆ TRUNG CẤP">
					TIN HỆ TRUNG CẤP
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="Tin hệ trung cấp" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php
							$args = new WP_Query('cat=868&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=868&showposts=5&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
	</div>
	<?php
}
/****** END ******/

/**
 * Những bài viết nhiều người xem nhất mà google tổng hợp
 */
 add_action('genesis_before_footer', 'view_google');
 function view_google(){
	?>
	<div class="slider-home">
		<h2 class="block-title">
			<a href="#" title="Ôn thi trực tuyến">
				<span class="icon-block-title"></span>
				<span>Ôn thi trực tuyến</span>
			</a>
		</h2>
		<h2 class="block-title-mobile">
			<a href="#" title="TIN HỆ TRUNG CẤP">
				Ôn thi trực tuyến
			</a>
		</h2>
		<div class="menu-luyenthi">
			<div class="cac-mon-hoc">
				<div class="menu-luyenthi-menu-container">
					<ul id="menu-luyenthi-menu" class="menu">
						<li>
							<!-- <div class="col-sm-6 col-xs"></div> -->
							<a href="https://kenhtuyensinh24h.vn/mon-toan/">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-toan-hoc-48x48.png" alt="icon môn toán học" srcset="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-toan-hoc-48x48.png 48w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-toan-hoc-110x110.png 110w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-toan-hoc.png 150w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-toan-hoc-24x24.png 24w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-toan-hoc-36x36.png 36w" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Toán</span>
							</a>
						</li>
						<li>
							<a href="https://kenhtuyensinh24h.vn/mon-vat-ly/">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-vat-ly-48x48.png" alt="icon môn vật lý" srcset="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-vat-ly-48x48.png 48w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-vat-ly-110x110.png 110w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-vat-ly.png 150w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-vat-ly-24x24.png 24w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-vat-ly-36x36.png 36w" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Vật Lý</span>
							</a>
						</li>
						<li>
							<a href="https://kenhtuyensinh24h.vn/mon-hoa-hoc/">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-hoa-hoc-48x48.png" alt="icon môn hóa học" srcset="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-hoa-hoc-48x48.png 48w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-hoa-hoc-110x110.png 110w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-hoa-hoc.png 150w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-hoa-hoc-24x24.png 24w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-hoa-hoc-36x36.png 36w" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Hóa Học</span>
							</a>
						</li>
						<li>
							<a href="https://kenhtuyensinh24h.vn/mon-ngu-van/">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-ngu-van-48x48.png" alt="icon môn ngữ văn" srcset="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-ngu-van-48x48.png 48w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-ngu-van-110x110.png 110w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-ngu-van.png 150w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-ngu-van-24x24.png 24w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-ngu-van-36x36.png 36w" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Ngữ Văn</span>
							</a>
						</li>
						<li>
							<a href="https://kenhtuyensinh24h.vn/mon-tieng-anh/">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-tieng-anh-48x48.png" alt="icon môn tiếng anh" srcset="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-tieng-anh-48x48.png 48w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-tieng-anh-110x110.png 110w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-tieng-anh.png 150w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-tieng-anh-24x24.png 24w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-tieng-anh-36x36.png 36w" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Tiếng Anh</span>
							</a>
						</li>
						<li>
							<a href="https://kenhtuyensinh24h.vn/mon-tieng-anh/">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-sinh-hoc-48x48.png" alt="icon môn sinh học" srcset="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-sinh-hoc-48x48.png 48w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-sinh-hoc-110x110.png 110w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-sinh-hoc.png 150w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-sinh-hoc-24x24.png 24w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-sinh-hoc-36x36.png 36w" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Sinh Học</span>
							</a>
						</li>
						<li>
							<a href="https://kenhtuyensinh24h.vn/mon-tieng-anh/">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-dia-ly-48x48.png" alt="icon môn địa lý" srcset="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-dia-ly-48x48.png 48w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-dia-ly-110x110.png 110w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-dia-ly.png 150w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-dia-ly-24x24.png 24w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-dia-ly-36x36.png 36w" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Địa Lý</span>
							</a>
						</li>
						<li>
							<a href="https://kenhtuyensinh24h.vn/mon-tieng-anh/">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-lich-su-48x48.png" alt="icon môn tiếng anh" srcset="https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-lich-su-48x48.png 48w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-lich-su-110x110.png 110w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-lich-su.png 150w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-lich-su-24x24.png 24w, https://kenhtuyensinh24h.vn/wp-content/uploads/2017/12/icon-lich-su-36x36.png 36w" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Lịch Sử</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="giao-vien">
			<ul class="giao-vien-ul">
				<li class="giao-vien-li">
					<div class="col">
						<div class="images">
							<a title="Khởi động môn toán" href="https://kenhtuyensinh24h.vn/giao-vien-toan/">
								<img class="image" alt="Facebook Marketing từ A - Z" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/MON-TOAN.png">
							</a>
							<div class="middle">
								<a title="Môn toán" href="https://kenhtuyensinh24h.vn/giao-vien-toan/"></a>
								<ul class="list-group hover_img">
									<a title="Môn toán" href="https://kenhtuyensinh24h.vn/giao-vien-toan/"></a>
									<center>
										<a title="môn toán" href="https://kenhtuyensinh24h.vn/giao-vien-toan/"></a>
										<a href="https://kenhtuyensinh24h.vn/giao-vien-toan/" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="#"><b>Khóa khởi động môn toán</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="#">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="#">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
				
				<li>
					<div class="col">
						<div class="images">
							<a title="Facebook Marketing từ A - Z" href="#">
								<img class="image" alt="Facebook Marketing từ A - Z" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/Vat-ly.png">
							</a>
							<div class="middle">
								<a title="Facebook Marketing từ A - Z" href="#"></a>
								<ul class="list-group hover_img">
									<a title="Facebook Marketing từ A - Z" href="#"></a>
									<center>
										<a title="Facebook Marketing từ A - Z" href="#"></a>
										<a href="#" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="#"><b>Khóa khởi động môn vật lý</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="#">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="#">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
				
				<li>
					<div class="col">
						<div class="images">
							<a title="Facebook Marketing từ A - Z" href="#">
								<img class="image" alt="Facebook Marketing từ A - Z" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/HOA-HOC.png">
							</a>
							<div class="middle">
								<a title="Facebook Marketing từ A - Z" href="#"></a>
								<ul class="list-group hover_img">
									<a title="Facebook Marketing từ A - Z" href="#"></a>
									<center>
										<a title="Facebook Marketing từ A - Z" href="#"></a>
										<a href="#" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="#"><b>Khóa khởi động môn hóa học</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="#">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="#">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
				
				<li class="giao-vien-li">
					<div class="col">
						<div class="images">
							<a title="Facebook Marketing từ A - Z" href="#">
								<img class="image" alt="Facebook Marketing từ A - Z" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/van-hoc.png">
							</a>
							<div class="middle">
								<a title="Facebook Marketing từ A - Z" href="#"></a>
								<ul class="list-group hover_img">
									<a title="Facebook Marketing từ A - Z" href="#"></a>
									<center>
										<a title="Facebook Marketing từ A - Z" href="#"></a>
										<a href="#" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="#"><b>Khóa khởi động môn ngữ văn</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="#">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="#">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
				
				<li class="giao-vien-li">
					<div class="col">
						<div class="images">
							<a title="Facebook Marketing từ A - Z" href="#">
								<img class="image" alt="Facebook Marketing từ A - Z" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/tieng-anh.png">
							</a>
							<div class="middle">
								<a title="Facebook Marketing từ A - Z" href="#"></a>
								<ul class="list-group hover_img">
									<a title="Facebook Marketing từ A - Z" href="#"></a>
									<center>
										<a title="Facebook Marketing từ A - Z" href="#"></a>
										<a href="#" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="#"><b>Khóa khởi động môn tiếng anh</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="#">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="#">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<?php
 }