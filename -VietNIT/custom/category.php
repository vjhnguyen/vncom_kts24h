<?php
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'tintuc' );
add_action( 'genesis_before_content_sidebar_wrap', 'tintucnoibatcat' );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
add_action('wp_head','position');
	
function tintuc(){
?>
<div id="tin-tuc">
	<div class="wrap-news">
		<div class="widget-area">
			<?php if(is_active_sidebar('first_adv_category') && !dynamic_sidebar('first_adv_category')):endif; ?>
		</div>
		
		<h1 class="archive-heading">
		<span class="icon-block-h1"></span>
		<span><?php single_cat_title(); ?></span>
		</h1>
		
		<div class="main-news">
			<?php
			while(have_posts()) : the_post();
				$thumbnail = genesis_get_image(
					array(
						'size' => 'news-td',
						'attr' => array(
							'alt' => caia_get_image_attr( 'alt' ),
							'title' => caia_get_image_attr( 'title' ),
							'class' => 'alignleft'
						)
					)
				);
			?>
			<div class="widget-tintuc">
				<div class="category-mobile">
					<h2 class="title-category">
					<a class="title-news "href="<?php the_permalink();?>"><?php the_title(); ?></a>
					</h2>
					<span class="time">Ngày đăng: <?php the_time('d') ?>/<?php the_time('m') ?>/<?php the_time('Y') ?></span>
				</div>
				<?php
					if ( ! empty( $thumbnail ) ) {
						printf( '<a class="aimg" href="%s" title="%s">%s</a>', get_permalink(), get_the_title(), $thumbnail );
					}
				?>
				<div class="desctt">
					<p><?php the_content_rss('', false,'', 15);?></p>
					<!-- RANDOM -->
					<div id="random-post">
						<ul>
							<?php
								$randPosts = new WP_Query();
								$randPosts->query('showposts=2&orderby=rand');
							while($randPosts->have_posts()) : $randPosts->the_post();?>
							<li>
								<a href="<?php the_permalink();?>"><?php the_title();?></a>
							</li>
							<?php endwhile; ?>
						</ul>
					</div>
					<!-- END -->
					<!-- Mobile -->
					<ul id="ct-mobile">
						<?php
							$randPosts = new WP_Query();
							$randPosts->query('showposts=3&orderby=rand');
						while($randPosts->have_posts()) : $randPosts->the_post();?>
						<li>
							<a href="<?php the_permalink();?>" title=""><h3 class="star-mobile" >
							<?php the_title(); ?>
							</h3></a>
							<a class="slide-mobile" href="<?php the_permalink();?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(120,120));} ;?>
							</a>
							<p><?php the_content_rss('', false,'', 25);?></p>
						</li>
						<?php endwhile; wp_reset_postdata();?>
					</ul>
					<!-- END -->
				</div>
				<span class="xt"> <a href="<?php the_permalink();?>">Xem tiếp</a></span>
			</div>
		<?php endwhile; wp_reset_postdata();?>
		<div class="clear" style="border-top: 1px solid #ddd;"></div>
		<div class="paging">
			<div class="navigation">
				<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
				<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
				<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
				<?php } ?>
			</div>
			<div class="nav_mobi">
				<div class="row">
					<div class="col-xs-12">
						<ul class="pager">
							<li><?php previous_posts_link('Prev') ?></li>
							<li><?php next_posts_link('Next') ?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="banner-category">
			<?php if(is_active_sidebar('middle_adv_category') && !dynamic_sidebar('middle_adv_category')):endif; ?>										
	</div>
</div>
</div>
<?php
}
/**
* Tin tức dưới cùng
* SANG MINI thêm 21/11/2017
*/



add_action('genesis_loop','new_tintuc' );
function new_tintuc(){
?>
<!---------------------------------------------------------------------------->
<!-- QUẢNG CÁO CỦA GOOGLE -->
<div class="quang-cao-google" style="width: 100%;float: left;">
	<div class="box_video">
		<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/Capture.PNG" alt="">
	</div>
<div class="quang-cao-1-category">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- kenhtuyensinh24h.vn - Banner 4 -->
	<ins class="adsbygoogle"
	style="display:inline-block;width:300px;height:250px"
	data-ad-client="ca-pub-1895892504965300"
	data-ad-slot="8543525730"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</div>
<div class="quang-cao-2-category">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- kenhtuyensinh24h.vn - Banner 7 -->
	<ins class="adsbygoogle"
	style="display:inline-block;width:300px;height:250px"
	data-ad-client="ca-pub-1895892504965300"
	data-ad-slot="4258010410"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</div>
</div>
<!-- END -->
<!---------------------------------------------------------------------------->

<!------- DESKTOP -------->
<div id="desktop-centent">
<!--------------->
<!-- Tin tức 1 -->
<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
	<h2 class="block-title">
	<a href="https://kenhtuyensinh24h.vn/tin-huong-nghiep/" title="TIN HƯỚNG NGHIỆP">
		<span class="icon-block-title"></span>
		<span>TIN HƯỚNG NGHIỆP</span>
	</a>
	<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Thủ tục, hồ sơ nhập học" class="xemtatca">Xem tất cả</a>
	</h2>
	<div class="block-wrap">
		<div class="main-posts">
			<div class="caia-block-item">
				<?php
					$args = new WP_Query('cat=232&showposts=1');
					while($args->have_posts()): $args->the_post();
				?>
				<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
					<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
				</a>
				<div class="older-posts-tintuc tintuc-0">
					<h3 class="title-block">
					<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php echo short_title('...', 11); ?></a>
					</h3>
					<p><?php the_content_rss('', false,'', 40);?></p>
					<?php endwhile; wp_reset_postdata(); ?>
					
					<ul>
						<?php
						$args = new WP_Query('cat=232&showposts=3&offset=1');
						while($args->have_posts()):$args->the_post();
						?>
						<li>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
						</li>
						<?php endwhile; wp_reset_postdata(); ?>
					</ul>
					</div> <!-- end older_posts -->
					<div class="clear"></div>
					</div><!-- end 1 post -->
					</div><!-- end .main-posts -->
					
					</div><!-- end .block-wrap -->
				</div>
				
				<!--------------->
				<!-- Tin tức 2 -->
				<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
					<h2 class="block-title">
					<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="ĐIỂM CHUẨN">
						<span class="icon-block-title"></span>
						<span>ĐIỂM CHUẨN</span>
					</a>
					<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Thủ tục, hồ sơ nhập học" class="xemtatca">Xem tất cả</a>
					</h2>
					<div class="block-wrap">
						<div class="main-posts">
							<div class="caia-block-item">
								<?php
									$args = new WP_Query('cat=7894&showposts=1');
									while($args->have_posts()): $args->the_post();
								?>
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
									<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
								</a>
								<div class="older-posts-tintuc tintuc-1">
									<h3 class="title-block">
									<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</h3>
									<p><?php the_content_rss('', false,'', 40);?></p>
									<?php endwhile; wp_reset_postdata(); ?>
									
									<ul>
										<?php
										$args = new WP_Query('cat=7894&showposts=3&offset=1');
										while($args->have_posts()):$args->the_post();
										?>
										<li>
											<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
										</li>
										<?php endwhile; wp_reset_postdata(); ?>
									</ul>
									</div> <!-- end older_posts -->
									<div class="clear"></div>
									</div><!-- end 1 post -->
									</div><!-- end .main-posts -->
									
									</div><!-- end .block-wrap -->
								</div>
								
								<!--------------->
								<!-- Tin tức 3 -->
								<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
									<h2 class="block-title">
									<a href="https://kenhtuyensinh24h.vn/tin-cong-nghe/" title="TIN CÔNG NGHỆ">
										<span class="icon-block-title"></span>
										<span>TIN CÔNG NGHỆ</span>
									</a>
									<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Thủ tục, hồ sơ nhập học" class="xemtatca">Xem tất cả</a>
									</h2>
									<div class="block-wrap">
										<div class="main-posts">
											<div class="caia-block-item">
												<?php
													$args = new WP_Query('cat=204&showposts=1');
													while($args->have_posts()): $args->the_post();
												?>
												<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
													<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
												</a>
												<div class="older-posts-tintuc tintuc-2">
													<h3 class="title-block">
													<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													</h3>
													<p><?php the_content_rss('', false,'', 40);?></p>
													<?php endwhile; wp_reset_postdata(); ?>
													
													<ul>
														<?php
														$args = new WP_Query('cat=204&showposts=3&offset=1');
														while($args->have_posts()):$args->the_post();
														?>
														<li>
															<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
														</li>
														<?php endwhile; wp_reset_postdata(); ?>
													</ul>
													</div> <!-- end older_posts -->
													<div class="clear"></div>
													</div><!-- end 1 post -->
													</div><!-- end .main-posts -->
													
													</div><!-- end .block-wrap -->
												</div>
												
												<!--------------->
												<!-- Tin tức 4 -->
												<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
													<h2 class="block-title">
													<a href="https://kenhtuyensinh24h.vn/tin-tuc-he-dai-hoc/" title="TIN HỆ ĐẠI HỌC">
														<span class="icon-block-title"></span>
														<span>TIN HỆ ĐẠI HỌC</span>
													</a>
													<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Thủ tục, hồ sơ nhập học" class="xemtatca">Xem tất cả</a>
													</h2>
													<div class="block-wrap">
														<div class="main-posts">
															<div class="caia-block-item">
																<?php
																	$args = new WP_Query('cat=870&showposts=1');
																	while($args->have_posts()): $args->the_post();
																?>
																<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																	<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
																</a>
																<div class="older-posts-tintuc tintuc-3">
																	<h3 class="title-block">
																	<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																	</h3>
																	<p><?php the_content_rss('', false,'', 40);?></p>
																	<?php endwhile; wp_reset_postdata(); ?>
																	
																	<ul>
																		<?php
																		$args = new WP_Query('cat=870&showposts=3&offset=1');
																		while($args->have_posts()):$args->the_post();
																		?>
																		<li>
																			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
																		</li>
																		<?php endwhile; wp_reset_postdata(); ?>
																	</ul>
																	</div> <!-- end older_posts -->
																	<div class="clear"></div>
																	</div><!-- end 1 post -->
																	</div><!-- end .main-posts -->
																	
																	</div><!-- end .block-wrap -->
																</div>
																
																<!--------------->
																<!-- Tin tức 5 -->
																<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
																	<h2 class="block-title">
																	<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="TIN HỆ TRUNG CẤP">
																		<span class="icon-block-title"></span>
																		<span>TIN HỆ TRUNG CẤP</span>
																	</a>
																	<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Thủ tục, hồ sơ nhập học" class="xemtatca">Xem tất cả</a>
																	</h2>
																	<div class="block-wrap">
																		<div class="main-posts">
																			<div class="caia-block-item">
																				<?php
																					$args = new WP_Query('cat=868&showposts=1');
																					while($args->have_posts()): $args->the_post();
																				?>
																				<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																					<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
																				</a>
																				<div class="older-posts-tintuc tintuc-4">
																					<h3 class="title-block">
																					<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																					</h3>
																					<p><?php the_content_rss('', false,'', 40);?></p>
																					<?php endwhile; wp_reset_postdata(); ?>
																					
																					<ul>
																						<?php
																						$args = new WP_Query('cat=868&showposts=3&offset=1');
																						while($args->have_posts()):$args->the_post();
																						?>
																						<li>
																							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
																						</li>
																						<?php endwhile; wp_reset_postdata(); ?>
																					</ul>
																					</div> <!-- end older_posts -->
																					<div class="clear"></div>
																					</div><!-- end 1 post -->
																					</div><!-- end .main-posts -->
																					</div><!-- end .block-wrap -->
																				</div>
																			</div>
																			
																			<!------------------------------------------------------------------------------------------------------------------------------------------------------------------>
																			<!-------------------------------------------------------------------------- Mobile -------------------------------------------------------------------------------->
																			<div id="mobile-centent">
																				<!-- Tin tức 1 -->
																				<div class="news-block-tintuc-mobile tintuc-mobile">
																					<h2 class="block-title-mobile">
																					<a href="https://kenhtuyensinh24h.vn/tin-huong-nghiep/" title="TIN HƯỚNG NGHIỆP">
																						TIN HƯỚNG NGHIỆP
																					</a>
																					<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Thủ tục, hồ sơ nhập học" class="xemtatca-mobile">
																						<i class="fa fa-angle-double-down" aria-hidden="true"></i>
																					</a>
																					</h2>
																					<div class="block-wrap-mobile">
																						<div class="main-posts-mobile">
																							<div class="caia-block-item-mobile">
																								<?php
																									$args = new WP_Query('cat=232&showposts=1');
																									while($args->have_posts()): $args->the_post();
																								?>
																								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																									<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
																								</a>
																								<h3 class="title-block-mobile">
																								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																								</h3>
																								<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																								<?php endwhile; wp_reset_postdata(); ?>
																								<div class="clear"></div>
																								</div><!-- end 1 post -->
																								</div><!-- end .main-posts -->
																								<ul class="older-posts-tintuc-mobile">
																									<?php
																									$args = new WP_Query('cat=232&showposts=3&offset=1');
																									while($args->have_posts()):$args->the_post();
																									?>
																									<li>
																										<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																											<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
																											<h3><?php the_title(); ?></h3>
																											<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																										</a>
																									</li>
																									<?php endwhile; wp_reset_postdata(); ?>
																								</ul>
																								</div><!-- end .block-wrap -->
																							</div>
																							<!-- Tin tức 2 -->
																							<div class="news-block-tintuc-mobile tintuc-mobile">
																								<h2 class="block-title-mobile">
																								<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="ĐIỂM CHUẨN">
																									ĐIỂM CHUẨN
																								</a>
																								<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="Điểm chuẩn" class="xemtatca-mobile">
																									<i class="fa fa-angle-double-down" aria-hidden="true"></i>
																								</a>
																								</h2>
																								<div class="block-wrap-mobile">
																									<div class="main-posts-mobile">
																										<div class="caia-block-item-mobile">
																											<?php
																												$args = new WP_Query('cat=7894&showposts=1');
																												while($args->have_posts()): $args->the_post();
																											?>
																											<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																												<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
																											</a>
																											<h3 class="title-block-mobile">
																											<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																											</h3>
																											<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																											<?php endwhile; wp_reset_postdata(); ?>
																											<div class="clear"></div>
																											</div><!-- end 1 post -->
																											</div><!-- end .main-posts -->
																											
																											<ul class="older-posts-tintuc-mobile">
																												<?php
																												$args = new WP_Query('cat=7894&showposts=3&offset=1');
																												while($args->have_posts()):$args->the_post();
																												?>
																												<li>
																													<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																														<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
																														<h3><?php the_title(); ?></h3>
																														<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																													</a>
																												</li>
																												<?php endwhile; wp_reset_postdata(); ?>
																											</ul>
																											</div><!-- end .block-wrap -->
																										</div>
																										<!-- Tin tức 3 -->
																										<div class="news-block-tintuc-mobile tintuc-mobile">
																											<h2 class="block-title-mobile">
																											<a href="https://kenhtuyensinh24h.vn/tin-cong-nghiep/" title="TIN CÔNG NGHIỆP">
																												TIN CÔNG NGHIỆP
																											</a>
																											<a href="https://kenhtuyensinh24h.vn/tin-cong-nghiep/" title="Tin công nghiệp" class="xemtatca-mobile">
																												<i class="fa fa-angle-double-down" aria-hidden="true"></i>
																											</a>
																											</h2>
																											<div class="block-wrap-mobile">
																												<div class="main-posts-mobile">
																													<div class="caia-block-item-mobile">
																														<?php
																															$args = new WP_Query('cat=204&showposts=1');
																															while($args->have_posts()): $args->the_post();
																														?>
																														<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																															<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
																														</a>
																														<h3 class="title-block-mobile">
																														<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																														</h3>
																														<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																														<?php endwhile; wp_reset_postdata(); ?>
																														<div class="clear"></div>
																														</div><!-- end 1 post -->
																														</div><!-- end .main-posts -->
																														<ul class="older-posts-tintuc-mobile">
																															<?php
																															$args = new WP_Query('cat=204&showposts=3&offset=1');
																															while($args->have_posts()):$args->the_post();
																															?>
																															<li>
																																<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																																	<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
																																	<h3><?php the_title(); ?></h3>
																																	<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																																</a>
																															</li>
																															<?php endwhile; wp_reset_postdata(); ?>
																														</ul>
																														</div><!-- end .block-wrap -->
																													</div>
																													<!-- Tin tức 4 -->
																													<div class="news-block-tintuc-mobile tintuc-mobile">
																														<h2 class="block-title-mobile">
																														<a href="https://kenhtuyensinh24h.vn/tin-he-dai-hoc/" title="TIN HỆ ĐẠI HỌC">
																															TIN HỆ ĐẠI HỌC
																														</a>
																														<a href="https://kenhtuyensinh24h.vn/tin-he-dai-hoc/" title="Tin hệ đại học" class="xemtatca-mobile">
																															<i class="fa fa-angle-double-down" aria-hidden="true"></i>
																														</a>
																														</h2>
																														<div class="block-wrap-mobile">
																															<div class="main-posts-mobile">
																																<div class="caia-block-item-mobile">
																																	<?php
																																		$args = new WP_Query('cat=870&showposts=1');
																																		while($args->have_posts()): $args->the_post();
																																	?>
																																	<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																																		<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
																																	</a>
																																	<h3 class="title-block-mobile">
																																	<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																																	</h3>
																																	<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																																	<?php endwhile; wp_reset_postdata(); ?>
																																	<div class="clear"></div>
																																	</div><!-- end 1 post -->
																																	</div><!-- end .main-posts -->
																																	<ul class="older-posts-tintuc-mobile">
																																		<?php
																																		$args = new WP_Query('cat=870&showposts=3&offset=1');
																																		while($args->have_posts()):$args->the_post();
																																		?>
																																		<li>
																																			<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																																				<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
																																				<h3><?php the_title(); ?></h3>
																																				<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																																			</a>
																																		</li>
																																		<?php endwhile; wp_reset_postdata(); ?>
																																	</ul>
																																	</div><!-- end .block-wrap -->
																																</div>
																																<!-- Tin tức 5 -->
																																<div class="news-block-tintuc-mobile tintuc-mobile">
																																	<h2 class="block-title-mobile">
																																	<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="TIN HỆ TRUNG CẤP">
																																		TIN HỆ TRUNG CẤP
																																	</a>
																																	<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="Tin hệ trung cấp" class="xemtatca-mobile">
																																		<i class="fa fa-angle-double-down" aria-hidden="true"></i>
																																	</a>
																																	</h2>
																																	<div class="block-wrap-mobile">
																																		<div class="main-posts-mobile">
																																			<div class="caia-block-item-mobile">
																																				<?php
																																					$args = new WP_Query('cat=868&showposts=1');
																																					while($args->have_posts()): $args->the_post();
																																				?>
																																				<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																																					<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
																																				</a>
																																				<h3 class="title-block-mobile">
																																				<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																																				</h3>
																																				<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																																				<?php endwhile; wp_reset_postdata(); ?>
																																				<div class="clear"></div>
																																				</div><!-- end 1 post -->
																																				</div><!-- end .main-posts -->
																																				<ul class="older-posts-tintuc-mobile">
																																					<?php
																																					$args = new WP_Query('cat=868&showposts=3&offset=1');
																																					while($args->have_posts()):$args->the_post();
																																					?>
																																					<li>
																																						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
																																							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
																																							<h3><?php the_title(); ?></h3>
																																							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
																																						</a>
																																					</li>
																																					<?php endwhile; wp_reset_postdata(); ?>
																																				</ul>
																																				</div><!-- end .block-wrap -->
																																			</div>
																																		</div>
																																		
																																		<!----------------------------------------------------------------------- END ------------------------------------------------------------------------------->
																																		<?php
																																		}
																																		/****** END ******/
																																		function position(){
																																		?>
																																		<script>
																																			$(function(){
																																				var elementPosition = $('#footer').offset().top;
																																					var elementPosition2 = $('#caia-post-list-9').offset().top;
																																				$(window).on('scroll', function(){
																																					if( $(window).scrollTop()>elementPosition2-200 && $(window).scrollTop() < elementPosition-810 ){
																																						$('#caia-post-list-9').addClass('fixed');
																																						$('#caia-post-list-9').removeClass('fixedpc');
																																					}
																																					else if($(window).scrollTop()>elementPosition-810)
																																					{
																																						$('#caia-post-list-9').addClass('fixedpc');
																																						$('#caia-post-list-9').removeClass('fixed');
																																					}
																																					else{
																																						$('#caia-post-list-9').removeClass('fixed');
																																						$('#caia-post-list-9').removeClass('fixedpc');
																																					}
																																				});
																																				$(window).on('scroll', function(){
																																					if( $(window).scrollTop()>1600 && $(window).scrollTop()< elementPosition-540){
																																						$('#text-23').removeClass('fixed2pc');
																																						$('#text-23').addClass('fixed2');
																																					}
																																					else if($(window).scrollTop()>elementPosition-540){
																																						$('#text-23').addClass('fixed2pc');
																																						$('#text-23').removeClass('fixed2');
																																					}
																																					else{
																																						$('#text-23').removeClass('fixed2');
																																						$('#text-23').removeClass('fixed2pc');
																																					}
																																				});
																																			});
																																		</script>
																																		<?php
																																		}