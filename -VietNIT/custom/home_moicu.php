<?php
add_action('genesis_before_content_sidebar_wrap','quangcao',1);
//add_action( 'genesis_before_content_sidebar_wrap', 'metro_menu', 2);
add_action( 'genesis_before_content_sidebar_wrap', 'featured', 3);
function quangcao(){
	if(is_active_sidebar("ads") && !dynamic_sidebar("ads")):endif;
}

function metro_menu(){
	?>
	<div id="metro_menu">
		<div class="tracuu"><a href="#" alt="">Tra cứu điểm chuẩn ĐH - CĐ toàn quốc năm 2015</a></div>
	<div id="ttc">	
		<div class="thuvien"><a href="#" alt="">Thư viện đề thi</a></div>
		<div class="tin"><a href="http://kenhtuyensinh24h.vn/tin-tuc/" alt="Tin tuyển sinh">Tin Tuyển sinh</a></div>
		<div class="chitieu"><a href="#" alt="">Chỉ tiêu tuyển sinh 2015</a></div>
	</div>	
	</div>
	<?php
}

    function featured() {      
    $featureds = new WP_Query( 'cat=3&showposts=1' ); ?>
	<div class="therain">
			<div  class="lof_slidecontent">
					<div class="lof-main-outer">
							<?php
							$args= array(
								'cat'=>'3',
								'showposts' => 1
								);
							$featuredPost = new WP_Query($args);							
							while($featuredPost->have_posts()) : $featuredPost->the_post();?>
									<a class="slide" href="<?php the_permalink();?>"><?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?></a>
									<div class="lof-main-item-desc">
										<h3 class="ginuaday"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
										<p><?php the_content_rss('', false,'', 36);?></p>
									</div>
							<?php endwhile; wp_reset_postdata();?>							
							<div id="ct-morelink">
									<?php 
									$args= array(
										'cat'=>'3',
										'showposts' => 3,
										'offset' =>1
										);
									$featuredPost = new WP_Query($args);	
									while($featuredPost->have_posts()) : $featuredPost->the_post();?>
									<p><span><a class="star" href="<?php the_permalink(); ?>"><i class="fa fa-hand-o-right" aria-hidden="true"></i><?php the_title(); ?></a></span></p>
									<?php endwhile; wp_reset_postdata();?>
							</div>
					</div>
					<div class="lof_navigator_outer">
						<ul class="lof_navigator">
							<?php
							$args = array(
									'post_type' =>'post',
								    'posts_per_page' => 5,
								    'cat'=>'3',
								    'offset'=>4
								);
							$featuredPost = new WP_Query($args);							
							while($featuredPost->have_posts()) : $featuredPost->the_post();
							$thumbnail = genesis_get_image( 
								array(
									'size' => 'vertical-hds',
									'attr' => array(
										'alt' => caia_get_image_attr( 'alt' ),
										'title' => caia_get_image_attr( 'title' ),
										'class' => 'alignleft'
									)
								)
							);
							?>
								<li>
									<h3 class="sub-title"><?php the_category(',');?></h3>
									<a class="atitle" href="<?php the_permalink();?>"><?php the_title();?></a>
								</li>
							<?php endwhile; wp_reset_postdata();?>
						</ul>
					</div>

			</div>
		<div class="right-new">
			<?php if ( is_active_sidebar( 'min' ) && !dynamic_sidebar('min') ) : endif; ?>
		</div><!--end right-new-->
	</div><!--End The Rain-->
<?php 
}

add_action('genesis_loop','daihoc' );


function daihoc(){
	?>
<div class="clear"></div>
	<!-- Đại học -->
<div id="home-block-news">
	<div class="news news1">
	<div role="tabpanel">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs responsive hidden-xs hidden-sm" role="tablist">
			<li role="presentation" class="active">
				<a href="#daihoc" aria-controls="daihoc" role="tab" data-toggle="tab">Đại học</a>
			</li>
			<li role="presentation">
				<a href="#caodang" role="tab" data-toggle="tab">Cao Đẳng</a>
			</li>
			<li role="presentation">
				<a href="#trungcap" role="tab" data-toggle="tab">Trung Cấp</a>
			</li>
			<li role="presentation">
				<a href="#vanbang2" role="tab" data-toggle="tab">Văn Bằng 2</a>
			</li>
			<li role="presentation">
				<a href="#caohoc" role="tab" data-toggle="tab">Cao Học</a>
			</li>
			<li role="presentation">
				<a href="#lienthong" role="tab" data-toggle="tab">Liên Thông</a>
			</li>			
		</ul>
				<!-- tab mobile -->
		<ul class="nav nav-tabs responsive visible-xs visible-sm" role="tablist">
			<li role="presentation" class="custom-drop dropdown active">
			<h4 class="title-tab">Đại học</h4>
			<button class="dropdown-toggle custom-toggle" type="button" id="myTabDrop1" data-toggle="dropdown">
			<i class="fa fa-angle-double-down" aria-hidden="true"></i></button>
				<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
					<li role="presentation" class="active">
					<a href="#daihoc" aria-controls="daihoc" role="tab" data-toggle="tab">Đại học</a>
					</li>
					<li role="presentation">
						<a href="#caodang" role="tab" data-toggle="tab">Cao Đẳng</a>
					</li>
					<li role="presentation">
						<a href="#trungcap" role="tab" data-toggle="tab">Trung cấp</a>
					</li>
					<li role="presentation">
						<a href="#vanbang2" role="tab" data-toggle="tab">Văn Bằng 2</a>
					</li>
					<li role="presentation">
						<a href="#caohoc" role="tab" data-toggle="tab">Cao Học</a>
					</li>
					<li role="presentation">
						<a href="#lienthong" role="tab" data-toggle="tab">Liên Thông</a>
					</li>
				</ul> 
			</li>
		</ul>
		
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active fade in" id="daihoc">			
				<div class="itemblock">
					<div class="item">
					<a class="item-img" href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-tp-ha-noi/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/hanoi.jpg" /></a>
					<a class="aname" href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-tp-ha-noi/">Danh sách các trường Đại Học và Học viện khu vực Hà Nội</a>
					</div>
				</div>
				<div class="itemblock">
					<div class="item">
					<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-tp-hcm/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/tphcm.jpg" /></a>
					<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-tp-hcm/" class="aname">Danh sách các trường Đại Học và Học viện khu vực TP.HCM</a>
					</div>
				</div>
				<div class="itemblock">
					<div class="item">
					<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-bac/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/mienbac.jpg" /></a>
					<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-bac/" class="aname">Danh sách các trường Đại Học và Học viện khu vực Miền Bắc</a>
					</div>
				</div>
				<div class="itemblock">
					<div class="item">
					<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-trung/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/mientrung.jpg" /></a>
					<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-trung/" class="aname">Danh sách các trường Đại Học và Học viện khu vực Miền Trung</a>
					</div>
				</div>
				<div class="itemblock">
					<div class="item">
					<a class="item-img" href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-nam/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/miennam.png" /></a>
					<a href="http://kenhtuyensinh24h.vn/dai-hoc/dai-hoc-khu-vuc-mien-nam/" class="aname">Danh sách các trường Đại Học và Học viện khu vực Miền Nam</a>
					</div>
				</div>
				<div class="itemblock">
					<div class="item">
					<a class="item-img" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-bo-doi-cong-an/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/bodoi.jpg" /></a>
					<a href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-bo-doi-cong-an/" class="aname">Danh sách các trường Khối Bộ đội - Công an</a>
					</div>
				</div>
			</div>
			<!-- tab dai hoc -->
			<div role="tabpanel" class="tab-pane fade" id="caodang">
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-ha-noi/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-ha-noi/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực Hà Nội</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-hcm/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-hcm/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực TP.HCM</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-bac/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-bac/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực Miền Bắc</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a class="item-img" href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-trung/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-trung/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực Miền Trung</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-nam/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-mien-nam/" class="aname">Danh sách các trường Cao Đẳng và Cao Đẳng Nghề khu vực Miền Nam</a>
						</div>
					</div>
			</div>
			<!-- end tab cao dang -->
			<div role="tabpanel" class="tab-pane fade in" id="trungcap">
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-tp-ha-noi/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-tp-ha-noi/" class="aname">Danh sách các trường Trung Cấp khu vực Hà Nội</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/cao-dang/cao-dang-khu-vuc-tp-hcm/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-tp-hcm/" class="aname">Danh sách các trường Trung Cấp khu vực TP.HCM</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-bac/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-bac/" class="aname">Danh sách các trường Trung Cấp khu vực Miền Bắc</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-trung/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-trung/" class="aname">Danh sách các trường Trung Cấp khu vực Miền Trung</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-nam/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/khong-logo.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-nam/" class="aname">Danh sách các trường Trung Cấp khu vực Miền Nam</a>
						</div>
					</div>			
			</div>
			<!-- trung cap -->
			<div role="tabpanel" class="tab-pane fade in" id="vanbang2">
					<?php 
						$args=new WP_Query('cat=76&showposts=6');
						if(have_posts()):
						while($args->have_posts()):$args->the_post();
					?>
						<div class="itemblock">
							<div class="item">
							<a href="<?php the_permalink(); ?>" class='item-img'>
							<?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?>
							</a>
							<a href="<?php the_permalink(); ?>" class='aname'><?php echo short_title('...', 10); ?></a>
							</div>
						</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: _e('Chưa có dữ liệu nào'); ?>
					<?php endif; ?>	
			</div>
			<!-- end van bang 2 -->
			<div role="tabpanel" class="tab-pane fade in" id="caohoc">			
					<?php 
							$args=new WP_Query('cat=85&showposts=6');
							if(have_posts()):
							while($args->have_posts()):$args->the_post();
					?>
							<div class="itemblock">
								<div class="item">
								<a href="<?php the_permalink(); ?>" class='item-img'><?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?></a>
								<a href="<?php the_permalink(); ?>" class='aname'><?php echo short_title('...', 10); ?></a>
								</div>
							</div>
						<?php endwhile; wp_reset_postdata(); ?>
						<?php else: _e('Chưa có dữ liệu nào'); ?>
						<?php endif; ?>
			</div>
			<!-- end cao hoc -->
			<div role="tabpanel" class="tab-pane fade in" id="lienthong">
			<?php 
				$args=new WP_Query('cat=84&showposts=6');
				if(have_posts()):
				while($args->have_posts()):$args->the_post();
			?>
				<div class="itemblock">
					<div class="item">
					<a href="<?php the_permalink(); ?>" class='item-img'><?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?></a>
					<a href="<?php the_permalink(); ?>" class='aname'><?php echo short_title('...', 10); ?></a>
					</div>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
			<?php else: _e('Chưa có dữ liệu nào'); ?>
			<?php endif; ?>
			</div>
			<!-- end lien thong -->
		</div><!--end tab-content -->
	</div> <!--end tabpanel -->
</div> <!-- end #news1 -->
<div class="clear"></div>
<div class="news news2">
	<div role="tabpanel">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs responsive hidden-xs hidden-sm" role="tablist">
			<li role="presentation" class="active">
				<a href="#diemchuan" aria-controls="diemchuan" role="tab" data-toggle="tab">Điểm chuẩn</a>
			</li>
			<li role="presentation">
				<a href="#thnn" role="tab" data-toggle="tab">Tìm hiểu ngành nghề</a>
			</li>
			<li role="presentation">
				<a href="#hdhs" role="tab" data-toggle="tab">Hướng dẫn hồ sơ</a>
			</li>
		</ul>
				<!-- tab mobile -->
		<ul class="nav nav-tabs responsive visible-xs visible-sm" role="tablist">
			<li role="presentation" class="custom-drop dropdown active">
			<h4 class="title-tab">Điểm chuẩn</h4>
			<button class="dropdown-toggle custom-toggle" type="button" id="myTabDrop1" data-toggle="dropdown">
			<i class="fa fa-angle-double-down" aria-hidden="true"></i></button>
				<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
					<li role="presentation" class="active">
					<a href="#diemchuan" aria-controls="diemchuan" role="tab" data-toggle="tab">Điểm chuẩn</a>
					</li>
					<li role="presentation">
						<a href="#thnn" role="tab" data-toggle="tab">Tìm hiểu ngành nghề</a>
					</li>
					<li role="presentation">
						<a href="#hdhs" role="tab" data-toggle="tab">Hướng dẫn hồ sơ</a>
					</li>
				</ul> 
			</li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active fade in" id="diemchuan">
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-ha-noi/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/dchanoi.jpg" /></a>
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-ha-noi/" class="aname">Điểm chuẩn các trường khu vực Hà Nội</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-tp-hcm/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/dctphcm.jpg" /></a>
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-tp-hcm/" class="aname">Điểm chuẩn các trường khu vực TP.HCM</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-mien-bac/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/dcmienbac.jpg" /></a>
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-mien-bac/" class="aname">Điểm chuẩn các trường khu vực Miền Bắc</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-mien-trung/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/dcmientrung.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-mien-trung/" class="aname">Điểm chuẩn các trường khu vực Miền Trung</a>
						</div>
					</div>
					<div class="itemblock">
						<div class="item">
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-mien-nam/" class="item-img"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/dcmiennam.png" /></a>
						<a href="http://kenhtuyensinh24h.vn/muc_diem_chuan/diem-chuan-khu-vuc-mien-nam/" class="aname">Điểm chuẩn các trường khu vực Miền Nam</a>
						</div>
					</div>
			</div>
			<!-- end diem chuan -->
			<div role="tabpanel" class="tab-pane fade" id="thnn">
			<?php 
				$args = array(
					'post_type' => 'tim_hieu_nganh_nghe',
					'posts_per_page' => 9,
					'tax_query' => array(
						array(
							'taxonomy' => 'muc_tim_hieu_nganh_nghe',
							'field' => 'id',
							'terms' => 2345
							)						
						)
					);
				$loop=new WP_Query($args);
				if(have_posts()):
				while($loop->have_posts()):$loop->the_post();
			?>
				<div class="itemblock">
					<div class="item">
						<a class="item-img" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?></a>
						<a class="aname" href="<?php the_permalink(); ?>"><?php echo short_title('...', 15); ?></a>
					</div>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
			<?php else: _e('Chưa có dữ liệu nào'); ?>
			<?php endif; ?>
			</div>
			<!--end tim hieu nganh nghe -->
			<div role="tabpanel" class="tab-pane fade" id="hdhs">
			<?php 
				$args = array(
					'post_type' => 'thu_tuc_ho_so',
					'posts_per_page' => 9,
					'tax_query' => array(
						array(
							'taxonomy' => 'nhung_dieu_can_biet',
							'field' => 'id',
							'terms' => 1303
							)						
						)
					);
				$loop=new WP_Query($args);
				if(have_posts()):
				while($loop->have_posts()):$loop->the_post();
			?>
				<div class="itemblock">
					<div class="item">
						<a class="item-img" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?></a>
						<a class="aname" href="<?php the_permalink(); ?>"><?php echo short_title('...', 15); ?></a>
					</div>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
			<?php else: _e('Chưa có dữ liệu nào'); ?>
			<?php endif; ?>
			</div>
			<!--end thu tuc ho so -->	
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="news group-area-home">
		<div class="namekn"><h4>Tuyển sinh theo khối ngành</h4></div>
		<div class="kn1 kn">
			<div class="gitem kinhte">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-kinh-te/">
					<i class="fa fa-money" aria-hidden="true"></i>		
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Kinh Tế</p>
						</div>
				</a>
			</div>
			<div class="gitem kythuat">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-ky-thuat/">
					<i class="fa fa-cogs" aria-hidden="true"></i>
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Kỹ Thuật</p>
						</div>
				</a>
			</div>
			<div class="gitem vanhoa">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-van-hoa-nghe-thuat/">
					<i class="vanhoa"></i>
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Văn Hóa</p>
						</div>
				</a>
			</div>
			<div class="gitem supham">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-su-pham/">
					<i class="fa fa-graduation-cap" aria-hidden="true"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Sư Phạm</p>
					</div>
				</a>
			</div>
			<div class="gitem yduoc">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-y-duoc/">
				<i class="yduoc"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Y Dược</p>
					</div>
				</a>
			</div>
			<div class="gitem congan">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-bo-doi-cong-an/">
				<i class="congan"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Công An</p>
					</div>
				</a>
			</div>
		</div>
</div> <!--end group-area-home-->
<div class="clear"></div>
<div class="giaitri tinmoi_header">		
		<div class="tinmoititletop"><a href="">Tiêu điểm nổi bật</a></div>
		<div class="viewgt">
			<?php if(is_active_sidebar('qcgth') && !dynamic_sidebar('qcgth')):endif; ?>
		</div>
</div> <!--end tinmoi_header -->
</div> <!--end home block news-->
<div id="sidebar-home" class="widget-area">
	<?php if(is_active_sidebar('sidebar-alt') && !dynamic_sidebar('sidebar-alt')):endif; ?>
</div> <!--end sidbar home -->
	<?php
}

add_action('genesis_loop','tintucts' );
function tintucts(){
	?>
	<div class="clear"></div>
	<div class="linearea"></div>
	<div class="tints">
		<div class="newsts newsdh">
			<div class="header-ts">
				<h4><a href="http://kenhtuyensinh24h.vn/tin-tuc-he-dai-hoc/">Tin tức Đại Học</a></h4>
			</div>
			<?php 
			$args = new WP_Query('cat=870&showposts=1');
			while($args->have_posts()): $args->the_post();
			?>
			<div class="intro-ts">
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
				<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
			</div>
			<?php endwhile; wp_reset_postdata(); ?>
			<ul>
				<?php 
					$args = new WP_Query('cat=870&showposts=2&offset=1');
					while($args->have_posts()):$args->the_post();
				?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php endwhile; wp_reset_postdata(); ?>
			</ul>
		</div>
		<div class="newsts newscd">
			<div class="header-ts">
				<h4><a href="http://kenhtuyensinh24h.vn/tin-tuc-he-cao-dang/">Tin tức Cao Đẳng</a></h4>
			</div>
				<?php 
				$args = new WP_Query('cat=869&showposts=1');
				while($args->have_posts()): $args->the_post();
				?>
				<div class="intro-ts">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
					<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
				</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<ul>
					<?php 
						$args = new WP_Query('cat=869&showposts=2&offset=1');
						while($args->have_posts()):$args->the_post();
					?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
		</div>
		<div class="newsts newstc">
			<div class="header-ts">
				<h4><a href="http://kenhtuyensinh24h.vn/tin-tuc-he-trung-cap/">Tin tức Trung Cấp</a></h4>
			</div>
				<?php 
				$args = new WP_Query('cat=868&showposts=1');
				while($args->have_posts()): $args->the_post();
				?>
				<div class="intro-ts">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
					<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
				</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<ul>
					<?php 
						$args = new WP_Query('cat=868&showposts=2&offset=1');
						while($args->have_posts()):$args->the_post();
					?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
		</div>
	</div>
	<?php
}