<?php
/* Template Name:teacher-english */
add_action( 'wp_head', 'add_my_style' );
function add_my_style() {
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/giaovien.css">';
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/font-awesome.min.css">';
}
add_action('genesis_loop','content');
function content(){
?>
<div class="col">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="home_slide">
				<?php echo do_shortcode('[rev_slider alias="page-english"]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="main">
			<div class="col-md-5 col-sm-12 col-xs-12">
				<div class="sideBar-infor">
					<div class="video-youtube">
						<iframe width="438" height="315" src="https://www.youtube.com/embed/ZUs-dru_oEc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
					<div class="sidebar-course">
						<h2> Chi tiết khóa học</h2>
						<form>
							<ul>
								<li><span>Giới thiệu khóa học</span>
								
							</li>
							<ul >
								<li><input type="checkbox" name="" value="">Bài:1</li>
							</ul>
							<li><span>Cơ bản</span>
						</li>
						<ul>
							<li><input type="checkbox" name="" value="">Bài:1</li>
							<li><input type="checkbox" name="" value="">Bài:2</li>
							<li><input type="checkbox" name="" value="">Bài:3</li>
							<li><input type="checkbox" name="" value="">Bài:4</li>
							<li><input type="checkbox" name="" value="">Bài:5</li>
							<li><input type="checkbox" name="" value="">Bài:6</li>
							<li><input type="checkbox" name="" value="">Bài:7</li>
							<li><input type="checkbox" name="" value="">Bài:8</li>
						</ul>
					</ul>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-sm-12 col-xs-12">
		<div class="main-content">
			<h2>SẤM SÉT EDUCATION</h2>
			<h1>KHÓA KHỞI ĐỘNG TIẾNG ANH</h1>
			<p>Cô Nguyệt Minh, một giáo viên Tiếng Anh vô cùng đặc biệt với xuất phát điểm là một học sinh chuyên Toán từng trải qua nỗi sợ học Tiếng Anh như bao bạn học sinh khối Tự nhiên khác, khi mà vào mỗi giờ Tiếng Anh, nỗi ác mộng về mệnh đề quan hệ, câu điều kiện, động từ khuyết thiếu,..vv cứ quay vòng vòng trong đầu. </p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Xem thêm...</button>
			<div id="demo" class="collapse see-more">
				Hiểu được những tâm tư của học sinh, cô luôn mong muốn các em có được phương pháp học hiệu quả mà không gây nhàm chán nhất. Những mẫu Ngữ pháp, cấu trúc hay từ vựng tiếng anh khô khan luôn được cô đưa vào vào những ví dụ thực tế vô cùng gần gũi với cuộc sống hằng ngày.<br>
				<h4>
				Khóa học Khởi động Tiếng Anh sẽ được chia ra làm:
				</h4>
				<p class="item-course">
					18 bài giảng theo từng chuyên đề được cô trực tiếp giảng dạy được biên soạn một cách chi tiết và dễ hiểu nhất. Sau mỗi bài giảng cô luôn rút ra những mẹo làm bài nhanh để các em không chỉ có kiến thức nền tốt mà còn có kĩ năng làm bài.
				</p>
				<p class="item-course">
					Hơn 100 bài tập được biên soạn bám sát đề THPT QG kèm đáp án và hướng dẫn chi tiết đối với từng câu
				</p>
				<p class="item-course">
					Tương tác trực tiếp thông qua các buổi Livestream để giải đáp thắc mắc của các bạn học viên đồng thời chỉ ra lỗi sai thường mắc phải trong kì thi.<br>
				</p>
				<p class="no-item">
					Sau khóa học, chỉ cần nắm vững được 70% nội dung kiến thức, các em chắc chắn sẽ nắm chắc trong tay tối thiểu 5 điểm với bài thi Tiếng Anh THPT Quốc gia 2018.
				</p>
			</div>
		</div>
		<div class="more">
			<h2>KNOW MORE</h2>
			<div class="row">
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
				<div class="col-md-6 text-center">
					<div class="study-time">
						<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/12.png" alt="">
						<h3>
						MALLEABLE STUDY TIME
						</h3>
						<P>
							Study material avaliable online 24/7 , Stuty in your free time , no time management issues , perfect balance between work and study time
						</P>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="image-left">
		<img src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/giaovien/2.png" alt="">
	</div>
	<div class="timeline">
		<div class="contain left">
			<div class="content">
				<h2>Câu nói ưa thích</h2>
				<p class="has-item"><i class="fas fa-quote-left"></i>
					You don’t know what you can achieve until you try”. Có nghĩa là: “Chúng ta Không biết những điều chúng ta có thể làm được cho đến khi ta thử làm điều đó<i class="fas fa-quote-right"></i>
				</p>
			</div>
		</div>
		<div class="contain right">
			<div class="content">
				<h2>Câu chuyện của cô</h2>
				<p>Xuất phát điểm là một học sinh chuyên Toán của trường THPT Chuyên Lam Sơn, cô yêu thích các môn học tự nhiên như Toán, Lí, Hóa và cảm thấy Tiếng Anh vô cùng kinh khủng với vô vàn từ mới phải nhớ, vô vàn cấu trúc phải học. Đến tận năm lớp 12, Tiếng Anh vẫn là nỗi ác mộng mỗi khi đến lớp của cô và động lực học duy nhất của cô thời điểm đó là… học để qua tốt nghiệp! </p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo1">XEM THÊM ...</button>
				<div id="demo1" class="collapse">
					<p>
						Cho đến khi cô có cơ hội tiếp xúc với một phương pháp học tập thú vị thông qua một buổi hội thảo do trường tổ chức. Lúc đó, cô đã nhận ra rằng Tiếng anh không hề khó như mình nghĩ mà chỉ vì phương pháp học tập của mình chưa đúng. Với niềm tin đó, cô đã dốc hết quyết tâm để học tiếng anh cô đã tìm tòi thật nhiều mẹo hay để biến những từ mới và cấu trúc khô khan trở nên gần gũi trong cuộc sống hằng ngày. Dần dần, nỗi sợ môn Tiếng Anh đã hoàn toàn được xua tan và thay vào đó là niềm yêu thích, sự háo hức chờ đợi mỗi khi đến tiết học mới. Và chỉ sau 4 tháng với phương pháp “Học đâu, nhớ đó” cô đã tự tin tham dự kì thi THPT Quốc gia với khối thi A1: Toán, Lý, Anh và đậu vào Đại học Ngoại thương - chuyên ngành Kinh tế đối ngoại, với số điểm môn ngoại ngữ nằm ngoài mong đợi.<br>
						Cô đã rút ra câu châm ngôn yêu thích của mình khi cô thử phương pháp học mới và nhận ra rằng mình có thể đạt được những điều mà bản thân chưa bao giờ dám nghĩ đến.
					</p>
				</div>
			</div>
		</div>
		<div class="contain left">
			<div class="content">
				<h2>Phương pháp giảng dạy</h2>
				<p>- Biến hóa từ vựng, cấu trúc khô khan thành những câu chữ biết nói trong cuộc sống để học sinh không chỉ nhớ lâu mà còn cảm thấy Tiếng Anh vô cùng thú vị.</p>
				<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo2">XEM THÊM...</button>
				<div id="demo2" class="collapse">
					<p>
						- “Học đâu, nhớ đó” – Lý thuyết đi đôi với ví dụ và bài tập, đồng thời chỉ ra những lỗi sai hay mắc phải của học sinh trong quá  trình làm bài.
						- Rút ra những mẹo làm bài nhanh sau mỗi bài học để học sinh Không chỉ có kiến thức nền tốt mà còn có kĩ năng làm bài thi siêu đỉnh
						- Chia sẻ câu chuyện về học tập, cuộc sống truyền cảm hứng, những điều thú vị bên ngoài khóa học để các em học sinh Không cảm thấy mệt mỏi hay nhàm chán trong suốt thời gian học.
					</p>
				</div>
			</div>
		</div>
		<div class="contain right">
			<div class="content">
				<h2>Câu chuyện của cô</h2>
				<p>Xuất phát điểm là một học sinh chuyên Toán của trường THPT Chuyên Lam Sơn, cô yêu thích các môn học tự nhiên như Toán, Lí, Hóa và cảm thấy Tiếng Anh vô cùng kinh khủng với vô vàn từ mới phải nhớ, vô vàn cấu trúc phải học. Đến tận năm lớp 12, Tiếng Anh vẫn là nỗi ác mộng mỗi khi đến lớp của cô và động lực học duy nhất của cô thời điểm đó là… học để qua tốt nghiệp! </p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3">XEM THÊM ...</button>
				<div id="demo3" class="collapse">
					<p>
						Cho đến khi cô có cơ hội tiếp xúc với một phương pháp học tập thú vị thông qua một buổi hội thảo do trường tổ chức. Lúc đó, cô đã nhận ra rằng Tiếng anh không hề khó như mình nghĩ mà chỉ vì phương pháp học tập của mình chưa đúng. Với niềm tin đó, cô đã dốc hết quyết tâm để học tiếng anh cô đã tìm tòi thật nhiều mẹo hay để biến những từ mới và cấu trúc khô khan trở nên gần gũi trong cuộc sống hằng ngày. Dần dần, nỗi sợ môn Tiếng Anh đã hoàn toàn được xua tan và thay vào đó là niềm yêu thích, sự háo hức chờ đợi mỗi khi đến tiết học mới. Và chỉ sau 4 tháng với phương pháp “Học đâu, nhớ đó” cô đã tự tin tham dự kì thi THPT Quốc gia với khối thi A1: Toán, Lý, Anh và đậu vào Đại học Ngoại thương - chuyên ngành Kinh tế đối ngoại, với số điểm môn ngoại ngữ nằm ngoài mong đợi.<br>
						Cô đã rút ra câu châm ngôn yêu thích của mình khi cô thử phương pháp học mới và nhận ra rằng mình có thể đạt được những điều mà bản thân chưa bao giờ dám nghĩ đến.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<?php
}
genesis();