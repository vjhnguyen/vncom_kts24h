<?php

remove_action( 'genesis_loop','genesis_do_loop' );
add_action('genesis_loop','mainpage');
add_action('genesis_after_loop','genesis_get_comments_template' );
add_action('genesis_after_loop','bosung');

function mainpage(){
	while(have_posts()):the_post();
	?>
	<div <?php post_class(); ?>>
		<h1 class="title entry-title updated"><?php the_title(); ?></h1>
		<span class="vcard author">
			<span class="fn" style="display:none;"><?php the_author(); ?></span>
		</span>
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
		<div class="likebox">
			<div class="liketext">Bạn thích bài viết này ? </div>
			<div class="likebutton">
				<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>										
				<!-- Đặt thẻ này vào nơi bạn muốn Nút +1 kết xuất. -->
				<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
			</div>
		</div>
	</div>
	<?php
	endwhile; wp_reset_postdata();
	comments_template();
}

function bosung(){
	?>
	<!-- ---------------- Hiện google ---------------- -->
	<div class="slider-home">
		<div class="box4T-bottom-home">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						style="display:block"
						data-ad-format="autorelaxed"
						data-ad-client="ca-pub-1895892504965300"
						data-ad-slot="7385791420"
						data-matched-content-ui-type="image_stacked"
						data-matched-content-rows-num="2"
						data-matched-content-columns-num="8">
					</ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
			</div>
		</div>
	</div>
	<!-- END hiện google -->
	<!---------------------------------------------------------------------------->
	<!-- QUẢNG CÁO CỦA GOOGLE -->
	<div class="quang-cao-google" style="width: 100%;float: left;margin-bottom: 20px;">
		<div class="quang-cao-1" style="width: 49%;float: left;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 4 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="8543525730"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<div class="quang-cao-2" style="width: 49%;float: right;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 7 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="4258010410"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
	<!-- END -->
	<!---------------------------------------------------------------------------->
	<div id="boxNewest" class="slwrap clearfix">
		<b class="sltitle">Có thể bạn đang quan tâm</b>
		<ul>
			<li>
				<a href="https://kenhtuyensinh24h.vn/cac-truong-dai-hoc-va-hoc-vien-khu-vuc-ha-noi/" title="Danh Sách Các Trường Đại Học Và Học Viện Khu Vực TP. Hà Nội">Danh Sách Các Trường Đại Học Và Học Viện Khu Vực TP. Hà Nội</a>
			</li>
			<li>
				<a href="https://kenhtuyensinh24h.vn/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-ho-chi-minh/" title="Các Trường Đại Học Và Học Viện Khu Vực TP. Hồ Chí Minh">Các Trường Đại Học Và Học Viện Khu Vực TP. Hồ Chí Minh</a>
			</li>
			<li>
				<a href="https://kenhtuyensinh24h.vn/danh-sach-cac-truong-dai-hoc-khu-vuc-mien-bac/" title="Danh Sách Các Trường Đại Học Khu Vực Miền Bắc">Danh Sách Các Trường Đại Học Khu Vực Miền Bắc</a>
			</li>
			<li>
				<a href="https://kenhtuyensinh24h.vn/danh-sach-cac-truong-dai-hoc-khu-vuc-mien-nam/" title="Danh Sách Các Trường Đại Học Khu Vực Miền Nam">Danh Sách Các Trường Đại Học Khu Vực Miền Nam</a>
			</li>
			<li>
				<a href="https://kenhtuyensinh24h.vn/cac-truong-dai-hoc-va-hoc-vien-khoi-bo-doi-cong-an/" title="Các Trường Đại Học Và Học Viện Khối Bồ Đội Công An">Các Trường Đại Học Và Học Viện Khối Bồ Đội Công An</a>
			</li>
			<li>
				<a href="https://kenhtuyensinh24h.vn/danh-ba-cac-truong-cao-dang-chuyen-nghiep-toan-quoc/" title="Danh Bạ Các Trường Cao Đẳng Chuyên Nghiệp Toàn Quốc">Danh Bạ Các Trường Cao Đẳng Chuyên Nghiệp Toàn Quốc</a>
			</li>
			<li>
				<a href="https://kenhtuyensinh24h.vn/danh-ba-cac-truong-trung-cap-chuyen-nghiep-toan-quoc/" title="Danh Bạ Các Trường Trung Cấp Chuyên Nghiệp Toàn Quốc">Danh Bạ Các Trường Trung Cấp Chuyên Nghiệp Toàn Quốc</a>
			</li>
			<li>
				<a href="https://kenhtuyensinh24h.vn/danh-sach-cac-truong-dai-hoc-mien-nam/" title="Các Trường Đại Học Miền Nam">Các Trường Đại Học Miền Nam</a>
			</li>
			<li>
				<a href="https://kenhtuyensinh24h.vn/danh-sach-cac-truong-dai-hoc-mien-bac/" title="Các Trường Đại Học Miền Bắc">Các Trường Đại Học Miền Bắc</a>
			</li>
			<li>
				<a href="https://kenhtuyensinh24h.vn/danh-sach-cac-truong-cao-dang-mien-bac/" title="Các Trường Cao Đẳng Miền Bắc">Các Trường Cao Đẳng Miền Bắc</a>
			</li>
		</ul>
	</div>
	<?php
}

genesis();
?>