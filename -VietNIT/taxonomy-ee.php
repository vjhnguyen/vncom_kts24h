<?php
/**
 * Handle the display of taxonomy page.

 * @package  Template
 * @category VIETNIT
 * @developer   VietNIT
 */

// Allow subchild theme modify
if ( file_exists( CAIA_CUSTOM_DIR . '/taxonomy.php' ) )
{
	require( CAIA_CUSTOM_DIR . '/taxonomy.php' );
}


genesis();