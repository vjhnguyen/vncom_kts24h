<?php
add_action( 'wp_head', 'add_my_style' );
function add_my_style() {
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/nguyenthang-monhoa.css">';
}
add_action('genesis_loop','content_hoahoc');
function content_hoahoc()
{
?>
<div class="container">
	<div class="row">
		<div class="img_bg">
		</div>
		<div>
			<img class="img_mon" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/images-thang/a.png"/>
		</div>
		<div class="ten_mon">
			<h1>MÔN TIẾNG ANH</h1>
			<h5>SẤM SÉT EDUCATION</h5>
		</div>
		<div class="phu"></div>
		<div class="header_content">
			<div class="row main">
				<div class="col-md-12 title_mon">
					<div class="row logo_content">
						<div class="col-md-5 ctdt">
							<h2>CHƯƠNG TRÌNH HỌC</h2>
						</div>
						<div class="col-md-7 mon">
							<h2>MÔN TIẾNG ANH</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 mota">
							<h5>Học trực tuyến bởi Công ty CP giáo dục và đào tạo SamSet</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ndmota">
					<p>
						Các em học sinh chắc chắn đều hiểu được tầm quan trọng của môn Tiếng Anh. Tiếng Anh là một môn thi bắt buộc trong kỳ thi THPT QG và năm 2018, cấu trúc đề Tiếng Anh đã thay đổi khá nhiều so với các năm trước.<br>
						Để hỗ trợ những bạn học sinh mất gốc, hổng kiến thức Chuyên trang thông tin tuyển sinh toàn quốc Kenhtuyensinh24h kết hợp với SamSet Education khởi động chương trình tiếp sức mùa thi 2018. Chương trình gồm 3 khóa học: Khởi động – Tăng tốc – Về đích.
					</p>
				</div>
			</div>
		</div>
		<div class="ctts">
			<h3> LỰA CHỌN KHÓA HỌC</h3>
			<div class="ctts_left">
				<button class="tablinks" onclick="openCity(event, 'khoaKD')" id="defaultOpen">Khóa Khởi Động</button></br></br>
				<button class="tablinks" onclick="openCity(event, 'khoaCT')">Khóa Tăng Tốc</button></br></br>
				<button class="tablinks" onclick="openCity(event, 'khoa')">Khóa Về Đích</button>
			</div>
		</div>
		<div id="khoaKD" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA KHỞI ĐỘNG</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>Khóa Khởi Động môn Tiếng Anh được thiết kế phù hợp cho các bạn mất gốc Tiếng Anh,  dẫn đến sợ học Tiếng Anh, với mục tiêu hệ thống lại một cách đầy đủ, chi tiết kiến thức trọng tâm từ ngữ âm, từ vựng cho đến ngữ pháp; cùng với các dạng bài tập bám sát ma trận đề thi của Bộ; đảm bảo các em có thể chắc chắn ghi điểm những câu cơ bản trong đề thi. Đồng thời, tạo nền tảng cho các em theo học các khóa luyện thi sâu hơn để nâng mức điểm bài thi của mình.</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>Cập nhật...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>Cập nhật...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Tham gia khóa học, các em sẽ nắm vững được các kiến thức ngữ pháp căn bản như hiểu được hoàn toàn 12 thì trong Tiếng Anh và các trường hợp áp dụng, các cấu trúc ngữ pháp hay được ra trong đề thi, giúp các em nắm được các cấu trúc để hình thành một câu đúng.<br>
							- Ngoài ra, các em cũng được cung cấp vốn từ vựng cơ bản đi kèm với quy tắc phát âm quốc tế, được thiết kế chia ra theo từng chủ đề quen thuộc trong đời sống hằng ngày, giúp các em có thể ghi điểm những câu hỏi ngắn ở mức độ cơ bản trong đề thi như trọng âm, phát âm, giao tiếp, viết lại câu… đảm bảo các em có thể đạt được tối thiểu 4 điểm trong bài thi của mình.</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoaCT" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA TĂNG TỐC</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Tăng Tốc môn Tiếng Anh được thiết kế phù hợp cho các bạn học sinh đã nắm được các kiến thức cơ bản cần thiết trong chương trình thi THPT nhưng chưa có nhiều kinh nghiệm làm bài tập, luyện đề.<br>
								- Nếu như Khóa Khởi Động là bước đệm cung cấp cho học sinh hành trang kiến thức thiết yếu, thì ở Khóa Tăng Tốc, học sinh sẽ tập trung vận dụng hết những kiến thức đó vào giải các dạng bài tập và luyện đề. <br>
								- Qua từng đề thi, các em sẽ được tích lũy thêm những cấu trúc ngữ pháp và từ vựng mới ở mức độ khó hơn, giúp các em nâng mức điểm cho bài thi của mình.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Dần dần qua từng đề ôn luyện, các em không những được củng cố và luyện tập những kiến thức căn bản mà còn được tiếp cận với những cấu trúc ngữ pháp và từ vựng mới ở mức độ khó hơn, được giải thích một cách chi tiết, súc tích, giúp các em sẽ nắm bắt ngay được kiến thức và hiểu cách áp dụng. <br>
								- Bên cạnh đó, với lượng kiến thức tích lũy được nhiều lên qua từng đề, các em sẽ làm được những dạng câu hỏi khó hơn như dạng tìm lỗi sai, từ đồng nghĩa trái nghĩa, điền từ hay đọc hiểu, qua đó, đảm bảo các em có thể đạt được từ 5 – 6 điểm trong bài thi của mình.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoa" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA VỀ ĐÍCH</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Về Đích môn Tiếng Anh được thiết kế với mục đích tổng ôn và hệ thống hóa kiến thức trọng tâm cho các bạn học sinh đã nắm vững kiến thức và đã có thời gian luyện tập với các dạng bài tập.<br>
								- Song song với đó, Khóa Về Đích môn Tiếng Anh giúp học sinh hoàn thiện những bước chạy nước rút cuối cùng, khắc phục những lỗi sai thường gặp, thuần thục các phương pháp, kỹ năng và mẹo làm bài và biết cách phân bổ thời gian hợp lý.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Với các dạng bài tập và kiến thức trọng tâm, sát nhất với cấu trúc đề thi của Bộ, học sinh sẽ được rèn luyện nhuần nhuyễn phương pháp, mẹo và kỹ năng phân bổ thời gian làm đề, được chỉ ra những lỗi sai thường gặp và cách phòng tránh; đảm bảo cho các bạn có thể ghi điểm chắc chắn những câu ở mức độ dễ như phát âm, trọng âm, viết lại câu... trong thời gian ngắn nhất, dành thời gian còn lại cho những câu khó hơn như tìm lỗi sai, điền từ và đọc hiểu để nâng cao mức điểm.<br>
								- Qua khóa Về Đích, các em sẽ cảm nhận sự tiến bộ rõ rệt, và hoàn toàn tự tin có thể đạt được tối thiểu 6 điểm cho bài thi của mình.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
	</div>
	
</div>
<script>
	function openCity(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tab");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
<?php
}
genesis();