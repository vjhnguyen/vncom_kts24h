<?php
/*teplate dai hoc ha noi*/
add_action( 'wp_head', 'add_my_style' );
function add_my_style() {
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/luyenthi.css">';
}
remove_action( 'genesis_loop', 'genesis_do_loop' );
remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
add_action('genesis_loop','luyenthi');
add_image_size( 'luyen-thi', 245, 138, true );
function luyenthi(){
?>
<div class="home-luyenthi">
	<p><span style="color: red;"><a href="https://kenhtuyensinh24h.vn/"><i class="fa fa-home" aria-hidden="true"></i> Trang chủ</a></span> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="https://kenhtuyensinh24h.vn/luyen-thi/">Luyện thi</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> Chương trình tiếp sức mùa thi</p>
</div>
<div class="cac-buoc-luyen-thi">
	<ul>
		<li>
			<div class="left">
				<i class="fa fa-hourglass-start" aria-hidden="true"></i>
			</div>
			<div class="right">
				<h2>KHỞI ĐỘNG</h2>
				<div class="time-khoidong time-cacbuoc">
					12.023
				</div>
				<p>Ôn kiến thức cơ bản</p>
			</div>
		</li>
		<li>
			<div class="left">
				<i class="fa fa-rocket" aria-hidden="true"></i>
			</div>
			<div class="right">
				<h2>TĂNG TỐC</h2>
				<div class="time-khoidong time-cacbuoc">
					12.023
				</div>
				<p>Luyện Đề và Giải Đề</p>
			</div>
		</li>
		<li>
			<div class="left">
				<i class="fa fa-graduation-cap" aria-hidden="true"></i>
			</div>
			<div class="right">
				<h2>VỀ ĐÍCH</h2>
				<div class="time-khoidong time-cacbuoc">
					12.023
				</div>
				<p>Tổng Ôn Kiến Thức</p>
			</div>
		</li>
	</ul>
</div>
<div class="thi-thpt-quoc-gia">
	<div class="content">
		<div class="time-thpt">
			<?php
				echo do_shortcode('[wpdevart_countdown text_for_day="Ngày" text_for_hour="Giờ" text_for_minut="Phút" text_for_second="Giây" countdown_end_type="time" end_date="31-01-2018 23:59" start_time="1517390471" end_time="141,8,40" action_end_time="hide" content_position="center" top_ditance="10" bottom_distance="10" ][/wpdevart_countdown]');
			?>
		</div>
	</div>
</div>
<div class="vi-sao">
	<h2>
	<div class="td-icon">
		<i class="fa fa-video-camera" aria-hidden="true"></i>
	</div>
	LUYỆN THI CÙNG <span style="color: #f58b10">KENHTUYENSINH24H.VN</span>
	</h2>
	<div class="wapper-visao">
		<div class="videos-visao">
			<iframe width="100%" height="225" src="https://www.youtube.com/embed/pW7pvUIXoDs" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
		</div>
		<div class="list-noibat">
			<ul>
				<li><a href="#">Học với giáo viên giỏi uy tín</a></li>
				<li><a href="#">Bài giảng chất lượng và bài tập, đề thi phong phú</a></li>
				<li><a href="#">Tiện lợi vì học ở bất cứ nơi đâu và bất cứ khi nào</a></li>
				<li><a href="#">Hỏi đáp với thầy cô khi không hiểu bài dễ dàng</a></li>
				<li><a href="#">Học phí tiết kiệm dùng cho cả 1 năm học</a></li>
			</ul>
			<a href="#"><div class="nhan-tu-van"><i class="fa fa-volume-control-phone" aria-hidden="true"></i> Nhận Tư vấn</div></a>
			<a href="#"><div class="tham-gia-ngay"><i class="fa fa-user-plus" aria-hidden="true"></i> Tham Gia Ngay</div></a>
		</div>
	</div>
</div>
<div class="menu-luyenthi">
	<h2>
	<div class="td-icon">
		<i class="fa fa-list-alt" aria-hidden="true"></i>
	</div>
	CÁC BỘ MÔN
	</h2>
	<div class="cac-mon-hoc">
		<?php wp_nav_menu( array('menu' => 'luyenthi-menu') ); ?>
	</div>
</div>
<div class="giao-vien">
	<h2>
	<div class="td-icon">
		<i class="fa fa-users" aria-hidden="true"></i>
	</div>
	CÁC GIÁO VIÊN TIÊU BIỂU
	</h2>
	<ul class="giao-vien-ul">
		<?php
		$vnkings = new WP_Query(array(
		'post_type'=>'giao-vien-tieu-bieu',
		'cat' => 9654,
		'orderby' => 'ID',
		'order' => 'DESC',
		'posts_per_page'=> 6));
		?>
		<?php $i=1; while ($vnkings->have_posts()) : $vnkings->the_post(); ?>
		<?php if($i==1){ ?>
		<li class="giao-vien-li">
			<div class="col">
				<div class="images">
					<a title="" href="#" class="image">
						<?php the_post_thumbnail("luyen-thi",array( "title" => get_the_title(),"alt" => get_the_title() ));?>
					</a>
					<div class="middle">
						<a title="" href="#"></a>
						<ul class="list-group hover_img">
							<a title="" href="#"></a>
							<center>
							<a title="" href="#"></a>
							<a href="#" class="btn btn-primary">Xem chi tiết</a>
							</center>
						</ul>
					</div>
				</div>
				<div class="title">
					<a title="" href="#"><b><?php the_excerpt() ;?></b></a>
				</div>
				<div class="star">
					<div class="number">
						<a href="<?php the_field('link_page_giao_viên'); ?>"><?php the_title() ;?></a>
					</div>
					<div class="text person">
						<ul class="item-list">
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
						</ul>
					</div>
				</div>
				<div class="price">
					<span class="sell-price">599,000<sup>đ</sup></span>
					<span class="old-price">700,000<sup>đ</sup></span>
					<span class="discount person">- 14%</span>
				</div>
			</div>
		</li>
		<?php } else { ?>
		<li>
			<div class="col">
				<div class="images">
					<a title="" href="#" class="image">
						<?php the_post_thumbnail("luyen-thi",array( "title" => get_the_title(),"alt" => get_the_title() ));?>
					</a>
					<div class="middle">
						<a title="" href="#"></a>
						<ul class="list-group hover_img">
							<a title="" href="#"></a>
							<center>
							<a title="" href="#"></a>
							<a href="#" class="btn btn-primary">Xem chi tiết</a>
							</center>
						</ul>
					</div>
				</div>
				<div class="title">
					<a title="" href="#"><b><?php the_excerpt() ;?></b></a>
				</div>
				<div class="star">
					<div class="number">
						<a href="<?php the_field('link_page_giao_viên'); ?>"><?php the_title() ;?></a>
					</div>
					<div class="text person">
						<ul class="item-list">
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li><i class="fa fa-star" aria-hidden="true"></i></li>
							<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
						</ul>
					</div>
				</div>
				<div class="price">
					<span class="sell-price">599,000<sup>đ</sup></span>
					<span class="old-price">700,000<sup>đ</sup></span>
					<span class="discount person">- 14%</span>
				</div>
			</div>
		</li>
		<?php } ?>
		<?php $i++; endwhile ; wp_reset_query() ;?>
	</ul>
</div>
<div class="slider-comment">
	<h2>
	CẢM NHẬN HỌC VIÊN
	</h2>
	<div id="carousel-id" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carousel-id" data-slide-to="0" class=""></li>
			<li data-target="#carousel-id" data-slide-to="1" class=""></li>
			<li data-target="#carousel-id" data-slide-to="2" class="active"></li>
		</ol>
		<div class="carousel-inner">
			<div class="item">
				<img alt="First slide" src="http://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/y-kien.jpg">
				<div class="container">
					<div class="carousel-caption">
						<h1>Example headline.</h1>
						<p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
					</div>
				</div>
			</div>
			<div class="item">
				<img alt="Second slide" src="http://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/y-kien.jpg">
				<div class="container">
					<div class="carousel-caption">
						<h1>Another example headline.</h1>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
					</div>
				</div>
			</div>
			<div class="item active">
				<img alt="Third slide" src="http://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/y-kien.jpg">
				<div class="container">
					<div class="carousel-caption">
						<h1>One more for good measure.</h1>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="login_post">
	<h3>
	Đăng ký ngay để được tư vấn MIỄN PHÍ về lực học  từ các giáo viên
	</h3>
	<p>đừng chần chừ khi mùa thi đã sắp tới gần</p>
</div>
<div class="form-register">
	<div class="form-left">
		<h2> Đăng ký </h2>
		<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
	</div>
	<div class="form-right">
		<img src="http://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/luyen-thi/1-01.png" alt="">
		<a href="#" title="" class="button btn btn-primary">Đăng ký</a>
	</div>
</div>
<?php
}
genesis();