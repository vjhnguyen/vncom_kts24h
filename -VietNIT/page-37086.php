<?php
add_action( 'wp_head', 'add_my_style' );
function add_my_style() {
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/nguyenthang-monhoa.css">';
}
add_action('genesis_loop','content_hoahoc');
function content_hoahoc()
{
?>
<div class="container">
	<div class="row">
		<div class="img_bg">
		</div>
		<div>
			<img class="img_mon" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/images-thang/a.png"/>
		</div>
		<div class="ten_mon">
			<h1>MÔN NGỮ VĂN</h1>
			<h5>SAMSET EDUCATION</h5>
		</div>
		<div class="phu"></div>
		<div class="header_content">
			<div class="row main">
				<div class="col-md-12 title_mon">
					<div class="row logo_content">
						<div class="col-md-5 ctdt">
							<h2>CHƯƠNG TRÌNH HỌC</h2>
						</div>
						<div class="col-md-7 mon">
							<h2>MÔN NGỮ VĂN</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 mota">
							<h5>Học trực tuyến bởi Công ty CP giáo dục và đào tạo SamSet</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ndmota">
					<p>
						- Năm 2018, môn Văn vẫn được giữ nguyên là môn thi với hình thức tự luận. Tuy nhiên tính chất đặc thù của môn văn với lượng kiến thức rộng, khó ôn luyện khiến nhiều thí sinh lo lắng, băn khoăn. <br>
						- Để hỗ trợ những bạn học sinh mất gốc, hổng kiến thức Chuyên trang thông tin tuyển sinh toàn quốc Kenhtuyensinh24h kết hợp với SamSet Education khởi động chương trình tiếp sức mùa thi 2018. Chương trình gồm 3 khóa học: Khởi động – Tăng tốc – Về đích.
					</p>
				</div>
			</div>
		</div>
		<div class="ctts">
			<h3> LỰA CHỌN KHÓA HỌC</h3>
			<div class="ctts_left">
				<button class="tablinks" onclick="openCity(event, 'khoaKD')" id="defaultOpen">Khóa Khởi Động</button></br></br>
				<button class="tablinks" onclick="openCity(event, 'khoaCT')">Khóa Tăng Tốc</button></br></br>
				<button class="tablinks" onclick="openCity(event, 'khoa')">Khóa về đích</button>
			</div>
		</div>
		<div id="khoaKD" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA KHỞI ĐỘNG</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Khởi Động là bước chân đầu tiên dành cho những bạn học sinh cuối cấp mới bắt tay vào chuẩn bị ôn luyện môn Văn cho kỳ thi THPT quốc gia. Đây là khóa học trang bị cho các em những kiến thức nền tảng về môn học, đặc biệt với môn văn có lượng kiến thức rất rộng và được đánh giá là khá nặng với các em có học lực trung bình. <br>
								- Để phù hợp với học sinh, Samset Education thiết kế chương trình ôn luyện môn văn với lượng kiến thức vừa đủ không nhồi nhét phù hợp với các bạn học sinh mất gốc, bị hổng kiến thức. Tạo tiền đề để tiếp tục ôn luyện sâu hơn để chinh phục kỳ thi THPT quốc gia với điểm số cao.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>Cập nhật...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>Cập nhật...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>-	Được lấy lại toàn bộ kiến thức cơ bản, nền tảng của môn văn <br>
								-	Được hệ thống lại toàn bộ kiến thức môn văn tạo tiền đề ôn luyện sau khi kết thúc khóa học. <br>
								-	Học sinh sẽ nắm được các phân tích đề thi để từ đó đưa ra các luận điểm chính, luận cứ… cách trình bày một bài thi cơ bản theo tiêu chuẩn đề thi chính thức.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>Cập nhật...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>Cập nhật...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoaCT" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA TĂNG TỐC</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Tiếp nối Khởi động, Khóa Tăng Tốc được thiết kế dành cho những bạn học sinh đã nắm được những kiến thức cơ bản trong chương trình THPT quốc gia nhưng chưa có kỹ năng luyện đề, chưa có kinh nghiệm giải nhiều bài tập.<br>
								- Để có thể chinh phục đề thi THPT quốc gia với môn Văn “khó nhằn”, thì khóa Tăng Tốc quan trọng hơn cả. Khóa học cung cấp chiến lược ôn thi môn ngữ văn phù hợp với cả những thí sinh cần lấy điểm xét tuyển đại học và để xét tốt nghiệp.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>Các em học sinh ôn luyện sẽ được tiếp cận kiến thức từ dễ đến khó, các dạng bài tập bám sát cấu trúc đề thi theo các dạng bài thương gặp, cụ thể: <br>
								Đọc – hiểu văn bản: <br>
								-	Phong cách ngôn ngữ <br>
								-	Giải thích từ khóa <br>
								-	Biện pháp tu từ, phương thức trần thuật <br>
								-	Tác dụng của phép tu từ  <br>
								-	Phân tích cảm xúc của tác giả <br>
								-	Cảm nhận nội dụng, tư tưởng <br>
								-	Bài học qua đoạn trích…. <br>
								Nghị luận xã hội <br>
								-	Viết đoạn văn nghị luận về tư tưởng, đạo lý <br>
								-	Viết đoạn văn nghị luận về một hiện tượng đời sống <br>
								Nghị luận văn học <br>
								-	Dạng bài phân tích, cảm nhận văn học <br>
								-	Dạng bài bình luận văn học <br>
								-	Dạng bài so sánh văn học <br>
								Đồng thời, các em cũng được chia sẻ những bí quyết, kỹ năng và mẹo làm bài. Từ đó trau dồi thêm bản lĩnh thi cử và tự tin bước vào giai đoạn về đích – giai đoạn nước rút chuẩn bị cho kỳ thi gần sát.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoa" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA VỀ ĐÍCH</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Về Đích được bắt đầu trong những ngày ôn luyện cuối cùng, cận kề những ngày thi đảm bảo cho các em nắm được lượng kiến thức “nóng hổi” bởi những bài giảng trọng tâm, đặc biệt học sinh sẽ nắm được cách phân bổ thời gian làm bài thi hợp lý, đạt được kết quả cao nhất. <br>
								- Đây là khóa học dành cho các em đã nắm được những kiến thức cơ bản và đã có kinh nghiệm luyện đề, tiếp xúc với nhiều dạng bài tập áp sát đề thi, áp dụng những kỹ năng và mẹo làm bài vào những đề thi thử nhằm khắc phục những lỗi sai thường gặp, hoàn thiện hành trang chinh phục kỳ thi THPT quốc gia.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Khóa Về Đích là giai đoạn tập trung luyện đề, học sinh được trang bị kiến thức trọng tâm, áp sát để thi. Với ngân hàng đề thi phong phú và da đạng, học sinh được trau dồi thêm kỹ năng phân bổ thời gian, tập trung những dạng bài dễ ăn điểm nhất. <br>
								- Ngoài ra, học sinh sau khi kết thúc khóa học còn được hỗ trợ tư vấn chọn những trường đại học, cao đẳng phù hợp với khả năng, sức học của mình. Hỗ trợ tư vấn các bước đăng ký thủ tục xét tuyển.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function openCity(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tab");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
<?php
}
genesis();