<?php
add_action( 'wp_head', 'add_my_style' );
function add_my_style() {
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/nguyenthang-monhoa.css">';
}
add_action('genesis_loop','content_hoahoc');
function content_hoahoc()
{
?>
<div class="container">
	<div class="row">
		<div class="img_bg">
		</div>
		<div>
			<img class="img_mon" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/images-thang/a.png"/>
		</div>
		<div class="ten_mon">
			<h1>MÔN HÓA HỌC</h1>
			<h5>SAMSET EDUCATION</h5>
		</div>
		<div class="phu"></div>
		<div class="header_content">
			<div class="row main">
				<div class="col-md-12 title_mon">
					<div class="row logo_content">
						<div class="col-md-5 ctdt">
							<h2>CHƯƠNG TRÌNH HỌC</h2>
						</div>
						<div class="col-md-7 mon">
							<h2>MÔN HÓA HỌC</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 mota">
							<h5>Học trực tuyến bởi Công ty CP giáo dục và đào tạo SamSet</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ndmota">
					<p>
						Nghĩ đến Hóa Học, chúng ta đều liên tưởng ngay đến hệ thống ký hiệu và con số phức tạp, rắc rối. Với những bạn đã bỏ lỡ những kiến thức từ các lớp dưới thì việc học môn Hóa càng nặng và khiến các bạn sợ Hóa hơn.<br>
						Hiểu được những khó khăn và những điều học sinh cần nắm vững, kenhtuyensinh24h kết hợp với Samset Education khởi động Chương trình Hỗ trợ luyện thi THPT Quốc gia cho môn Hóa Học, được chia làm 3 giai đoạn  tương ứng với 3 khóa học theo trình tự từ cơ bản đến nâng cao: Khởi Động – Tăng Tốc và Về Đích.
					</p>
				</div>
			</div>
		</div>
		<div class="ctts">
			<h3> LỰA CHỌN KHÓA HỌC</h3>
			<div class="ctts_left">
				<button class="tablinks" onclick="openCity(event, 'khoaKD')" id="defaultOpen">Khóa Khởi Động</button></br></br>
				<button class="tablinks" onclick="openCity(event, 'khoaCT')">Khóa Tăng Tốc</button></br></br>
				<button class="tablinks" onclick="openCity(event, 'khoa')">Khóa Về Đích</button>
			</div>
		</div>
		<div id="khoaKD" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA KHỞI ĐỘNG</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Khởi Động môn Hóa Học được thiết kế dành cho các bạn mất gốc, với mục tiêu giúp cho học sinh hiểu được bản chất những kiến thức Hóa Học cơ bản và trọng tâm được ra trong đề thi THPT. <br>
								- Để phù hợp với học sinh, Khóa Khởi Động với phương châm ôn luyện kiến thức vừa đủ, không nhồi nhét, chỉ tập trung vào những kiến thức căn bản nhất, với cách truyền đạt suc tích, dễ hiểu, giúp các em lấy lại được “gốc” và tự tin cho những bước nhảy vọt trong kỳ thi sắp tới.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Dưới phương pháp giảng dạy dễ hiểu, cô đọng, sau mỗi bài học lý thuyết kèm theo ví dụ minh họa, các em sẽ nắm bắt và biết cách áp dụng được các kiến thức căn bản được đưa vào kỳ thi THPT bao gồm cả Hóa học Vô cơ và Hữu cơ. <br>- Điều đặc biệt là, khóa học sẽ tập trung lớn vào phần lý thuyết - là phần dễ ăn điểm và chiếm hơn 50% câu hỏi trong đề thi. Chỉ khi nắm vững lý thuyết môn Hóa, các em mới làm được bài tập. <br>
								- Sau khóa học, các em hoàn toàn tự tin làm được những câu hỏi ở mức độ nhận biết – thông hiểu chiếm 50% (20 câu) trong đề thi chính thức.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>Cập nhật...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>Cập nhật...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoaCT" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA TĂNG TỐC</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Tiếp nối Khởi Động, Khóa Tăng Tốc môn Hóa Học được thiết kế phù hợp cho các bạn học sinh đã nắm được các kiến thức cơ bản cần thiết trong chương trình thi THPT nhưng chưa có kỹ năng luyện đề, chưa có kinh nghiệm giải nhiều bài tập. <br>
								- Nếu như Khóa Khởi Động là bước đệm cung cấp cho học sinh hành trang kiến thức thiết yếu, thì Tăng Tốc lại là bước không thể thiếu, giúp học sinh tập trung vận dụng hết những kiến thức đó vào giải các dạng bài tập và luyện đề.<br>-  Đồng thời qua từng đề thi, các em sẽ được tiếp xúc với những dạng bài nâng cao hơn, tích lũy được thêm các kiến thức và phương pháp giải các bài tập khó hơn, giúp nâng mức điểm cho bài thi của mình.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Qua từng đề thi ôn luyện, các em không những được củng cố và luyện tập thuần thục những kiến thức căn bản và bài tập tương ứng mà còn được tiếp cận với những dạng bài tập và kiến thức sâu hơn, được giải thích một cách chi tiết, súc tích, giúp các em sẽ nắm bắt ngay được kiến thức và hiểu cách áp dụng. <br>
								- Với lượng kiến thức tích lũy được nhiều lên qua từng đề như vậy, giúp các em có thể bắt đầu làm được những câu ở mức độ vận dụng chiếm 16 câu, đạt được tối thiểu 6 điểm trong bài thi của mình.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoa" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA VỀ ĐÍCH</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Về Đích môn Hóa Học được thiết kế với mục đích tổng ôn và hệ thống hóa kiến thức trọng tâm cho các bạn học sinh đã nắm vững kiến thức và đã có thời gian luyện tập với các dạng câu hỏi và bài tập. <br>
								- Song song với đó, Khóa Về Đích giúp học sinh hoàn thiện những bước chạy nước rút cuối cùng, khắc phục những lỗi sai thường gặp, thuần thục các phương pháp, kỹ năng và mẹo làm bài và biết cách phân bổ thời gian làm bài hợp lý. <br>
								- Đặc biệt các bạn học sinh sau khi kết thúc khóa học sẽ được hỗ trợ tư vấn lựa chọn các trường đại học, cao đẳng phù hợp với năng lực, sức học của mình.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Với các dạng bài tập và kiến thức trọng tâm, sát nhất với cấu trúc đề thi của Bộ, học sinh sẽ được rèn luyện nhuần nhuyễn phương pháp, mẹo và kỹ năng phân bổ thời gian làm đề; đảm bảo cho các bạn có thể ghi điểm và trong thời gian nhanh nhất cho những câu ở mức độ dễ ăn điểm, dành thời gian còn lại cho những câu khó hơn để nâng cao mức điểm. <br>
								- Ngoài ra, trong quá trình tổng ôn, các em cũng tích lũy được những kiến thức nâng cao hơn qua từng lần rèn luyện với ngân hàng đề thi thử đa dạng và phong phú. Qua khóa Về Đích, các em sẽ cảm nhận sự tiến bộ rõ rệt, và hoàn toàn tự tin có thể đạt được tối thiểu 6 - 7 điểm cho bài thi của mình.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
	</div>
	
</div>
<script>
	function openCity(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tab");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
<?php
}
genesis();