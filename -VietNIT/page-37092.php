<?php
add_action( 'wp_head', 'add_my_style' );
function add_my_style() {
	echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/nguyenthang-monhoa.css">';
}
add_action('genesis_loop','content_hoahoc');
function content_hoahoc()
{
?>
<div class="container">
	<div class="row">
		<div class="img_bg">
		</div>
		<div>
			<img class="img_mon" src="https://kenhtuyensinh24h.vn/wp-content/themes/VietNIT/images/images-thang/a.png"/>
		</div>
		<div class="ten_mon">
			<h1>MÔN VẬT LÝ</h1>
			<h5>SAMSET EDUCATION</h5>
		</div>
		<div class="phu"></div>
		<div class="header_content">
			<div class="row main">
				<div class="col-md-12 title_mon">
					<div class="row logo_content">
						<div class="col-md-5 ctdt">
							<h2>CHƯƠNG TRÌNH HỌC</h2>
						</div>
						<div class="col-md-7 mon">
							<h2>MÔN VẬT LÝ</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 mota">
							<h5>Học trực tuyến bởi Công ty CP giáo dục và đào tạo SamSet</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ndmota">
					<p>
						- Vật Lý được coi là một trong những môn học khó trong chương trình học THPT bởi môn học này yêu cầu khả năng tư duy và óc phân tích. Từ năm 2017, theo quy định mới của Bộ Giáo dục và Đào tạo, thời gian làm bài thi môn Vật Lý chỉ còn 50 phút với 40 câu hỏi trắc nghiệm tương ứng chỉ 1,25 phút cho mỗi câu hỏi do vậy môn Lý càng trở thành “cơn ác mộng” đối với nhiều học sinh.<br>- Để giúp đỡ các em học sinh vượt qua được những khó khăn của môn Lý, kenhtuyensinh24h kết hợp với Samset Education khởi động Chương trình Hỗ trợ luyện thi THPT Quốc gia cho môn Vật Lý, được chia làm 3 giai đoạn  tương ứng với 3 khóa học theo trình tự từ cơ bản đến nâng cao: Khởi Động – Tăng Tốc và Về Đích.
					</p>
				</div>
			</div>
		</div>
		<div class="ctts">
			<h3> LỰA CHỌN KHÓA HỌC</h3>
			<div class="ctts_left">
				<button class="tablinks" onclick="openCity(event, 'khoaKD')" id="defaultOpen">Khóa Khởi Động</button></br></br>
				<button class="tablinks" onclick="openCity(event, 'khoaCT')">Khóa Tăng Tốc</button></br></br>
				<button class="tablinks" onclick="openCity(event, 'khoa')">Khóa Về Đích</button>
			</div>
		</div>
		<div id="khoaKD" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA KHỞI ĐỘNG</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Khởi Động môn Vật Lý được thiết kế dành cho các bạn mất gốc, với mục tiêu giúp cho học sinh hiểu được bản chất những kiến thức Vật lý cơ bản và trọng tâm được ra trong đề thi THPT. <br> - Không học dàn trải hay ôm đồm quá nhiều kiến thức, Khóa Khởi Động chỉ tập trung vào những kiến thức căn bản nhất, với cách truyền đạt đơn giản, dễ hiểu, giúp các em lấy lại được “gốc” và có những bước nhảy vọt trong kỳ thi sắp tới.</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Dưới phương pháp giảng dạy dễ hiểu, cô đọng súc tích của các thầy cô, sau mỗi bài học lý thuyết kèm theo ví dụ minh họa, các em sẽ nắm bắt và biết cách áp dụng được các kiến thức căn bản được đưa vào kỳ thi THPT bao gồm Dao động cơ, Sóng cơ, Dòng điện xoay chiều, Dao động và Sóng điện từ, Sóng ánh sáng… <br> - Từ đó, các em hoàn toàn tự tin làm được những câu hỏi và bài tập ở mức độ dễ chiếm 60% (24 câu) đề thi THPT Quốc gia môn Vật Lý.</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>Cập nhật...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoaCT" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA TĂNG TỐC</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Tăng Tốc môn Vật Lý được thiết kế phù hợp cho các bạn học sinh đã nắm được các kiến thức cơ bản cần thiết trong chương trình thi THPT nhưng chưa tiếp cận nhiều với các dạng bài tập .<br>
								- Nếu như Khóa Khởi Động là bước đệm cung cấp cho học sinh hành trang kiến thức thiết yếu, thì ở Khóa Tăng Tốc, học sinh sẽ tập trung vận dụng hết những kiến thức đó vào giải các dạng bài tập và luyện đề. <br>- Đồng thời qua từng đề thi đó, các em sẽ được làm quen với những dạng bài nâng cao hơn so với những kiến thức căn bản các em đang có. Tích lũy được thêm các kiến thức và phương pháp giải các bài tập khó hơn đó, các em nâng mức điểm cho bài thi của mình.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Qua từng đề thi ôn luyện, các em không những được củng cố và luyện tập thuần thục những kiến thức căn bản và bài tập tương ứng mà còn được tiếp cận với những dạng bài tập và kiến thức sâu hơn, được giải thích một cách chi tiết, súc tích, giúp các em sẽ nắm bắt ngay được kiến thức và hiểu cách áp dụng. <br>
								- Với lượng kiến thức tích lũy được nhiều lên qua từng đề như vậy, giúp các em có thể bắt đầu làm được những câu ở mức độ vận dụng chiếm 30% (12 câu) đạt được tối thiểu 6 điểm trong bài thi của mình.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
		<div id="khoa" class="tab">
			<div class="cndt">
				<div style="padding-top:55px;">
					<h1>KHÓA VỀ ĐÍCH</h1>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giới thiệu khóa học</h4>
							<p>- Khóa Về Đích môn Vật Lý được thiết kế với mục đích tổng ôn và hệ thống hóa kiến thức trọng tâm cho các bạn học sinh đã nắm vững kiến thức và đã có kinh nghiệm luyện đề <br>
								- Song song với đó, Khóa Về Đích giúp học sinh hoàn thiện những bước chạy nước rút cuối cùng, khắc phục những lỗi sai thường gặp, thuần thục các phương pháp, kỹ năng và mẹo làm bài và biết cách phân bổ thời gian làm bài hợp lý.
							</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Đang cập nhật</h4>
							<p>...</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Nội dụng khóa học</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Kết quả đạt được</h4>
							<p>- Với các dạng bài tập và kiến thức trọng tâm, sát nhất với cấu trúc đề thi của Bộ, học sinh sẽ được rèn luyện nhuần nhuyễn phương pháp, mẹo và kỹ năng phân bổ thời gian làm đề; đảm bảo cho các bạn có thể ghi điểm chắc chắn và trong thời gian nhanh nhất những câu ở mức độ dễ như sóng ánh sáng, sóng điện từ, lượng tử ánh sáng, hạt nhân nguyên tử…, dành thời gian còn lại cho những câu khó hơn như dòng điện xoay chiều, sóng cơ học, dao động cơ học để nâng cao mức điểm. <br>
								- Ngoài ra, trong quá trình tổng ôn, các em cũng tích lũy được những kiến thức ở mức độ nâng cao hơn qua từng đề thi. Qua khóa Về Đích, các em sẽ cảm nhận sự tiến bộ rõ rệt, và hoàn toàn tự tin có thể đạt được tối thiểu 6 - 7 điểm cho bài thi của mình.
							</p>
						</div>
					</div>
					<div class="row body_cndt">
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Lộ trình</h4>
							<p>...</p>
						</div>
						<div class="col-md-5 body_cndt_left">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;Giáo viên</h4>
							<p>...</p>
						</div>
					</div>
				</div>
			</div>
			<div class="chnn">
				<div class="title_chnn">
					ĐĂNG KÍ KHÓA HỌC
				</div>
				<div class="form_login">
					<?php echo do_shortcode('[fc id="4"][/fc]'); ?>
				</div>
			</div>
		</div>
	</div>
	
</div>
<script>
	function openCity(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tab");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
<?php
}
genesis();