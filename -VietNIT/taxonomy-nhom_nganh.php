<?php

/*template Nganh kinh te */
remove_action( 'genesis_loop', 'genesis_do_loop' );
//remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );

$terms = get_the_terms( get_the_ID(), 'nhom_nganh');
$id=$terms[0]->term_id;

if($id==3060){
	add_action('genesis_loop','newskinhte');
	add_action('genesis_after_content_sidebar_wrap','khoinganh' );
}
elseif($id==3059){
	add_action('genesis_loop','newskythuat');
	add_action('genesis_after_content_sidebar_wrap','khoinganh' );
}
elseif($id==3061){
	add_action('genesis_loop','newssupham');
	add_action('genesis_after_content_sidebar_wrap','khoinganh' );
}
elseif ($id==3062) {
	add_action('genesis_loop','newsyduoc');
	add_action('genesis_after_content_sidebar_wrap','khoinganh' );
}
elseif($id==4806){
	add_action('genesis_loop','newsvanhoa');
	add_action('genesis_after_content_sidebar_wrap','khoinganh' );
}
else{
	add_action('genesis_loop','newscongan');
	add_action('genesis_after_content_sidebar_wrap','khoinganh' );
}

//add_action('genesis_after_content_sidebar_wrap','lienquanall');
//add_action('genesis_after_content_sidebar_wrap','lienquan' );


function newskinhte(){
	?>
	<div class="taxonomy-count">
		<div class="archive-head">
			<div class="nums-post">
			<?php							
				$terms = get_the_terms( get_the_ID(), 'nhom_nganh');
				echo '<p class="num">'.$terms[0]->count.'</p>';
				echo '<p class="truong">Trường</p>';
			?>
			</div>
		</div>
	</div>
	<div class="news-tb">
		<?php formsearch(); ?>			
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Đại học khối ngành kinh tế</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Đại học khối ngành kinh tế
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1292'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3060'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail("full"); ?>
							<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Học viện khối ngành kinh tế</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Học viện khối ngành kinh tế
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1293'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3060'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail("full"); ?>
							<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Cao đẳng khối ngành kinh tế</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Cao đẳng khối ngành kinh tế
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1294'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3060'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail("full"); ?>
							<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Trung cấp khối ngành kinh tế</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Trung cấp khối ngành kinh tế
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1295'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3060'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php
}
//end kinh te

function newskythuat(){
	?>
	<div class="taxonomy-count">
		<div class="archive-head">
			<div class="nums-post">
			<?php							
				$terms = get_the_terms( get_the_ID(), 'nhom_nganh');
				echo '<p class="num">'.$terms[0]->count.'</p>';
				echo '<p class="truong">Trường</p>';
			?>
			</div>
		</div>
	</div>
	<div class="news-tb">
		<?php formsearch(); ?>			
		<div class="thongbao">
			<!--div class="nametype">
				<h4>Đại học khối ngành Kỹ thuật</h4>
			</div-->
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Đại học khối ngành Kỹ thuật</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Đại học khối ngành Kỹ thuật
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1292'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3059'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Học viện khối ngành Kỹ thuật</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Học viện khối ngành Kỹ thuật
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1293'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3059'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Cao đẳng khối ngành Kỹ thuật</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Cao đẳng khối ngành Kỹ thuật
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1294'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3059'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Trung cấp khối ngành Kỹ thuật</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Trung cấp khối ngành Kỹ thuật
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1295'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3059'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php
}// end ky thuat

function newssupham(){
	?>
	<div class="taxonomy-count">
		<div class="archive-head">
			<div class="nums-post">
			<?php							
				$terms = get_the_terms( get_the_ID(), 'nhom_nganh');
				echo '<p class="num">'.$terms[0]->count.'</p>';
				echo '<p class="truong">Trường</p>';
			?>
			</div>
		</div>
	</div>
	<div class="news-tb">
		<?php formsearch(); ?>			
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Đại học khối ngành Sư phạm</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Đại học khối ngành Sư phạm
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1292'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3061'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Học viện khối ngành Sư phạm</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Học viện khối ngành Sư phạm
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1293'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3061'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Cao đẳng khối ngành Sư phạm</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Cao đẳng khối ngành Sư phạm
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1294'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3061'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Trung cấp khối ngành Sư phạm</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Trung cấp khối ngành Sư phạm
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1295'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3061'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php
}// end su pham


function newsyduoc(){
	?>
	<div class="taxonomy-count">
		<div class="archive-head">
			<div class="nums-post">
			<?php							
				$terms = get_the_terms( get_the_ID(), 'nhom_nganh');
				echo '<p class="num">'.$terms[0]->count.'</p>';
				echo '<p class="truong">Trường</p>';
			?>
			</div>
		</div>
	</div>
	<div class="news-tb">
		<?php formsearch(); ?>				
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Đại học khối ngành Y dược</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Đại học khối ngành Y dược
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1292'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3062'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Học viện khối ngành Y dược</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Học viện khối ngành Y dược
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1293'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3062'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Cao đẳng khối ngành Y dược</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Cao đẳng khối ngành Y dược
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1294'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3062'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Trung cấp khối ngành Y dược</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Trung cấp khối ngành Y dược
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1295'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3062'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php
}// end y duoc


function newsvanhoa(){
	?>
	<div class="taxonomy-count">
		<div class="archive-head">
			<div class="nums-post">
			<?php							
				$terms = get_the_terms( get_the_ID(), 'nhom_nganh');
				echo '<p class="num">'.$terms[0]->count.'</p>';
				echo '<p class="truong">Trường</p>';
			?>
			</div>
		</div>
	</div>
	<div class="news-tb">
		<?php formsearch(); ?>				
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Đại học khối Văn hóa - Nghệ thuật</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Đại học khối Văn hóa - Nghệ thuật
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1292'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '4806'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Học viện khối Văn hóa - Nghệ thuật</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Học viện khối Văn hóa - Nghệ thuật
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 100,
					'tax_query' => array(
					  array(
						'taxonomy' => 'trinh_do',
						'field' => 'id',
						'terms' => '1293'
					  ),
					  array(
						'taxonomy' => 'nhom_nganh',
						'field' => 'id',
						'terms' => '4806'
					  )
					)
				);
				$featureds = new WP_Query( $args );
				if($featureds->have_posts()):
				while($featureds->have_posts()) : $featureds->the_post();
				?>
				<div class="itemblock">
					<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
					<?php the_title(); ?>	
					</a>
				</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Cao đẳng khối Văn hóa - Nghệ thuật</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Cao đẳng khối Văn hóa - Nghệ thuật
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 100,
					'tax_query' => array(
					  array(
						'taxonomy' => 'trinh_do',
						'field' => 'id',
						'terms' => '1294'
					  ),
					  array(
						'taxonomy' => 'nhom_nganh',
						'field' => 'id',
						'terms' => '4806'
					  )
					)
				);
				$featureds = new WP_Query( $args );
				if($featureds->have_posts()):
				while($featureds->have_posts()) : $featureds->the_post();
				?>
				<div class="itemblock">
					<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
					<?php the_title(); ?>	
					</a>
				</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Trung cấp khối Văn hóa - Nghệ thuật</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Trung cấp khối Văn hóa - Nghệ thuật
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1295'
						  ),
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '4806'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php
}// end van hoa

function newscongan(){
	?>
	<div class="taxonomy-count">
		<div class="archive-head">
			<div class="nums-post">
			<?php							
				$terms = get_the_terms( get_the_ID(), 'nhom_nganh');
				echo '<p class="num">'.$terms[0]->count.'</p>';
				echo '<p class="truong">Trường</p>';
			?>
			</div>
		</div>
	</div>
	<div class="news-tb">
		<?php formsearch(); ?>
		<div class="thongbao">
			<h2 class="block-title">
				<a href="#">
					<span class="icon-block-title"></span>
					<span>Khối Bộ đội Công an</span>
				</a>
				<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
			</h2>
			<!-- Mobile -->
			<h2 class="block-title-mobile">
				<a href="#">
					Khối Bộ đội Công an
				</a>
			</h2>
			<!-- END -->
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(									  
						  array(
							'taxonomy' => 'nhom_nganh',
							'field' => 'id',
							'terms' => '3063'
						  )
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php
}// end cong an

//khoi nganh

function khoinganh(){
	?>
	<div class="clear"></div>
	<div class="linearea"></div>
	<div class="group-area-ct">
		<div class="area-head"><h4>Nhóm các trường tuyển sinh cùng khối ngành</h4></div>		
		<div class="gitem kythuat">
			<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-ky-thuat/">
				<i class="fa fa-cogs" aria-hidden="true"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Kỹ Thuật</p>
				</div>
			</a>
		</div>
		<div class="gitem yduoc">
			<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-y-duoc/">
			<i class="yduoc"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Y Dược</p>
				</div>
			</a>
		</div>
		<div class="gitem supham">
			<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-su-pham/">
				<i class="fa fa-graduation-cap" aria-hidden="true"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Sư Phạm</p>
				</div>
			</a>
		</div>
		<div class="gitem vanhoa">
			<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-van-hoa-nghe-thuat/">
				<i class="vanhoa"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Văn Hóa</p>
				</div>
			</a>
		</div>
		<div class="gitem congan">
			<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-bo-doi-cong-an/">
			<i class="congan"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Công An</p>
				</div>
			</a>
		</div>
	</div>
	<?php
}

genesis();