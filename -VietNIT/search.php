<?php
/*
Template Name: Search Results
 */
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action('genesis_loop','searchct');
function searchct(){
	?>
					<div class="relatedvisa">
						<h1>Kết quả tìm kiếm</h1>
						<div class="timkiem">
						<?php formsearch(); ?>
						</div>
						<div class="clear"></div>
						<div class="listpost">
							<?php 
								
								while(have_posts()) : the_post();
								?>
								<div class="itemblock">
									<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
									<?php the_title(); ?>	
									</a>
								</div>
							<?php endwhile; wp_reset_postdata(); ?>							
						</div>
					</div><!--end Related news -->
<?php
}
genesis();
