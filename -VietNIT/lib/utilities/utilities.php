<?php
/**
 * Handles including the utilities files
 * WordPress.
 *
 * @category VietNIT
 * @package  Utilities
 * @author   VietNIT
 */

// nhóm utilites dành cho admin
if(is_admin()){
	require('vietnit-user-roles.php');	
}

require('vietnit-do-standard-slug.php');	
require('vietnit-featured.php');
require('login-log.php');