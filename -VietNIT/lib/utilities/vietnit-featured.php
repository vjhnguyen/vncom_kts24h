<?php
/*
 * Handles including the utilities VietNIT-Featured
 * WordPress.
 *
 * @category VietNIT
 * @package  Utilities
 */
 
if( !defined('GF_URL') )
    define( 'GF_URL', get_stylesheet_directory_uri() . '/' );
    
if(is_admin()){    
    //Register option in order to store max value featured order
    register_activation_hook(__FILE__, 'gf_active_hook');
    if(function_exists('register_uninstall_hook'))
        register_uninstall_hook(__FILE__, 'gf_uninstall_hook');
}

function gf_active_hook() {
    if(get_option('gf_featured_order_num') == '')
        add_option('gf_featured_order_num', 0, '', 'no');
        
    if(get_option('gf_fbc_order_num') == '')
        add_option('gf_fbc_order_num', 0, '', 'no');
}

/**********************************************************/
// require_once(GF_DIR . 'includes/featured-meta-box.php');
// Tich hop featured meta box
if(is_admin()){
    //Add Featured Meta Box
    add_action('add_meta_boxes', 'gf_featured_meta_box');
    add_action( 'save_post', 'gf_save_featureddata' );
}
function gf_featured_meta_box() {
    $post_types = get_post_types( array( 'public' => true, '_builtin' => false ) );
    add_meta_box(
        'vietnit_featured',
        __( 'Featured', 'VietNIT' ),
        'gf_inner_featured_box',
        'post',
        'side',
        'high'
    );
    foreach( $post_types as $post_type ) {
        add_meta_box(
            'vietnit_featured',
            __( 'Featured', 'VietNIT' ),
            'gf_inner_featured_box',
            $post_type,
            'side',
            'high'
        );
    }
}

function gf_inner_featured_box($post) {
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'gf_nonce' );
    
    $featured = get_post_custom($post->ID);
    $gf_featured = isset($featured['gf_featured']) ? $featured['gf_featured'][0] : 'false';
    $gf_featured_order = $gf_featured == 'true' ? $featured['gf_featured_order'][0] : 0;
    $gf_featured_by_cat = isset($featured['gf_featured_by_cat']) ? $featured['gf_featured_by_cat'][0] : 'false';
    $gf_featured_by_cat_order = $gf_featured_by_cat == 'true' ? $featured['gf_featured_by_cat_order'][0] : 0;

?>
    <div style="margin-bottom: 20px;">
        <input type="checkbox" id="gf_featured" name="gf_featured" value="true" <?php checked('true', $gf_featured ); ?> />
        <label for="gf_featured">
           <?php _e("Featured this post", 'VietNIT' ); ?>
        </label><br />
        
        <label for="gf_featured_order">
           <?php _e("Order", 'VietNIT' ); ?>
        </label>
        <input type="text" id="gf_featured_order" name="gf_featured_order" value="<?php echo $gf_featured_order; ?>" size="2" />
        
        <input type="button" id="gf_make_newest_featured" class="button" style="cursor: pointer;" value="<?php _e('Be newest', 'VietNIT'); ?>" /> Or 
        <input type="button" id="gf_make_oldest_featured" class="button" style="cursor: pointer;" value="<?php _e('Oldest', 'VietNIT'); ?>" />
        
    </div>

    <div>
        <input type="checkbox" id="gf_featured_by_cat" name="gf_featured_by_cat" value="true" <?php checked('true', $gf_featured_by_cat ); ?> />
        <label for="gf_featured_by_cat">
           <?php _e("Featured this post for category", 'VietNIT' ); ?>
        </label><br />
        
        <label for="gf_featured_order">
           <?php _e("Order", 'VietNIT' ); ?>
        </label>
        <input type="text" id="gf_featured_by_cat_order" name="gf_featured_by_cat_order" value="<?php echo $gf_featured_by_cat_order; ?>" size="2" />
        
        <input type="button" id="gf_make_newest_fbc" class="button" style="cursor: pointer;" value="<?php _e('Be newest', 'VietNIT'); ?>" /> Or 
        <input type="button" id="gf_make_oldest_fbc" class="button" style="cursor: pointer;" value="<?php _e('Oldest', 'VietNIT'); ?>" />
    </div>
    
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function($) {
            $("#gf_make_newest_featured").click(function(){
                 $("#gf_featured_order").val(<?php echo get_option('gf_featured_order_num') + 1; ?>);
            });
            
            $("#gf_make_oldest_featured").click(function(){
                $("#gf_featured_order").val(0);
            });
            
            $("#gf_make_newest_fbc").click(function(){
                 $("#gf_featured_by_cat_order").val(<?php echo get_option('gf_fbc_order_num') + 1; ?>);
            });
            
            $("#gf_make_oldest_fbc").click(function(){
                $("#gf_featured_by_cat_order").val(0);
            });
        });
    </script>
    
<?php
}

function gf_save_featureddata($post_id) {
    // verify if this is an auto save routine. 
    // If it is our form has not been submitted, so we dont want to do anything
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
        return;
    
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    
    if ( !wp_verify_nonce( $_POST['gf_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    
    
    // Check permissions
    if ( 'page' === $_POST['post_type'] ) {
        return;
    }
    else {
        if ( !current_user_can( 'edit_post', $post_id ) )
            return;
    }
    
    // OK, we're authenticated: we need to find and save the data
    
    //Current post featured status
    $featured = get_post_custom($post_id);
    
    //New post featured status
    $gf_featured = $_POST['gf_featured'];
    $gf_featured_order = $_POST['gf_featured_order'];
    $gf_fbc = $_POST['gf_featured_by_cat'];
    $gf_fbc_order = $_POST['gf_featured_by_cat_order'];
    
    $gf_featured_num = get_option('gf_featured_order_num');
    $gf_fbc_num = get_option('gf_fbc_order_num');
    
    //Update featured post meta
    if($gf_featured == 'true')
        update_post_meta($post_id, 'gf_featured', 'true');
    else
        update_post_meta($post_id, 'gf_featured', 'false');
    
    if(!empty( $gf_featured_order) || $gf_featured_order === '0' )
        update_post_meta($post_id, 'gf_featured_order', $gf_featured_order);
    elseif( ( empty($featured['gf_featured'][0]) || $featured['gf_featured'][0] == 'false' ) && $gf_featured == 'true' )
        update_post_meta($post_id, 'gf_featured_order', $gf_featured_num + 1);

        
    //Update featured post by category meta
    if($gf_fbc == 'true')
        update_post_meta($post_id, 'gf_featured_by_cat', 'true');
    else
        update_post_meta($post_id, 'gf_featured_by_cat', 'false');
    
    if( !empty($gf_fbc_order) || $gf_fbc_order === '0' )
        update_post_meta($post_id, 'gf_featured_by_cat_order', $gf_fbc_order);
    elseif( ( empty($featured['gf_featured_by_cat'][0]) || $featured['gf_featured_by_cat'][0] == 'false' ) && $gf_fbc == 'true' )
        update_post_meta($post_id, 'gf_featured_by_cat_order', $gf_fbc_num + 1);
        
    //Update options
    if($gf_featured_num < $gf_featured_order)
        update_option('gf_featured_order_num', $gf_featured_order);
        
    if($gf_fbc_num < $gf_fbc_order)
        update_option('gf_fbc_order_num', $gf_fbc_order);
}

// End tich hop feature meta box

/************************************************************/
// require_once(GF_DIR . 'includes/featured-admin-header.php');
// Tich hop featured admin header -> Ajax
if(is_admin()){
    //Add script and css to edit post page
    add_action( 'admin_head-edit.php', 'gf_admin_head' );    
    add_action( 'admin_footer-edit.php', 'do_caia_featured_javascript' );
    add_action( 'wp_ajax_do_caia_featured', 'do_caia_featured_ajax' );
}
function gf_admin_head() {
?>
    <style>
        .feature-star {
            display: inline-block;
            width: 32px;
            height: 32px;
            background: url(<?php echo GF_URL; ?>images/VietNIT-featured/featured-stars.png) no-repeat 0 0;
            vertical-align: middle;
            text-indent: -9999px;
        }
        
        .gf-featured {
            background-position: -32px 0 ;
        }
        
        .gf-not-featured {
            background-position: 0 0 ;
        }
        
        td.gf_featured, td.gf_featured_by_cat {
            text-align: center;
            vertical-align: middle;
        }
        
        .gf-loading {
            background: url(<?php echo GF_URL; ?>images/VietNIT-featured/loading.gif) no-repeat 0 0;
        }
    </style>    
        
<?php
}


function do_caia_featured_javascript() {
    ?>
    <script type="text/javascript" >
    jQuery.noConflict();
    jQuery(document).ready(function($) {
        $("#gf-link-ajax").live('click', function(){
            var cur_postid = $(this).attr('post');
            var cur_action = $(this).attr('action');
            var currentItem = $(this);
            var data = {
                action : 'do_caia_featured',
                do_action: cur_action,
                post : cur_postid                 
            };
            

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: data,
                dataType: 'json',
                beforeSend : function(){
                    currentItem.addClass("gf-loading");
                },
                success: function(data) {
                    // alert(data);             
                    // currentItem.removeClass("gf-loading");
                    if(cur_action.indexOf('set')) {                 
                        currentItem.removeClass('gf-featured').addClass('gf-not-featured');
                    } else {                  
                        currentItem.removeClass('gf-not-featured').addClass('gf-featured');
                    }
                    
                    if(data.order)
                        currentItem.next('span').text(data.order);
                    
                    currentItem.attr('action', data.newAction);
                    currentItem.removeClass("gf-loading");
                }
            });

            // de ko scroll browser len top
            return false; 
            
        });
    });
    </script>
    <?php
}


function do_caia_featured_ajax() {
    /* Begin ajax */
    $action = $_POST['do_action'];
    $post_id = $_POST['post'];
    

    $post_custom = get_post_custom($post_id);
    $featured_num = get_option('gf_featured_order_num');
    $featured_by_cat_num = get_option('gf_fbc_order_num');

    if( isset($action) && isset($post_id) ) {
        $data = array();
        switch( $action ) {
            case 'setFeatured' :
                update_post_meta($post_id, 'gf_featured', 'true');
                if( empty($post_custom['gf_featured_order'][0]) ) {
                    update_post_meta($post_id, 'gf_featured_order', ++$featured_num);
                    update_option('gf_featured_order_num', $featured_num);
                    $data['order'] = $featured_num;
                }
                $data['newAction'] = 'removeFeatured';
                break;

            case 'removeFeatured' :
                update_post_meta($post_id, 'gf_featured', 'false');
                $data['newAction'] = 'setFeatured';
                break;

            case 'setFeaturedByCat' :
                update_post_meta($post_id, 'gf_featured_by_cat', 'true');
                if( empty($post_custom['gf_featured_by_cat_order'][0]) ) {
                    update_post_meta($post_id, 'gf_featured_by_cat_order', ++$featured_by_cat_num);
                    update_option('gf_fbc_order_num', $featured_by_cat_num);
                    $data['order'] = $featured_by_cat_num;
                }
                $data['newAction'] = 'removeFeaturedByCat';
                break;

            case 'removeFeaturedByCat' :
                update_post_meta($post_id, 'gf_featured_by_cat', 'false');
                $data['newAction'] = 'setFeaturedByCat';
                break;
        }
                
        echo json_encode($data);
    }

    die(); // this is required to return a proper result
}

// End featured admin header


/**************************************************/
// require_once(GF_DIR . 'includes/manage-column.php');
// Tich hop manage column
if(is_admin()){
    //Get all custom post type
    $post_types = get_post_types( array( 'public' => true, '_builtin' => false ) );

    //Add featured columns
    add_filter('manage_posts_columns', 'gf_manage_posts_columns');
    foreach( $post_types as $post_type )
        add_filter('manage_' . $post_type . '_columns', 'gf_manage_posts_columns');
}
function gf_manage_posts_columns($columns) {
    $columns['gf_featured'] = __('Feautured', 'VietNIT');
    $columns['gf_featured_by_cat'] = __('Feautured By Category', 'VietNIT');
    
    return $columns;
}

if(is_admin()){
    add_action('manage_posts_custom_column', 'gf_manage_posts_custom_columns');
    foreach( $post_types as $post_type )
        add_action('manage_' . $post_type . '_custom_column', 'gf_manage_posts_columns');
}
function gf_manage_posts_custom_columns($name) {
    global $post;
    $post_custom = get_post_custom($post->ID);
    // print_r($post_custom);
    switch($name) {
        case 'gf_featured' :
            $gf_featured = isset($post_custom['gf_featured']) ? $post_custom['gf_featured'][0] : 'false';
            $gf_featured_order = $gf_featured == 'true' ? $post_custom['gf_featured_order'][0] : 0;

            $featured = $gf_featured == 'true' ? 'gf-featured' : 'gf-not-featured';
            $action = $featured == 'gf-featured' ? 'removeFeatured' : 'setFeatured';
            echo '<a title="Featured it!" href="#" id="gf-link-ajax" class="feature-star '.$featured.' " post="' . $post->ID . '" action="' . $action . '" >Featured</a>';
            echo '<span style="display: inline-block; margin-left: 10px;">' . $gf_featured_order . '</span>';
            break;
        
        case 'gf_featured_by_cat' :
            $gf_featured_by_cat = isset($post_custom['gf_featured_by_cat']) ? $post_custom['gf_featured_by_cat'][0] : 'false';
            $gf_featured_by_cat_order = $gf_featured_by_cat == 'true' ? $post_custom['gf_featured_by_cat_order'][0] : 0;

            $featured = $gf_featured_by_cat == 'true' ? 'gf-featured' : 'gf-not-featured';
            $action = $featured == 'gf-featured' ? 'removeFeaturedByCat' : 'setFeaturedByCat';
            echo '<a title="Featured it by category!" href="#" id="gf-link-ajax" class="feature-star '.$featured.' " post="' . $post->ID . '" action="' . $action . '" >Not Featured</a>';
            echo '<span style="display: inline-block; margin-left: 10px;">' . $gf_featured_by_cat_order . '</span>';
            break;
    }
}


if(is_admin()){
    add_filter("manage_edit-post_sortable_columns", 'gf_sortable_columns');
    foreach( $post_types as $post_type )
        add_filter('manage_edit-' . $post_type . '_sortable_columns', 'gf_manage_posts_columns');

}
function gf_sortable_columns( $columns ) {
    $custom = array(
        // meta column id => sortby value used in query
        'gf_featured'    => 'gf_featured',
        'gf_featured_by_cat' => 'gf_featured_by_cat',
    );
    
    return wp_parse_args($custom, $columns);
    
}

add_filter( 'request', 'gf_column_orderby' );
function gf_column_orderby( $vars ) {
	if ( isset( $vars['orderby'] ) && 'gf_featured' === $vars['orderby'] ) {
		$vars = array_merge( $vars, array(
			'meta_key' => 'gf_featured_order',
			'orderby' => 'meta_value_num'
		) );
	}
    
    if ( isset( $vars['orderby'] ) && 'gf_featured_by_cat' === $vars['orderby'] ) {
		$vars = array_merge( $vars, array(
			'meta_key' => 'gf_featured_by_cat_order',
			'orderby' => 'meta_value_num'
		) );
	}
 
	return $vars;
}

// End Manage column

/**********************************************************/
// require_once(GF_DIR . 'includes/featured-query-vars.php');
// Tich hop featured query vars

//Add new query var
add_filter('query_vars', 'gf_add_query_vars', 5, 1);
function gf_add_query_vars($public_query_vars) {
	$public_query_vars[] = 'featured';	
	return $public_query_vars;
}

//Add filter to pre_get_posts
add_filter('pre_get_posts', 'gf_pre_get_posts', 1);
function gf_pre_get_posts( $query ) {    

    if($query->get('featured') === 'all') {
        $featured_query = array();
        
        if($query->get('orderby') === 'featured_order') {
            $query->set('meta_key', 'gf_featured_order');
            $query->set('orderby', 'meta_value_num');            
            $featured_query[] = array( 'key' => 'gf_featured_order' );
        }
        
        $featured_query[] = array(
            'key' => 'gf_featured',
			'value' => 'true',
			'compare' => '='
        );
       $query->set('meta_query', $featured_query);       

    } elseif($query->get('featured') === 'category') {
        $featured_query = array();
        
        if($query->get('orderby') === 'featured_by_cat_order') {
            $query->set('meta_key', 'gf_featured_by_cat_order');
            $query->set('orderby', 'meta_value_num');            
            $featured_query[] = array( 'key' => 'gf_featured_by_cat_order' );
        }
        
        $featured_query[] = array(
            'key' => 'gf_featured_by_cat',
			'value' => 'true',
			'compare' => '='
        );        
        $query->set('meta_query', $featured_query);        
    }    

    return $query;
}

// end tich hop featured-query-vars

