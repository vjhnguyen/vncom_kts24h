/**
 * The script for VietNIT Admin
 *
 * Since: 1.0
 * Developer ThanhVu
 */

jQuery( document ).ready( function( $ ) {

	/** Home settings */
		// Init sortable
	$( '.actived-blocks-settings' ).sortable( {
		revert  : true,
		handle  : '> h4',
		cursor  : 'move',
		update  : function( event, ui ) {
			var n = 0;
			$( this ).find( '.block' ).each( function() {
				$( this ).children( '.block-inside' ).find( 'input, textarea, select' ).each( function() {
					$( this ).attr( 'name', $( this ).attr( 'name' ).replace( /\[__i__\]|\[\d+\]/g, '[' + n.toString() + ']' ) );
				} );
				n++;
			} );
		},
		receive : function( event, ui ) {
			var addedItem = $( this ).find( 'div.block.deactived' );

			addedItem.removeClass( 'deactived' );
			addedItem.find( '.block-inside' ).slideDown( 'fast' ).removeClass( 'hidden' );
		}
	} );

	// Init draggable
	$( '.block-holder .block' ).draggable( {
		connectToSortable : '.actived-blocks-settings',
		handle            : '> h4',
		helper            : 'clone',
		revert            : 'invalid'
	} );

	// Block delete action
	$( '.block-title-action .block-delete' ).live( 'click', function() {
		var n = 0;

		if ( confirm( 'This block will be deleted. Are you sure want to do this?' ) ) {
			$( this ).parents( '.actived-blocks-settings' ).addClass( 'deleting' );

			$( this ).parents( '.block' ).slideUp( 400, function() {
				$( this ).remove();

				$( '.actived-blocks-settings.deleting' ).removeClass( 'deleting' ).children( '.block' ).each( function() {
					$( this ).children( '.block-inside' ).find( 'input, textarea, select' ).each( function() {
						$( this ).attr( 'name', $( this ).attr( 'name' ).replace( /\[__i__\]|\[\d+\]/g, '[' + n.toString() + ']' ) );
					} );
					n++;
				} );
			} );
		}

		return false;
	} );

	// Block toggle action
	$( '.block-title-action .block-toggle' ).live( 'click', function() {
		$( this ).parents( '.block' ).toggleClass( 'closed' ).find( '.block-inside' ).slideToggle( 400 );

		return false;
	} );

} );