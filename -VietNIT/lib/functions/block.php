<?php
/**
 * Helper functions to handle the theme blocks on homepage and more in future.
 *
 * @category VietNIT
 * @package  Block
 * @author   VietNIT
 */

global $_caia_registed_blocks;

global $_caia_block_code;


/**
 * Store the registed blocks.
 *
 * @since 1.0
 */
$_caia_registed_blocks = array();

/**
 * Store the registed block code.
 *
 * @since 2.0
 */
$_caia_block_code = array();

/**
 * Register a block.
 *
 * @since 1.0
 *
 * @param string $class The Class name
 */
function caia_register_block( $class )
{
	global $_caia_registed_blocks;

	// don't register block if there are a block exists with the same name
	if ( ! array_key_exists( $class, $_caia_registed_blocks ) )
	{
		$_caia_registed_blocks[$class] = new $class();
	}
}

/**
 * Unregister a block
 *
 * @since 1.0
 *
 * @param string $class The Class name
 */
function caia_unregister_block( $class )
{
	global $_caia_registed_blocks;

	unset( $_caia_registed_blocks[$class] );
}

function caia_get_registed_blocks()
{
	global $_caia_registed_blocks;
	return $_caia_registed_blocks;
}

function caia_register_block_code($code)
{
	global $_caia_block_code;
	if(is_string($code) && !empty($code)){
		$code = strtolower($code);		

		$code = str_replace(' ', '-', $code);
		if(!in_array($code, $_caia_block_code)){
			$_caia_block_code[] = $code;
		}
	}
}

/*
* This function generate dropdown to select block code
* @param $input: is an array('id' => '...', 'name' => '...', 'class' => '...', 'selected' => '...')
*/
function caia_dropdown_block_code($input)
{
	global $_caia_block_code;
	extract($input);
	?>
	<select id="<?php echo $id; ?>" name="<?php echo $name; ?>">
		<option value="" <?php selected( '', $selected ); ?>><?php _e('None', 'caia'); ?></option>
    	<?php foreach( $_caia_block_code as $code ) : ?>
        	<option value="<?php echo $code; ?>" <?php selected( $code, $selected ); ?>><?php echo $code; ?></option>
		<?php endforeach; ?>
    </select>
	<?php
}



caia_register_block( 'Caia_News_Block' );
caia_register_block( 'Caia_Banner_Block' );
caia_register_block( 'Caia_Code_Block' );
caia_register_block( 'Caia_Flexible_Block' );


