<?php
/**
 * Handles including the widget class files, and registering the widgets in
 * WordPress.
 *
 * @category VietNIT

 */

global $_vietnit_widget_code;
$_vietnit_widget_code = array();

function caia_register_widget_code($code)
{
	global $_vietnit_widget_code;
	if(is_string($code) && !empty($code)){
		$code = strtolower($code);
		$code = str_replace(' ', '-', $code);
		if(!in_array($code, $_vietnit_widget_code)){
			$_vietnit_widget_code[] = $code;
		}
	}
}

/*
* This function generate dropdown to select widget code
* @param $input: is an array('id' => '...', 'name' => '...', 'class' => '...', 'selected' => '...')
*/
function caia_dropdown_widget_code($input)
{
	global $_vietnit_widget_code;
	extract($input);
	?>
	<select id="<?php echo $id; ?>" name="<?php echo $name; ?>">
		<option value="" <?php selected( '', $selected ); ?>><?php _e('None', 'caia'); ?></option>
    	<?php foreach( $_vietnit_widget_code as $code ) : ?>
        	<option value="<?php echo $code; ?>" <?php selected( $code, $selected ); ?>><?php echo $code; ?></option>
		<?php endforeach; ?>
    </select>
	<?php
}

/** Include widget class files */
require( CAIA_WIDGETS_DIR . '/support-online-widget.php' );
require( CAIA_WIDGETS_DIR . '/post-list-widget.php' );
require( CAIA_WIDGETS_DIR . '/ads-widget.php' );
require( CAIA_WIDGETS_DIR . '/code-widget.php' );

add_action( 'widgets_init', 'vietnit_load_widgets' );

// remove all genesis widget -> faster than unregister each widget
remove_action( 'widgets_init', 'genesis_load_widgets' );

/**
 * Register widgets for use in the Genesis theme.
 *
 * @category VIETNIT
 * @package Widgets
 *
 * @since 1.0
 */
function vietnit_load_widgets()
{
	register_widget( 'VietNIT_Online_Support_Widget' );
	register_widget( 'VietNIT_Post_List_Widget' );
	register_widget( 'VietNIT_Ads_Widget' );
	register_widget( 'VietNIT_Code_Widget' );

	

	if(is_admin()){
		// unregister some unuse widget
		// unregister_widget('Genesis_Featured_Post');
		// unregister_widget('Genesis_Featured_Page');
		// unregister_widget('Genesis_User_Profile_Widget');
		unregister_widget('WP_Widget_Recent_Posts');
		// unregister_widget('WP_Widget_Recent_Comments');
		unregister_widget('WP_Widget_Meta');
		unregister_widget('WP_Widget_Archives');
		unregister_widget('WP_Widget_RSS');
		unregister_widget('WP_Widget_Pages');
	}
		
	/** Allow extended themes register more widgets */
	// do_action( 'caia_init_widgets' );
}