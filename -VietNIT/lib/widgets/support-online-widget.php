<?php
/**
 * VietNIT support online widget
 *
 * @category VietNIT
 * @package  Widgets
 */

/**
 * VietNIT support online widget class
 *
 * @category VietNIT
 * @package  Widgets
 * @since   1.0
 */
class VietNIT_Online_Support_Widget extends WP_Widget
{
	/**
	 * Holds widget settings defaults, populated in constructor.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Constructor. Set the default widget options and create widget.
	 *
	 * @since 1.0
	 */
	function __construct()
	{
		$this->defaults = array(
			'title'         => '',
			'group_num'     => 1,
			'tel'           => '',
			'hotline'       => '',
			'email'         => '',
			'use_caia_custom_icon' => 0,
			'skype_on'      => '',
			'skype_off'     => '',
			'yahoo_on'      => '',
			'yahoo_off'     => '',
			'support_extra' => ''
		);

		$widget_ops  = array(
			'classname' => 'support-online-widget',
			'description' => __( 'Support online by Yahoo and Skype .....', 'caia' )
		);

		$control_ops = array(
			'width' => 505,
			'height' => 250,
			'id_base' => 'support-online'
		);

		$this->WP_Widget( 'support-online', __( 'VietNIT - Online Support', 'caia' ), $widget_ops, $control_ops );
	}

	/**
	 * Echo the widget content.
	 *
	 * @since 1.0
	 */
	function widget( $args, $instance )
	{
		extract( $args );

		/** Merge with defaults */
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		echo $before_widget;
		if ( ! empty( $instance['title'] ) )
		{
			echo $before_title . apply_filters( 'widget_title', $instance['title'] ) . $after_title;
		}
		
		//var_dump( $instance['use_caia_custom_icon']);
		
		if ( empty( $instance['use_caia_custom_icon'])){
		?>
		
			<!-- Support group -->
			<?php for ( $i = 0; $i < $instance['group_num']; $i ++ ) : ?>
				<div id="support-group-<?php echo $i + 1; ?>" class="support-group">
					<?php if ( ! empty( $instance['group_title'][$i] ) ) : ?>
					<div class="group-name"><span><?php echo $instance['group_title'][$i]; ?></span></div><?php endif; ?>
					<?php for ( $j = 0; $j < $instance['group_supporters_num'][$i]; $j ++ ) : ?>
						<div id="supporter-<?php echo $j + 1; ?>" class="<?php echo $j % 2 == 0 ? 'supp-odd' : 'supp-even'; ?> supporter">
							<div class="supporter-info">
								<?php if ( ! empty( $instance['supporter_name'][$i][$j] ) ) : ?>
								<p class="supporter-name"><?php echo $instance['supporter_name'][$i][$j]; ?></p><?php endif; ?>

								<?php if ( ! empty( $instance['supporter_phone'][$i][$j] ) ) : ?>
								<p class="supporter-phone"><?php echo $instance['supporter_phone'][$i][$j]; ?></p><?php endif; ?>
							</div>
							<!-- end .supporter-info -->
							<div class="supporter-online">
								<?php if ( ! empty( $instance['supporter_yahoo'][$i][$j] ) ) : ?><p class="supporter-yahoo">
								
								<a href="<?php echo 'ymsgr:sendim?' . $instance['supporter_yahoo'][$i][$j]; ?>"><img src="<?php echo 'http://opi.yahoo.com/online?u=' . $instance['supporter_yahoo'][$i][$j] . '&m=g&t=' . $instance['supporter_yahoo_opi'][$i][$j]; ?>" alt="<?php _e( 'Hỗ trợ trực tuyến qua Yahoo', 'caia' ); ?>" title="Trò chuyện với <?php echo $instance['supporter_yahoo'][$i][$j]; ?>" /></a>						
							</p><?php endif; ?>

								<?php if ( ! empty( $instance['supporter_skype'][$i][$j] ) ) : ?><p class="supporter-skype">
								<a href="<?php echo 'skype:' . $instance['supporter_skype'][$i][$j] . '?chat'; ?>" onclick="return skypeCheck();"><img src="http://mystatus.skype.com/<?php echo $instance['supporter_skype_icon'][$i][$j]; ?>/<?php echo $instance['supporter_skype'][$i][$j]; ?>" alt="<?php _e( 'Hỗ trợ trực tuyến qua Skype', 'caia' ); ?>" title="Trò chuyện với <?php echo $instance['supporter_skype'][$i][$j]; ?>" /></a>
							</p><?php endif; ?>
							</div>
							<!-- end .supporter-online -->
						</div><!-- end .supporter -->
					<?php endfor; ?>
				</div><!-- end .supporter-group -->
			<?php endfor; ?>
			<!-- End support group -->
		
		<?php
		} 
		else{
		?>
		
			<?php 
			if ( ! empty( $instance['skype_off'] ) ) {
				$skypeicon_off=$instance['skype_off'];
			}
			else{ 
				$skypeicon_off= CHILD_URL.'/images/supportonline/skyper-offline.png';
			}
								
			if ( ! empty( $instance['skype_on'] ) ) {
				$skypeicon_on=$instance['skype_on'];
			}
			else{ 
				$skypeicon_on= CHILD_URL.'/images/supportonline/skyper-online.png';
			}
			
			if ( ! empty( $instance['yahoo_off'] ) ) {
				$yahooicon_off=$instance['yahoo_off'];
			}
			else{ 
				$yahooicon_off= CHILD_URL.'/images/supportonline/yahoo-offline.png';
			}
									
			if ( ! empty( $instance['yahoo_on'] ) ) {
				$yahooicon_on=$instance['yahoo_on'];
			}
			else{ 
				$yahooicon_on= CHILD_URL.'/images/supportonline/yahoo-online.png';
			}
			
			$supporter_list="";
			?>

			<!-- Support group -->
			<?php for ( $i = 0; $i < $instance['group_num']; $i ++ ) : ?>
				<div id="support-group-<?php echo $i + 1; ?>" class="support-group">
					<?php if ( ! empty( $instance['group_title'][$i] ) ) : ?>
					<div class="group-name"><span><?php echo $instance['group_title'][$i]; ?></span></div><?php endif; ?>
					<?php for ( $j = 0; $j < $instance['group_supporters_num'][$i]; $j ++ ) : ?>
						<div id="supporter-<?php echo $j + 1; ?>" class="<?php echo $j % 2 == 0 ? 'supp-odd' : 'supp-even'; ?> supporter">
							<div class="supporter-info">
								<?php if ( ! empty( $instance['supporter_name'][$i][$j] ) ) : ?>
								<p class="supporter-name"><?php echo $instance['supporter_name'][$i][$j]; ?></p><?php endif; ?>

								<?php if ( ! empty( $instance['supporter_phone'][$i][$j] ) ) : ?>
								<p class="supporter-phone"><?php echo $instance['supporter_phone'][$i][$j]; ?></p><?php endif; ?>
							</div>
							<!-- end .supporter-info -->
							<div class="supporter-online">
								<?php if ( ! empty( $instance['supporter_yahoo'][$i][$j] ) ) : ?><p class="supporter-yahoo">
								
								<?php $supporter_list=$supporter_list.$instance['supporter_yahoo'][$i][$j].'/id/'.'supporter-yahoo-'.$i.$j.'|';?>
								
								<a href="<?php echo 'ymsgr:sendim?' . $instance['supporter_yahoo'][$i][$j]; ?>"><img id="supporter-yahoo-<?php echo $i.$j; ?>" src="<?php echo $yahooicon_off; ?>" alt="<?php _e( 'Hỗ trợ trực tuyến qua Yahoo', 'caia' ); ?>" title="Trò chuyện với <?php echo $instance['supporter_yahoo'][$i][$j]; ?>"/></a>						
							</p><?php endif; ?>

								<?php if ( ! empty( $instance['supporter_skype'][$i][$j] ) ) : ?><p class="supporter-skype">
								
								<?php $supporter_list=$supporter_list.$instance['supporter_skype'][$i][$j].'/id/'.'supporter-skype-'.$i.$j.'|';?>
								
								<a href="<?php echo 'skype:' . $instance['supporter_skype'][$i][$j] . '?chat'; ?>" onclick="return skypeCheck();"><img id="supporter-skype-<?php echo $i.$j; ?>" src="<?php echo $skypeicon_off; ?>" alt="<?php _e( 'Hỗ trợ trực tuyến qua Skype', 'caia' ); ?>" title="Trò chuyện với <?php echo $instance['supporter_skype'][$i][$j]; ?>" /></a>
							</p><?php endif; ?>
							</div>
							<!-- end .supporter-online -->
						</div><!-- end .supporter -->
					<?php endfor; ?>
				</div><!-- end .supporter-group -->
			<?php endfor; ?>
			<!-- End support group -->
			
			<?php //echo $supporter_list; ?>
			
			<script type="text/javascript" language="javaScript">
			jQuery(document).ready(function($) {
				$.ajax({ //tạo đối tượng ajax kết nối tới check-status.php để check trạng thái
					type: "POST",
					url: "<?php echo CHILD_URL; ?>/lib/widgets/support-online-check-status.php",
					data: 'listsuporter=<?php echo $supporter_list; ?>', // tham số kèm theo để file php sử lý
					cache: false,
					dataType: "html", // trả về kết quả html khi xử lý xong
					success: function(string){
						var getData = $.parseJSON(string);
						//tách mảng 2 chiều trả về,để xử lý thông tin
						for ( var i = 0; i < getData.length; i++ ) {
							//nếu trạng thái thay đổi thì mới xử lý
							if (getData[i][0]=='01' || getData[i][0]=='2'){
								if (getData[i][0]=='01'){
									//đổi ảnh
									$('img#' + getData[i][1]).attr({
										'src': '<?php echo $yahooicon_on; ?>'
									});
								}
								if (getData[i][0]=='2'){
									$('img#' + getData[i][1]).attr({
										'src': '<?php echo $skypeicon_on; ?>'
									});
								}
							}
							
						}
					},
					error: function (){
						//alert('nếu có lỗi không làm gì cả, dữ nguyên trạng thái ban đầu');
					}		
				});
			});// end document.ready
			</script>
			
		<?php }//end else support group ?>
		
		<?php if ( ! empty( $instance['tel'] ) || ! empty( $instance['hotline'] ) || ! empty( $instance['email'] ) ) : ?>
		    <div id="hotline-area" class="hotline-area">
				<?php if ( ! empty( $instance['tel'] ) ) : ?>
					<span class="tel">
						<span class="label"><?php _e( 'Tel: ', 'caia' ); ?></span>
		        		<span class="num"><?php echo $instance['tel']; ?></span>
		        	</span>
		        <?php endif; ?>

				<?php if ( ! empty( $instance['hotline'] ) ) : ?>
		        	<span class="hotline">
		        		<span class="label"><?php _e( 'Hotline: ', 'caia' ); ?></span>
		        		<span class="num"><?php echo $instance['hotline']; ?></span>
		        	</span>
		        <?php endif; ?>

				<?php if ( ! empty( $instance['email'] ) ) : ?>
					<p class="email">
						<span><?php _e('Email: ', 'caia'); ?></span>
		        		<a href="mailto:<?php echo $instance['email'] ?>"><?php echo $instance['email']; ?></a>
		        	</p>
		        <?php endif; ?>
		    </div><!-- end #hotline-area -->
		<?php endif; ?>

		<?php

		if ( ! empty( $instance['support_extra'] ) )
		{
			echo '<div class="support-extra">' . $instance['support_extra'] . '</div>';
		}
		echo '<br class="clear"/>';
		echo $after_widget;
	}

	/**
	 * Update a particular instance.
	 *
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	function update( $new_instance, $old_instance )
	{
		return $new_instance;
	}

	/**
	 * Echo the settings update form.
	 *
	 * @since 1.0
	 *
	 * @param array $instance Current settings
	 * @return string|void
	 */
	function form( $instance )
	{
		/** Merge with defaults */
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		?>


	    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'caia' ); ?>:</label>
	        <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" style="width:98%;" />
	    </p>

	    <hr />
	    <p>
	        <label for="<?php echo $this->get_field_id( 'tel' ); ?>" style="width: 49%; display: inline-block;"><?php _e( 'Tel', 'caia' ); ?>:</label>
	        <label for="<?php echo $this->get_field_id( 'hotline' ); ?>" style="width: 49%; display: inline-block;"><?php _e( 'Hotline', 'caia' ); ?>:</label>
	    </p>

	    <p>
	        <input type="text" id="<?php echo $this->get_field_id( 'tel' ); ?>" name="<?php echo $this->get_field_name( 'tel' ); ?>" value="<?php echo esc_attr( $instance['tel'] ); ?>" style="width:48%;" />
	        <input type="text" id="<?php echo $this->get_field_id( 'hotline' ); ?>" name="<?php echo $this->get_field_name( 'hotline' ); ?>" value="<?php echo esc_attr( $instance['hotline'] ); ?>" style="width:49%;" />
	    </p>

	    <p><label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e( 'Email', 'caia' ); ?>:</label>
	        <input type="text" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo esc_attr( $instance['email'] ); ?>" style="width:98%;" />
	    </p>

	    <hr />

	    <p style="background: #ccc; padding: 5px;">
	        <label for="<?php echo $this->get_field_id( 'group_num' ); ?>"><?php _e( 'Number of support group', 'caia' ); ?>:</label>
	        <input type="text" id="<?php echo $this->get_field_id( 'group_num' ); ?>" name="<?php echo $this->get_field_name( 'group_num' ); ?>" value="<?php echo esc_attr( $instance['group_num'] ); ?>" size="2" />
	        <input type="submit" name="savewidget" id="savewidget" class="button-secondary widget-control-save" value="Save" />
	        <img alt="" style="padding-bottom: 4px;" title="" class="ajax-feedback " src="<?php echo admin_url( '/' ); ?>images/wpspin_light.gif" />
	    </p>

	    <hr />

		<?php for ( $i = 0; $i < $instance['group_num']; $i ++ ) : ?>
		    <p>
		        <label for="<?php echo $this->get_field_id( 'group_title_' . $i ); ?>"><strong><?php _e( 'Group title ', 'caia' ); echo $i + 1; ?></strong></label>&nbsp;&nbsp;&nbsp;
		        <input type="text" id="<?php echo $this->get_field_id( 'group_title_' . $i ); ?>" name="<?php echo $this->get_field_name( 'group_title' ); ?>[]" value="<?php echo esc_attr( $instance['group_title'][$i] ); ?>" size="25%" />

		        <label for="<?php echo $this->get_field_id( 'supports_num_' . $i ); ?>"><?php _e( 'Supporters', 'caia' ) ?></label>
		        <input type="text" id="<?php echo $this->get_field_id( 'supports_num_' . $i ); ?>" name="<?php echo $this->get_field_name( 'group_supporters_num' ); ?>[]" value="<?php echo esc_attr( $instance['group_supporters_num'][$i] ); ?>" size="1" />
		        <input type="submit" name="savewidget" id="savewidget" class="button-secondary widget-control-save" value="Save" />
		        <img alt="" style="padding-bottom: 4px;" title="" class="ajax-feedback " src="<?php echo admin_url( '/' ); ?>images/wpspin_light.gif" />
		    </p>

	        <p>
                <?php $supporter_num = intval( $instance['group_supporters_num'][$i] ); ?>
				<?php for ( $j = 0; $j < $supporter_num; $j ++ ) : ?>
		        <div style="width: 47%; padding: 5px; margin: 5px 0; background: #eee; <?php echo $j % 2 == 0 ? 'float: left;' : 'float: right;'; ?>">
		            <p>
		                <label for="<?php echo $this->get_field_id( 'supporter_name_' . $i . $j ); ?>"><?php _e( 'Name', 'caia' ); ?></label>
		                <input type="text" id="<?php echo $this->get_field_id( 'supporter_name_' . $i . $j ); ?>" name="<?php echo $this->get_field_name( 'supporter_name' ); ?>[<?php echo $i; ?>][]" value="<?php echo esc_attr( $instance['supporter_name'][$i][$j] ); ?>" size="20%" />
		            </p>

		            <p>
		                <label for="<?php echo $this->get_field_id( 'supporter_phone_' . $i . $j ); ?>"><?php _e( 'Phone', 'caia' ); ?></label>
		                <input type="text" id="<?php echo $this->get_field_id( 'supporter_phone_' . $i . $j ); ?>" name="<?php echo $this->get_field_name( 'supporter_phone' ); ?>[<?php echo $i; ?>][]" value="<?php echo esc_attr( $instance['supporter_phone'][$i][$j] ); ?>" size="20%" />
		            </p>

		            <p>
		                <label for="<?php echo $this->get_field_id( 'supporter_skype_' . $i . $j ); ?>"><?php _e( 'Skype', 'caia' ); ?></label>
		                <input type="text" id="<?php echo $this->get_field_id( 'supporter_skype_' . $i . $j ); ?>" name="<?php echo $this->get_field_name( 'supporter_skype' ); ?>[<?php echo $i; ?>][]" value="<?php echo esc_attr( $instance['supporter_skype'][$i][$j] ); ?>" size="20%" />
						<br/>
						<label for="<?php echo $this->get_field_id( 'supporter_skype_icon_' . $i . $j ); ?>"><?php _e( 'Skype Icon', 'caia' ); ?></label>
		                <select id="<?php echo $this->get_field_id( 'supporter_skype_icon_' . $i . $j ); ?>" name="<?php echo $this->get_field_name( 'supporter_skype_icon' ); ?>[<?php echo $i; ?>][]">
		                    <option value="mediumicon" <?php selected( 'mediumicon', $instance['supporter_skype_icon'][$i][$j] ); ?>><?php echo 'mediumicon'; ?></option>
							<option value="bigclassic" <?php selected( 'bigclassic', $instance['supporter_skype_icon'][$i][$j] ); ?>><?php echo 'bigclassic'; ?></option>
							<option value="smallclassic" <?php selected( 'smallclassic', $instance['supporter_skype_icon'][$i][$j] ); ?>><?php echo 'smallclassic'; ?></option>
							<option value="balloon" <?php selected( 'balloon', $instance['supporter_skype_icon'][$i][$j] ); ?>><?php echo 'balloon'; ?></option>							
		                </select>
		            </p>

		            <p>
		                <label for="<?php echo $this->get_field_id( 'supporter_yahoo_' . $i . $j ); ?>"><?php _e( 'Yahoo', 'caia' ); ?></label>
		                <input type="text" id="<?php echo $this->get_field_id( 'supporter_yahoo_' . $i . $j ); ?>" name="<?php echo $this->get_field_name( 'supporter_yahoo' ); ?>[<?php echo $i; ?>][]" value="<?php echo esc_attr( $instance['supporter_yahoo'][$i][$j] ); ?>" size="20%" />
		                <br/>
		                <label for="<?php echo $this->get_field_id( 'supporter_yahoo_opi_' . $i . $j ); ?>"><?php _e( 'Yahoo Icon', 'caia' ); ?></label>
		                <select id="<?php echo $this->get_field_id( 'supporter_yahoo_opi_' . $i . $j ); ?>" name="<?php echo $this->get_field_name( 'supporter_yahoo_opi' ); ?>[<?php echo $i; ?>][]">
							<?php for ( $k = 1; $k <= 22; $k ++ ) : ?>
		                    <option value="<?php echo $k; ?>" <?php selected( $k, $instance['supporter_yahoo_opi'][$i][$j] ); ?>><?php echo $k; ?></option>
							<?php endfor; ?>																				
		                </select>
		            </p>
		        </div>
				<?php endfor; ?>
		    </p>
		    <hr class="clear" />
		<?php endfor; ?>
		
		<p><label for="<?php echo $this->get_field_id( 'use_caia_custom_icon' ); ?>"><?php _e( 'Use Caia Custom Icon ', 'caia' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'use_caia_custom_icon' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'use_caia_custom_icon' ); ?>" value="1" <?php checked( $instance['use_caia_custom_icon'] ); ?>/>
	    </p>
		
		<p><label for="<?php echo $this->get_field_id( 'yahoo_off' ); ?>"><?php _e( 'Yahoo Off Icon (optional)', 'caia' ); ?>:</label>
	        <input type="text" id="<?php echo $this->get_field_id( 'yahoo_off' ); ?>" name="<?php echo $this->get_field_name( 'yahoo_off' ); ?>" value="<?php echo esc_attr( $instance['yahoo_off'] ); ?>" style="width:67%;" />
	    </p>
	    <p><label for="<?php echo $this->get_field_id( 'yahoo_on' ); ?>"><?php _e( 'Yahoo On Icon (optional)', 'caia' ); ?>:</label>
	        <input type="text" id="<?php echo $this->get_field_id( 'yahoo_on' ); ?>" name="<?php echo $this->get_field_name( 'yahoo_on' ); ?>" value="<?php echo esc_attr( $instance['yahoo_on'] ); ?>" style="width:67%;" />
	    </p>
	    <p><label for="<?php echo $this->get_field_id( 'skype_off' ); ?>"><?php _e( 'Skype Off Icon (optional)', 'caia' ); ?>:</label>
	        <input type="text" id="<?php echo $this->get_field_id( 'skype_off' ); ?>" name="<?php echo $this->get_field_name( 'skype_off' ); ?>" value="<?php echo esc_attr( $instance['skype_off'] ); ?>" style="width:67%;" />
	    </p>
	    <p><label for="<?php echo $this->get_field_id( 'skype_on' ); ?>"><?php _e( 'Skype On Icon (optional)', 'caia' ); ?>:</label>
	        <input type="text" id="<?php echo $this->get_field_id( 'skype_on' ); ?>" name="<?php echo $this->get_field_name( 'skype_on' ); ?>" value="<?php echo esc_attr( $instance['skype_on'] ); ?>" style="width:67%;" />
	    </p>

	    <label for="<?php echo $this->get_field_id( 'support_extra' ); ?>"><strong><?php _e( 'Extra :', 'caia' ); ?></strong></label>
	    <textarea id="<?php echo $this->get_field_id( 'support_extra' ); ?>" name="<?php echo $this->get_field_name( 'support_extra' ); ?>" style="width: 98%;"><?php echo esc_attr( $instance['support_extra'] ); ?></textarea>
		<?php
	}
}
