<?php
/**
 * API for register blocks.
 *
 * @category VietNIT
 * @package  Blocks
 * @developer Thanh Vu
 * @version  1.1
 * Date: 14/01/2014
 */

/**
 * Default class for creating blocks.
 *
 * @since 1.0
 */

class Caia_Block
{
	var $id_base;
	var $name;
	var $number;
	var $options;
	var $settings_field;
	var $options_group;
	var $defaults;

	/**
	 * PHP4 constructor
	 *
	 * @since 1.0
	 */
	function Caia_Block( $id_base, $name, $options = array(), $settings_field = '' )
	{
		Caia_Block::__construct( $id_base, $name, $options, $settings_field );
	}

	/**
	 * PHP5 Constructor
	 *
	 * @since    1.0
	 *
	 * @param        $id_base
	 * @param        $name
	 * @param string $options
	 * @param string $settings_field
	 */
	function __construct( $id_base, $name, $options, $settings_field = '' )
	{
		$this->id_base        = $id_base;
		$this->name           = $name;
		$this->options        = $options;
		$this->settings_field = $settings_field;
	}

	/**
	 * This function to display the block.
	 * This function need to be overrided in extended class.
	 *
	 * @since 1.0
	 */
	function show() {}

	/**
	 * This function display the option form.
	 *
	 * @since 1.0
	 */
	function form() {}


	/**
	 * This function to display the block from $args as input.
	 * This function need to be overrided in extended class.
	 * @param array $args
	 *
	 * @since 2.0
	 */
	function print_block($args)
	{		
		$this->set_options($args);
		$this->show();
	}

	/**
	 * This function set options property
	 * @param $options
	 *
	 * @since 2.0
	 */
	function set_options($options)
	{
		// $this->options = array_merge($this->defaults, $options);
		$this->options = wp_parse_args( $options, $this->defaults);
	}

	/**
	 * Helper function that constructs id attributes for use in form fields.
	 *
	 * @since 1.0
	 * @param $option
	 *
	 * @return string
	 */
	protected function get_field_id( $option )
	{
		$id = '';
		if ( empty( $this->settings_field ) ) {
			$id = $option;
		} else {
			if ( empty( $this->options_group ) ) {
				$id = sprintf( '%s[%s]', $this->settings_field, $option );
			} else {
				$number = empty( $this->number ) ? '__i__' : $this->number;
				$id = sprintf( '%s[%s][%s][%s]', $this->settings_field, $this->options_group, $number, $option );
			}
		}

		return $id;		
		
	}

	/**
	 * Helper function that constructs name attributes for use in form fields.
	 *
	 * @since 1.0
	 * @param      $option
	 * @param bool $multi
	 *
	 * @return string
	 */
	public function get_field_name( $option, $multi = false )
	{
		$field_name = '';
		if ( empty( $this->settings_field ) ) {
			$field_name = $option;
		} else {
			if ( empty( $this->options_group ) ) {
				$field_name = sprintf( '%s[%s]', $this->settings_field, $option );
			} else {
				$number = ! isset( $this->number ) ? '__i__' : $this->number;
				$field_name = sprintf( '%s[%s][%s][%s]', $this->settings_field, $this->options_group, $number, $option );
			}
		}

		if ( $multi )
			$field_name .= '[]';

		return $field_name;	
		
	}

	/**
	 * Helper function that get the value of an option key.
	 *
	 * @since 1.0
	 * @param string $option
	 *
	 * @return mixed
	 */
	protected function get_field_value( $option )
	{
		$value = false;

		if ( isset( $this->options['__class_name'] ) )
		{
			$value = isset( $this->options[$option] ) ? $this->options[$option] : false;
		}
		else
		{
			if ( ! empty( $this->options_group ) && isset( $this->number ) )
			{
				$values = genesis_get_option( $this->options_group, $this->settings_field );
				if ( is_array( $values ) )
				{
					unset( $values['__i__'] );
					$this->options = $values[$this->number];
				}

				$value = $this->options[$option];
			}
			elseif( isset( $this->number ) )
			{
				$value = genesis_get_option( $option, $this->settings_field );
			}
			else
			{
				$value = $this->options[$option];
			}
		}

		return $value;		
		
	}

	/**
	 * Helper function that echo the block classes.
	 *
	 * @since 1.0.4
	 * @param string $class The additional classes user want to add
	 *
	 */
	protected function block_class( $class = '' )
	{
		$readable_number = $this->number + 1;

		$classes = array();
		$classes[] = $this->id_base . '-' . $readable_number;
		$classes[] = $this->id_base;
		$classes[] = $this->number % 2 ? 'block-even' : 'block-odd';
		$classes[] = 'block-' . $readable_number;
		$classes[] = 'block';
		
		echo implode( ' ', $classes );
	}
} // end Caia_Block



/**
 * Banner block
 *
 * @since 1.0
 * @author HoangLT
 */
class Caia_Banner_Block extends Caia_Block
{
	// protected $defaults;

	function __construct()
	{
		$this->defaults = array(
				'title' => '',
				'content' => ''
			);
		

		$id_base = 'banner-block';
		$name = __( 'Banner Block', 'caia' );

		$this->Caia_Block( $id_base, $name, $this->defaults );
	}

	/**
	 * Display this block base on its settings.
	 * This function override the parents function.
	 *
	 * @since 1.0
	 */
	function show()
	{
		?>

		<div id="<?php echo $this->id_base . '-' . $this->number; ?>" class="<?php $this->block_class(); ?>">
			<div class="banner-content">
				<?php echo do_shortcode( $this->get_field_value( 'content' ) ); ?>
			</div>
		</div>

		<?php
	}

	/**
	 * Display this block settings on admin screen.
	 * This function override the parents function.
	 *
	 * @since 1.0
	 */
	function form()
	{
		?>

	    <p>
	        <label>
				<?php _e( 'Block title:', 'caia' ); ?><br/>
	            <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $this->get_field_value( 'title' ); ?>" />
	        </label>
	    </p>

		<p>
			<label>
				<?php _e( 'HTML Code:', 'caia' ); ?><br/>
				<textarea rows="6" class="widefat" name="<?php echo $this->get_field_name( 'content' ); ?>"><?php echo $this->get_field_value( 'content' ); ?></textarea>
			</label><br/>
			<span class="description"><?php _e( 'Allow shortcodes', 'caia' ); ?></span>
		</p>

		<?php
	}
}

require('class-news-block.php');
require('class-code-block.php');
require('class-flexible-block.php');