<?php

/**
 * Flexible block
 * @since 2.0
 * @developer Thanh Vu
 */
class Caia_Flexible_Block extends Caia_Block
{	

	var $main_post_show_function = '';
	var $next_post_show_function = '';
	var $just_shown_post_ids = array();
	var $exclude_post_ids;
	var $_my_hook;

	function __construct()
	{
		$this->defaults = array(
				'code'					   => '',
				'title'                    => '',
				'auto_title'               => 0,
				'taxonomy'                 => 'category',				
				'term'	                   => '', // term_id
				'post_type'                => 'post',
				'post_info'                => '', // '[post_date] ' . __( 'By', 'caia' ) . ' [post_author_posts_link] [post_comments]'
				'num_posts'                => 1,
				'num_relateds'             => 0,
				'related_title'            => '',
				'image_size'               => 'thumbnail',				
				'related_post_image_size'  => 'thumbnail',
				'image_align'              => 'alignnone',
				'related_post_image_align' => 'alignnone',
				'title_position'		   => 'before_thumbnail',
				'related_image'            => 0,
				'featured'                 => 0,				
				'content_limit'            => 200,
				'read_more'                => '',
				'more_text'                => '',
				'more_position'            => 'top',
				'menu'                     => '',
				'support_jcarousel'		   => 0,
				);
		

		$id_base = 'flexible-block';
		$name = __( 'Flexible Block', 'caia' );

		$this->Caia_Block( $id_base, $name, $this->defaults );
		
	}


	/**
	 * Add classes to news block
	 *
	 * @since  2.0.0
	 *
	 * @param array $classes The default classes
	 * @param string $id_base
	 *
	 * @return array
	 */
	protected function block_class( $class = '' ){

		$classes = array();
		if ($this->get_field_value( 'code' )) $classes[] = 'code-' . $this->get_field_value( 'code' );
		if ($this->get_field_value( 'featured' )) $classes[] = 'featured-posts-enable';
		if ($this->get_field_value( 'num_relateds' )) $classes[] = 'has-related';
		if ($this->get_field_value( 'related_image' )) $classes[] = 'has-related-image';
		if ($this->get_field_value( 'related_title' )) $classes[] = 'has-related-title';
		if ($this->get_field_value( 'more_text' )) $classes[] = 'has-cat-more';
		if (!$this->get_field_value( 'title' )) $classes[] = 'empty-block-title';

		parent::block_class();

		$my_classes = implode( ' ', $classes );
		echo ' ' . $my_classes;
	}

	/**
	 * This function will save hook than clean hook of this class.
	 * After this, we can add new hook without conflict.
	 * Its save hooks as follows:
	 *  - caia_flexible_block_main_post_show
	 *  - caia_flexible_block_next_post_show	 
	 * @since 2.0.1
	 */	
	function save_than_clean_hooks()
	{
		global $wp_filter;
		$this->_my_hook = array();
		$this->_my_hook['caia_flexible_block_main_post_show'] = $wp_filter['caia_flexible_block_main_post_show'];
		$this->_my_hook['caia_flexible_block_next_post_show'] = $wp_filter['caia_flexible_block_next_post_show'];
		$this->_my_hook['caia_flexible_block_before_content'] = $wp_filter['caia_flexible_block_before_content'];
		$this->_my_hook['caia_flexible_block_after_content'] = $wp_filter['caia_flexible_block_after_content'];

		

		$wp_filter['caia_flexible_block_main_post_show'] = array();
		$wp_filter['caia_flexible_block_next_post_show'] = array();		
		$wp_filter['caia_flexible_block_before_content'] = array();		
		$wp_filter['caia_flexible_block_after_content'] = array();		
	}

	/**
	 * This function will restore hooks whick are saved by function save_than_clean_hooks	 
	 * @since 2.0.1
	 */	
	function restore_hooks()
	{
		global $wp_filter;
		if($this->_my_hook){
			$wp_filter['caia_flexible_block_main_post_show'] = $this->_my_hook['caia_flexible_block_main_post_show'];
			$wp_filter['caia_flexible_block_next_post_show'] = $this->_my_hook['caia_flexible_block_next_post_show'];
			$wp_filter['caia_flexible_block_before_content'] = $this->_my_hook['caia_flexible_block_before_content'];
			$wp_filter['caia_flexible_block_after_content'] = $this->_my_hook['caia_flexible_block_after_content'];
			$this->_my_hook = NULL;
		}
	}

	/**
	 * This function to display the block from $args as input.
	 * This function need to be overrided in extended class.
	 * @param array $args
	 * @param string $main_post_show_function: function display a main post
	 * @param string $next_post_show_function: function display a next post
	 * @since 2.0
	 */	
	function print_block($args, $main_post_show_function = '', $next_post_show_function = '')
	{
		$this->main_post_show_function = $main_post_show_function;
		$this->next_post_show_function = $next_post_show_function;
		$this->set_options($args);
		$this->show();
	}

	/**
	 * This function return array of post_ids just shown by print_block() or show() method	 	 
	 * @since 2.0
	 */	
	function get_just_shown_post_ids()
	{
		return $this->just_shown_post_ids;
	}

	/**
	 * This function return array of post_ids just shown by print_block() or show() method	 	 
	 * @param $post_ids: array of post_id to exclude from result
	 * @since 2.0
	 */	
	function set_exclude_post_ids($post_ids)
	{
		$this->exclude_post_ids = $post_ids;
	}

	

	/**
	 * Display this block base on its settings.
	 * This function override the parents function.
	 *
	 * @since 1.0
	 */
	function show( $heading = 'h2', $post_heading = 'h3' )
	{
		extract($this->options);
		
		$total_posts = $num_posts + $num_relateds;		
		

		$args = array(
			'showposts'           => $total_posts,			
			'post_type'           => $post_type,
			'ignore_sticky_posts' => 1
		);

		if($this->exclude_post_ids){
			$args['post__not_in'] = $this->exclude_post_ids;
		}

		if($term){
			if($taxonomy === 'category'){
				$args['cat'] = $term;
			}else if($taxonomy === 'post_tag'){
				$args['tag_id'] = $term;
			}else {
				$args['tax_query'] = array( 
										array('taxonomy' => $taxonomy, 
												'field' => 'id', 
												'terms' => $term));
			}
		}

		if ( $featured )
		{
			$args = array_merge(
				$args,
				array(
					'featured'  => $term ? 'category' : 'all',
					'orderby'  => $term ? 'featured_by_cat_order' : 'featured_order',
				)
			);
		}

		if ( $total_posts )
		{
			$args = apply_filters( 'caia_flexible_block_query', $args, $code, $this->options );			
			$news = new WP_Query( $args );
			$still_have_posts = $news->have_posts();
		}
		
		$this->just_shown_post_ids = array();

		
		$block_title = '';
		if ( $title )
		{
			$title = __($title, 'caia');
			if ( $term )
			{
				$title_link = get_term_link( intval($term), $taxonomy );
				$block_title = "<$heading class='block-title'><a href='$title_link' title='$title'><span>$title</span></a></$heading>";
			}
			else
			{
				$block_title = "<$heading class='block-title'><span>$title</span></$heading>";
			}
		} else if( $auto_title && $term ){
			if($taxonomy === 'category'){
				$my_title = get_cat_name( $term );
			}else{
				$term_obj = get_term_by('id', $term, $taxonomy, ARRAY_A);
				$my_title = $term_obj['name'];	
			}
			
			$title_link = get_term_link( intval($term), $taxonomy );
			$block_title = "<$heading class='block-title'><a href='$title_link' title='$my_title'><span>$my_title</span></a></$heading>";
		}

		// to support wpml
		if($more_text) $more_text = __($more_text, 'caia');
		if($read_more) $read_more = __($read_more, 'caia');
		if($related_title) $related_title = __($related_title, 'caia');

		?>

		<div id="<?php echo $this->id_base . '-' . $this->number; ?>" class="<?php $this->block_class(); ?>">

			<?php if ( $more_text && $more_position === 'top' ) : ?>
				<a href="<?php echo $title_link; ?>" class="more-from-category"><?php echo $more_text; ?></a>
			<?php endif; ?>

			<?php echo $block_title; 
			if ( $menu ) 
			{
				echo '<div id="menu-' . $menu . '" class="extra-menu">';
				wp_nav_menu( array( 'fallback_cb' => '', 'menu' => $menu ) );
				echo '</div>';
			}
			?>
			<div class="block-wrap">
				<?php if ( $num_posts ) : 
					do_action( 'caia_flexible_block_before_content', $code, $this->options);
				?>
					<div class="main-posts">
						<?php 
						// do_action( 'caia_flexible_block_main_before_post_show', $code, $this->options);
						if($support_jcarousel){
							echo '<a class="prev" href="#">' . apply_filters('caia_flexible_block_jcarousel_previous', 'previous', $code, $this->options ) . '</a>';
							echo '<div class="slide-' . $code . '">';
							echo '<ul>';
						}						
						$index = 0;
						while( ($still_have_posts = $news->have_posts()) && $index < $num_posts ) : $news->the_post(); 
							$this->just_shown_post_ids[] = get_the_ID();
							if($support_jcarousel){
								echo '<li id="post-' . get_the_ID() . '" class="caia-block-item" style="list-style-type: none;">';
								if($this->main_post_show_function){
									call_user_func( $this->main_post_show_function, $code, $this->options, $post_heading);
								}else{									
									do_action( 'caia_flexible_block_main_post_show', $code, $this->options, $post_heading);
								}
								
								echo '</li><!-- end 1 post -->';

							}else{
								echo '<div id="post-' . get_the_ID() . '" class="caia-block-item">';
								if($this->main_post_show_function){
									call_user_func( $this->main_post_show_function, $code, $this->options, $post_heading);
								}else{									
									do_action( 'caia_flexible_block_main_post_show', $code, $this->options, $post_heading);
								}

								if($image_align !== 'alignnone') echo '<div class="clear"></div>';
								echo '</div><!-- end 1 post -->';	
							}
													
							$index++;
						endwhile; 
						
						if($support_jcarousel){
							echo '</ul>';
							echo '</div><!--end the slideShow -->';
							echo '<a class="next" href="#">' . apply_filters('caia_flexible_block_jcarousel_previous', 'previous', $code, $this->options ) . '</a>';
						}
						// do_action( 'caia_flexible_block_main_after_post_show', $code, $this->options);
						?>						
					</div><!-- end .main-posts -->
				<?php endif; ?>

			    <?php if ( $num_relateds && $still_have_posts ) : ?>
					<div class="older-posts">
						<?php if ( $related_title ) echo '<p class="older-posts-text">' . $related_title . '</p>'; ?>

						<ul>
							<?php while( $news->have_posts() && $index < $total_posts ) : $news->the_post();

								$this->just_shown_post_ids[] = get_the_ID();

								echo '<li>';
								if($this->next_post_show_function){
									call_user_func( $this->next_post_show_function, $code, $this->options);
								}else{									
									do_action( 'caia_flexible_block_next_post_show', $code, $this->options );
								}
							?>
								<p class="time time_mobi">
									<i class="fa fa-clock-o"></i>
									<?php the_time('d') ?>/<?php the_time('m') ?>/<?php the_time('Y') ?>
								</p>
							<?php
								echo '</li>';
								$index ++;
								endwhile;
							?>
						</ul>
					</div> <!-- end older_posts -->
				<?php endif;

				do_action( 'caia_flexible_block_after_content', $code, $this->options);
				
				if ( $more_text && $more_position === 'bottom' ) : ?>
					<a href="<?php echo $title_link; ?>" class="more-from-category"><?php echo $more_text; ?></a>
				<?php endif; ?>

			</div><!-- end .block-wrap -->
		</div>

		<?php
		wp_reset_postdata();
	}

	/**
	 * Display this block settings on admin screen.
	 * This function override the parents function.
	 *
	 * @since 2.0
	 */
	function form()
	{	
			
		?>
		<p>
			<label><?php _e( 'Only Coder should use this block, if not, use News Blocks instead!)', 'caia' ); ?><label>
		</p>
		<p>
			<label>
				<?php caia_dropdown_block_code(array('name' => $this->get_field_name( 'code' ),
													'selected' => $this->get_field_value( 'code' )));
				?>								
				<?php _e( 'Block code (only coder use, don\'t change!)', 'caia' ); ?><br />
			</label>
		</p>

		<p>
			<label>
				<?php _e( 'Title:', 'caia' ); ?><br />
				<input type="text" class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $this->get_field_value( 'title' ); ?>" />
			</label>
		</p>

		<p>
	        <label>
	            <input type="checkbox" name="<?php echo $this->get_field_name( 'auto_title' ); ?>" value="1" <?php checked( 1, $this->get_field_value( 'auto_title' ) ); ?> />
				<?php _e( 'Auto title by term name', 'caia' ); ?>
	        </label>
	    </p>

		<p>
	        <label>
				<?php
		        caia_dropdown_post_types(
					array(
						'selected' => $this->get_field_value( 'post_type' ),
						'name' => $this->get_field_name( 'post_type' )
					)
				);
				?>
		        <?php _e( 'Post type', 'caia' ); ?>
	        </label>
	    </p>

		<p>
		    <label>
		    	<?php 
		    	$taxonomies = get_taxonomies( array('public' => true), 'objects');  
		    	$ajax_id = '_ajax_' . $this->id_base . '-' . $this->number; ?>
				<select name="<?php echo $this->get_field_name( 'taxonomy' ); ?>" class="caia-tax-ajax-select <?php echo $ajax_id; ?>">
					<?php foreach ($taxonomies as $name => $value) {?>					
					<option value="<?php echo $name; ?>" <?php selected( $name, $this->get_field_value( 'taxonomy' ))?> ><?php echo $value->labels->name?></option>
					<?php } ?>			
				</select>
				<?php _e( 'Taxonomies', 'caia' ); ?>
		        
		    </label>
		</p>

		<p>
			<label>				
				<?php					
					wp_dropdown_categories(
						array(
							'hide_empty'      => 0,
							'depth'           => 100,
							'hierarchical'    => 1,
							'show_option_all' => __( 'All Items', 'caia' ),
							'selected'        => $this->get_field_value( 'term' ),
							'name'            => $this->get_field_name( 'term' ),
							'taxonomy'		  => $this->get_field_value( 'taxonomy' ),
							'class'			  => 'caia-term-ajax_' . $ajax_id ,							
						)
					);
					_e( 'Terms', 'caia' );
				?>
			</label>
			<label class='caia-loading-ajax_<?php echo $ajax_id;?>'>&nbsp;</label>
		</p>	    

		<p>
            <label>
                <input type="text" name="<?php echo $this->get_field_name( 'num_posts' ); ?>" value="<?php echo $this->get_field_value( 'num_posts' ); ?>" size="2" />
				<?php _e( 'Number of posts to display', 'caia' ); ?>
            </label>
		</p>
		
	    <p>
	        <label>
	            <input type="text" name="<?php echo $this->get_field_name( 'content_limit' ); ?>" value="<?php echo $this->get_field_value( 'content_limit' ); ?>" size="2" />
				<?php _e( 'The content limit', 'caia' ); ?>
	        </label>
	    </p>

	    <p>
	        <label>
	        	<?php _e( 'Post info (ex: [post_date] by [post_author_posts_link] [post_comments] )', 'caia' ); ?>:<br>
	            <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'post_info' ); ?>" value="<?php echo $this->get_field_value( 'post_info' ); ?>" />
				
	        </label>
	    </p>

	    <p>
	        <label>
	            <input type="text" name="<?php echo $this->get_field_name( 'read_more' ); ?>" value="<?php echo $this->get_field_value( 'read_more' ); ?>" />
				<?php _e( 'Read more link text', 'caia' ); ?>
	        </label>
	    </p>

	    <p>
	        <label>
				<?php
				caia_dropdown_image_sizes(
					array(
						'name' => $this->get_field_name( 'image_size' ),
						'selected' => $this->get_field_value( 'image_size' )
					)
				);
				?>
		        <?php _e( 'Image size', 'caia' ); ?>
	        </label>
	    </p>

	    <p>
	        <label>
				<select name="<?php echo $this->get_field_name( 'image_align' ); ?>">
					<option value="alignnone" <?php selected( 'alignnone', $this->get_field_value( 'image_align' ) ); ?>>- <?php _e( 'None', 'caia' ); ?> -</option>
					<option value="alignleft" <?php selected( 'alignleft', $this->get_field_value( 'image_align' ) ); ?>><?php _e( 'Left', 'caia' ); ?></option>
					<option value="alignright" <?php selected( 'alignright', $this->get_field_value( 'image_align' ) ); ?>><?php _e( 'Right', 'caia' ); ?></option>
				</select>
		        <?php _e( 'Image alignment', 'caia' ); ?>
	        </label>
	    </p>

	    <p>
	        <label>
				<select name="<?php echo $this->get_field_name( 'title_position' ); ?>">					
					<option value="before_thumbnail" <?php selected( 'before_thumbnail', $this->get_field_value( 'title_position' ) ); ?>><?php _e( 'Before', 'caia' ); ?></option>
					<option value="after_thumbnail" <?php selected( 'after_thumbnail', $this->get_field_value( 'title_position' ) ); ?>><?php _e( 'After', 'caia' ); ?></option>
				</select>
		        <?php _e( 'Position of title compare to thumbnail', 'caia' ); ?>
	        </label>
	    </p>

	    <p>
	        <label>
	            <input type="checkbox" name="<?php echo $this->get_field_name( 'featured' ); ?>" value="1" <?php checked( 1, $this->get_field_value( 'featured' ) ); ?> />
				<?php _e( 'Display featured posts only', 'caia' ); ?>
	        </label>
	    </p>

	    <p>
	        <label>
	            <input type="checkbox" name="<?php echo $this->get_field_name( 'support_jcarousel' ); ?>" value="1" <?php checked( 1, $this->get_field_value( 'support_jcarousel' ) ); ?> />
				<?php _e( 'Support jCarousel (slider block), have to a Code too.', 'caia' ); ?>
	        </label>
	    </p>

	    <p>
	        <label>
				<?php _e( 'Display the older posts with the title:', 'caia' ); ?><br />
	            <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'related_title' ); ?>" value="<?php echo $this->get_field_value( 'related_title' ); ?>" />
	        </label>
	    </p>

	    <p>
            <label>
                <input type="text" name="<?php echo $this->get_field_name( 'num_relateds' ); ?>" value="<?php echo $this->get_field_value( 'num_relateds' ); ?>" size="2" />
			    <?php _e( 'Number of related/older posts to display', 'caia' ); ?>
            </label>
	    </p>

		<p>
            <label>
                <input type="checkbox" name="<?php echo $this->get_field_name( 'related_image' ); ?>" value="1" <?php checked( 1, $this->get_field_value( 'related_image' ) ); ?> />
				<?php _e( 'Display featured image within related/older post', 'caia' ) ?>
            </label>
		</p>

	    <p>
	        <label>
				<?php
				caia_dropdown_image_sizes(
					array(
						'name' => $this->get_field_name( 'related_post_image_size' ),
						'selected' => $this->get_field_value( 'related_post_image_size' )
					)
				);
				?>
		        <?php _e( 'Image size', 'caia' ); ?>
	        </label>
	    </p>

	    <p>
	        <label>
				<select name="<?php echo $this->get_field_name( 'related_post_image_align' ); ?>">
					<option value="alignnone" <?php selected( 'alignnone', $this->get_field_value( 'related_post_image_align' ) ); ?>>- <?php _e( 'None', 'caia' ); ?> -</option>
					<option value="alignleft" <?php selected( 'alignleft', $this->get_field_value( 'related_post_image_align' ) ); ?>><?php _e( 'Left', 'caia' ); ?></option>
					<option value="alignright" <?php selected( 'alignright', $this->get_field_value( 'related_post_image_align' ) ); ?>><?php _e( 'Right', 'caia' ); ?></option>
				</select>
		        <?php _e( 'Image alignment', 'caia' ); ?>
	        </label>
	    </p>

        <br/>

	    <p>
		    <label>
			    <input type="text" name="<?php echo $this->get_field_name( 'more_text' ); ?>" value="<?php echo $this->get_field_value( 'more_text' ); ?>" />
			    <?php _e( '"More from category" link text', 'caia' ); ?>
		    </label>
	    </p>

		<p>
			<label>
				<select name="<?php echo $this->get_field_name( 'more_position' ); ?>">
					<option value="top" <?php selected( 'top', $this->get_field_value( 'more_position' ) ) ?>><?php _e( 'Top', 'caia' ); ?></option>
					<option value="bottom" <?php selected( 'bottom', $this->get_field_value( 'more_position' ) ) ?>><?php _e( 'Bottom', 'caia' ); ?></option>
				</select>
				<?php _e( 'Link position', 'caia' ) ?>
            </label>
		</p>

		<?php $menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );?>
		
		<p>
			<label>
				<select name="<?php echo $this->get_field_name( 'menu' ); ?>">
					<option value=""><?php _e( 'No Menu', 'caia' ); ?></option>
					<?php foreach ( $menus as $menu ) : ?>
						<option value="<?php echo $menu->term_id; ?>" <?php selected( $menu->term_id, $this->get_field_value( 'menu' ) ) ?>><?php echo $menu->name; ?></option>
					<?php endforeach; ?>
				</select>
				<?php _e( 'Select menu', 'caia' ) ?>
            </label>
		</p>
		<?php
	}
}


add_action('caia_flexible_block_main_post_show', 'caia_flexible_block_main_post_show', 10, 3);
/*
* This function will display main post in Flexible Block
*
* @author TuanNM
* @since 2.0
*/
function caia_flexible_block_main_post_show($code, $options, $post_heading = 'h3')
{		
	extract($options);	
	
	$cur_post_title_attr = the_title_attribute( 'echo=0' );

	if ( 'before_thumbnail' === $title_position ){
		printf( '<%s><a href="%s" title="%s">%s</a></%s>', $post_heading, get_permalink(), $cur_post_title_attr, get_the_title(), $post_heading );
		if($post_info)
			echo '<p class="byline post-info">' . do_shortcode( $post_info ) . '</p>';
	}

	$thumbnail = genesis_get_image(
		array(
			'size' => $image_size,
			'attr' => array(				
				'title' => $cur_post_title_attr,
				'alt'   => $cur_post_title_attr,
				'class' => $image_align
			)
		)
	);

	if ( ! empty( $thumbnail ) ){
		printf( '<a href="%s" title="%s">%s</a>', get_permalink(), $cur_post_title_attr, $thumbnail );
	}

	if ( 'after_thumbnail' === $title_position ){
		printf( '<%s><a href="%s" title="%s">%s</a></%s>', $post_heading, get_permalink(), $cur_post_title_attr, get_the_title(), $post_heading );
		if($post_info)
			echo '<p class="byline post-info">' . do_shortcode( $post_info ) . '</p>';
	}

	if ( $content_limit ){
		the_content_limit(
			$content_limit,
			$read_more
		);
	}	
}




add_action( 'caia_flexible_block_next_post_show', 'caia_flexible_block_next_post_show', 10, 2 );

/*
* This function will display next post in Flexible Block
*
* @author TuanNM
* @since 2.0
*/
function caia_flexible_block_next_post_show($code, $options)
{
	extract($options);

	$cur_post_title_attr = the_title_attribute( 'echo=0' );
	if ( $related_image )
	{
		$related_thumbnail = genesis_get_image(
			array(
				'size' => $related_post_image_size,
				'attr' => array(					
					'title' => $cur_post_title_attr,
					'alt'   => $cur_post_title_attr,
					'class' => $related_post_image_align
				)
			)
		);

		if ( ! empty( $related_thumbnail ) )
		{
			printf( '<a href="%s" title="%s">%s</a>', get_permalink(), $cur_post_title_attr, $related_thumbnail );
		}
	}
	?>
	<a href="<?php the_permalink(); ?>" title="<?php echo $cur_post_title_attr; ?>"><?php the_title(); ?></a>
	<?php
}

// caia_register_block( 'Caia_Flexible_Block' );

