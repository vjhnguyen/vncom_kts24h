<?php
/**
 * Custom hooks and functions.

 * @package  Functions
 * @category VIETNIT
 * @developer   VietNIT
 */

// Block edit theme and plugin in daskboard.
define('DISALLOW_FILE_EDIT',true);

// Init theme
require_once( TEMPLATEPATH . '/lib/init.php' );
require( CHILD_DIR . '/lib/init.php' );



/** Load custom functions */
if ( file_exists( CAIA_CUSTOM_DIR . '/functions.php' ) )
{
	require( CAIA_CUSTOM_DIR . '/functions.php' );
}
