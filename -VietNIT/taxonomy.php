<?php
/**
 * Handle the display of taxonomy page.

 * @package  Template
 * @category VIETNIT
 * @developer   VietNIT
 */

remove_action( 'genesis_loop', 'genesis_do_loop' );
remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
add_action('genesis_loop','newsthongbao');

function newsthongbao(){
	?>
	<div class="news-tb">
		<div class="thongbao">
			<div class="nametype">
				<!--h4><?php //single_term_title(); ?></h4-->
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span><?php single_term_title(); ?></span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
			</div>
			<div class="listpost">
				<?php
					$args = array(
						'posts_per_page' => 29
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
			<div class="paging">
				<div class="navigation">
					<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
					<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
					<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
					<?php } ?>
				</div>
				<div class="nav_mobi">
					<div class="row">
						<div class="col-xs-12">
							<ul class="pager">
								<li><?php previous_posts_link('Prev') ?></li>
								<li><?php next_posts_link('Next') ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="clear"></div>
	<div class="news group-area-home">
		<div class="namekn"><h4>Tuyển sinh theo khối ngành</h4></div>
		
		<div class="quang-cao">
			<div class="content-quangcao">
				<div class="viewgt">
					<?php if(is_active_sidebar('banner_adv_home') && !dynamic_sidebar('banner_adv_home')):endif; ?>
				</div>
			</div>
		</div>
		
		<div class="kn1 kn">
			<div class="gitem kinhte">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-kinh-te/">
					<i class="fa fa-money" aria-hidden="true"></i>		
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Kinh Tế</p>
						</div>
				</a>
			</div>
			<div class="gitem kythuat">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-ky-thuat/">
					<i class="fa fa-cogs" aria-hidden="true"></i>
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Kỹ Thuật</p>
						</div>
				</a>
			</div>
			<div class="gitem vanhoa">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-van-hoa-nghe-thuat/">
					<i class="vanhoa"></i>
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Văn Hóa</p>
						</div>
				</a>
			</div>
			<div class="gitem supham">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-su-pham/">
					<i class="fa fa-graduation-cap" aria-hidden="true"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Sư Phạm</p>
					</div>
				</a>
			</div>
			<div class="gitem yduoc">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-y-duoc/">
				<i class="yduoc"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Y Dược</p>
					</div>
				</a>
			</div>
			<div class="gitem congan">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-bo-doi-cong-an/">
				<i class="congan"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Công An</p>
					</div>
				</a>
			</div>
		</div>
	</div> <!--end group-area-home-->
	
	<!-- ---------------- Hiện google ---------------- -->
	<div class="slider-home" style="margin-top: 20px;">
		<h1 class="archive-heading">
			<span class="icon-block-h1"></span>
			<span>Bài viết nổi bật</span>
		</h1>
		<div class="box4T-bottom-home">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						style="display:block"
						data-ad-format="autorelaxed"
						data-ad-client="ca-pub-1895892504965300"
						data-ad-slot="2175974571"
						data-matched-content-ui-type="image_stacked"
						data-matched-content-rows-num="2"
						data-matched-content-columns-num="8">
					</ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
			</div>
		</div>
	</div>
	<!---------------------------------------------------------------------------->
	<!-- QUẢNG CÁO CỦA GOOGLE -->
	<div class="quang-cao-google" style="width: 100%;float: left;margin-bottom: 20px;">
		<div class="quang-cao-1" style="width: 49%;float: left;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 4 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="8543525730"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<div class="quang-cao-2" style="width: 49%;float: right;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 7 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="4258010410"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
	<!-- END -->
	<!---------------------------------------------------------------------------->
	
	<?php
}

genesis();