<?php
/*teplate dai hoc ha noi*/
remove_action( 'genesis_loop', 'genesis_do_loop' );
remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
add_action('genesis_loop','newsthongbao');

function newsthongbao(){
	?>
	<div class="news-tb">
		<div class="thongbao">
			<div class="nametype">
				<h4><?php single_term_title(); ?></h4>
			</div>
			<div class="listpost">
			<ul>
				<?php
					while(have_posts()) : the_post();
					?>
					<li class="col-md-4">
					 <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				<?php endwhile; wp_reset_postdata(); ?>
			</ul>
			</div>
			<div class="paging">
				<div class="navigation">
					<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
					<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
					<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
					<?php } ?>
				</div>
				<div class="nav_mobi">
					<div class="row">
						<div class="col-xs-12">
							<ul class="pager">
								<li><?php previous_posts_link('Prev') ?></li>
								<li><?php next_posts_link('Next') ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}

genesis();