<?php
/*template trung cap TPHCM */
remove_action( 'genesis_loop', 'genesis_do_loop' );
//remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
add_action('genesis_loop','newsthongbao');
//add_action('genesis_after_content_sidebar_wrap','lienquan' );
//add_action('genesis_after_content_sidebar_wrap','fkhoinganh' );


function newsthongbao(){
	?>
	<div class="taxonomy-count">
		<div class="archive-head">
			<div class="nums-post">
			<?php
				$category = get_the_category();
				echo '<p class="num">'.$category[0]->category_count.'</p>';
				echo '<p class="truong">Trường</p>';
			?>
			</div>
		</div>
	</div>
	<div class="news-tb">
		<?php formsearch(); ?>
		<div class="thongbao">
			<div class="nametype">
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span>Các trường Trung cấp chuyên nghiệp</span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
				<!-- Mobile -->
				<h2 class="block-title-mobile">
					<a href="#">
						Các trường Trung cấp chuyên nghiệp
					</a>
				</h2>
				<!-- END -->
			</div>
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1295'
						  ),
						  array(
							'taxonomy' => 'khu_vuc',
							'field' => 'id',
							'terms' => '1248'
						  ),
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<div class="nametype">
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span>Các trường trung cấp nghề</span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
				<!-- Mobile -->
				<h2 class="block-title-mobile">
					<a href="#">
						Các trường trung cấp nghề
					</a>
				</h2>
				<!-- END -->
			</div>
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'trinh_do',
							'field' => 'id',
							'terms' => '1297'
						  ),
						  array(
							'taxonomy' => 'khu_vuc',
							'field' => 'id',
							'terms' => '1248'
						  ),
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<!-- ---------------- Hiện google ---------------- -->
	<div class="slider-home" style="margin-top: 20px;">
		<h1 class="archive-heading">
			<span class="icon-block-h1"></span>
			<span>Bài viết nổi bật</span>
		</h1>
		<div class="box4T-bottom-home">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						style="display:block"
						data-ad-format="autorelaxed"
						data-ad-client="ca-pub-1895892504965300"
						data-ad-slot="2175974571"
						data-matched-content-ui-type="image_stacked"
						data-matched-content-rows-num="2"
						data-matched-content-columns-num="8">
					</ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
			</div>
		</div>
	</div>
	<!---------------------------------------------------------------------------->
	<!-- QUẢNG CÁO CỦA GOOGLE -->
	<div class="quang-cao-google" style="width: 100%;float: left;margin-bottom: 20px;">
		<div class="quang-cao-1-category">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 4 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="8543525730"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<div class="quang-cao-2-category">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 7 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="4258010410"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
	<!-- END -->
	<!---------------------------------------------------------------------------->
	<?php
}

function lienquan(){
	?>
	<div class="clear"></div>	
	<div class="school-area">
		<div class="school-header"><h4>Danh sách các trường Cao đẳng theo khu vực</h4></div>
		<div class="row list-area">
			<div class="col-md-3 col-xs-6 col-sm-4">
				<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-tp-ha-noi/"><img src="<?php bloginfo( 'stylesheet_directory'); ?>/images/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-tp-ha-noi.jpg"></a>
				<h4><a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-tp-ha-noi/">Danh sách các trường Trung cấp khu vực Hà Nội</a></h4>
			</div>			
			<div class="col-md-3 col-xs-6 col-sm-4">
				<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-bac/"><img src="<?php bloginfo( 'stylesheet_directory'); ?>/images/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-tp-ha-noi.jpg"></a>
				<h4><a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-bac/">Danh sách các trường Trung cấp khu vực Miền Bắc</a></h4>
			</div>
			<div class="col-md-3 col-xs-6 col-sm-4">
				<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-trung/"><img src="<?php bloginfo( 'stylesheet_directory'); ?>/images/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-tp-ha-noi.jpg"></a>
				<h4><a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-trung/">Danh sách các trường Trung cấp khu vực Miền Trung</a></h4>
			</div>
			<div class="col-md-3 col-xs-6 col-sm-4">
				<a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-nam/"><img src="<?php bloginfo( 'stylesheet_directory'); ?>/images/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-tp-ha-noi.jpg"></a>
				<h4><a href="http://kenhtuyensinh24h.vn/trung-cap/trung-cap-khu-vuc-mien-nam/">Danh sách các trường Trung cấp khu vực Miền Nam</a></h4>
			</div>
		</div>
	</div>
	<?php
}

add_action('wp_head', 'bk_banner_scroll');

genesis();