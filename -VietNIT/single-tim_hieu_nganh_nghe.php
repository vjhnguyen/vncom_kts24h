<?php

add_filter( 'the_content', 'prefix_insert_post_ads' );
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action("genesis_loop","tintuc");
add_action( 'genesis_loop', 'genesis_get_comments_template' );
add_action("genesis_after_loop","nganhnghe_ccm");

function tintuc(){
	?>
	<div id="tin-tuc">
		<div class="wrap-news">
			<div class="main-news">
				<?php 
					while(have_posts()):the_post();
				?>
					<h1 class="entry-title">
						<i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php the_title(); ?>
					</h1>
					<span class="vcard author">
						<span class="fn" style="display: none;"><?php the_author(); ?></span>
					</span>
					<div class="breadrum">
					<?php the_breadcrumb(); ?>
						<div class="socialbutton">					
							<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
							
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink(); ?>">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

							<!-- Đặt thẻ này vào phần đầu hoặc ngay trước thẻ đóng phần nội dung của bạn. -->
							<script src="https://apis.google.com/js/platform.js" async defer>
							  {lang: 'vi'}
							</script>

							<!-- Đặt thẻ này vào nơi bạn muốn nút chia sẻ kết xuất. -->
							<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<div class="likebox">
						<div class="liketext">Bạn thích bài viết này ? </div>
						<div class="likebutton">
							<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>										
							<!-- Đặt thẻ này vào nơi bạn muốn Nút +1 kết xuất. -->
							<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
						</div>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
			<br/>
			<!-- ---------------- Hiện google ---------------- -->
			<div class="slider-home">
				<div class="box4T-bottom-home">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<ins class="adsbygoogle"
								style="display:block"
								data-ad-format="autorelaxed"
								data-ad-client="ca-pub-1895892504965300"
								data-ad-slot="7385791420"
								data-matched-content-ui-type="image_stacked"
								data-matched-content-rows-num="2"
								data-matched-content-columns-num="6">
							</ins>
							<script>
								(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
						</div>
					</div>
				</div>
			</div>
			<!-- END hiện google -->
		</div>
	</div>	
	<?php
}

//Chèn bài liên quan vào giữa nội dung 
function prefix_insert_post_ads( $content ) {
		$post1=types_render_field( "post-1", array( ) );
		$link1=types_render_field( "link-post-1", array( ) );
		$post2=types_render_field( "post-2", array( ) );
		$link2=types_render_field( "link-post-2", array( ) );
		$post3=types_render_field( "post-3", array( ) );
		$link3=types_render_field( "link-post-3", array( ) );
		if(isset($post1) && !empty($link1)){
		$related_posts= '
		<div id="ct-morelink">
		<p><span><a class="star" href="'.$link1.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post1.'</a></span></p>
		<p><span><a class="star" href="'.$link2.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post2.'</a></span></p>
		</div>
		';	
		}
		else{
			$related_posts="";
		}
        
 
        if ( is_single() ) {
                return count_paragraph( $related_posts, 1, $content );
        }
 
        return $content;
}

// cung chuyen muc
function nganhnghe_ccm(){
	
	global $post;
	$terms = get_the_terms( get_the_ID(), 'muc_tim_hieu_nganh_nghe');
	$my_query = new WP_Query(
		array(
			'post_type' => 'tim_hieu_nganh_nghe',
			'showposts' => 10,
			'post__not_in' => array( $post->ID )
		)
	);
	 // loop over query
	if( $my_query->have_posts() ):
	?>
	<!---------------------------------------------------------------------------->
	<!-- QUẢNG CÁO CỦA GOOGLE -->
	<div class="quang-cao-google" style="width: 100%;float: left;margin-bottom: 20px;">
		<div class="quang-cao-1" style="width: 49%;float: left;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 4 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="8543525730"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<div class="quang-cao-2" style="width: 49%;float: right;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 7 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="4258010410"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
	<!-- END -->
	<!---------------------------------------------------------------------------->
	<div id="boxNewest" class="slwrap clearfix">
		<b class="sltitle">Bài viết cùng chuyên mục</b>
		<ul>
			<?php
			while( $my_query->have_posts()):$my_query->the_post(); ?>
				<li>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</li>
			<?php endwhile; wp_reset_postdata(); ?>
		</ul>
	</div>
	<?php  endif;
}

genesis();