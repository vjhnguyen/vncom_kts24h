<?php

global $post;
$categories = get_the_category( $post->ID);
$cat = $categories[0]->cat_ID;

if(has_post_format( 'aside' )){
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action('genesis_loop','main_content' );
	add_action('genesis_before_content','nut_information');
	//add_action('genesis_after_loop', 'tag');
	add_action('genesis_after_loop','line');
	add_action( 'genesis_after_loop', 'genesis_get_comments_template' );
	add_action("genesis_after_content_sidebar_wrap","tinlienquan");
	add_action("genesis_after_loop","nhomnganh");
	add_action('wp_head','position2');
	add_filter( 'the_content', 'prefix_insert_post_ads' );
}
elseif($cat==3 || $cat==232 || $cat==203 || $cat==870 || $cat==869 || $cat==868 || $cat==7901 || $cat==8479 || $cat==7893 || $cat==8501 || $cat==8500 || $cat==8498 || $cat==8497 || $cat==8496 || $cat==8495 || $cat==7900 || $cat==7904 || $cat==7899 || $cat==7903 || $cat==7891 || $cat==7898 || $cat==7902 || $cat==7897 || $cat==7889 || $cat==8494 || $cat==8493 || $cat==7896 || $cat==7888 || $cat==7895 || $cat==7894 || $cat==9596 ){
	add_filter( 'the_content', 'prefix_insert_post_ads' );
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action("genesis_loop","tintuc");
	add_action('genesis_after_loop','line');
	//add_action( 'genesis_loop', 'genesis_get_comments_template' );
	add_action("genesis_after_loop","cungchuyenmuc");
	// add_action("genesis_after_content","chuyenmuc");

	//add_action('wp_head','position1');
	add_action("genesis_after_content_sidebar_wrap","involve_tag");
}
elseif($cat==84 || $cat==76 || $cat==85){
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action('genesis_loop','main_content_lienthong' );
	add_action('genesis_before_content','information');
	//add_action('genesis_after_loop', 'tag');
	add_action('genesis_after_loop','line');
	add_action( 'genesis_after_loop', 'genesis_get_comments_template' );
	add_action("genesis_after_content_sidebar_wrap","tinlienquan");
	add_action("genesis_after_loop","cungchuyenmucvb2");
	add_action('genesis_before_content_sidebar_wrap',"opendom" );
	add_action('genesis_after_content_sidebar_wrap',"closedom" );
	add_action('wp_head','position2');

	function opendom(){
		?>
		<div id="lienthong">
		<?php
	}
	function closedom(){
		?>
		</div>
		<?php
	}
}
else{
	remove_action( 'genesis_loop', 'genesis_do_loop' );	
	add_action("genesis_loop","tintuc");
	//add_action('wp_head','position1');
}

add_action("wp_head","olwslider");
function olwslider(){
	echo '<script src="'.CHILD_URL.'/js/olw/owl.carousel.js"></script>';	
	echo '<link type="text/css" href="'.CHILD_URL.'/css/olw/owl.carousel.css" rel="stylesheet" />';
	echo '<link type="text/css" href="'.CHILD_URL.'/css/olw/owl.theme.css" rel="stylesheet" />';
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			var owl = $(".slcontent");
			owl.owlCarousel({
				items : 4, //10 items above 1000px browser width
				itemsDesktop : [1000,5], //5 items between 1000px and 901px
				itemsDesktopSmall : [900,3], // betweem 900px and 601px
				itemsTablet: [600,2], //2 items between 600 and 0
				itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
			});
		 
			// Custom Navigation Events
			$(".next").click(function(){
				owl.trigger('owl.next');
			})
			$(".prev").click(function(){
				owl.trigger('owl.prev');
			})
		});
	</script>	    
	<?php
}

function line(){
	?>
	<div class="linearea"></div>
	<?php
}
function position1(){
	?>
	<script>
    $(function(){
	    var elementPosition = $('.linearea').offset().top;
	    var elementPosition2 = $('#caia-post-list-9').offset().top;
    	console.log(elementPosition-250);
    	console.log(elementPosition2-200);
    	$(window).on('scroll', function(){
			if( $(window).scrollTop()>elementPosition2-200 && $(window).scrollTop() < elementPosition-650 ){
				$('#caia-post-list-9').addClass('fixed');
				$('#caia-post-list-9').removeClass('fixedp');
			}
			else if($(window).scrollTop()>elementPosition-650){
				$('#caia-post-list-9').removeClass('fixed');
				$('#caia-post-list-9').addClass('fixedp');
			}
			else{
				$('#caia-post-list-9').removeClass('fixedp');
				$('#caia-post-list-9').removeClass('fixed');
			}
		});
		$(window).on('scroll', function(){
			if( $(window).scrollTop()>1670 && $(window).scrollTop()< elementPosition-400){
				$('#text-23').addClass('fixed2');
				$('#text-23').removeClass('fixed2p');
			}
			else if($(window).scrollTop() > elementPosition-400){
				$('#text-23').removeClass('fixed2');
				$('#text-23').addClass('fixed2p');
			}
			else{
				$('#text-23').removeClass('fixed2');
				$('#text-23').removeClass('fixed2p');
			}
		});
	});
    </script>
	<?php
}

function position2(){
	?>
	<script>
		$(function(){
			var elementPosition = $('#nhomnganh').offset().top;
			//console.log(elementPosition);    	    	
			$(window).on('scroll', function(){
				if( $(window).scrollTop()>1600 && $(window).scrollTop()< elementPosition-500){
					$('#text-23').removeClass('fixedtb');
					$('#text-23').addClass('fixed2');
				}
				else if($(window).scrollTop()>elementPosition-500){
					$('#text-23').removeClass('fixed2');
					$('#text-23').addClass('fixedtb');
				}
				else{
					$('#text-23').removeClass('fixed2');
					$('#text-23').removeClass('fixedtb');
				}
			});
		});
    </script>
	<?php
}

/*
 * SANG MINI thêm ngày 22/11/2017
 */
function information(){
		while(have_posts()):the_post();
		$hinhanhtruong= types_render_field( "hinh-anh-truong", array( "alt" => "hình ảnh trường", "class" => "img-responsive", "proportional" => "true" ) );
		$kihieu=types_render_field( "ki-hieu-truong", array( ) );
		$loaihinhdt=types_render_field( "loai-hinh-dao-tao", array( ) );
		$diachi=types_render_field( "dia-chi", array( ) );
		$web=types_render_field( "website", array( ) );
		$phone=types_render_field( "dien-thoai", array( ) );
	?>
	<div id="main-info">
	<div class="top-info">
			<div class="hinh-anh-truong">
				<?php echo $hinhanhtruong; ?>
			</div>
			<div class="desc-info">
				<h1 class="info-title entry-title updated"><?php the_title(); ?></h1>
				<p class="stylekh"><strong>Mã trường:</strong> <?php echo $kihieu; ?></p>
				<p><strong>Loại hình đào tạo:</strong> <?php echo $loaihinhdt; ?></p>
				<p><strong>Địa chỉ:</strong> <?php echo $diachi; ?></p>
				<p><strong>Website:</strong> <?php echo $web; ?></p>
				<p><strong>Điện thoại:</strong> <span style="font-weight: 700;"><?php echo $phone; ?></span></p>
			</div>
	</div><!--end top-info -->
	</div>
	<?php endwhile; wp_reset_postdata(); ?>
	<?php
}
/* END */

/** Sang mini thêm nút cho bài thông báo : ngày 10/01/2018 **/
function nut_information(){
		while(have_posts()):the_post();
		$hinhanhtruong= types_render_field( "hinh-anh-truong", array( "alt" => "hình ảnh trường", "class" => "img-responsive", "proportional" => "true" ) );
		$kihieu=types_render_field( "ki-hieu-truong", array( ) );
		$loaihinhdt=types_render_field( "loai-hinh-dao-tao", array( ) );
		$diachi=types_render_field( "dia-chi", array( ) );
		$web=types_render_field( "website", array( ) );
		$phone=types_render_field( "dien-thoai", array( ) );
	?>
	<div id="main-info">
	<div class="top-info">
			<div class="hinh-anh-truong">
				<?php echo $hinhanhtruong; ?>
			</div>
			<div class="desc-info">
				<h1 class="info-title entry-title updated"><?php the_title(); ?></h1>
				<div style="width: 100%;float: left;margin-bottom: 10px;">
					<a href="#">
						<div class="butom-gioi-thieu">VIDEO GIỚI THIỆU TRƯỜNG</div>
					</a>
					<a href="https://kenhtuyensinh24h.vn/luyen-thi/">
						<div class="tai-lieu-on-mien-phi">NHẬN TÀI LIỆU ÔN MIỄN PHÍ</div>
					</a>
				</div>
				<p class="stylekh"><strong>Mã trường:</strong> <?php echo $kihieu; ?></p>
				<p class="stylekh"><strong>Loại hình đào tạo:</strong> <?php echo $loaihinhdt; ?></p>
				<p class="stylekh"><strong>Địa chỉ:</strong> <?php echo $diachi; ?></p>
				<p class="stylekh"><strong>Website:</strong> <?php echo $web; ?></p>
				<p class="stylekh"><strong>Điện thoại:</strong> <span style="font-weight: 700;"><?php echo $phone; ?></span></p>
			</div>
	</div><!--end top-info -->
	</div>
	<?php endwhile; wp_reset_postdata(); ?>
	<?php
}
/** END **/

function main_content(){
	?>
	<div id="main-info">
		<div class="wrap-info">
			<?php 
				while(have_posts()):the_post();
				$hinhanhtruong= types_render_field( "hinh-anh-truong", array( "alt" => "hình ảnh trường", "class" => "img-responsive", "proportional" => "true" ) );
				$kihieu=types_render_field( "ki-hieu-truong", array( ) );
				$loaihinhdt=types_render_field( "loai-hinh-dao-tao", array( ) );
				$diachi=types_render_field( "dia-chi", array( ) );
				$web=types_render_field( "website", array( ) );
				$phone=types_render_field( "dien-thoai", array( ) );
			?>
			<div class="under-info">
				<div class="block-gd">
					<?php // if(is_single('21273')){ ?>
					<div class="clear"></div>
					<div class="bk-video">
						<?php if ( is_active_sidebar( 'bk-video' ) && !dynamic_sidebar('bk-video') ) : endif; ?>
					</div>
					<?php // } ?>
				</div> <!--end block gd -->
				
				<div class="block-nd entry-content">
					<?php the_content(); ?>
				</div>
				<br/>
				<div class="likebox" style="width: 100%;float: left;">
					<div class="liketext">Bạn thích bài viết này ? </div>
					<div class="likebutton">
						<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>										
						<!-- Đặt thẻ này vào nơi bạn muốn Nút +1 kết xuất. -->
						<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
					</div>
				</div>
			</div><!--end under-info -->
		<?php endwhile; wp_reset_postdata(); ?>
		</div><!--end wrap-info -->
		<!-- ---------------- Hiện google ---------------- -->
		<div class="slider-home" style="margin-top: 20px;">
			<div class="box4T-bottom-home">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							style="display:block"
							data-ad-format="autorelaxed"
							data-ad-client="ca-pub-1895892504965300"
							data-ad-slot="2175974571"
							data-matched-content-ui-type="image_stacked"
							data-matched-content-rows-num="2"
							data-matched-content-columns-num="8">
						</ins>
						<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					</div>
				</div>
			</div>
		</div>
	</div><!--end main-info -->
	<?php
}
//end main content

function main_content_lienthong(){
	?>
	<div id="main-info">
		<div class="wrap-info">
		<?php 
			while(have_posts()):the_post();
			$hinhanhtruong= types_render_field( "hinh-anh-truong", array( "alt" => "hình ảnh trường", "class" => "img-responsive", "proportional" => "true" ) );
			$kihieu=types_render_field( "ki-hieu-truong", array( ) );
			$loaihinhdt=types_render_field( "loai-hinh-dao-tao", array( ) );
			$diachi=types_render_field( "dia-chi", array( ) );
			$web=types_render_field( "website", array( ) );
			$phone=types_render_field( "dien-thoai", array( ) );
			?>
			<div class="under-info">
				<div class="block-nd entry-content">
					<?php the_content(); ?>
				</div>
				<br/>
				<div class="likebox" style="width: 100%;float: left;">
					<div class="liketext">Bạn thích bài viết này ? </div>
					<div class="likebutton">
						<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>										
						<!-- Đặt thẻ này vào nơi bạn muốn Nút +1 kết xuất. -->
						<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
					</div>
				</div>
			</div><!--end under-info -->

		<?php endwhile; wp_reset_postdata(); ?>
		</div><!--end wrap-info -->
		<!-- ---------------- Hiện google ---------------- -->
		<div class="slider-home" style="margin-top: 20px;">
			<div class="box4T-bottom-home">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							style="display:block"
							data-ad-format="autorelaxed"
							data-ad-client="ca-pub-1895892504965300"
							data-ad-slot="2175974571"
							data-matched-content-ui-type="image_stacked"
							data-matched-content-rows-num="2"
							data-matched-content-columns-num="8">
						</ins>
						<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					</div>
				</div>
			</div>
		</div>
	</div><!--end main-info -->
	<?php
}
//end main content lienthong

// bai viet cung tag với mục tin tuc hien thi trong cac bai thong bao
function tinlienquan(){
	global $post;
	$tags = wp_get_post_tags($post->ID);
	if ($tags) {
	$tag_ids = array();				
	foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
	$args = array(
		'tag__in' => $tag_ids,
		'post__not_in' => array($post->ID),
		'posts_per_page'=>20, // Number of related posts that will be shown.
		'cat' => 3,
		'caller_get_posts'=>1
	);
	$my_query = new wp_query( $args );
	if( $my_query->have_posts() ):
	?>
	<div id="boxNewest" class="slwrap clearfix">
		<b class="sltitle">Tin liên quan</b>
		<ul class="slcontent">
			<?php
			while( $my_query->have_posts()):$my_query->the_post(); ?>
			<li class="item">
				<a class="avatar" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>				
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			</li>
			<?php endwhile; wp_reset_postdata(); ?>
		</ul>
		<p class="slprev"><a class="prev">Prev</a></p>
		<p class="slnext"><a class="next">Next</a></p>						
	</div>
	<?php  endif; }
}

// bai viet cung nhom nganh vung mien hien thị trong cac bai thong bao
function nhomnganh(){
	?>
	<!---------------------------------------------------------------------------->
	<!-- QUẢNG CÁO CỦA GOOGLE -->
	<div class="quang-cao-google" style="width: 100%;float: left;margin-bottom: 20px;">
		<div class="quang-cao-1" style="width: 49%;float: left;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 4 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="8543525730"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<div class="quang-cao-2" style="width: 49%;float: right;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 7 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="4258010410"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
	<!-- END -->
	<!---------------------------------------------------------------------------->

	<?php
		global $post;
		$categories = get_the_category( $post->ID);
		$cat = $categories[0]->cat_ID;
		$args=array(
			'post__not_in' => array($post->ID),
			'posts_per_page'=>10, // Number of related posts that will be shown.
			'cat'=> $cat,
			//$t->term_id
			'caller_get_posts'=>1
		);
		$my_query = new wp_query( $args );
		if( $my_query->have_posts() ):
	?>
	<div id="boxNewest" class="slwrap clearfix">
		<b class="sltitle">Các trường khác tuyển sinh cùng nhóm ngành</b>
		<ul>
			<?php
				while( $my_query->have_posts()):$my_query->the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</li>
			<?php endwhile; wp_reset_postdata(); ?>
		</ul>
		<?php  endif;
		// end if đại học
		?>
	</div>
<?php
}
add_action('genesis_after_post', 'tag');
function tag() {
echo '<div class="t" >' ;
 the_tags('Tag: ' , ','); 
 echo '</div>' ;
};


//template tin tuc 
function tintuc(){
	?>
	<div id="tin-tuc">
		<div class="wrap-news">
			<div class="main-news">
				<?php 
					while(have_posts()):the_post();
				?>
					<h1 class="entry-title updated">
						<i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php the_title(); ?>
					</h1>
					<span class="vcard author">
						<span class="fn" style="display: none;"><?php the_author(); ?></span>
					</span>
					<div class="breadrum">
					<?php the_breadcrumb(); ?>
						<div class="socialbutton">
							<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink(); ?>">Twitter</a>
							<script>
								function(d,s,id) {
									var js,fjs=d.getElementsByTagName(s)[0],
									p=/^http:/.test(d.location)?'http':'https';
									if(!d.getElementById(id)){
										js=d.createElement(s);
										js.id=id;
										js.src=p+'://platform.twitter.com/widgets.js';
										fjs.parentNode.insertBefore(js,fjs);
									}
								}(document, 'script', 'twitter-wjs');
							</script>

							<!-- Đặt thẻ này vào phần đầu hoặc ngay trước thẻ đóng phần nội dung của bạn. -->
							<script src="https://apis.google.com/js/platform.js" async defer>
								{lang: 'vi'}
							</script>

							<!-- Đặt thẻ này vào nơi bạn muốn nút chia sẻ kết xuất. -->
							<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<div class="likebox">
						<div class="liketext">Bạn thích bài viết này ? </div>
						<div class="likebutton">
							<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>										
							<!-- Đặt thẻ này vào nơi bạn muốn Nút +1 kết xuất. -->
							<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
						</div>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
			<br/>
			<!-- ---------------- Hiện google ---------------- -->
			<div class="slider-home">
				<div class="box4T-bottom-home">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<ins class="adsbygoogle"
								style="display:block"
								data-ad-format="autorelaxed"
								data-ad-client="ca-pub-1895892504965300"
								data-ad-slot="7385791420"
								data-matched-content-ui-type="image_stacked"
								data-matched-content-rows-num="2"
								data-matched-content-columns-num="8">
							</ins>
							<script>
								(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
						</div>
					</div>
				</div>
			</div>
			<!-- END hiện google -->
			<?php comments_template(); ?>
		</div>
	</div>	
	<?php
}
//Chèn bài liên quan vào giữa nội dung 
function prefix_insert_post_ads( $content ) {
		$post1=types_render_field( "post-1", array( ) );
		$link1=types_render_field( "link-post-1", array( ) );
		$post2=types_render_field( "post-2", array( ) );
		$link2=types_render_field( "link-post-2", array( ) );
		$post3=types_render_field( "post-3", array( ) );
		$link3=types_render_field( "link-post-3", array( ) );
		if(isset($post1) && !empty($link1)){
			$related_posts= '
			<div id="ct-morelink">
				<p><span><a class="star" href="'.$link1.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post1.'</a></span></p>
				<p><span><a class="star" href="'.$link2.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post2.'</a></span></p>
				<p><span><a class="star" href="'.$link3.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post3.'</a></span></p>
			</div>
			';
		}
		else{
			$related_posts="";
		}
        if ( is_single() ) {
            return count_paragraph( $related_posts, 1, $content );
        }
        return $content;
}

// hien thi bai viet cung chuyen muc trong post tin tuc
function cungchuyenmuc(){
	global $post;
	$categories = get_the_category( $post->ID);
	$cat = $categories[0]->cat_ID;
	
	$args=array(				
		'post__not_in' => array($post->ID),
		'posts_per_page'=>10, // Number of related posts that will be shown.
		'cat' => $cat,
		'caller_get_posts'=>1
	);
	$my_query = new wp_query( $args );
	if( $my_query->have_posts() ):
	?>
	<!---------------------------------------------------------------------------->
	<!-- QUẢNG CÁO CỦA GOOGLE -->
	<div class="quang-cao-google" style="width: 100%;float: left;margin-bottom: 20px;">
		<div class="quang-cao-1" style="width: 49%;float: left;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 4 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="8543525730"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<div class="quang-cao-2" style="width: 49%;float: right;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 7 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="4258010410"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
	<!-- END -->
	<!---------------------------------------------------------------------------->
	<div id="boxNewest" class="slwrap clearfix">
		<b class="sltitle">Bài viết cùng chuyên mục</b>
		<ul>
			<?php
			while( $my_query->have_posts()):$my_query->the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</li>
			<?php endwhile; wp_reset_postdata(); ?>
		</ul>
	</div>
	<?php  endif;

	// chuyen mục chân trang chi tiết bài viết

	?>
	<div style="clear: both;"></div>
	<div style="margin-top:10px; width: 100%;height: auto;">
	<div id="mobile-centent">
		<!-- Tin tức 1 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/van-bang-2-sau-dai-hoc/" title="Văn bằng 2">
					Văn bằng 2
				</a>
				<a href="https://kenhtuyensinh24h.vn/van-bang-2-sau-dai-hoc/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=76&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=76&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 2 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-tuc/" title="Văn bằng 2">
					Giáo dục và đào tạo
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-tuc/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=3&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=3&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 3 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/thi-thpt-quoc-gia/" title="Văn bằng 2">
					Thi THPT quốc gia
				</a>
				<a href="https://kenhtuyensinh24h.vn/thi-thpt-quoc-gia/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=7889&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=7889&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 4 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/nhip-song-tre/" title="Văn bằng 2">
					Nhịp sống trẻ
				</a>
				<a href="https://kenhtuyensinh24h.vn/nhip-song-tre/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=203&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=203&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 5 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/lien-thong-dai-hoc-2/" title="Văn bằng 2">
					Tin hệ liên thông
				</a>
				<a href="https://kenhtuyensinh24h.vn/lien-thong-dai-hoc-2/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=8498&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=8498&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 6 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/cac-khoi-thi/" title="Văn bằng 2">
					Khối thi
				</a>
				<a href="https://kenhtuyensinh24h.vn/cac-khoi-thi/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=7893&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=7893&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 7 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Văn bằng 2">
					Thủ tục hồ sơ nhập học
				</a>
				<a href="https://kenhtuyensinh24h.vn/thu-tuc-ho-so-nhap-hoc-dai-hoc/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=7904&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=7904&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		<!-- Tin tức 8 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/xet-tuyen-dot-2-he-dai-hoc/" title="Văn bằng 2">
					Nguyện vọng bổ sung
				</a>
				<a href="https://kenhtuyensinh24h.vn/xet-tuyen-dot-2-he-dai-hoc/" title="Văn bằng 2" class="xemtatca-mobile">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php 
							$args = new WP_Query('cat=7901&showposts=1');
							while($args->have_posts()): $args->the_post();
						?>
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
								<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
							</a>
							<h3 class="title-block-mobile">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div><!-- end 1 post -->						
				</div><!-- end .main-posts -->

				<ul class="older-posts-tintuc-mobile">
					<?php
					$args = new WP_Query('cat=7901&showposts=3&offset=1');
					while($args->have_posts()):$args->the_post();
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div><!-- end .block-wrap -->
		</div>
		
	</div>
		
		
	</div>
	<?php

}

// hien thi bai viet cung chuyen muc trong van bang 2
function cungchuyenmucvb2(){
	
	global $post;
	$categories = get_the_category( $post->ID);
	$cat = $categories[0]->cat_ID;

	$args=array(
		'post__not_in' => array($post->ID),
		'posts_per_page'=>10, // Number of related posts that will be shown.
		'cat' => $cat,
		'caller_get_posts'=>1
	);
	$my_query = new wp_query( $args );
	if( $my_query->have_posts() ):
	?>
	<!---------------------------------------------------------------------------->
	<!-- QUẢNG CÁO CỦA GOOGLE -->
	<div class="quang-cao-google" style="width: 100%;float: left;margin-bottom: 20px;">
		<div class="quang-cao-1" style="width: 49%;float: left;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 4 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="8543525730"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<div class="quang-cao-2" style="width: 49%;float: right;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 7 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="4258010410"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
	<!-- END -->
	<!---------------------------------------------------------------------------->
	<div id="boxNewest" class="slwrap clearfix">
		<b class="sltitle">Bài viết cùng chuyên mục</b>
		<ul>
			<?php
			while( $my_query->have_posts()):$my_query->the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</li>
			<?php endwhile; wp_reset_postdata(); ?>
		</ul>
	</div>
	<?php  endif; // end if đại học
}

// Hiển thị bài viết theo cùng tag
function involve_tag(){
	?>
	<!-- Hiển thị bài viết theo Tag -->
	<div id="relatedposttags">    
		<?php
			$tags1 = wp_get_post_tags($post->ID);
			if ($tags1){
				$tag_ids1 = array();
				foreach($tags1 as $individual_tag) $tag_ids1[] = $individual_tag->term_id;
				// lấy danh sách các tag liên quan
				$args1=array(
					'tag__in' => $tag_ids1,
					'post__not_in' => array($post->ID), // Loại trừ bài viết hiện tại
					'showposts' => 5, // Số bài viết bạn muốn hiển thị.
					'caller_get_posts' => 1
				);
				$my_query = new wp_query($args1);
				if( $my_query->have_posts() ){
					echo '<h3>Bài viết liên quan thêm:</h3><ul>';
					while ($my_query->have_posts()) 
					{
						$my_query->the_post();
						?>
						<li>
							<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
								<?php the_title(); ?>
							</a>
						</li>
						<?php
					}
					echo '</ul>';
				}
			}
		?>
	</div>
	<?php
}
// chuyên mục cuối chi tiết bài viết

genesis();