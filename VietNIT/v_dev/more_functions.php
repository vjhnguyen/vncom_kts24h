<?php

/** Luyện thi đại học **/
add_action('genesis_before_footer', 'on_thi_truc_tuyen');
function on_thi_truc_tuyen(){
	?>
	<div class="slider-home">
		<h2 class="block-title">
			<a href="javascript: ;" title="Ôn thi trực tuyến">
				<span class="icon-block-title"></span>
				<span>Ôn thi trực tuyến</span>
			</a>
		</h2>
		<div class="menu-luyenthi">
			<div class="cac-mon-hoc">
				<div class="menu-luyenthi-menu-container">
					<ul id="menu-luyenthi-menu" class="menu">
						<li>
							<!-- <div class="col-sm-6 col-xs"></div> -->
							<a href="javascript: ;">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-toan-hoc-48x48.png" alt="icon môn toán học"  sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Toán</span>
							</a>
						</li>
						<li>
							<a href="javascript: ;">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-vat-ly-48x48.png" alt="icon môn vật lý" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Vật Lý</span>
							</a>
						</li>
						<li>
							<a href="javascript: ;">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-hoa-hoc-48x48.png" alt="icon môn hóa học"  sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Hóa Học</span>
							</a>
						</li>
						<li>
							<a href="javascript: ;">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-ngu-van-48x48.png" alt="icon môn ngữ văn"  sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Ngữ Văn</span>
							</a>
						</li>
						<li>
							<a href="javascript: ;">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-tieng-anh-48x48.png" alt="icon môn tiếng anh"  sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Tiếng Anh</span>
							</a>
						</li>
						<li>
							<a href="javascript: ;">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-sinh-hoc-48x48.png" alt="icon môn sinh học"  sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Sinh Học</span>
							</a>
						</li>
						<li>
							<a href="javascript: ;">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-dia-ly-48x48.png" alt="icon môn địa lý" sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Địa Lý</span>
							</a>
						</li>
						<li>
							<a href="javascript: ;">
								<span class="menu-image-hover-wrapper">
									<img width="48" height="48" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-lich-su-48x48.png" alt="icon môn tiếng anh"  sizes="(max-width: 48px) 100vw, 48px">
								</span>
								<span class="menu-image-title">Môn Lịch Sử</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="giao-vien">
			<ul class="giao-vien-ul">
				<li class="giao-vien-li">
					<div class="col">
						<div class="images">
							<a title="Khởi động môn toán" href="javascript: ;">
								<img class="image" alt="Facebook Marketing từ A - Z" src="<?php echo get_stylesheet_directory_uri() ?>/images/MON-TOAN.png">
							</a>
							<div class="middle">
								<a title="Môn toán" href="javascript: ;"></a>
								<ul class="list-group hover_img">
									<a title="Môn toán" href="javascript: ;"></a>
									<center>
										<a title="môn toán" href="javascript: ;"></a>
										<a href="javascript: ;" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="javascript: ;"><b style="font-size: 13px;">Khóa khởi động môn toán</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="javascript: ;">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="javascript: ;">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
				
				<li>
					<div class="col">
						<div class="images">
							<a title="Facebook Marketing từ A - Z" href="javascript: ;">
								<img class="image" alt="Facebook Marketing từ A - Z" src="<?php echo get_stylesheet_directory_uri() ?>/images/Vat-ly.png">
							</a>
							<div class="middle">
								<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
								<ul class="list-group hover_img">
									<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
									<center>
										<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
										<a href="javascript: ;" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="javascript: ;"><b style="font-size: 13px;">Khóa khởi động môn vật lý</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="javascript: ;">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="javascript: ;">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
				
				<li>
					<div class="col">
						<div class="images">
							<a title="Facebook Marketing từ A - Z" href="javascript: ;">
								<img class="image" alt="Facebook Marketing từ A - Z" src="<?php echo get_stylesheet_directory_uri() ?>/images/HOA-HOC.png">
							</a>
							<div class="middle">
								<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
								<ul class="list-group hover_img">
									<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
									<center>
										<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
										<a href="javascript: ;" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="javascript: ;"><b style="font-size: 13px;">Khóa khởi động môn hóa học</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="javascript: ;">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="javascript: ;">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
				
				<li class="giao-vien-li">
					<div class="col">
						<div class="images">
							<a title="Facebook Marketing từ A - Z" href="javascript: ;">
								<img class="image" alt="Facebook Marketing từ A - Z" src="<?php echo get_stylesheet_directory_uri() ?>/images/van-hoc.png">
							</a>
							<div class="middle">
								<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
								<ul class="list-group hover_img">
									<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
									<center>
										<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
										<a href="javascript: ;" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="javascript: ;"><b style="font-size: 13px;">Khóa khởi động môn ngữ văn</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="javascript: ;">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="javascript: ;">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
				
				<li class="giao-vien-li">
					<div class="col">
						<div class="images">
							<a title="Facebook Marketing từ A - Z" href="javascript: ;">
								<img class="image" alt="Facebook Marketing từ A - Z" src="<?php echo get_stylesheet_directory_uri() ?>/images/tieng-anh.png">
							</a>
							<div class="middle">
								<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
								<ul class="list-group hover_img">
									<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
									<center>
										<a title="Facebook Marketing từ A - Z" href="javascript: ;"></a>
										<a href="javascript: ;" class="btn btn-primary">Xem chi tiết</a>
									</center>
								</ul>
							</div>
						</div>
						<div class="title">
							<a title="Facebook Marketing từ A - Z" href="javascript: ;"><b style="font-size: 13px;">Khóa khởi động môn tiếng anh</b></a>
						</div>
						<div class="star">
							<div class="number">
								<a href="javascript: ;">Giảng viên</a>
							</div>
							<div class="text person">
								<ul class="item-list">
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li class="color_ghi"><i class="fa fa-star" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="price">
							<span class="sell-price">100,000<sup>đ</sup></span>
							<a href="javascript: ;">
								<div class="btn-dk">
									Đăng Kí
								</div>
							</a>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<?php
}



 /** Move On Top Button */
add_action('genesis_before_footer', 'move_top_button');
function move_top_button(){
	?>
	<div id="return-to-top">
		<img src="https://kenhtuyensinh24h.vn/wp-content/uploads/2018/07/arrow3.png" alt="black cartoon arrow">
	</div>
	<style>
	#return-to-top {
		position: fixed;
		bottom: 20px;
		right: 20px;
		width: 50px;
		height: 50px;
		display: block;
		text-decoration: none;
		-webkit-border-radius: 35px;
		-moz-border-radius: 35px;
		border-radius: 35px;
		display: none;
		-webkit-transition: all 0.3s linear;
		-moz-transition: all 0.3s ease;
		-ms-transition: all 0.3s ease;
		-o-transition: all 0.3s ease;
		transition: all 0.3s ease;
		cursor: pointer;
	}
	</style>
	<script>
	jQuery(document).ready(function ($) {
		$(document).ready(function () {
			// ===== Scroll to Top ==== 
			$(window).scroll(function () {
				if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
					$('#return-to-top').fadeIn(200); // Fade in the arrow
				} else {
					$('#return-to-top').fadeOut(200); // Else fade out the arrow
				}
			});
			$('#return-to-top').click(function () { // When arrow is clicked
				$('body,html').animate({
					scrollTop: 0 // Scroll to top of body
				}, 500);
			});
		});
	});
	</script>
	<?php
}




/* Lọc với AJAX */
add_action('wp_ajax_getpost', 'get_posts_by_term');
add_action('wp_ajax_nopriv_getpost', 'get_posts_by_term');
function get_posts_by_term() {
	// $term_id = $_POST[ 'cat_id' ];
	$term_id = isset($_POST['term_id']) ? (int)$_POST['term_id'] : 10013;
	$cat_parent = isset($_POST['cat_parent']) ? (int)$_POST['cat_parent'] : 84;
	$args = array(
		'posts_per_page' => -1,
		'cat' => $cat_parent,
		'tax_query' => array(
			array(
				'taxonomy' => 'khoi_nganh',
				'field' => 'id',
				'terms' => $term_id,
			),
		),
	);

	global $post;
	$myposts = get_posts( $args );
	ob_start (); ?>

	<?php foreach( $myposts as $post ) : setup_postdata($post); ?>
		<div class="itemblock">
			<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
			<?php the_title(); ?>
			</a>
		</div>
	<?php endforeach; ?>

	<?php wp_reset_postdata(); 
	$response = ob_get_contents();
	ob_end_clean();
	echo $response;
	die(1);
}




/* Block top - above logo */
add_action("genesis_before_header","blocktop",2);
function blocktop(){
	?>
	<div id="blocktop">
		<div class="blocktop-centent">
			<div class="date">
				<div class="below_body">
					<span class="icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
					<span id="clock">Loading...</span>
					<script type="text/javascript">
						function refrClock() {
							var d=new Date();
							var s=d.getSeconds();
							var m=d.getMinutes();
							var h=d.getHours();
							var day=d.getDay();
							var date=d.getDate();
							var month=d.getMonth();
							var year=d.getFullYear();
							var days=new Array("Chủ nhật","Thứ hai","Thứ 3","Thứ 4","Thứ 5","Thứ 6","Thứ 7");
							var months=new Array("1","2","3","4","5","6","7","8","9","10","11","12");
							var am_pm;
							if (s<10) {s="0" + s}
							if (m<10) {m="0" + m}
							if (h>12) {h-=12;AM_PM = "PM"}
							else {AM_PM="AM"}
							if (h<10) {h="0" + h}
							document.getElementById("clock").innerHTML=days[day] + " Ngày " + date + "/" +months[month] + "/" + year + " || "+ "  " + h + ":" + m + ":" + s + " " + AM_PM;
							setTimeout("refrClock()",1000);
						}
						refrClock();
					</script>
				</div>
			</div>		
			<div class="mntop">
                <div style="float: inherit;">
                    <div class="menu-blocktop-container">
                        <ul id="menu-blocktop" class="menu">
                            <li rel="nofollow" id="menu-item-32186" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-32186">
                                <a ref="nofollow" href="<?php echo get_home_url(); ?>/hop-tac-tuyen-sinh-cung-kenhtuyensinh24h-vn/">Hợp tác tuyển sinh</a>
                            </li>
                            <li rel="nofollow" id="menu-item-6913" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6913">
                                <a ref="nofollow" href="<?php echo get_home_url(); ?>/chinh-sach-bao-mat/">Chính sách bảo mật</a>
                            </li>
                            <li rel="nofollow" id="menu-item-7169" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7169">
                                <a ref="nofollow" href="<?php echo get_home_url(); ?>/lien-he-quang-cao/">Liên hệ Quảng Cáo</a>
                            </li>
                        </ul>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<?php
}