<?php

/* Search in Theme Customizer */
function ggsearch_customize_register( $wp_customize ) {
    // Add and manipulate theme images to be used.
    $wp_customize->add_section('ggsearch', array(
		"title" => 'Mã chèn trước tiêu đề Post',
		"priority" => 28,
		"description" => __( 'Ví dụ như mã quảng cáo, mã google search...', 'theme-slug' )
    ));
    $wp_customize->add_setting('ggsearch_code', array(
        'default' => '',
        'type' => 'theme_mod',
    ));
    $wp_customize->add_control('ggsearch_code', array(
		'type' => 'textarea',
		'label' => __( 'Nội dung', 'theme-slug' ),
		'section' => 'ggsearch',
    ));
}

add_action( 'customize_register', 'ggsearch_customize_register' );


/* Theme customizer link liên quan cho toàn trang */
function links_customize_register( $wp_customize ) {
    // Add and manipulate theme images to be used.
    $wp_customize->add_section('imageoner', array(
    "title" => 'Links trong toàn post',
    "priority" => 28,
    "description" => __( 'Điền link và tiêu đề tương ứng', 'theme-slug' )
    ));

	for( $i=1; $i<=4; $i++){
		$wp_customize->add_setting("link_related_{$i}", array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));
		$wp_customize->add_control("link_related_{$i}", array(
		'label' => __( "Link {$i}", 'theme-slug' ),
		'section' => 'imageoner',
		'settings' => "link_related_{$i}",
		));
		
		$wp_customize->add_setting("tit_related_{$i}", array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));
		$wp_customize->add_control("tit_related_{$i}", array(
		'label' => __( "Title {$i}", 'theme-slug' ),
		'section' => 'imageoner',
		'settings' => "tit_related_{$i}",
		));
	}
}

add_action( 'customize_register', 'links_customize_register' );