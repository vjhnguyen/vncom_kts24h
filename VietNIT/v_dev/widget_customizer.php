<?php

if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'          => 'Ads giữa trang Home',
		'id'            => 'banner_adv_home',
		'description'   => 'Trong "Tuyển Sinh Khối Ngành" với Kích thước 300x250px',
		'before_widget' => '<div class="banner_adv_home"><div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));
};



if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'          => 'Ads đầu các trang category',
		'id'            => 'first_adv_category',
		'description'   => 'Banner đặt Ads đầu trang category kích thước 615 x 155(px)',
		'before_widget' => '<div class="first_adv_category"><div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));
};




if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name'          => 'Ads trong các trang category',
        'id'            => 'middle_adv_category',
        'description'   => 'Banner đặt Ads giữa trang category kích thước 300x250px(px)',
        'before_widget' => '<div class="middle_adv_category"><div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));
};



if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name'          => 'Ads trang home mobile',
        'id'            => 'adv_home_moblie',
        'description'   => 'Banner đặt Ads giữa trang category kích thước 300x250px(px)',
        'before_widget' => '<div class="adv_home_moblie"><div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));
};

// Ads trong home mobile
add_action('genesis_after_header','before_featured');
function before_featured(){
	?>
	<div class="adver_top_home">
		<?php if(is_active_sidebar('adv_home_moblie') && !dynamic_sidebar('adv_home_moblie')):endif; ?>			
	</div>
	<?php
}
