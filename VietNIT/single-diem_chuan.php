<?php

add_filter( 'the_content', 'prefix_insert_post_ads' );
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action("genesis_loop","tintuc");
add_action( 'genesis_loop', 'genesis_get_comments_template' );
//add_action("genesis_after_loop","tinlienquan");
add_action("genesis_after_loop","nhomnganh");
add_action("wp_head","olwslider");
function olwslider(){
	echo '<script src="'.CHILD_URL.'/js/olw/owl.carousel.js"></script>';
	//echo '<script src="'.CHILD_URL.'/js/jquery.bxslider.min.js"></script>';
	//echo '<link type="text/css" href="'.CHILD_URL.'/css/jquery.bxslider.css" rel="stylesheet" />';
	echo '<link type="text/css" href="'.CHILD_URL.'/css/olw/owl.carousel.css" rel="stylesheet" />';
	echo '<link type="text/css" href="'.CHILD_URL.'/css/olw/owl.theme.css" rel="stylesheet" />';
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			var owl = $(".slcontent");
			owl.owlCarousel({
				items : 4, //10 items above 1000px browser width
				itemsDesktop : [1000,5], //5 items between 1000px and 901px
				itemsDesktopSmall : [900,3], // betweem 900px and 601px
				itemsTablet: [600,2], //2 items between 600 and 0
				itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
			});
		 
			// Custom Navigation Events
			$(".next").click(function(){
				owl.trigger('owl.next');
			})
			$(".prev").click(function(){
				owl.trigger('owl.prev');
			})
		});
	</script>
	<script>
    $(function(){
	    var elementPosition = $('#boxNewest').offset().top;
    	console.log(elementPosition);
    	$(window).on('scroll', function(){
			if( $(window).scrollTop()>1200 && $(window).scrollTop() < elementPosition-500 ){
				$('#caia-post-list-9').addClass('fixed');
			}
			else{
				$('#caia-post-list-9').removeClass('fixed');
			}
		});
		$(window).on('scroll', function(){
			if( $(window).scrollTop()>1600 && $(window).scrollTop()< elementPosition-500){
				$('#text-23').addClass('fixed2');
			}
			else{
				$('#text-23').removeClass('fixed2');
			}                
		});
	});
    </script>
	<?php
}
function tintuc(){
	?>
	<div id="tin-tuc">
		<div class="wrap-news">
			<div class="main-news">
				<?php 
					while(have_posts()):the_post();
				?>
					<h1 class="entry-title">
						<i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php the_title(); ?>
					</h1>
					<span class="vcard author">
						<span class="fn" style="display: none;"><?php the_author(); ?></span>
					</span>
					<div class="breadrum">
					<?php the_breadcrumb(); ?>
						<div class="socialbutton">					
							<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
							
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink(); ?>">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

							<!-- Đặt thẻ này vào phần đầu hoặc ngay trước thẻ đóng phần nội dung của bạn. -->
							<script src="https://apis.google.com/js/platform.js" async defer>
							  {lang: 'vi'}
							</script>

							<!-- Đặt thẻ này vào nơi bạn muốn nút chia sẻ kết xuất. -->
							<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<div class="likebox">
						<div class="liketext">Bạn thích bài viết này ? </div>
						<div class="likebutton">
							<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>										
							<!-- Đặt thẻ này vào nơi bạn muốn Nút +1 kết xuất. -->
							<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php the_permalink(); ?>"></div>
						</div>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
			<br/>
			<!-- ---------------- Hiện google ---------------- -->
			<div class="slider-home">
				<div class="box4T-bottom-home">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<ins class="adsbygoogle"
								style="display:block"
								data-ad-format="autorelaxed"
								data-ad-client="ca-pub-1895892504965300"
								data-ad-slot="7385791420"
								data-matched-content-ui-type="image_stacked"
								data-matched-content-rows-num="2"
								data-matched-content-columns-num="6">
							</ins>
							<script>
								(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
						</div>
					</div>
				</div>
			</div>
			<!-- END hiện google -->
			<?php comments_template(); ?>
		</div>
	</div>
	<?php
}
//Chèn bài liên quan vào giữa nội dung 
function prefix_insert_post_ads( $content ) {
	$post1=types_render_field( "post-1", array( ) );
	$link1=types_render_field( "link-post-1", array( ) );
	$post2=types_render_field( "post-2", array( ) );
	$link2=types_render_field( "link-post-2", array( ) );
	$post3=types_render_field( "post-3", array( ) );
	$link3=types_render_field( "link-post-3", array( ) );
	if(isset($post1) && !empty($link1)){
	$related_posts= '
	<div id="ct-morelink">
	<p><span><a class="star" href="'.$link1.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post1.'</a></span></p>
	<p><span><a class="star" href="'.$link2.'" target="_blank"><i class="fa fa-hand-o-right" aria-hidden="true"></i>'.$post2.'</a></span></p>
	</div>
	';	
	}
	else {
		$related_posts="";
	}
	if ( is_single() ) {
		return count_paragraph( $related_posts, 1, $content );
	}
	return $content;
}

//tin lien quan cung tag

function tinlienquan(){
	?>	
	<?php
		$orig_post = $post;
		global $post;
		$tags = wp_get_post_tags($post->ID);
		if ($tags) {
		$tag_ids = array();
		foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
		$args=array(
		'tag__in' => $tag_ids,
		'post__not_in' => array($post->ID),
		'posts_per_page'=>20, // Number of related posts that will be shown.				
		'caller_get_posts'=>1
		);
		$my_query = new wp_query( $args );
		if( $my_query->have_posts() ):
		?>
		<div id="boxNewest" class="slwrap clearfix">
			<b class="sltitle">Tin liên quan</b>
			<ul class="slcontent">
			<?php
			while( $my_query->have_posts()):$my_query->the_post(); ?>
			  <li class="item">
				<a class="avatar" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>				
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			  </li>
			<?php endwhile; wp_reset_postdata(); ?>
			</ul>
			<p class="slprev"><a class="prev">Prev</a></p>
			<p class="slnext"><a class="next">Next</a></p>
		</div>				
	<?php  endif; }	?>
	<?php
}

// cung chuyen muc

function nhomnganh(){
	
	global $post;
	//$categories = get_the_terms( $post->ID, 'muc_diem_chuan' );
	$terms = get_the_terms( get_the_ID(), 'muc_diem_chuan');
	//var_dump($terms);
	//echo $terms[1]->term_id;
	$my_query = new WP_Query(
		array(
			'post_type' => 'diem_chuan',
			'showposts' => 10,
			'tax_query' => array(
				array(
					'taxonomy' => 'muc_diem_chuan',
					'field' => 'id',
					'terms' => $terms[1]->term_id
					)
				),
			'post__not_in' => array( $post->ID ),
		)
	);
	 // loop over query				    
	if( $my_query->have_posts() ):
	?>
	<!---------------------------------------------------------------------------->
	<!-- QUẢNG CÁO CỦA GOOGLE -->
	<div class="quang-cao-google" style="width: 100%;float: left;margin-bottom: 20px;">
		<div class="quang-cao-1" style="width: 49%;float: left;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 4 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="8543525730"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<div class="quang-cao-2" style="width: 49%;float: right;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 7 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:250px"
				 data-ad-client="ca-pub-1895892504965300"
				 data-ad-slot="4258010410"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
	<!-- END -->
	<!---------------------------------------------------------------------------->
	<div id="boxNewest" class="slwrap clearfix">
		<b class="sltitle">Bài viết cùng chuyên mục</b>
		<ul>
			<?php
			while( $my_query->have_posts()):$my_query->the_post(); ?>
				<li>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</li>
			<?php endwhile; wp_reset_postdata(); ?>
		</ul>			
	</div>
	<?php  endif;
}

genesis();