<?php
/*template van bang 2 */

remove_action( 'genesis_loop', 'genesis_do_loop' );
//remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
add_action('genesis_loop','newsthongbao');
add_action('genesis_loop','danhsachkhoinganh');
add_action('genesis_before_loop','ggsearch_position');

function ggsearch_position(){
	?>
	<div class="ggsearch">
	<?php echo get_theme_mod( "ggsearch_code", '111111111' ); ?>
	</div>
	<?php
}

function newsthongbao(){
	?>
	<div class="taxonomy-count">
		<div class="archive-head">
			<div class="nums-post">
			<?php
				$category = get_the_category();
				echo '<p class="num">'.$category[0]->category_count.'</p>';
				echo '<p class="truong">Trường</p>';
			?>
			</div>
		</div>
	</div>
	<div class="news-tb">
		<div class="thongbao">
			<div class="nametype">
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span>Hệ văn bằng 2</span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
				<!-- Mobile -->
				<h2 class="block-title-mobile">
					<a href="#">
						Hệ văn bằng 2
					</a>
				</h2>
				<!-- END -->
			</div>
			<div class="listpost">
				<?php 
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 100,
						'cat'=>'76'
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
			<div id="loading-animation">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/preloader.gif" alt="">
			</div>
		</div>
	</div>
	<?php
}



function danhsachkhoinganh(){
	?>
		<ul class="list_cat_btn" >
			<?php $args = array( 
			'hide_empty' => 0,
			'taxonomy' => 'khoi_nganh',
			);
			$cates = get_categories( $args ); 
			foreach ( $cates as $cate ) { ?>
				<?php if ( $cate->count > 0 ): ?>
					<li class="list-cat" data-id="<?php echo $cate->term_id; ?>"><?php echo $cate->name ?></li>
				<?php endif; ?>	
			<?php } ?>
		</ul>
	
		<script>
		$(document).ready(function () {
			$('.list-cat').click(function () { // Khi click vào category bất kỳ
				var term_id = $(this).data('id');
	
				$('.list-cat').removeClass('active');
				$(this).addClass('active');
				$("#loading-animation").css('display', 'flex') ;
	
				$.ajax({ // Hàm ajax
					type: "post",
					dataType: "html",
					url: '<?php echo admin_url('admin-ajax.php ');?>',
					data: {
						action: "getpost",
						term_id: term_id,
						cat_parent: <?php $category = get_queried_object(); echo $category->term_id; ?>
					},
					beforeSend: function () {
						// Có thể thực hiện công việc load hình ảnh quay quay trước khi đổ dữ liệu ra
					},
					success: function (response) {
						$('.listpost').html(response);
						$("#loading-animation").hide();
					},
					error: function (jqXHR, textStatus, errorThrown) {
						console.log('The following error occured: ' + textStatus, errorThrown);
					}
				});
			});
		});
		</script>
	
	<?php
	}


require get_stylesheet_directory() . '/inc/post_cate.php';


genesis();