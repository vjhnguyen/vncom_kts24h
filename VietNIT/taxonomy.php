<?php
/**
 * Handle the display of taxonomy page.

 * @package  Template
 * @category VIETNIT
 * @developer   VietNIT
 */

remove_action( 'genesis_loop', 'genesis_do_loop' );
remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
add_action('genesis_loop','newsthongbao');

function newsthongbao(){
	?>
	<div class="news-tb">
		<div class="thongbao">
			<div class="nametype">
				<!--h4><?php //single_term_title(); ?></h4-->
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span><?php single_term_title(); ?></span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
			</div>
			<div class="listpost">
				<?php
					$args = array(
						'posts_per_page' => 29
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
			<div class="paging">
				<div class="navigation">
					<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
					<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
					<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
					<?php } ?>
				</div>
				<div class="nav_mobi">
					<div class="row">
						<div class="col-xs-12">
							<ul class="pager">
								<li><?php previous_posts_link('Prev') ?></li>
								<li><?php next_posts_link('Next') ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="clear"></div>
	<div class="news group-area-home">
		<div class="namekn"><h4>Tuyển sinh theo khối ngành</h4></div>
		
		<div class="quang-cao">
			<div class="content-quangcao">
				<div class="viewgt">
					<?php if(is_active_sidebar('banner_adv_home') && !dynamic_sidebar('banner_adv_home')):endif; ?>
				</div>
			</div>
		</div>
		
		<div class="kn1 kn">
			<div class="gitem kinhte">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-kinh-te/">
					<i class="fa fa-money" aria-hidden="true"></i>		
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Kinh Tế</p>
						</div>
				</a>
			</div>
			<div class="gitem kythuat">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-ky-thuat/">
					<i class="fa fa-cogs" aria-hidden="true"></i>
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Kỹ Thuật</p>
						</div>
				</a>
			</div>
			<div class="gitem vanhoa">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-van-hoa-nghe-thuat/">
					<i class="vanhoa"></i>
						<div class="textarea">
							<p class="ng1">Khối Ngành</p>
							<p class="ng2">Văn Hóa</p>
						</div>
				</a>
			</div>
			<div class="gitem supham">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-su-pham/">
					<i class="fa fa-graduation-cap" aria-hidden="true"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Sư Phạm</p>
					</div>
				</a>
			</div>
			<div class="gitem yduoc">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-y-duoc/">
				<i class="yduoc"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Y Dược</p>
					</div>
				</a>
			</div>
			<div class="gitem congan">
				<a class="aarea" href="http://kenhtuyensinh24h.vn/nhom_nganh/khoi-bo-doi-cong-an/">
				<i class="congan"></i>
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Công An</p>
					</div>
				</a>
			</div>
		</div>
	</div> <!--end group-area-home-->
	
	
	<?php
}

require get_stylesheet_directory() . '/inc/post_cate.php';

genesis();