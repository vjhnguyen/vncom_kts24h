<?php
/**
* Template Name: Category Đại Học
*/

remove_action( 'genesis_loop', 'genesis_do_loop' );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
add_action('genesis_before_loop','ggsearch_position');
function ggsearch_position(){
	?>
	<div class="ggsearch">
	<?php echo get_theme_mod( "ggsearch_code", 'Vị trí của thanh google search' ); ?>
	</div>
	<?php
}

add_action('genesis_loop','taxanomy_count');
function taxanomy_count(){
	?>
		<div class="taxonomy-count">
			<div class="archive-head">
				<div class="nums-post">
				<?php
					$category = get_the_category();
					echo '<p class="num">'.$category[0]->category_count.'</p>';
					echo '<p class="truong">Trường</p>';
				?>
				</div>
			</div>
		</div>
	<?php
}

add_action('genesis_loop','show_list_posts');
function show_list_posts(){
	?>
	<div class="news-tb">
		<div class="thongbao">
			<div class="nametype">
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span>Các trường Công lập</span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
				<h2 class="block-title-mobile">
					<a href="#">
						Các trường Công lập
					</a>
				</h2>
			</div>
			<div class="listpost">
			<?php $category = get_queried_object(); $cur_cat = $category->term_id; ?>

				<?php
					/* Loại trường 1: 10020 */
					/* Loại trường 2: 10021 */
					/* Loại trường 3: 10022 */
					$args = array(
						'cat'=>$cur_cat,
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'loai_truong',
							'field' => 'id',
							'terms' => '10020',
						  ),
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
				<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<div class="nametype">
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span>Các trường học viện</span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
				<h2 class="block-title-mobile">
					<a href="#">
						Các trường học viện
					</a>
				</h2>
			</div>
			<div class="listpost">
				<?php
					$args = array(
						'cat'	=> $cur_cat,
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'loai_truong',
							'field' => 'id',
							'terms' => '10021',
						  ),
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<div class="nametype">
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span>Các trường dân lập</span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
				<h2 class="block-title-mobile">
					<a href="#">
						Các trường dân lập
					</a>
				</h2>
			</div>
			<div class="listpost">
				<?php
					$args = array(
						'cat'	=> $cur_cat,
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'loai_truong',
							'field' => 'id',
							'terms' => '10022',
						  ),
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	<?php
}


require get_stylesheet_directory() . '/inc/post_cate.php';


genesis();