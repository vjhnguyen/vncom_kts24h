<?php

@ini_set( 'upload_max_size' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'max_execution_time', '300' );


/* 1. Enqueue Asset File */
function wpdocs_theme_name_scripts()
{
    wp_enqueue_script('jquery_new', '//code.jquery.com/jquery-3.3.1.min.js');
    wp_enqueue_script('main', get_stylesheet_directory_uri() . '/custom/js/main.js');
}
add_action('wp_enqueue_scripts', 'wpdocs_theme_name_scripts');

function enqueue_front_files(){

    // Fancy Box
    // wp_enqueue_style('fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css');
    // wp_enqueue_script('fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js');
}
add_action('wp_enqueue_scripts', 'enqueue_front_files');



/** End thêm widget cho quảng cáo google */
/*****************************************/

//require_once('wp_bootstrap_navwalker.php');
add_action("genesis_before_header","menu_bootstrap",1);
function menu_bootstrap(){
?>
<div class="clear"></div>
<nav class="navbar navbar-custom navbar-static-top navbar-fixed-top" role="navigation">
<!-- Brand and toggle get grouped for better mobile display --> 
  <div class="navbar-header">
  <div class="navbar-right">
	<ul class="navbar-toggle icon-nav">
		<li><a class="ttgd" title="tin tức giáo dục" href="<?php echo get_home_url(); ?>/tin-tuc/"><img title="tin tức giáo dục" alt="tin tức giáo dục" class="icon-news" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ico1.png" alt="" /></li></a>
		<li><a href="#menu" data-toggle="collapse"><img class="icon-toggle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ico2.png" alt="danh mục" title="danh-mục" /></li></a>
	</ul>
  </div>
    <a class="navbar-brand" href="<?php bloginfo('url'); ?>"><img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-tet-mobile.png" alt="logo" title="logo" /></a>
  </div>
  <div class="clear"></div>
<div class="view">
	<div class="collapse navbar-collapse" id="menu">
        <ul class="menu_policy">
            <li>
                <a href="<?php echo get_home_url(); ?>/hop-tac-tuyen-sinh-cung-kenhtuyensinh24h-vn/" title="Chuyên trang tuyển sinh 24h">
                    <span><i class="fa fa-handshake-o" aria-hidden="true"></i>Hợp tác tuyển sinh</span>
                </a>
            </li>
            <li>
                <a href="<?php echo get_home_url(); ?>/lien-he-quang-cao/" title="Liên hệ quảng cáo">
                    <span><i class="fa fa-bullhorn" aria-hidden="true"></i>Liên hệ quảng cáo</span>
                </a>
            </li>
        </ul>
        <ul class="menu_cat_main">
            <li>
                <a data-toggle="modal" href="<?php echo get_home_url(); ?>/dai-hoc/">
                    <span><i class="fa fa-university" aria-hidden="true"></i></span>
                    <span class="txt">Đại học</span>
                </a>           
            </li>
            <li>
                <a href="<?php echo get_home_url(); ?>/lien-thong-dai-hoc-2/">
                    <span><i class="fa fa-university" aria-hidden="true"></i></span>
                    <span class="txt">Liên thông</span>
                </a>
            </li>
            <li>
                <a data-toggle="modal" href="<?php echo get_home_url(); ?>/cao-dang/">
                    <span><i class="fa fa-university" aria-hidden="true"></i></span>
                    <span class="txt">Cao đẳng</span>
                </a>
            </li>
            <li>
                <a href="<?php echo get_home_url(); ?>/van-bang-2-sau-dai-hoc/">
                    <span><i class="fa fa-university" aria-hidden="true"></i></span>
                    <span class="txt">Văng bằng 2</span>
                </a>
            </li>
            <li>
                <a data-toggle="modal" href="<?php echo get_home_url(); ?>/trung-cap/">
                    <span><i class="fa fa-university" aria-hidden="true"></i></span>
                    <span class="txt">Trung cấp</span>
                </a>
            </li>
            <li>
                <a href="<?php echo get_home_url(); ?>/cao-hoc/">
                    <span><i class="fa fa-university" aria-hidden="true"></i></span>
                    <span class="txt">Cao học</span>
                </a>
            </li>
        </ul>        
        <!-- Modal -->
          <div class="modal fade" id="colldh" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Danh sách các trường Đại học theo khu vực</h4>
                </div>
                <div class="modal-body">
                  <ul class="ovh menu_cat_main">
                        <li>
                            <a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-tp-ha-noi/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Đại học khu vực Hà Nội</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-tp-hcm/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Đại học khu vực TP.HCM</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-mien-bac/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Đại học khu vực Miền Bắc</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-mien-trung/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Đại học khu vực Miền Trung</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-mien-nam/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Đại học khu vực Miền Nam</span>
                            </a>           
                        </li>
                  </ul>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
              </div>
              
            </div>
        </div><!-- dai hoc modal end -->
        <!-- Modal -->
          <div class="modal fade" id="collcd" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Danh sách các trường Cao Đẳng theo khu vực</h4>
                </div>
                <div class="modal-body">
                  <ul class="ovh menu_cat_main">
                        <li>
                            <a href="<?php echo get_home_url(); ?>/cao-dang/cao-dang-khu-vuc-tp-ha-noi/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Cao đẳng khu vực Hà Nội</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/cao-dang/cao-dang-khu-vuc-tp-hcm/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Cao đẳng khu vực TP.HCM</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/cao-dang/cao-dang-khu-vuc-mien-bac/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Cao đẳng khu vực Miền Bắc</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/cao-dang/cao-dang-khu-vuc-mien-trung/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Cao đẳng khu vực Miền Trung</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/cao-dang/cao-dang-khu-vuc-mien-nam/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Cao đẳng khu vực Miền Nam</span>
                            </a>           
                        </li>
                  </ul>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
              </div>
              
            </div>
        </div><!-- cao dang modal end -->
        <!-- Modal -->
          <div class="modal fade" id="colltc" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Danh sách các trường Trung Cấp theo khu vực</h4>
                </div>
                <div class="modal-body">
                  <ul class="ovh menu_cat_main">
                        <li>
                            <a href="<?php echo get_home_url(); ?>/trung-cap/trung-cap-khu-vuc-tp-ha-noi/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Trung Cấp khu vực Hà Nội</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/trung-cap/trung-cap-khu-vuc-tp-hcm/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Trung Cấp khu vực TP.HCM</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/trung-cap/trung-cap-khu-vuc-mien-bac/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Trung Cấp khu vực Miền Bắc</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/trung-cap/trung-cap-khu-vuc-mien-trung/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Trung Cấp khu vực Miền Trung</span>
                            </a>           
                        </li>
                        <li>
                            <a href="<?php echo get_home_url(); ?>/trung-cap/trung-cap-khu-vuc-mien-nam/">
                                <span><i class="fa fa-university" aria-hidden="true"></i></span>
                                <span class="txt">Trung Cấp khu vực Miền Nam</span>
                            </a>           
                        </li>
                  </ul>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
              </div>
              
            </div>
        </div><!-- trung cap modal end -->
		<div class="menu_cat_sub">
            <h4>Các Chuyên Mục Khác</h4>
		      <ul>
                <li>
                    <a href="<?php echo get_home_url(); ?>/tin-tuc-he-dai-hoc/">Tin đại học</a>
                </li>
                <li>
                    <a href="<?php echo get_home_url(); ?>/tin-huong-nghiep/">Tin hướng nghiệp</a>
                </li>
                <li>
                    <a href="<?php echo get_home_url(); ?>/tin-tuc-he-cao-dang/">Tin cao đẳng</a>
                </li>
                <li>
                    <a href="<?php echo get_home_url(); ?>/nhip-song-tre/">Nhịp sống trẻ</a>
                </li>
                <li>
                    <a href="<?php echo get_home_url(); ?>/tin-tuc-he-trung-cap/">Tin trung cấp</a>
                </li>
                <li>
                    <a href="<?php echo get_home_url(); ?>/muc_tim_hieu_nganh_nghe/tim-hieu-nganh-nghe/">Tìm hiểu ngành nghề</a>
                </li>
                <li>
                    <a href="<?php echo get_home_url(); ?>/muc_diem_chuan/muc-diem-chuan/">Điểm chuẩn</a>
                </li>
                <li>
                    <a href="<?php echo get_home_url(); ?>/tin-tuc/">Tin tức giáo dục</a>
                </li>
                <li>
                    <a href="<?php echo get_home_url(); ?>/luyen-thi/">luyện thi trực tuyến</a>
                </li>
              </ul>
		</div>
	</div>
</div>
</nav>

	<?php
}


remove_action( 'genesis_before_post_content', 'genesis_post_info' );
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
add_action( 'genesis_post_title', 'genesis_post_meta' );
add_theme_support( 'genesis-structural-wraps', array( 'header', 'nav', 'subnav', 'inner', 'footer-widgets', 'footer' ) );
add_image_size( 'vertical-hd', 150, 130, true );
add_image_size( 'tuyen-sinh', 150, 150, true );
add_image_size( 'featured-thumbnail', 245, 155, true );
add_image_size( 'featured-thumbnail-tiny', 75, 40, true );
add_image_size( 'featured-category', 415,280, true );
add_image_size( 'featured-sidebar', 50,35, true );
add_image_size( 'widget-thumbnail', 120, 90, true );
add_image_size( 'video', 350, 350, true );
add_image_size( 'videobe', 110, 110, true );
add_image_size( 'thum_single', 100, 60, true );
add_image_size('blockts',500,150,true);
add_image_size('hd-image-mobile',600,350,true);
add_image_size('news-td',324,216,true);
set_post_thumbnail_size( 190, 130, true );






// Hien thi son luong Comment
add_filter( 'genesis_title_comments', 'sp_genesis_title_comments' );
function sp_genesis_title_comments() {
 return __(comments_number( '<h3>0 Bình luận</h3>', '<h3>1 Bình luận</h3>', '<h3>% Bình luận</h3>' ), 'genesis' );
}
// Hien thi da thiet bi

add_action( 'wp_head', 'link_head' );
function link_head(){
    echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
    //echo '<meta name="fragment" content="!">';
}
remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
add_action( 'wp_head', 'add_my_script' );
function add_my_script() {
		echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/bootstrap.min.css">';        
		echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/font-awesome.min.css">';
        echo '<link rel="stylesheet" href="'.CHILD_URL.'/style.css">';
        echo '<link rel="stylesheet" href="'.CHILD_URL.'/css/reponsive.css">';
		echo '<script src="'.CHILD_URL.'/js/jquery.js"></script>';
		echo '<script src="'.CHILD_URL.'/js/bootstrap.min.js"></script>';
		echo '<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">';
		?>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<?php
}
// dem so nguoi xem bai viet
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count.'';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
add_filter( 'genesis_breadcrumb_args', 'vncom_breadcrumb_args' );
function vncom_breadcrumb_args( $args ) {
    $args['home'] = __( 'Trang chủ', 'caia' );
    $args['sep'] = ' / ';
    $args['list_sep'] = ', ';
    $args['prefix'] = '<div class="breadcrumb">';
    $args['suffix'] = '</div>';
    $args['heirarchial_attachments'] = true;
    $args['heirarchial_categories'] = true;
    $args['display'] = true;
    $args['labels']['prefix'] = '';
    $args['labels']['author'] = '';
    $args['labels']['category'] = '';
    $args['labels']['tag'] = '';
    $args['labels']['date'] = '';
    $args['labels']['search'] = __( 'Tìm kiếm', 'caia' );
    $args['labels']['tax'] = '';
    $args['labels']['post_type'] = '';
    $args['labels']['404'] = '';
return $args;
}

/* phân trang comment */
add_action('genesis_after_comments', 'rayno_comments_nav');
function rayno_comments_nav() {
    ?><div class="paginate-com"> <?php 
        //Create pagination links for the comments on the current post, with single arrow heads for previous/next
        paginate_comments_links( array('prev_text' => '‹ Previous', 'next_text' => 'Next ›')); 
    ?></div> <?php
}
function remove_comment_fields($fields) {
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields', 'remove_comment_fields');

/*--------custom comments form------------------*/
add_filter( 'comment_form_defaults', 'rayno_comment_form_args' );
function rayno_comment_form_args($defaults) {
    global $user_identity, $id;
    $commenter = wp_get_current_commenter();
    $req       = get_option( 'require_name_email' );
    $aria_req  = ( $req ? ' aria-required="true"' : '' );
    $author = 	'<p class="comment-form-author">' .
					'<input id="author" name="author" type="text" class="author" placeholder="Nhập tên của bạn" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" tabindex="1"' . $aria_req . '/>' .
				'<label for="name">Name</label><span class="required">*</span></p><!-- #form-section-author .form-section -->';
    $email = 	'<p class="comment-form-email">' .
					'<input id="email" name="email" type="text" class="email" placeholder="Nhập email của bạn" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" tabindex="2"' . $aria_req . ' />' .
				'<label for="email">Email</label><span class="required">*</span></p><!-- #form-section-email .form-section -->';
    $comment_field = 	'<p class="comment-form-comment">' .
							'<textarea id="comment" name="comment" cols="45" rows="8" class="form" tabindex="4" aria-required="true"></textarea>' .
						'</p><!-- #form-section-comment .form-section -->';

    $args = array(
        'fields' => array(
        'author' => $author,
        'email'  => $email
        
        ),
        'comment_field'        => $comment_field,
        'title_reply'          => __( '<span class="binhluan"> Bình luận của bạn: </span>
                            <p class="thanhvu">
Nếu bạn có thắc mắc, ý kiến đóng góp của bạn xung quanh vấn đề này. Vui lòng điền thông tin theo mẫu dưới đây rồi nhấn nút GỬI BÌNH LUẬN.
Mọi ý kiến của bạn đều được Kenhtuyensinh24h.vn đón đợi và quan tâm. <p>
<p>Cảm ơn các bạn! </p>' ),
        'comment_notes_before' => '',
        'comment_notes_after'  => '',
    );

    
    
    $args = wp_parse_args( $args, $defaults );
    return apply_filters( 'raynoblog_comment_form_args', $args, $user_identity, $id, $commenter, $req, $aria_req );
}

add_action( 'comment_form_logged_in_after', 'additional_fields' );
add_action( 'comment_form_after_fields', 'additional_fields' );

function additional_fields () {
    echo '<p class="comment-form-phone">'.
            '<input id="phone" name="phone" placeholder="Nhập số điện thoại của bạn" type="text" size="30"  tabindex="4" />'.
            '<label for="phone">' . __( 'Phone' ) . '</label><span class="required">*</span></p>';
}

// Save the comment meta data along with comment

add_action( 'comment_post', 'save_comment_meta_data' );
function save_comment_meta_data( $comment_id ) {
    if ( ( isset( $_POST['phone'] ) ) && ( $_POST['phone'] != '') )
    $phone = wp_filter_nohtml_kses($_POST['phone']);
    add_comment_meta( $comment_id, 'phone', $phone );
}


// Add the filter to check if the comment meta data has been filled or not

//add_filter( 'preprocess_comment', 'verify_comment_meta_data' );
function verify_comment_meta_data( $commentdata ) {
    if (empty( $_POST['phone'] ) )
    wp_die( __( 'Error: bạn chưa điền thông tin số điện thoại của bạn. Nhấn nút quay lại để điền thông tin vào trường này' ) );
    return $commentdata;
}

//Add an edit option in comment edit screen  

add_action( 'add_meta_boxes_comment', 'extend_comment_add_meta_box' );
function extend_comment_add_meta_box() {
    add_meta_box( 'title', __( 'Comment Metadata - Extend Comment' ), 'extend_comment_meta_box', 'comment', 'normal', 'high' );
}
 
function extend_comment_meta_box ( $comment ) {
    $phone = get_comment_meta( $comment->comment_ID, 'phone', true );
    $title = get_comment_meta( $comment->comment_ID, 'title', true );
    $rating = get_comment_meta( $comment->comment_ID, 'rating', true );
    wp_nonce_field( 'extend_comment_update', 'extend_comment_update', false );
    ?>
    <p>
        <label for="phone"><?php _e( 'Phone' ); ?></label>
        <input type="text" name="phone" value="<?php echo esc_attr( $phone ); ?>" class="widefat" />
    </p>
    <p>
        <label for="title"><?php _e( 'Comment Title' ); ?></label>
        <input type="text" name="title" value="<?php echo esc_attr( $title ); ?>" class="widefat" />
    </p>
    <p>
        <label for="rating"><?php _e( 'Rating: ' ); ?></label>
            <span class="commentratingbox">
            <?php for( $i=1; $i <= 5; $i++ ) {
                echo '<span class="commentrating"><input type="radio" name="rating" id="rating" value="'. $i .'"';
                if ( $rating == $i ) echo ' checked="checked"';
                echo ' />'. $i .' </span>'; 
                }
            ?>
            </span>
    </p>
    <?php
}

// Update comment meta data from comment edit screen 

add_action( 'edit_comment', 'extend_comment_edit_metafields' );
function extend_comment_edit_metafields( $comment_id ) {
    if( ! isset( $_POST['extend_comment_update'] ) || ! wp_verify_nonce( $_POST['extend_comment_update'], 'extend_comment_update' ) ) return;

    if ( ( isset( $_POST['phone'] ) ) && ( $_POST['phone'] != '') ) : 
    $phone = wp_filter_nohtml_kses($_POST['phone']);
    update_comment_meta( $comment_id, 'phone', $phone );
    else :
    delete_comment_meta( $comment_id, 'phone');
    endif;
        
    if ( ( isset( $_POST['title'] ) ) && ( $_POST['title'] != '') ):
    $title = wp_filter_nohtml_kses($_POST['title']);
    update_comment_meta( $comment_id, 'title', $title );
    else :
    delete_comment_meta( $comment_id, 'title');
    endif;

    if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != '') ):
    $rating = wp_filter_nohtml_kses($_POST['rating']);
    update_comment_meta( $comment_id, 'rating', $rating );
    else :
    delete_comment_meta( $comment_id, 'rating');
    endif;
    
}

// Add the comment meta (saved earlier) to the comment text 
// You can also output the comment meta values directly in comments template  

add_filter( 'comment_text', 'modify_comment');
function modify_comment( $text ){

    $plugin_url_path = WP_PLUGIN_URL;

    if( $commenttitle = get_comment_meta( get_comment_ID(), 'title', true ) ) {
        $commenttitle = '<strong>' . esc_attr( $commenttitle ) . '</strong><br/>';
        $text = $commenttitle . $text;
    } 

    if( $commentrating = get_comment_meta( get_comment_ID(), 'rating', true ) ) {
        $commentrating = '<p class="comment-rating">    <img src="'. $plugin_url_path .
        '/ExtendComment/images/'. $commentrating . 'star.gif"/><br/>Rating: <strong>'. $commentrating .' / 5</strong></p>';
        $text = $text . $commentrating;
        return $text;       
    } else {
        return $text;       
    }    
}
// Facebook Open Graph
add_action('wp_head', 'add_fb_open_graph_tags');
function add_fb_open_graph_tags() {
	if (is_single()) {
		global $post;
		if(get_the_post_thumbnail($post->ID, 'thumbnail')) {
			$thumbnail_id = get_post_thumbnail_id($post->ID);
			$thumbnail_object = get_post($thumbnail_id);
			$image = $thumbnail_object->guid;
		} else {	
			$image = '<?php echo get_home_url(); ?>/wp-content/uploads/2015/12/lien-thong.jpg'; // Change this to the URL of the logo you want beside your links shown on Facebook
		}
		//$description = get_bloginfo('description');
		$description = my_excerpt( $post->post_content, $post->post_excerpt );
		$description = strip_tags($description);
		$description = str_replace("\"", "'", $description);
		?>
		<meta property="og:title" content="<?php echo the_title(); ?>" />
		<meta property="og:image" content="<?php echo $image; ?>" />
		<meta property="og:url" content="<?php the_permalink(); ?>" />
		<meta property="og:description" content="<?php echo $description; ?>" />
		<meta property="og:site_name" content="<?php echo get_bloginfo('name'); ?>" />
		<meta property="fb:app_id" content="139395743428487"/>
		<meta property="og:type"   content="article" />

	<?php }
}

function my_excerpt($text, $excerpt){
	
    if ($excerpt) return $excerpt;

    $text = strip_shortcodes( $text );

    $text = apply_filters('the_content', $text);
    $text = str_replace(']]>', ']]>', $text);
    $text = strip_tags($text);
    $excerpt_length = apply_filters('excerpt_length', 55);
    $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
    $words = preg_split("/[\n
	 ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
    if ( count($words) > $excerpt_length ) {
            array_pop($words);
            $text = implode(' ', $words);
            $text = $text . $excerpt_more;
    } else {
            $text = implode(' ', $words);
    }

    return apply_filters('wp_trim_excerpt', $text, $excerpt);
}


// change text reply
add_filter('comment_reply_link', 'raynoblog_change_reply_text');
function raynoblog_change_reply_text($args) {
    $args = str_replace('Reply', 'Trả lời', $args);
    return $args;
}
//kich thuoc avata
add_filter( 'genesis_comment_list_args', 'rayno_comment_avatar_args' );
function rayno_comment_avatar_args( $args ) {
    $args['avatar_size'] = 30;
    return $args;
}
// rut gon title
function short_title($after = '', $length) {
    $qdztitle = explode(' ', get_the_title(), $length);
    if (count($qdztitle)>=$length) {
        array_pop($qdztitle);
        $qdztitle = implode(" ",$qdztitle). $after;
    } else {
        $qdztitle = implode(" ",$qdztitle);
    }
    return $qdztitle;
}
add_filter( 'genesis_search_text', 'sp_search_text' );
function sp_search_text( $text ) {
	return esc_attr( 'Nhập nội dung tìm kiếm ...' );
}
add_filter('get_search_form', 'new_search_button');
function new_search_button($text) {
$text = str_replace('value="Search"', 'value="Tìm kiếm"', $text);
return $text;
}

//* Customize the submit button text in comments
add_filter( 'comment_form_defaults', 'sp_comment_submit_button' );
function sp_comment_submit_button( $defaults ) {
 
        $defaults['label_submit'] = __( 'Gửi bình luận', 'custom' );
        return $defaults;
 
}
// Code đếm số dòng trong văn bản
function count_paragraph( $insertion, $paragraph_id, $content ) {
        $closing_p = '</p>';
        $paragraphs = explode( $closing_p, $content );
        foreach ($paragraphs as $index => $paragraph) {
                if ( trim( $paragraph ) ) {
                        $paragraphs[$index] .= $closing_p;
                }
                if ( $paragraph_id == $index + 1 ) {
                        $paragraphs[$index] .= $insertion;
                }
        }
 
        return implode( '', $paragraphs );
}
//add_action( 'wp_head', 'protect' );
function protect(){
	
echo '<script type="text/javascript">

document.onkeypress = function(event) {
event = (event || window.event);
if (event.keyCode === 123) {
alert("No F-12");	
return false;
}
};
document.onmousedown = function(event) {
event = (event || window.event);
if (event.keyCode === 123) {
return false;
}
};
document.onkeydown = function(event) {
event = (event || window.event);
if (event.keyCode === 123) {
return false;
}
};

function contentprotector() {
return false;
}
function mousehandler(e) {
var myevent = (isNS) ? e : event;
var eventbutton = (isNS) ? myevent.which : myevent.button;
if ((eventbutton === 2) || (eventbutton === 3))
return false;
}
document.oncontextmenu = contentprotector;
document.onmouseup = contentprotector;
var isCtrl = false;
window.onkeyup = function(e)
{
if (e.which === 17)
isCtrl = false;
}

window.onkeydown = function(e)
{
if (e.which === 17)
isCtrl = true;
if (((e.which === 85) || (e.which === 65) || (e.which === 88) || (e.which === 67) || (e.which === 86) || (e.which === 83)) && isCtrl === true)
{
return false;
}
}
isCtrl = false;
document.ondragstart = contentprotector;

</script>';
}

function add_copyright_text() {
if (is_single()) { ?>
 
<script type='text/javascript'>
function addLink() {
 if (
window.getSelection().containsNode(
document.getElementsByClassName('entry-content')[0], true)) {
 var body_element = document.getElementsByTagName('body')[0];
 var selection;
 selection = window.getSelection();
 var oldselection = selection
 var pagelink = "<div class='display:none;'><br /><br /> Nội dung thuộc website của chúng tôi nghiêm cấm sao chép, sao chép phải để lại nguồn: <?php the_title(); ?> <a href='<?php echo wp_get_shortlink(get_the_ID()); ?>'><?php echo wp_get_shortlink(get_the_ID()); ?></a></div>"; //Change this if you like
 var copy_text = selection + pagelink;
 var new_div = document.createElement('div');
 new_div.style.left='-99999px';
 new_div.style.position='absolute';
 
body_element.appendChild(new_div );
 new_div.innerHTML = copy_text ;
 selection.selectAllChildren(new_div );
 window.setTimeout(function() {
 body_element.removeChild(new_div );
 },0);
}
}
document.oncopy = addLink;
</script>
 
<?php
}
}
add_action( 'wp_head', 'add_copyright_text');



add_action( 'after_setup_theme', 'childtheme_formats', 11 );
function childtheme_formats(){
     add_theme_support( 'post-formats', array( 'aside') );
}
function the_breadcrumb() {
    echo '<ul id="crumbs">';
        if (!is_home()) {
            ?>
            <li><a href="<?php get_option('home'); ?>"><i class="fa fa-home" aria-hidden="true"></i> Trang chủ /</a></li>
            <?php 
            if(is_category() || is_single()) 
                {
                    ?>
                    <li><?php the_category(); ?></li>             
                    <?php
                }            
            $custom_taxonomy    = 'muc_diem_chuan';
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);            
            if($taxonomy_exists) {
                the_terms( $post->ID, 'muc_diem_chuan', '<li>', ' / ','</li>' );
                }
            elseif(is_page()){
                ?>
                <li><?php the_title(); ?></li>
                <?php
            }
        }
        elseif (is_tag()) {single_tag_title();}
        elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
        elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
        elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
        elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
        elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
        elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
        echo '</ul>';
}

function formsearch(){
}

function formsearch_backup(){
	?>
    <!-- /* Form Search */ -->
	<div class="searchformts">
		<div class="search-wrap">
			<div class="intro-search"><!--<i class="fa fa-search" aria-hidden="true"></i> <h4>Hỗ trợ thí sinh tìm nhanh hơn</h4> -->
				<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
			</div>
			<?php
				echo do_shortcode('	[ULWPQSF id=16327]' );
			?>
		</div>
	</div>
	<?php
}

//khoi nganh

function fkhoinganh(){
	?>
	<div class="clear"></div>
	<div class="linearea"></div>
	<div class="group-area">
		<div class="area-head"><h4>Nhóm các trường tuyển sinh cùng khối ngành</h4></div>
		<div class="gitem kinhte">
			<a class="aarea" href="<?php echo get_home_url(); ?>/nhom_nganh/khoi-kinh-te/">
				<i class="fa fa-money" aria-hidden="true"></i>		
					<div class="textarea">
						<p class="ng1">Khối Ngành</p>
						<p class="ng2">Kinh Tế</p>
					</div>
			</a>
		</div>
		<div class="gitem kythuat">
			<a class="aarea" href="<?php echo get_home_url(); ?>/nhom_nganh/khoi-ky-thuat/">
				<i class="fa fa-cogs" aria-hidden="true"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Kỹ Thuật</p>
				</div>
			</a>
		</div>
		<div class="gitem yduoc">
			<a class="aarea" href="<?php echo get_home_url(); ?>/nhom_nganh/khoi-y-duoc/">
				<i class="yduoc"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Y Dược</p>
				</div>
			</a>
		</div>
		<div class="gitem supham">
			<a class="aarea" href="<?php echo get_home_url(); ?>/nhom_nganh/khoi-su-pham/">
				<i class="fa fa-graduation-cap" aria-hidden="true"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Sư Phạm</p>
				</div>
			</a>
		</div>
		<div class="gitem vanhoa">
			<a class="aarea" href="<?php echo get_home_url(); ?>/nhom_nganh/khoi-van-hoa-nghe-thuat/">
				<i class="vanhoa"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Văn Hóa</p>
				</div>
			</a>
		</div>
		<div class="gitem congan">
			<a class="aarea" href="<?php echo get_home_url(); ?>/nhom_nganh/khoi-bo-doi-cong-an/">
				<i class="congan"></i>
				<div class="textarea">
					<p class="ng1">Khối Ngành</p>
					<p class="ng2">Công An</p>
				</div>
			</a>
		</div>
	</div>
<?php
}

function lienquanall(){
	?>
	<div class="clear"></div>
	<div class="linearea"></div>
	<div class="school-area">
		<div class="school-header"><h4>Danh sách các trường theo khu vực Đại Học</h4></div>
			<div class="list-area">
					<div class="itemkv">
						<a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-tp-ha-noi/"><img src="<?php bloginfo( 'stylesheet_directory'); ?>/images/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-tp-ha-noi.jpg"></a>
						<h4><a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-tp-ha-noi/">Danh sách các trường Đại Học Và Học Viện khu vực TP Hà Nội</a></h4>
					</div>
					<div class="itemkv">
						<a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-tp-hcm/"><img src="<?php bloginfo( 'stylesheet_directory'); ?>/images/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-tp-ha-noi.jpg"></a>
						<h4><a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-tp-hcm/">Danh sách các trường Đại Học Và Học Viện khu vực TP HCM</a></h4>
					</div>
					<div class="itemkv">
						<a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-mien-bac/"><img src="<?php bloginfo( 'stylesheet_directory'); ?>/images/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-tp-ha-noi.jpg"></a>
						<h4><a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-mien-bac/">Danh sách các trường Đại Học Và Học Viện khu vực Miền Bắc</a></h4>
					</div>
					<div class="itemkv">
						<a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-mien-trung/"><img src="<?php bloginfo( 'stylesheet_directory'); ?>/images/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-tp-ha-noi.jpg"></a>
						<h4><a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-mien-trung/">Danh sách các trường Đại Học Và Học Viện khu vực Miền Trung</a></h4>
					</div>
					<div class="itemkv">
						<a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-mien-nam/"><img src="<?php bloginfo( 'stylesheet_directory'); ?>/images/danh-sach-cac-truong-dai-hoc-va-hoc-vien-khu-vuc-tp-ha-noi.jpg"></a>
						<h4><a href="<?php echo get_home_url(); ?>/dai-hoc/dai-hoc-khu-vuc-mien-nam/">Danh sách các trường Đại Học Và Học Viện khu vực Miền Nam</a></h4>
					</div>
			</div>
	</div>	
<?php
}

    
    

function bk_banner_scroll(){ ?>
    <script type="text/javascript">
		jQuery(document).ready(function($) {    
			var banner_cdtc = $("#banner-under-menu-mobile");
			var banner_under_menu = $("#banner-under-menu");
			$(window).scroll(function(){
				if($(this).scrollTop()>120){
				banner_cdtc.hide();
				banner_under_menu.show();
				}else if($(this).scrollTop()==0){
				banner_cdtc.show();
				banner_under_menu.hide();
				}}
			)
		})
    </script>
<?php
}





/** SANGMINI ngày 29/11/2017 ERROR -> JQMIGRATE: Migrate is installed, version 1.4.1 **/

function cedaro_dequeue_jquery_migrate( $scripts ) {
	if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) {
		$jquery_dependencies = $scripts->registered['jquery']->deps;
		$scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
	}
}
add_action( 'wp_default_scripts', 'cedaro_dequeue_jquery_migrate' );

/**
 * Thêm quảng cáo cho page
 */
// add_action('genesis_before_loop','adv_gle');

function adv_gle(){
	
	if( is_page() ){
		?>
			<div class="adv_gle">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<ins class="adsbygoogle"
					style="display:block; text-align:center;"
					data-ad-layout="in-article"
					data-ad-format="fluid"
					data-ad-client="ca-pub-1895892504965300"
					data-ad-slot="7371921627">
				</ins>
				<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			<br/>
			</div>
		<?php
	}
}


