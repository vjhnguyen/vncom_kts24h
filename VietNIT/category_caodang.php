<?php
/**
* Template Name: Category Cao Đẳng
*/

remove_action( 'genesis_loop', 'genesis_do_loop' );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
add_action('genesis_loop','newsthongbao');
add_action('wp_head', 'bk_banner_scroll');


add_action('genesis_before_loop','ggsearch_position');

function ggsearch_position(){
	?>
	<div class="ggsearch">
	<?php echo get_theme_mod( "ggsearch_code", '111111111' ); ?>
	</div>
	<?php
}

function newsthongbao(){
	?>
	<div class="taxonomy-count">
		<div class="archive-head">
			<div class="nums-post">
			<?php
				$category = get_the_category();
				echo '<p class="num">'.$category[0]->category_count.'</p>';
				echo '<p class="truong">Trường</p>';
			?>
			</div>
		</div>
	</div>
	<div class="news-tb">
		<?php formsearch(); ?>
		<div class="thongbao">
			<div class="nametype">
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span>Các trường Cao đẳng Chính quy</span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
				<!-- Mobile -->
				<h2 class="block-title-mobile">
					<a href="#">
						Các trường Cao đẳng Chính quy
					</a>
				</h2>
				<!-- END -->
			</div>
			<div class="listpost">
			<?php $category = get_queried_object(); $cur_cat = $category->term_id; ?>

				<?php 
					/* Loại trường 1: 10020 */
					/* Loại trường 2: 10021 */
					/* Loại trường 3: 10022 */
					$args = array(
						'cat'=>$cur_cat,
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'loai_truong',
							'field' => 'id',
							'terms' => '10020',
						  ),
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="thongbao">
			<div class="nametype">
				<h2 class="block-title">
					<a href="#">
						<span class="icon-block-title"></span>
						<span>Các trường Cao Đẳng nghề</span>
					</a>
					<div class="click-thongbao"> Click vào tên trường để xem thông báo tuyển sinh</div>
				</h2>
				<!-- Mobile -->
				<h2 class="block-title-mobile">
					<a href="#">
						Các trường Cao Đẳng nghề
					</a>
				</h2>
				<!-- END -->
			</div>
			<div class="listpost">
				<?php 
					$args = array(
						'cat'	=> $cur_cat,
						'post_type' => 'post',
						'posts_per_page' => 100,
						'tax_query' => array(
						  array(
							'taxonomy' => 'loai_truong',
							'field' => 'id',
							'terms' => '10021',
						  ),
						)
					);
					$featureds = new WP_Query( $args );
					if($featureds->have_posts()):
					while($featureds->have_posts()) : $featureds->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>	
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
				<?php else: ?>
					<p class="update">Hiện đang cập nhật</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php
}




require get_stylesheet_directory() . '/inc/post_cate.php';

genesis();