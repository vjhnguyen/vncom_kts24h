<?php
/**
 * Handle the display of tag page.

 * @package  Template
 * @category VIETNIT
 * @developer   VietNIT
 */

// Allow subchild theme modify
if ( file_exists( CAIA_CUSTOM_DIR . '/tag.php' ) )
{
	require( CAIA_CUSTOM_DIR . '/tag.php' );
}


genesis();