<?php
remove_action( 'genesis_loop', 'genesis_do_loop' );
remove_action( 'genesis_before_loop', 'caia_archive_heading', 5 );
add_action('genesis_loop','newsthongbao');
add_action('genesis_loop','new_tintuc' );



function newsthongbao(){
	
	$current_tax = get_queried_object()->term_id;

	?>

	<div class="news-tb">
		<div class="thongbao">
			<!-- <div class="nametype"> -->
				<!-- <h4><?php single_term_title(); ?></h4> -->
			<!-- </div> -->


			<div class="listpost">
				<?php
					$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                    // $category = get_queried_object(); $cur_cat = $category->term_id;

					$args = array( 
						'posts_per_page' => 28, //-1 to get all post
                        'paged' => $paged,
                        // 'cat' => $cur_cat 
					);


					$query = new WP_Query( $args);


					while($query->have_posts()) : $query->the_post();
					?>
					<div class="itemblock">
						<a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail("full"); ?>
						<?php the_title(); ?>
						</a>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>


			<div class="paging">
				<div class="navigation">
					<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
					<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
					<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
					<?php } ?>
				</div>
				<div class="nav_mobi">
					<div class="row">
						<div class="col-xs-12">
							<ul class="pager">
								<li><?php previous_posts_link('Prev') ?></li>
								<li><?php next_posts_link('Next') ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
	<?php
}


function new_tintuc(){
?>
	<!-- QUẢNG CÁO CỦA GOOGLE -->
	<div class="quang-cao-google" style="width: 100%;float: left;">
		<div class="box_posts_cate">
			<h3>Lao động du học</h3>
			<div class="box_posts">
				<?php
				$args = array(
					'posts_per_page' => 6,
					'category_name' => 'lao-dong-du-hoc'
				);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
				?>
					<div class="box_post-item">
						<a class="img" href="<?php the_permalink( ); ?>">
							<?php the_post_thumbnail( 'thumbnail' );   ?>
						</a>
						<a class="tit" href="<?php the_permalink( ); ?>">
							<?php the_title(); ?>
						</a>
					</div>
				<?php endwhile; endif; wp_reset_postdata(); ?>
			</div>
		</div>
		<div class="quang-cao-1-category">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 4 -->
			<ins class="adsbygoogle"
			style="display:inline-block;width:300px;height:250px"
			data-ad-client="ca-pub-1895892504965300"
			data-ad-slot="8543525730"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<div class="quang-cao-2-category">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- kenhtuyensinh24h.vn - Banner 7 -->
			<ins class="adsbygoogle"
			style="display:inline-block;width:300px;height:250px"
			data-ad-client="ca-pub-1895892504965300"
			data-ad-slot="4258010410"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
	<!-- QUẢNG CÁO CỦA GOOGLE END -->


	<div id="desktop-centent">
		<!--------------->
		<!-- Tin tức 1 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/tin-huong-nghiep/" title="TIN HƯỚNG NGHIỆP">
				<span class="icon-block-title"></span>
				<span>TIN HƯỚNG NGHIỆP</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-huong-nghiep/" title="TIN HƯỚNG NGHIỆP" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php
							$args = new WP_Query('cat=232&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-0">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php echo short_title('...', 11); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
							<?php endwhile; wp_reset_postdata(); ?>
							<ul>
								<?php
									$args = new WP_Query('cat=232&showposts=3&offset=1');
									while($args->have_posts()):$args->the_post();
									?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div>
						<!-- end older_posts -->
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
			</div>
			<!-- end .block-wrap -->
		</div>
		<!--------------->
		<!-- Tin tức 2 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="ĐIỂM CHUẨN">
				<span class="icon-block-title"></span>
				<span>ĐIỂM CHUẨN</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="ĐIỂM CHUẨN" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php
							$args = new WP_Query('cat=7894&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-1">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
							<?php endwhile; wp_reset_postdata(); ?>
							<ul>
								<?php
									$args = new WP_Query('cat=7894&showposts=3&offset=1');
									while($args->have_posts()):$args->the_post();
									?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div>
						<!-- end older_posts -->
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
			</div>
			<!-- end .block-wrap -->
		</div>
		<!--------------->
		<!-- Tin tức 3 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/tin-cong-nghe/" title="TIN CÔNG NGHỆ">
				<span class="icon-block-title"></span>
				<span>TIN CÔNG NGHỆ</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-cong-nghe/" title="TIN CÔNG NGHỆ" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php
							$args = new WP_Query('cat=204&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-2">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
							<?php endwhile; wp_reset_postdata(); ?>
							<ul>
								<?php
									$args = new WP_Query('cat=204&showposts=3&offset=1');
									while($args->have_posts()):$args->the_post();
									?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div>
						<!-- end older_posts -->
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
			</div>
			<!-- end .block-wrap -->
		</div>
		<!--------------->
		<!-- Tin tức 4 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/tin-tuc-he-dai-hoc/" title="TIN HỆ ĐẠI HỌC">
				<span class="icon-block-title"></span>
				<span>TIN HỆ ĐẠI HỌC</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-tuc-he-dai-hoc/" title="TIN HỆ ĐẠI HỌC" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php
							$args = new WP_Query('cat=870&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-3">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
							<?php endwhile; wp_reset_postdata(); ?>
							<ul>
								<?php
									$args = new WP_Query('cat=870&showposts=3&offset=1');
									while($args->have_posts()):$args->the_post();
									?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div>
						<!-- end older_posts -->
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
			</div>
			<!-- end .block-wrap -->
		</div>
		<!--------------->
		<!-- Tin tức 5 -->
		<div id="news-block-tintuc" class="news-block-tintuc news-block-tintuc block-odd block-tintuc block has-related">
			<h2 class="block-title">
				<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="TIN HỆ TRUNG CẤP">
				<span class="icon-block-title"></span>
				<span>TIN HỆ TRUNG CẤP</span>
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="TIN HỆ TRUNG CẤP" class="xemtatca">Xem tất cả</a>
			</h2>
			<div class="block-wrap">
				<div class="main-posts">
					<div class="caia-block-item">
						<?php
							$args = new WP_Query('cat=868&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
						</a>
						<div class="older-posts-tintuc tintuc-4">
							<h3 class="title-block">
								<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<p><?php the_content_rss('', false,'', 40);?></p>
							<?php endwhile; wp_reset_postdata(); ?>
							<ul>
								<?php
									$args = new WP_Query('cat=868&showposts=3&offset=1');
									while($args->have_posts()):$args->the_post();
									?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								</li>
								<?php endwhile; wp_reset_postdata(); ?>
							</ul>
						</div>
						<!-- end older_posts -->
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
			</div>
			<!-- end .block-wrap -->
		</div>
	</div>


	<div id="mobile-centent">
		<!-- Tin tức 1 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-huong-nghiep/" title="TIN HƯỚNG NGHIỆP">
				TIN HƯỚNG NGHIỆP
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-huong-nghiep/" title="TIN HƯỚNG NGHIỆP" class="xemtatca-mobile">
				<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php
							$args = new WP_Query('cat=232&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
						</a>
						<h3 class="title-block-mobile">
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
				<ul class="older-posts-tintuc-mobile">
					<?php
						$args = new WP_Query('cat=232&showposts=3&offset=1');
						while($args->have_posts()):$args->the_post();
						?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div>
			<!-- end .block-wrap -->
		</div>
		<!-- Tin tức 2 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="ĐIỂM CHUẨN">
				ĐIỂM CHUẨN
				</a>
				<a href="https://kenhtuyensinh24h.vn/diem-chuan/" title="Điểm chuẩn" class="xemtatca-mobile">
				<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php
							$args = new WP_Query('cat=7894&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
						</a>
						<h3 class="title-block-mobile">
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
				<ul class="older-posts-tintuc-mobile">
					<?php
						$args = new WP_Query('cat=7894&showposts=3&offset=1');
						while($args->have_posts()):$args->the_post();
						?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div>
			<!-- end .block-wrap -->
		</div>
		<!-- Tin tức 3 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-cong-nghe/" title="TIN CÔNG NGHỆ">
				TIN CÔNG NGHỆ
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-cong-nghe/" title="TIN CÔNG NGHỆ" class="xemtatca-mobile">
				<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php
							$args = new WP_Query('cat=204&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
						</a>
						<h3 class="title-block-mobile">
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
				<ul class="older-posts-tintuc-mobile">
					<?php
						$args = new WP_Query('cat=204&showposts=3&offset=1');
						while($args->have_posts()):$args->the_post();
						?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div>
			<!-- end .block-wrap -->
		</div>
		<!-- Tin tức 4 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-he-dai-hoc/" title="TIN HỆ ĐẠI HỌC">
				TIN HỆ ĐẠI HỌC
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-he-dai-hoc/" title="Tin hệ đại học" class="xemtatca-mobile">
				<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php
							$args = new WP_Query('cat=870&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
						</a>
						<h3 class="title-block-mobile">
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
				<ul class="older-posts-tintuc-mobile">
					<?php
						$args = new WP_Query('cat=870&showposts=3&offset=1');
						while($args->have_posts()):$args->the_post();
						?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div>
			<!-- end .block-wrap -->
		</div>
		<!-- Tin tức 5 -->
		<div class="news-block-tintuc-mobile tintuc-mobile">
			<h2 class="block-title-mobile">
				<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="TIN HỆ TRUNG CẤP">
				TIN HỆ TRUNG CẤP
				</a>
				<a href="https://kenhtuyensinh24h.vn/tin-he-trung-cap/" title="Tin hệ trung cấp" class="xemtatca-mobile">
				<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</h2>
			<div class="block-wrap-mobile">
				<div class="main-posts-mobile">
					<div class="caia-block-item-mobile">
						<?php
							$args = new WP_Query('cat=868&showposts=1');
							while($args->have_posts()): $args->the_post();
							?>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
						<?php if(has_post_thumbnail()){ the_post_thumbnail(array(400,250));} ;?>
						</a>
						<h3 class="title-block-mobile">
							<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<p style="color: #888; font-size: 11px; width: 100%; float: left; margin: 0 0px 10px 0;"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div>
					<!-- end 1 post -->
				</div>
				<!-- end .main-posts -->
				<ul class="older-posts-tintuc-mobile">
					<?php
						$args = new WP_Query('cat=868&showposts=3&offset=1');
						while($args->have_posts()):$args->the_post();
						?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
							<?php echo get_the_post_thumbnail( get_the_id(),'thumbnail', array('class'=>'thumnail')); ?>
							<h3><?php the_title(); ?></h3>
							<p class="time-mobile"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
						</a>
					</li>
					<?php endwhile; wp_reset_postdata(); ?>
				</ul>
			</div>
			<!-- end .block-wrap -->
		</div>
	</div>

	<?php
}



genesis();