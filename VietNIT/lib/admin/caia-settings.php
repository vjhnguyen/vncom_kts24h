<?php
/**
 * Create the VNCOM Theme settings page
 *
 * @category VNCOM
 * @package  Admin
 * @author   VNCOM
 */

/**
 * Registers a new admin page, providing content and corresponding menu item
 * for the Theme Settings page.
 *
 * @category VNCOM
 * @package  Admin
 *
 * @since    1.0
 */
class CAIA_Theme_Settings extends Genesis_Admin_Boxes
{
	/**
	 * Create an admin menu item and settings page.
	 *
	 * @since 1.0
	 *
	 * @uses  GENESIS_SETTINGS_FIELD settings field key
	 * @uses  genesis_get_default_layout() Get default layout
	 *
	 * @global string $_genesis_theme_settings_pagehook Theme Settings page hook,
	 *        kept for backwards compatibility, since this class now uses $this->pagehook.
	 */
	function __construct()
	{
		$page_id = 'caia-settings';

		$menu_ops = apply_filters(
			'caia_settings_menu_ops',
			array(
				'submenu' => array(
					'parent_slug' => 'themes.php',
					'page_title'  => __( 'VNCOM Theme Settings', 'caia' ),
					'menu_title'  => __( 'VNCOM Settings', 'caia' )
				)
			)
		);

		$page_ops = apply_filters(
			'caia_settings_page_ops',
			array(
				'screen_icon'       => 'options-general',
				'save_button_text'  => __( 'Save Settings', 'caia ' ),
				'reset_button_text' => __( 'Reset Settings', 'caia' ),
				'saved_notice_text' => __( 'Settings saved.', 'caia' ),
				'reset_notice_text' => __( 'Settings reset.', 'caia' ),
				'error_notice_text' => __( 'Error saving settings.', 'caia' ),
			)
		);

		$settings_field = CAIA_SETTINGS_FIELD;

		$default_settings = apply_filters(
			'caia_settings_defaults',
			array(
				'caia_version'          => CHILD_THEME_VERSION,
				'use_default_thumbnail' => 0,
				'default_thumbnail'     => '',
				'footer_text'           => '',
			)
		);

		$this->create( $page_id, $menu_ops, $page_ops, $settings_field, $default_settings );

		add_action( 'genesis_settings_sanitizer_init', array( $this, 'sanitizer_filters' ) );
	}

	/**
	 * Registers each of the settings with a sanitization filter type.
	 *
	 * @since 1.0
	 *
	 * @uses  genesis_add_option_filter() Assign filter to array of settings
	 *
	 * @see   Genesis_Settings_Sanitizer::add_filter()
	 */
	public function sanitizer_filters()
	{
		genesis_add_option_filter(
			'one_zero',
			$this->settings_field,
			array(
				'use_default_thumbnail',
			)
		);
	}

	/**
	 * Register the metaboxes.
	 *
	 * @since 1.0
	 */
	public function metaboxes()
	{
		add_meta_box( 'caia-settings-general', __( 'General', 'caia' ), array( $this, 'general_box' ), $this->pagehook, 'main', 'high' );
		add_meta_box( 'caia-settings-footer', __( 'Footer Text', 'caia' ), array( $this, 'footer_box' ), $this->pagehook, 'main' );

		do_action( 'caia_settings_metaboxes', $this->pagehook );
	}

	/**
	 * Callback for Theme Settings Information meta box.
	 *
	 * @since 1.0
	 *
	 * @uses  Genesis_Admin::get_field_name() Construct full field name
	 * @uses  Genesis_Admin::get_field_value() Retrieve value of key under $this->settings_field
	 *
	 * @return null
	 */
	function general_box()
	{
		?>

		<p>
			<strong><?php _e( 'Version:', 'genesis' ); ?></strong> <?php echo CHILD_THEME_VERSION; ?>
		</p>

		<p>
			<label>
				<input type="checkbox" name="<?php echo $this->get_field_name( 'use_default_thumbnail' ); ?>" value="1" <?php checked( 1, $this->get_field_value( 'use_default_thumbnail' ) ); ?> />
				<?php _e( 'Use default thumbnail image.', 'caia' ); ?>
			</label>
		</p>

		<p class="default-thumbnail-uploader <?php echo $this->get_field_value( 'use_default_thumbnail' ) ? '' : 'hidden'; ?>">

		</p>

		<?php
	}

	/**
	 * Callback for Theme Settings Information meta box.
	 *
	 * @since 1.0
	 *
	 * @uses  Genesis_Admin::get_field_name() Construct full field name
	 * @uses  Genesis_Admin::get_field_value() Retrieve value of key under $this->settings_field
	 *
	 * @return null
	 */
	function footer_box()
	{
		?>

		<textarea rows="5" cols="60" class="widefat" name="<?php echo $this->get_field_name( 'footer_text' ); ?>"><?php echo $this->get_field_value( 'footer_text' ); ?></textarea><br/>

		<p class="description">
			<?php _e( 'Allow shortcodes' ); ?>
		</p>

		<?php
	}
}