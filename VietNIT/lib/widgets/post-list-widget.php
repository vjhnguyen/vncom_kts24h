<?php
/**
 * VietNIT Recent Posts Widget.
 *
 * @category VietNIT
 * @package  Widgets
 */

/**
 * VietNIT Recent Post widget class.
 *
 * @category VietNIT
 * @package Widgets
 *
 * @since 1.0
 */
class VietNIT_Post_List_Widget extends WP_Widget
{

	/**
	 * Holds widget settings defaults, populated in constructor.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Constructor. Set the default widget options and create widget.
	 *
	 * @since 1.0
	 */
	function __construct()
	{
		$this->defaults = array(
			'code'					  => '', // widget code
			'title'                   => '',
			'post_type'               => 'post',
			'taxonomy'				  => 'category',
			'term'	               	  => '', // term_id replace 'posts_cat'			
			'auto_detect_category' 	  => 0,			
			'ignore_sticky_posts'     => 0,
			'featured'			      => 0, // featured or not			
			'posts_num'               => 1,
			'posts_offset'            => 0,
			'orderby'                 => '',
			'order'                   => '',
			'show_image'              => 0,
			'image_alignment'         => '',
			'image_size'              => '',
			'show_gravatar'           => 0,
			'gravatar_alignment'      => '',
			'gravatar_size'           => '',
			'show_title'              => 0,
			'show_byline'             => 0,
			'post_info'               => '[post_date] ' . __( 'By', 'vietnit' ) . ' [post_author_posts_link] [post_comments]',
			'show_content'            => 'excerpt',
			'content_limit'           => '',
			'more_text'               => __( '[Read More...]', 'caia' ),
			'extra_num'               => 0, // number of extra post to show
			'extra_title'             => '',
			'more_from_category'      => '',
			'more_from_category_text' => __( 'More Posts from this Category', 'caia' ),
			'support_extra'		      => '',
		);

		$widget_ops = array(
			'classname'   => 'vietnit-post-list-widget',
			'description' => __( 'Displays recent / featured posts or custom post type, and support widget code.', 'caia' ),
		);

		$control_ops = array(
			'id_base' => 'caia-post-list',
			'width'   => 505,
			'height'  => 350,
		);

		$this->WP_Widget( 'caia-post-list', __( 'VietNIT - Post List', 'caia' ), $widget_ops, $control_ops );
	}

	/**
	 * Echo the widget content.
	 *
	 * @since 1.0
	 *
	 * @param array $args Display arguments including before_title, after_title, before_widget, and after_widget.
	 * @param array $instance The settings for the particular instance of the widget
	 */
	function widget( $args, $instance )
	{		
		extract( $args );

		/** Merge with defaults */
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		extract( $instance );

		$total_posts = $posts_num + $extra_num;

		if( !$total_posts )
			return;

		if( $auto_detect_category && !is_category() && !is_single() && !is_tag() && !is_tax() )
            return;
        		

		$query_args = array(                 
                'showposts' 			=> $total_posts,
                'offset' 				=> $posts_offset,                
                'order' 				=> $order,
                'post_type' 			=> $post_type,
                'ignore_sticky_posts' 	=> $ignore_sticky_posts ? 1 : 0
        	);

		
		
		// xu ly auto detect category bao gom auto detect tax
        if($auto_detect_category) {
            if(is_category() || is_tag() || is_tax() ) {
            	$cur_queried_obj = get_queried_object();
            	$term = $cur_queried_obj->term_id;
            	$taxonomy = $cur_queried_obj->taxonomy;             	               
            } elseif(is_single()) {
            	global $post;
            	$cur_post_id = $post->ID;

            	if($post->post_type === $post_type){

	            	if($taxonomy === 'category'){            		
		            	$term_arr = get_the_category($cur_post_id);
		                $term = isset($term_arr[0]) ? $term_arr[0]->cat_ID : '';	
	            	}else{
	            		// neu ko phai category, la other tax
	            		$term_arr = wp_get_post_terms( $cur_post_id, $taxonomy);

	            		$term = isset($term_arr[0]) ? $term_arr[0]->term_id : '';	
	            	}
            	}else{
            		// neu khac post_type thi return luon
            		return;
            	}
                
            }                        
        }
        
        // tax_query
        if($term && $taxonomy){
			if($taxonomy === 'category'){
				$query_args['cat'] = $term;
			}else if( $taxonomy === 'post_tag'){
				$query_args['tag_id'] = $term;
			}else{
				$query_args['tax_query'] = array( 
										array('taxonomy' => $taxonomy, 
												'field' => 'id', 
												'terms' => $term));
			}
		}

		// featured & orderby
		if($featured){
			$query_args['featured'] = $term ? 'category' : 'all';

			if( $orderby === 'featured_order' ){				
				$query_args['orderby'] = $term ? 'featured_by_cat_order' : 'featured_order';
			}else{
				$query_args['orderby'] = $orderby;
			}
		}else{
			if( $orderby === 'featured_order' ){	
				// khi user chon orderby feature but dont check feautured => orderby date is default
				$query_args['orderby'] = 'date'; // by date default
			}else{
				$query_args['orderby'] = $orderby;
			}
		}
				
		
        // print_r($query_args);

		$featured_posts = new WP_Query( $query_args );

		if(!$featured_posts->have_posts())
		{
			wp_reset_postdata();
			return;
		}
		$still_have_posts = true;			

		// begin show widget after query
		echo $before_widget;

		/** Set up the author bio */
		if ( ! empty( $title ) )
			echo $before_title . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $after_title;		

		echo '<div class="main-posts sildeShow"><ul>';

		// co the dung de them vao duoi va cuoi main nhung thanh phan html can thiet
		do_action( 'caia_post_list_widget_before_main_posts', $code, $instance );

		$index = 0;

		if ( $posts_num > 0 && $featured_posts->have_posts() ){
			while ( ($still_have_posts = $featured_posts->have_posts()) && $index < $posts_num ){
				$featured_posts->the_post();
				
				do_action( 'caia_post_list_widget_do_post', $code, $instance );
				
				$index ++;
			} // end while
		} // end if main post

		do_action( 'caia_post_list_widget_after_main_posts', $code, $instance );

		echo '</div></ul>'; // div main-posts

		/** The EXTRA Posts (list) */
		if ( ! empty( $extra_num ) && $still_have_posts ) {
			if ( ! empty( $extra_title ) )
				echo '<p class="widget-sub-title">' . esc_html( $extra_title ) . '</p>';			

			$listitems = '';						
			while ( $featured_posts->have_posts() && $index < $total_posts) {
				$featured_posts->the_post();
				$listitems .= sprintf( '<li><a href="%s" title="%s">%s</a></li>', 
										get_permalink(), 
										the_title_attribute( 'echo=0' ), 
										get_the_title() );
				$index ++;
			}

			if ( strlen( $listitems ) > 0 )
				echo '<ul>' . $listitems  . '</ul>';
			
		}

		if ( $term && $taxonomy && ! empty( $more_from_category )){

			if($taxonomy === 'category'){
				$term_name = get_cat_name( $term );
			}else{
				$term_arra = get_term_by('id', $term, $taxonomy, ARRAY_A);
				$term_name = $term_arra['name'];
			}

			$cur_href = esc_url( get_term_link( intval($term), $taxonomy ) );
			$cur_title = esc_attr( $term_name );
			$cur_anchor_text = ( $more_from_category_text );
			echo '<p class="more-from-category"><a href="' . $cur_href . '" title="' . $cur_title . '">' . $cur_anchor_text . '</a></p>'; 
			
		}
		if( !empty($support_extra)){
			echo '<div class="support-extra">' . $support_extra . '</div>';
		}
		echo $after_widget;
		wp_reset_postdata();
	}

	/**
	 * Update a particular instance.
	 *
	 * This function should check that $new_instance is set correctly.
	 * The newly calculated value of $instance should be returned.
	 * If "false" is returned, the instance won't be saved/updated.
	 *
	 * @since 1.0
	 *
	 * @param array $new_instance New settings for this instance as input by the user via form()
	 * @param array $old_instance Old settings for this instance
	 * @return array Settings to save or bool false to cancel saving
	 */
	function update( $new_instance, $old_instance )
	{
		$new_instance['title']     = strip_tags( $new_instance['title'] );
		$new_instance['more_text'] = strip_tags( $new_instance['more_text'] );
		$new_instance['post_info'] = wp_kses_post( $new_instance['post_info'] );
		return $new_instance;
	}

	/**
	 * Echo the settings update form.
	 *
	 * @since 1.0
	 *
	 * @param array $instance Current settings
	 */
	function form( $instance )
	{
		/** Merge with defaults */
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		?>
	    <p>
	        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'caia' ); ?>:</label>
	        <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" class="widefat" />
	    </p>

	    <div class="genesis-widget-column" style="width: 49%;">

	        <div class="genesis-widget-column-box genesis-widget-column-box-top">
	            
	            <p>
	            	<label>
	            		<?php _e( 'Code:',  'caia' );
						caia_dropdown_widget_code(array('id' => $this->get_field_id( 'code' ), 
															'name' => $this->get_field_name( 'code' ),
															'selected' => $instance['code']));
													
						_e( '(For coder)', 'caia' ); ?>
						
					</label>
	            </p>

		        <p>
			        <?php $post_types = get_post_types( array( 'public' => true ), 'objects' ); ?>
			        <label for="<?php echo $this->get_field_id( 'post_type' ); ?>"><?php _e( 'Post type' ) ?>:</label>
                    <select id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>">
	                	<?php foreach( $post_types as $post_type => $post_type_obj ) : ?>
	                    	<option value="<?php echo $post_type; ?>" <?php selected( $post_type, $instance['post_type'] ); ?>><?php echo $post_type_obj->labels->singular_name; ?></option>
						<?php endforeach; ?>
	                </select>
		        </p>

		        <p>
				    <label>
				    	<?php 
				    	$taxonomies = get_taxonomies( array('public' => true), 'objects');  
				    	$ajax_id = '_ajax_' . $this->id_base . '-' . $this->number; 
				    	_e( 'Tax', 'caia' );?>:
						<select name="<?php echo $this->get_field_name( 'taxonomy' ); ?>" class="caia-tax-ajax-select <?php echo $ajax_id; ?>">
							<?php foreach ($taxonomies as $name => $value) {?>					
							<option value="<?php echo $name; ?>" <?php selected( $name,  $instance['taxonomy'] )?> ><?php echo $value->labels->name?></option>
							<?php } ?>			
						</select>										        
				    </label>
				    <label class='caia-loading-ajax_<?php echo $ajax_id;?>'>&nbsp;</label>
				</p>

		        <p>
	                <label for="<?php echo $this->get_field_id( 'term' ); ?>"><?php _e( 'Term', 'caia' ); ?>:</label>
					<?php
					$categories_args = array(
						'name'            => $this->get_field_name( 'term' ),
						'selected'        => $instance['term'],
						'orderby'         => 'Name',
						'hierarchical'    => 1,
						'show_option_all' => __( 'All Items', 'caia' ),
						'hide_empty'      => '0',
						'taxonomy'		  => $instance['taxonomy'],
						'class'			  => 'caia-term-ajax_' . $ajax_id ,	
					);
					wp_dropdown_categories( $categories_args ); ?>
	            </p>

		        <p>
		        	<input type="checkbox" id="<?php echo $this->get_field_id('auto_detect_category'); ?>" name="<?php echo $this->get_field_name('auto_detect_category'); ?>" value="1" <?php checked(1, $instance['auto_detect_category']); ?>/> 
		        	<label for="<?php echo $this->get_field_id('auto_detect_category'); ?>"><?php _e('Auto detect category', 'caia'); ?></label>
		        </p>
		        <p>
		        	<input type="checkbox" id="<?php echo $this->get_field_id( 'ignore_sticky_posts' ); ?>" name="<?php echo $this->get_field_name( 'ignore_sticky_posts' ); ?>" value="1" <?php checked( 1, $instance['ignore_sticky_posts'] ); ?> />
	                <label for="<?php echo $this->get_field_id( 'ignore_sticky_posts' ); ?>"><?php _e( 'Ignore sticky posts', 'caia' ); ?></label>
	            </p>
	            <p>
		        	<input type="checkbox" id="<?php echo $this->get_field_id( 'featured' ); ?>" name="<?php echo $this->get_field_name( 'featured' ); ?>" value="1" <?php checked( 1, $instance['featured'] ); ?> />
	                <label for="<?php echo $this->get_field_id( 'featured' ); ?>"><?php _e( 'Only Featured', 'caia' ); ?></label>	                
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'posts_num' ); ?>"><?php _e( 'Number of Posts to Show', 'caia' ); ?>:</label>
	                <input type="text" id="<?php echo $this->get_field_id( 'posts_num' ); ?>" name="<?php echo $this->get_field_name( 'posts_num' ); ?>" value="<?php echo esc_attr( $instance['posts_num'] ); ?>" size="2" />
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'posts_offset' ); ?>"><?php _e( 'Number of Posts to Offset', 'caia' ); ?>:</label>
	                <input type="text" id="<?php echo $this->get_field_id( 'posts_offset' ); ?>" name="<?php echo $this->get_field_name( 'posts_offset' ); ?>" value="<?php echo esc_attr( $instance['posts_offset'] ); ?>" size="2" />
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'orderby' ); ?>"><?php _e( 'Order By', 'caia' ); ?>:</label>
	                <select id="<?php echo $this->get_field_id( 'orderby' ); ?>" name="<?php echo $this->get_field_name( 'orderby' ); ?>">
	                	<option style="padding-right:10px;" value="featured_order" <?php selected('featured_order', $instance['orderby']); ?>><?php _e('Featured', 'caia'); ?></option>						
	                    <option value="date" <?php selected( 'date', $instance['orderby'] ); ?>><?php _e( 'Date', 'caia' ); ?></option>
	                    <option value="title" <?php selected( 'title', $instance['orderby'] ); ?>><?php _e( 'Title', 'caia' ); ?></option>
	                    <option value="parent" <?php selected( 'parent', $instance['orderby'] ); ?>><?php _e( 'Parent', 'caia' ); ?></option>
	                    <option value="ID" <?php selected( 'ID', $instance['orderby'] ); ?>><?php _e( 'ID', 'caia' ); ?></option>
	                    <option value="comment_count" <?php selected( 'comment_count', $instance['orderby'] ); ?>><?php _e( 'Comment Count', 'caia' ); ?></option>
	                    <option value="rand" <?php selected( 'rand', $instance['orderby'] ); ?>><?php _e( 'Random', 'caia' ); ?></option>
	                </select>
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Sort Order', 'caia' ); ?>:</label>
	                <select id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>">
	                    <option value="DESC" <?php selected( 'DESC', $instance['order'] ); ?>><?php _e( 'Descending (3, 2, 1)', 'caia' ); ?></option>
	                    <option value="ASC" <?php selected( 'ASC', $instance['order'] ); ?>><?php _e( 'Ascending (1, 2, 3)', 'caia' ); ?></option>
	                </select>
	            </p>

	        </div>
	        

	        <div class="genesis-widget-column-box">

	            <p>
	                <input id="<?php echo $this->get_field_id( 'show_image' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'show_image' ); ?>" value="1" <?php checked( $instance['show_image'] ); ?>/>
	                <label for="<?php echo $this->get_field_id( 'show_image' ); ?>"><?php _e( 'Show Featured Image', 'caia' ); ?></label>
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'image_size' ); ?>"><?php _e( 'Image Size', 'caia' ); ?>:</label>
	                <select id="<?php echo $this->get_field_id( 'image_size' ); ?>" name="<?php echo $this->get_field_name( 'image_size' ); ?>">
	                    <option value="thumbnail">thumbnail (<?php echo get_option( 'thumbnail_size_w' ); ?>x<?php echo get_option( 'thumbnail_size_h' ); ?>)</option>
						<?php
						$sizes = genesis_get_additional_image_sizes();
						foreach( (array) $sizes as $name => $size )
							echo '<option value="'.esc_attr( $name ).'" '.selected( $name, $instance['image_size'], FALSE ).'>'.esc_html( $name ).' ( '.$size['width'].'x'.$size['height'].' )</option>';
						?>
	                </select>
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'image_alignment' ); ?>"><?php _e( 'Image Alignment', 'caia' ); ?>:</label>
	                <select id="<?php echo $this->get_field_id( 'image_alignment' ); ?>" name="<?php echo $this->get_field_name( 'image_alignment' ); ?>">
	                    <option value="alignnone">- <?php _e( 'None', 'caia' ); ?> -</option>
	                    <option value="alignleft" <?php selected( 'alignleft', $instance['image_alignment'] ); ?>><?php _e( 'Left', 'caia' ); ?></option>
	                    <option value="alignright" <?php selected( 'alignright', $instance['image_alignment'] ); ?>><?php _e( 'Right', 'caia' ); ?></option>
	                </select>
	            </p>

	        </div>

			<div class="genesis-widget-column-box">

	            <p>
	                <input id="<?php echo $this->get_field_id( 'more_from_category' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'more_from_category' ); ?>" value="1" <?php checked( $instance['more_from_category'] ); ?>/>
	                <label for="<?php echo $this->get_field_id( 'more_from_category' ); ?>"><?php _e( 'Show Category Archive Link', 'caia' ); ?></label>
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'more_from_category_text' ); ?>"><?php _e( 'Link Text', 'caia' ); ?>:</label>
	                <input type="text" id="<?php echo $this->get_field_id( 'more_from_category_text' ); ?>" name="<?php echo $this->get_field_name( 'more_from_category_text' ); ?>" value="<?php echo esc_attr( $instance['more_from_category_text'] ); ?>" class="widefat" />
	            </p>

	        </div>
	    </div>

	    <div class="genesis-widget-column genesis-widget-column-right" style="width:49%">

	        <div class="genesis-widget-column-box genesis-widget-column-box-top">

	            <p>
	                <input id="<?php echo $this->get_field_id( 'show_title' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'show_title' ); ?>" value="1" <?php checked( $instance['show_title'] ); ?>/>
	                <label for="<?php echo $this->get_field_id( 'show_title' ); ?>"><?php _e( 'Show Post Title', 'caia' ); ?></label>
	            </p>

	            <p>
	                <input id="<?php echo $this->get_field_id( 'show_byline' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'show_byline' ); ?>" value="1" <?php checked( $instance['show_byline'] ); ?>/>
	                <label for="<?php echo $this->get_field_id( 'show_byline' ); ?>"><?php _e( 'Show Post Info', 'caia' ); ?></label>
	                <input type="text" id="<?php echo $this->get_field_id( 'post_info' ); ?>" name="<?php echo $this->get_field_name( 'post_info' ); ?>" value="<?php echo esc_attr( $instance['post_info'] ); ?>" class="widefat" />
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'show_content' ); ?>"><?php _e( 'Content Type', 'caia' ); ?>:</label>
	                <select id="<?php echo $this->get_field_id( 'show_content' ); ?>" name="<?php echo $this->get_field_name( 'show_content' ); ?>">
	                    <option value="content" <?php selected( 'content' , $instance['show_content'] ); ?>><?php _e( 'Show Content', 'caia' ); ?></option>
	                    <option value="excerpt" <?php selected( 'excerpt' , $instance['show_content'] ); ?>><?php _e( 'Show Excerpt', 'caia' ); ?></option>
	                    <option value="content-limit" <?php selected( 'content-limit' , $instance['show_content'] ); ?>><?php _e( 'Show Content Limit', 'caia' ); ?></option>
	                    <option value="" <?php selected( '' , $instance['show_content'] ); ?>><?php _e( 'No Content', 'caia' ); ?></option>
	                </select>
	                <br />
	                <label for="<?php echo $this->get_field_id( 'content_limit' ); ?>"><?php _e( 'Limit content to', 'caia' ); ?>
	                    <input type="text" id="<?php echo $this->get_field_id( 'image_alignment' ); ?>" name="<?php echo $this->get_field_name( 'content_limit' ); ?>" value="<?php echo esc_attr( intval( $instance['content_limit'] ) ); ?>" size="3" />
						<?php _e( 'characters', 'caia' ); ?>
	                </label>
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'more_text' ); ?>"><?php _e( 'More Text (if applicable)', 'caia' ); ?>:</label>
	                <input type="text" id="<?php echo $this->get_field_id( 'more_text' ); ?>" name="<?php echo $this->get_field_name( 'more_text' ); ?>" value="<?php echo esc_attr( $instance['more_text'] ); ?>" />
	            </p>

	        </div>

	        <div class="genesis-widget-column-box">

	            <p><?php _e( 'To display an unordered list of more posts from this category, please fill out the information below', 'caia' ); ?>:</p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'extra_title' ); ?>"><?php _e( 'Title', 'caia' ); ?>:</label>
	                <input type="text" id="<?php echo $this->get_field_id( 'extra_title' ); ?>" name="<?php echo $this->get_field_name( 'extra_title' ); ?>" value="<?php echo esc_attr( $instance['extra_title'] ); ?>" class="widefat" />
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'extra_num' ); ?>"><?php _e( 'Number of Posts to Show', 'caia' ); ?>:</label>
	                <input type="text" id="<?php echo $this->get_field_id( 'extra_num' ); ?>" name="<?php echo $this->get_field_name( 'extra_num' ); ?>" value="<?php echo esc_attr( $instance['extra_num'] ); ?>" size="2" />
	            </p>

	        </div>	        

	        <div class="genesis-widget-column-box">

	            <p>
	                <input id="<?php echo $this->get_field_id( 'show_gravatar' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'show_gravatar' ); ?>" value="1" <?php checked( $instance['show_gravatar'] ); ?>/>
	                <label for="<?php echo $this->get_field_id( 'show_gravatar' ); ?>"><?php _e( 'Show Author Gravatar', 'caia' ); ?></label>
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'gravatar_size' ); ?>"><?php _e( 'Gravatar Size', 'caia' ); ?>:</label>
	                <select id="<?php echo $this->get_field_id( 'gravatar_size' ); ?>" name="<?php echo $this->get_field_name( 'gravatar_size' ); ?>">
	                    <option value="45" <?php selected( 45, $instance['gravatar_size'] ); ?>><?php _e( 'Small (45px)', 'caia' ); ?></option>
	                    <option value="65" <?php selected( 65, $instance['gravatar_size'] ); ?>><?php _e( 'Medium (65px)', 'caia' ); ?></option>
	                    <option value="85" <?php selected( 85, $instance['gravatar_size'] ); ?>><?php _e( 'Large (85px)', 'caia' ); ?></option>
	                    <option value="125" <?php selected( 105, $instance['gravatar_size'] ); ?>><?php _e( 'Extra Large (125px)', 'caia' ); ?></option>
	                </select>
	            </p>

	            <p>
	                <label for="<?php echo $this->get_field_id( 'gravatar_alignment' ); ?>"><?php _e( 'Gravatar Alignment', 'caia' ); ?>:</label>
	                <select id="<?php echo $this->get_field_id( 'gravatar_alignment' ); ?>" name="<?php echo $this->get_field_name( 'gravatar_alignment' ); ?>">
	                    <option value="alignnone">- <?php _e( 'None', 'caia' ); ?> -</option>
	                    <option value="alignleft" <?php selected( 'alignleft', $instance['gravatar_alignment'] ); ?>><?php _e( 'Left', 'caia' ); ?></option>
	                    <option value="alignright" <?php selected( 'alignright', $instance['gravatar_alignment'] ); ?>><?php _e( 'Right', 'caia' ); ?></option>
	                </select>
	            </p>

	        </div>

	        <div class="genesis-widget-column-box">

	            <p><?php _e( 'Show extra html, fill on the below textbox', 'caia' ); ?>:</p>

	            <p>	                
	                <textarea id="<?php echo $this->get_field_id( 'support_extra' ); ?>" name="<?php echo $this->get_field_name( 'support_extra' ); ?>" style="width: 98%;"><?php echo esc_textarea( $instance['support_extra'] ); ?></textarea>
	            </p>

	        </div>	

	    </div>
		<?php
	}

} // end widget


add_action('caia_post_list_widget_do_post', 'caia_post_list_widget_do_post', 10, 2);

function caia_post_list_widget_do_post($widget_code, $instance)
{	
	extract($instance);


	echo '<li>';

	$cur_post_title_attr = the_title_attribute( 'echo=0' );

	if ( ! empty( $show_image ) )
		printf(
			'<a href="%s" title="%s" class="%s">%s</a>',
			get_permalink(),
			$cur_post_title_attr,
			esc_attr( $image_alignment ),
			genesis_get_image( array( 
					'format' => 'html', 
					'size' 	 => $image_size, 					
					'attr' => array(							
							'title' => $cur_post_title_attr,
							'alt'   => $cur_post_title_attr,
							)					
					))
		);

	if ( ! empty( $show_gravatar ) ) {
		echo '<span class="' . esc_attr( $gravatar_alignment ) . '">';
		echo get_avatar( get_the_author_meta( 'ID' ), $gravatar_size );
		echo '</span>';
	}

	if ( ! empty( $show_title ) )
		$titleshort= short_title('...', 12);
		printf( '<h3 class="widget-item-title"><a href="%s" title="%s">%s</a></h3>', get_permalink(), $cur_post_title_attr, $titleshort);

	if ( ! empty( $show_byline ) && ! empty( $post_info ) )
		printf( '<p class="byline post-info">%s</p>', do_shortcode( $post_info ) );

	if ( ! empty( $show_content ) ) {
		if ( 'excerpt' === $show_content ){			
			$my_excerpt = get_the_excerpt();
			echo mb_substr($my_excerpt, 0, $content_limit);
			if($content_limit < strlen($my_excerpt)) echo ''; 
		}elseif ( 'content-limit' === $show_content )
			the_content_limit( (int)$content_limit, esc_html( $more_text ) );
		else
			the_content( esc_html( $more_text ) );
	}

	if($image_alignment !== 'alignnone') echo '<div class="clear"></div>';
	?>		
	</li><!--end post_class()-->
	<?php
}