<?php
/*
 * User roles management by VietNIT Standards
 * WordPress.
 *
 * @category VietNIT
 * @package  Utilities
 */

/*
* Chú ý:
* 'delete_users' => true, -> cho phép vào cấu hình wpsupercache
* Ideas:
* VietNIT Client Admin tự lấy thêm capa từ administrator khi click update.
*/

// ----------------------------------------------------------------
// PHẦN CODE PHẦN QUYỀN LẠI CHO CẤP VietNIT ADMIN VÀ CLIENT ADMIN
if(is_admin())
{
    add_vietnit_role();

    //------------------------------
    // PHÂN QUYỀN LẠI TỪ ĐÂY
    $user = wp_get_current_user();
    // print_r($user);
    // print_r($user->get_role_caps());
    
    if($user->roles[0] === 'administrator') // neu là super admin
    {
        // hiển thị quản trị VietNIT Roles
        $vietnit_roles = new VIETNIT_Roles_Setting();

    }else if($user->roles[0] === 'vietnit_sub_admin') // neu ko phai la super admin
    {
                    
        // cấm user admin cấp thấp sửa user admin cấp cao hơn
        add_filter( 'editable_roles', 'editable_roles' );
        add_filter( 'map_meta_cap', 'vietnit_map_meta_cap', 10, 4 ); 

    } else if($user->roles[0] === 'vietnit_client_admin'){ 
        // phân quyền Client Admin

        // Disable change link structure        
        add_action('admin_head-options-permalink.php', 'vietnit_hide_permalink', 5); 

        // echo 'Day la client admin';
        // remove các thành phần menu bớt đi
        add_action( 'admin_menu', 'vietnit_remove_menus', 999 );        
    }

    // remove bớt metabox dư thừa ở daskboard đi => áp dụng với mọi user
    add_action( 'wp_dashboard_setup', 'vietnit_remove_dashboard_widgets' );

    
}

function add_vietnit_role($sub_admin_capas = array(), $client_admin_capas = array())
{

    if(!get_role('vietnit_sub_admin')){
        $def_vietnit_sub_admin_capas = array(
                                        // 'switch_themes' => true,
                                        // 'edit_themes' => true,
                                        'activate_plugins' => true,
                                        // 'edit_plugins' => true,
                                        'edit_users' => true,
                                        'edit_files' => true,
                                        'manage_options' => true,
                                        'moderate_comments' => true,
                                        'manage_categories' => true,
                                        'manage_links' => true,
                                        'upload_files' => true,
                                        'import' => true,
                                        'unfiltered_html' => true,
                                        'edit_posts' => true,
                                        'edit_others_posts' => true,
                                        'edit_published_posts' => true,
                                        'publish_posts' => true,
                                        'edit_pages' => true,
                                        'read' => true,
                                        // 'level_10' => true,
                                        'level_9' => true,
                                        'level_8' => true,
                                        'level_7' => true,
                                        'level_6' => true,
                                        'level_5' => true,
                                        'level_4' => true,
                                        'level_3' => true,
                                        'level_2' => true,
                                        'level_1' => true,
                                        'level_0' => true,
                                        'edit_others_pages' => true,
                                        'edit_published_pages' => true,
                                        'publish_pages' => true,
                                        'delete_pages' => true,
                                        'delete_others_pages' => true,
                                        'delete_published_pages' => true,
                                        'delete_posts' => true,
                                        'delete_others_posts' => true,
                                        'delete_published_posts' => true,
                                        'delete_private_posts' => true,
                                        'edit_private_posts' => true,
                                        'read_private_posts' => true,
                                        'delete_private_pages' => true,
                                        'edit_private_pages' => true,
                                        'read_private_pages' => true,
                                        'delete_users' => true,
                                        'create_users' => true,
                                        'unfiltered_upload' => true,
                                        'edit_dashboard' => true,
                                        // 'update_plugins' => true,
                                        'delete_plugins' => true,
                                        'install_plugins' => true,
                                        // 'update_themes' => true,
                                        // 'install_themes' => true,
                                        // 'update_core' => true,
                                        'list_users' => true,
                                        'remove_users' => true,
                                        'add_users' => true,
                                        'promote_users' => true,
                                        'edit_theme_options' => true,
                                        // 'delete_themes' => true,
                                        'export' => true,
                                        'administrator' => true, // ho tro cac quyen truy cap cua administrator
                                        'gform_full_access' => true, // danh cho gravity form
                                        
                                    );

        if($sub_admin_capas){
            foreach ($sub_admin_capas as $value) {
                $def_vietnit_sub_admin_capas[$value] =  true;
            }
        }

        $result = add_role(
            'vietnit_sub_admin',
            __( 'VIETNIT Sub Administrator', 'VietNIT' ),
            $def_vietnit_sub_admin_capas
        );
    }

    if(!get_role('vietnit_client_admin')){

        
        $def_vietnit_client_admin_capas = array(
                                    'edit_files' => true,
                                    'manage_options' => true,
                                    'moderate_comments' => true,
                                    'manage_categories' => true,
                                    'manage_links' => true,
                                    'upload_files' => true,
                                    'import' => true,
                                    'unfiltered_html' => true,
                                    'edit_posts' => true,
                                    'edit_others_posts' => true,
                                    'edit_published_posts' => true,
                                    'publish_posts' => true,
                                    'edit_pages' => true,
                                    'read' => true,            
                                    'level_8' => true,
                                    'level_7' => true,
                                    'level_6' => true,
                                    'level_5' => true,
                                    'level_4' => true,
                                    'level_3' => true,
                                    'level_2' => true,
                                    'level_1' => true,
                                    'level_0' => true,
                                    'edit_others_pages' => true,
                                    'edit_published_pages' => true,
                                    'publish_pages' => true,
                                    'delete_pages' => true,
                                    'delete_others_pages' => true,
                                    'delete_published_pages' => true,
                                    'delete_posts' => true,
                                    'delete_others_posts' => true,
                                    'delete_published_posts' => true,
                                    'delete_private_posts' => true,
                                    'edit_private_posts' => true,
                                    'read_private_posts' => true,
                                    'delete_private_pages' => true,
                                    'edit_private_pages' => true,
                                    'read_private_pages' => true,            
                                    'unfiltered_upload' => true,
                                    'edit_dashboard' => true,                                                                        
                                    'edit_theme_options' => true,            
                                    'export' => true,
                                    'administrator' => true, // ho tro cac quyen truy cap cua administrator
                                    'gform_full_access' => true, // danh cho gravity form
                                    
                                );

        if($client_admin_capas){
            foreach ($client_admin_capas as $value) {
                $def_vietnit_client_admin_capas[$value] =  true;
            }
        }


        $result = add_role(
            'vietnit_client_admin',
            __( 'VIETNIT Client Administrator', 'VietNIT' ),
            $def_vietnit_client_admin_capas
        );
    }
}

// hàm này sẽ ẩn form cập nhật permalink đi
function vietnit_hide_permalink(){
    echo '<style>table.form-table.permalink-structure {display: none;}</style>';
}


// gỡ bớt những menu ko nên dùng dành cho Client Admin
function vietnit_remove_menus() {

    remove_menu_page( 'genesis' );
    remove_submenu_page( 'themes.php', 'themes.php' );
    remove_submenu_page( 'themes.php', 'customize.php' );

    // thao tac vao 2 bien global $menu va $submenu;    
    // global $ca_pages_ok;
    $ca_pages_ok = VIETNIT_Roles_Setting::get_accepted_slugs();

    // echo 'Slugs: ';
    // print_r($ca_pages_ok);    
    // xu ly menu tong the
    global $menu;

    // echo '<!--';
    // print_r($menu);
    // echo '-->'; 
    
    // những thành phần menu dc phép hiện thị cho Client Admins
    $def_ca_pages_ok = array('options-general.php');    
    $my_pages_ok = array_merge($def_ca_pages_ok, $ca_pages_ok);

    // themes.php : users.php : options-general.php : separator : genesis : WP-Optimize : custompage :
    foreach ($menu as $key => $value) {        
        if(isset($value[1]) && $value[1] === 'manage_options'){ // capabilities của admins
            if(isset($value[2]) && !in_array($value[2], $my_pages_ok)){                
                remove_menu_page( $value[2] );
            }
        }            
    }

    // xu ly setting menu
    global $submenu;
    // print_r($submenu);
    if(isset($submenu['options-general.php'])){

        // $tmp = in_array('wpsupercache', $ca_pages_ok);

        foreach ($submenu['options-general.php'] as $key => $value) {            
            if(isset($value[2]) && !in_array($value[2], $ca_pages_ok)){
                // remove_submenu_page( 'options-general.php', $value[2] );
            }
        }
    }
    // xu ly menu tool
    if(isset($submenu['tools.php'])){
        foreach ($submenu['tools.php'] as $key => $value) {                
            if(isset($value[2]) && !in_array($value[2], $ca_pages_ok)){
                // remove_submenu_page( 'tools.php', $value[2] );
            }          
        }
    }
}



// bỏ bớt những metabox dư thừa đi
function vietnit_remove_dashboard_widgets() {   
    
    // thuc chat la thao tac vao bien global $wp_meta_boxes
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );    
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );      

    // thu remove metabox cua genesis
    // remove_meta_box( 'genesis-theme-settings-version', 'genesis', 'normal' );      

      
}


// Remove 'Administrator' from the list of roles if the current user is not an admin
function editable_roles( $roles ){
    $current_user = wp_get_current_user();
    if( isset( $roles['administrator'] ) && $current_user->roles[0] !== 'administrator' ){
        unset( $roles['administrator']);
    }
    return $roles;
}

// If someone is trying to edit or delete and admin and that user isn't an admin, don't allow it
function vietnit_map_meta_cap( $caps, $cap, $user_id, $args ){
    $current_user = wp_get_current_user();
    switch( $cap ){
        case 'edit_user':
        case 'remove_user':
        case 'promote_user':
            if( isset($args[0]) && $args[0] == $user_id )
                break;
            elseif( !isset($args[0]) )
                $caps[] = 'do_not_allow';
            $other = new WP_User( absint($args[0]) );
            if( $other->roles[0] === 'administrator' ){
                if($current_user->roles[0] !== 'administrator'){
                    $caps[] = 'do_not_allow';
                }
            }            
            break;
        case 'delete_user':
        case 'delete_users':
            if( !isset($args[0]) )
                break;
            $other = new WP_User( absint($args[0]) );
            if( $other->roles[0] === 'administrator' ){
                if($current_user->roles[0] !== 'administrator'){
                    $caps[] = 'do_not_allow';
                }
            }            
            break;
        default:
            break;
    }
    return $caps;
}


class VIETNIT_Roles_Setting
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options; // la array cac slug and capabilities   

    /**
     * Start up
     */
    public function __construct()
    {
        

        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'VietNIT Roles Admin', 
            'administrator', 
            'VietNIT-roles-management', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property               
        $this->options = get_option( '_vietnit_roles_option', array());
        

        ?>
        <div class="wrap">
            
            <h2>Quản lý truy cập của VietNIT Sub và VietNIT Client Administrator</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'vietnit_roles_option_group' );   // group option name
                do_settings_sections( 'VietNIT-roles-admin' ); // page
                submit_button(); 
            ?>
            </form>
            
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        // group ------------
        register_setting(
            'vietnit_roles_option_group', // Option group
            '_vietnit_roles_option', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );  

        add_settings_section(
            'vietnit_roles_section_id', // ID
            '', // Title
            array( $this, 'vietnit_roles_section_info' ), // Callback
            'VietNIT-roles-admin' // Page
        );

        add_settings_field(
            'slugs_list', // id
            'Danh sách slug cho phép Client Admin: ', // title
            array( $this, 'vietnit_slugs_callback' ), // callback
            'VietNIT-roles-admin', // page
            'vietnit_roles_section_id' // section id
        ); 

        add_settings_field(
            'capas_list', // id
            'Danh sách capabilities cho phép cả Sub and Client Admin: ', // title
            array( $this, 'vietnit_capas_callback' ), // callback
            'VietNIT-roles-admin', // page
            'vietnit_roles_section_id' // section id
        );   
    }

    /**
     * Sanitize each setting field as needed => làm sạch dữ liệu input
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {       
        
        $new_input = array();
        if(isset($input['slugs_list'])){
                        
            $slugs = $input['slugs_list'];            
            $arr = explode(',', $slugs);
            $new_arr = array();
            foreach ($arr as $value) {
                $tmp = esc_attr(trim($value));
                if($tmp) $new_arr[] = $tmp;
            }             
            
            $new_input['slugs_list'] = implode(', ', $new_arr);                       
        }

        if(isset($input['capas_list'])){
                        
            $capas = $input['capas_list'];            
            $arr = explode(',', $capas);
            $new_arr = array();
            foreach ($arr as $value) {
                $tmp = esc_attr(trim($value));
                if($tmp) $new_arr[] = $tmp;
            }             
            
            $new_input['capas_list'] = implode(', ', $new_arr);     

            // cap nhat lai capability cho Vietnit Client Admin va Vietnit Sub Admin
            remove_role('vietnit_sub_admin');
            remove_role('vietnit_client_admin');
            add_vietnit_role($new_arr, $new_arr);
            // end here                  
        }

        return $new_input;

    }

    public function vietnit_slugs_callback()
    {
        
        $slugs = isset($this->options['slugs_list']) ? $this->options['slugs_list'] : '';       
                

        ?>        
        <input type='textbox' id='slugs_list' name='_vietnit_roles_option[slugs_list]' value='<?php echo $slugs;?>' size='150'><br>
        <h4>Ví dụ:<h4>
        <i>
        - http://.../add-new.php => slug: add-new.php<br>
        - http://.../admin.php?page=vietnit-context => slug: vietnit-context<br>        
        - http://.../options-general.php?page=vietnit-roles-page => slug: vietnit-roles-page<br>
        Cần nhập vào: add-new.php, vietnit-context, vietnit-roles-page (mỗi slug cách nhau dấy ,) để cho phép VietNIT Client Admin truy cập 3 thành phần trên.<br>
        </i>
        <?php
        // global $menu;
        // global $submenu;
        // echo '<!--';
        // print_r($menu);
        // print_r($submenu);
        // echo '-->';
    }

    public function vietnit_capas_callback()
    {            
        
        $capabilities = isset($this->options['capas_list']) ? $this->options['capas_list'] : '';

        ?>        
        <input type='textbox' id='capas_list' name='_vietnit_roles_option[capas_list]' value='<?php echo $capabilities;?>' size='150'><br>
        <h4>Guide:<h4>
        <i> Trường hợp plugin tự tạo riêng capability của mình, và tự gán capability đó cho Administrator thì có thể VietNIT Sub Admin và VietNIT Client Admin sẽ ko có capability đó.
            Và khi đó, ta cần tự bổ sung thêm capbility đó vào hai nhóm user riêng của VietNIT. <br>
            Để xem danh sách capability của administrator, bạn cần View Source Html trang này, và tìm đến đoạn comment chứa 'WP_Role Object' 
            rồi tự đoán xem capability do plugin thêm vào, thường các capabilities đó được thêm vào ở cuối mảng capabilities trong comment nói trên.            
        </i>
        <?php
        echo '<!--';
        $val = get_role('administrator');
        print_r($val);
        echo '-->';

    }

    public function vietnit_roles_section_info()
    {
        echo '<h3>Nhập danh sách các plugin slug thuộc quản lý của Administrator mà VietNIT Client Admin được phép truy cập:</h3>';        
    }   

    public static function get_accepted_slugs(){
        $value = get_option('_vietnit_roles_option');
        
        if(isset($value['slugs_list'])){
            $res = explode(', ', $value['slugs_list']); 
            return $res;
        }else
            return array();
    }     
}