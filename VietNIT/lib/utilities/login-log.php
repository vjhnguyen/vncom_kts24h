<?php
/*
* This utilitied is copy (with remove unuse function and improve performance) of Simple Login Log plugin
* Modified by VietNIT
*/


class VietnitLoginLog
{
    private $is_installed;
    public $table = 'caia_login_log';        
    private $login_success = 1;
    public $data_labels = array();
    private $values;

    function __construct()
    {
        global $wpdb;

        if ( is_multisite() )
        {
            // get main site's table prefix
            $main_prefix = $wpdb->get_blog_prefix(1);
            $this->table = $main_prefix . $this->table;
        }
        else
        {
            // non-multisite - regular table name
            $this->table = $wpdb->prefix . $this->table;
        }
        
        //Install DB if not yet
        $this->is_installed = get_option( "caia_sll_installed", false );
        if(!$this->is_installed){
            $this->install();            
        }

        //Init login actions
        add_action( 'init', array($this, 'init_login_actions') );


        if(is_admin()){
            add_action( 'admin_menu', array($this, 'sll_admin_menu') );            
            add_action( 'admin_head-users_page_login_log', array($this, 'screen_options') );
            

            //Init CSV Export            
            add_action('admin_init-users_page_login_log', array($this, 'delete_old90days') );

            //Style the log table
            add_action( 'admin_head-users_page_login_log', array($this, 'admin_header') );
        }

        
        //For translation purposes
        $this->data_labels = array(
            'Successful'        => __('Successful', 'sll'),
            'Failed'            => __('Failed', 'sll'),
            'Login'             => __('Login', 'sll'),
            'User Agent'        => __('User Agent', 'sll'),
            'Login Redirect'    => __('Login Redirect', 'sll'),
            'id'                => __('#', 'sll'),
            'uid'               => __('User ID', 'sll'),
            'user_login'        => __('Username', 'sll'),
            'user_role'         => __('User Role', 'sll'),
            'name'              => __('Name', 'sll'),
            'time'              => __('Time', 'sll'),
            'ip'                => __('IP Address', 'sll'),
            'login_result'      => __('Login Result', 'sll'),
            'data'              => __('Data', 'sll'),
        );
    }


    function set($name, $value)
    {
        $this->values[$name] = $value;
    }


    function get($name)
    {
        return (isset($this->values[$name])) ? $this->values[$name] : false;
    }      


    function screen_options()
    {
        $page = ( isset($_GET['page']) ) ? esc_attr($_GET['page']) : false;
        if( 'login_log' !== $page )
            return;

        //needs to be initialized early enough to pre-fill screen options section in the upper (hidden) area.
        $this->log_table = new SLL_List_Table;
    }


    function init_login_actions()
    {
        //condition to check if "log failed attempts" option is selected

        //Action on successful login
        add_action( 'wp_login', array($this, 'login_success') );

        //Action on failed login
        // if( isset($this->opt['failed_attempts']) ){
        //     add_action( 'wp_login_failed', array($this, 'login_failed') );
        // }

    }


    function login_success( $user_login )
    {
        $this->login_success = 1;
        $this->login_action( $user_login );
    }


    function login_failed( $user_login )
    {
        $this->login_success = 0;
        $this->login_action( $user_login );
    }



    function delete_old90days()
    {
        $page = ( isset($_GET['page']) ) ? esc_attr($_GET['page']) : false;
        if( 'login_log' !== $page )
            return;

        $nonce = isset($_REQUEST['_wpnonce']) ? $_REQUEST['_wpnonce'] : false;

        if (!wp_verify_nonce($nonce, 'delete_sll'))
        {
            return;
        }
        else
        {
            global $wpdb;
            // $sql = "DELETE FROM {$this->table}";
            $sql = $wpdb->prepare( "DELETE FROM {$this->table} WHERE time < DATE_SUB(CURDATE(),INTERVAL %d DAY)", 90);

            if ($wpdb->query($sql))
            {
                $this->set('deleted', true);
            }
        }
    }



    /**
    * Runs installed at the firstime use
    */
    function install()
    {
        
        if( !$this->is_installed )
        {
            global $wpdb;
            //if table does't exist, create a new one
            if( !$wpdb->get_row("SHOW TABLES LIKE '{$this->table}'") ){
                $sql = "CREATE TABLE  " . $this->table . "
                    (
                        id INT( 11 ) NOT NULL AUTO_INCREMENT ,
                        uid INT( 11 ) NOT NULL ,
                        user_login VARCHAR( 60 ) NOT NULL ,
                        user_role VARCHAR( 30 ) NOT NULL ,
                        time DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL ,
                        ip VARCHAR( 100 ) NOT NULL ,
                        login_result VARCHAR (1) ,
                        data LONGTEXT NOT NULL ,
                        PRIMARY KEY ( id ) ,
                        INDEX ( uid, ip, login_result )
                    );";

                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
                dbDelta($sql);
                
            }
            update_option( "caia_sll_installed", true );
        }
    }



    function sll_admin_menu()
    {
        add_submenu_page( 'users.php', __('Simple Login Log', 'sll'), __('Login Log', 'sll'), 'list_users', 'login_log', array($this, 'log_manager') );
    }



    function admin_header()
    {
        $page = ( isset($_GET['page']) ) ? esc_attr($_GET['page']) : false;
        if( 'login_log' != $page )
            return;

        echo '<style type="text/css">';
        echo 'table.users { table-layout: auto; }';
        echo '</style>';
    }


    //Catch messages on successful login
    function login_action($user_login)
    {

        $userdata = get_user_by('login', $user_login);

        $uid = ($userdata && $userdata->ID) ? $userdata->ID : 0;

        $data[$this->data_labels['Login']] = ( 1 == $this->login_success ) ? $this->data_labels['Successful'] : $this->data_labels['Failed'];
        if ( isset( $_REQUEST['redirect_to'] ) ) { $data[$this->data_labels['Login Redirect']] = esc_attr( $_REQUEST['redirect_to'] ); }
        $data[$this->data_labels['User Agent']] = esc_attr( $_SERVER['HTTP_USER_AGENT'] );

        $serialized_data = serialize($data);

        //get user role
        $user_role = '';
        if( $uid ){
            $user = new WP_User( $uid );
            if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
                $user_role = implode(', ', $user->roles);
            }
        }


        $values = array(
            'uid'           => $uid,
            'user_login'    => $user_login,
            'user_role'     => $user_role,
            'time'          => current_time('mysql'),
            'ip'            => isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? esc_attr($_SERVER['HTTP_X_FORWARDED_FOR']) : esc_attr($_SERVER['REMOTE_ADDR']),
            'login_result'  => $this->login_success,
            'data'          => $serialized_data,
            );

        $format = array('%d', '%s', '%s', '%s', '%s', '%s', '%s');

        $this->save_data($values, $format);
    }


    function save_data($values, $format)
    {
        global $wpdb;

        $wpdb->insert( $this->table, $values, $format );
    }


    function make_where_query()
    {
        $where = false;
        if( isset($_GET['filter']) && '' != $_GET['filter'] )
        {
            $filter = esc_attr( $_GET['filter'] );
            $where['filter'] = "(user_login LIKE '%{$filter}%' OR ip LIKE '%{$filter}%')";
        }
        if( isset($_GET['user_role']) && '' != $_GET['user_role'] )
        {
            $user_role = esc_attr( $_GET['user_role'] );
            $where['user_role'] = "user_role = '{$user_role}'";
        }
        if( isset($_GET['result']) && '' != $_GET['result'] )
        {
            $result = esc_attr( $_GET['result'] );
            $where['result'] = "login_result = '{$result}'";
        }
        if( isset($_GET['datefilter']) && '' != $_GET['datefilter'] )
        {
            $datefilter = esc_attr( $_GET['datefilter'] );
            $year = substr($datefilter, 0, 4);
            $month = substr($datefilter, -2);
            $where['datefilter'] = "YEAR(time) = {$year} AND MONTH(time) = {$month}";
        }

        return $where;
    }


    function getLimit()
    {
        // return ' LIMIT ' . get_option('users_page_login_log_per_page', 20);
        return ' LIMIT ' . 20;
    }


    function log_get_data($orderby = false, $order = false, $limit = 0, $offset = 0)
    {
        global $wpdb;

        $where = '';

        $where = $this->make_where_query();

        $orderby = (!isset($orderby) || $orderby == '') ? 'time' : $orderby;
        $order = (!isset($order) || $order == '') ? 'DESC' : $order;

        if( is_array($where) && !empty($where) )
            $where = ' WHERE ' . implode(' AND ', $where);

        $sql = "SELECT * FROM $this->table" . $where . " ORDER BY {$orderby} {$order} " . 'LIMIT ' . $limit . ' OFFSET ' . $offset;
        $data = $wpdb->get_results($sql, 'ARRAY_A');

        return $data;
    }


    function log_manager()
    {

        $log_table = $this->log_table;

        $log_table->prepare_items();

        echo '<div class="wrap srp">';
            echo '<h2>' . __('Login Log', 'sll') . '</h2>';

            if ($this->get('deleted'))
            {
                echo '<div class="updated"><p>All records older than 90 days were deleted.</p></div>';
            }

            echo '<div class="tablenav top">';
                echo '<div class="alignleft actions">';
                    echo $this->date_filter();
                echo '</div>';

                $username = ( isset($_GET['filter']) ) ? esc_attr($_GET['filter']) : false;
                echo '<form method="get" class="alignright">';
                    echo '<p class="search-box">';
                        echo '<input type="hidden" name="page" value="login_log" />';
                        echo '<label>' . __('Username:', 'sll') . ' </label><input type="text" name="filter" class="filter-username" value="' . $username . '" /> <input class="button" type="submit" value="' . __('Filter User', 'sll') . '" />';
                        echo '<br />';
                    echo '</p>';
                echo '</form>';
            echo '</div>';
            echo '<div class="tablenav top">';                   

                echo '<div class="alignright actions">';
                $mode = ( isset($_GET['mode']) ) ? esc_attr($_GET['mode']) : false;
                $log_table->view_switcher($mode);
                echo '</div>';
            echo '</div>';

            $log_table->display();

            echo '<form method="get" id="export-login-log">';
            if ( function_exists('wp_nonce_field') )
                wp_nonce_field('ssl_export_log');

            echo '<input type="hidden" name="page" value="login_log" />';
            echo '<input type="hidden" name="download-login-log" value="true" />';
            echo '<p class="submit">';
            // echo '<input type="submit" name="submit" id="submit" class="button" value="Export Log to CSV">';
            echo '&nbsp;&nbsp;<a id="delete-all" href="' . wp_nonce_url('users.php?page=login_log&action=delete', 'delete_sll') . '" onclick="return confirm(\'IMPORTANT: All User Log records older than 90 days will be deleted.\')">Delete recorder older 90 days</a>';
            echo '</p>';
            echo '</form>';
            //if filtered results - add export filtered results button
            $where = false;
            if( isset( $_GET['filter'] ) || isset( $_GET['user_role'] ) || isset( $_GET['datefilter'] ) || isset( $_GET['result'] ) )
            {
                $where = array();
                foreach($_GET as $k => $v)
                {
                    $where[$k] = @esc_attr($v);
                }
                echo '<form method="get" id="export-login-log">';
                if ( function_exists('wp_nonce_field') )
                    wp_nonce_field('ssl_export_log');

                echo '<input type="hidden" name="page" value="login_log" />';
                echo '<input type="hidden" name="download-login-log" value="true" />';
                echo '<input type="hidden" name="where" value="' . esc_attr(serialize($where)) . '" />';
                // submit_button( __('Export Current Results to CSV', 'sll'), 'secondary' );
                echo '</form>';

            }

        echo '</div>';
    }


    function date_filter()
    {
        global $wpdb;
        $sql = "SELECT DISTINCT YEAR(time) as year, MONTH(time)as month FROM {$this->table} ORDER BY YEAR(time), MONTH(time) desc";
        $results = $wpdb->get_results($sql);

        if(!$results)
            return;


        $option = '';
        foreach($results as $row)
        {
            //represent month in double digits
            $timestamp = mktime(0, 0, 0, $row->month, 1, $row->year);
            $month = (strlen($row->month) == 1) ? '0' . $row->month : $row->month;
            $datefilter = ( isset($_GET['datefilter']) ) ? $_GET['datefilter'] : false;
            $option .= '<option value="' . $row->year . $month . '" ' . selected($row->year . $month, $datefilter, false) . '>' . date('F', $timestamp) . ' ' . $row->year . '</option>';
        }

        $output = '<form method="get">';
        $output .= '<input type="hidden" name="page" value="login_log" />';
        $output .= '<select name="datefilter"><option value="">' . __('View All', 'sll') . '</option>' . $option . '</select>';
        $output .= '<input class="button" type="submit" value="' . __('Filter', 'sll') . '" />';
        $output .= '</form>';
        return $output;
    }

} // end class Caia Login Log



if( class_exists( 'VietnitLoginLog' ) )
{
    $sll = new VietnitLoginLog;
}

if(!class_exists('WP_List_Table'))
{
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class SLL_List_Table extends WP_List_Table
{
    private $sllData;

    function __construct()
    {
        global $sll, $_wp_column_headers;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'user',     //singular name of the listed records
            'plural'    => 'users',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

        $this->data_labels = $sll->data_labels;

    }


    function set($name, $value)
    {
        $this->sllData[$name] = $value;
    }


    function get($name)
    {
        return (isset($this->sllData[$name])) ? $this->sllData[$name] : false;
    }


    function column_default($item, $column_name)
    {
        $item = apply_filters('sll-output-data', $item);

        //unset existing filter and pagination
        $args = wp_parse_args( parse_url($_SERVER["REQUEST_URI"], PHP_URL_QUERY) );
        unset($args['filter']);
        unset($args['paged']);

        switch($column_name){
            case 'id':
            case 'uid':
            case 'time':
            case 'ip':
                return $item[$column_name];
            case 'user_login':
                return "<a href='" . add_query_arg( array('filter' => $item[$column_name]), menu_page_url('login_log', false) ) . "' title='" . __('Filter log by this name', 'sll') . "'>{$item[$column_name]}</a>";
            case 'name':
                $user_info = get_userdata($item['uid']);
                return ( is_object($user_info) ) ? $user_info->first_name .  " " . $user_info->last_name : false;
            case 'login_result':
                if ( '' == $item[$column_name]) return '';
                return ( '1' == $item[$column_name] ) ? __($this->data_labels['Successful'], 'sll') : '<div class="login-failed">' . __($this->data_labels['Failed'], 'sll') . '</div>';
            case 'user_role':
                if( !$item['uid'] )
                    return;

                $user = new WP_User( $item['uid'] );
                if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
                    foreach($user->roles as $role){
                        $roles[] = "<a href='" . add_query_arg( array('user_role' => $role), menu_page_url('login_log', false) ) . "' title='" . __('Filter log by User Role', 'sll') . "'>{$role}</a>";
                    }
                    return implode(', ', $roles);
                }
                break;
            case 'data':
                $data = unserialize($item[$column_name]);
                if(is_array($data))
                {
                    $output = '';
                    foreach($data as $k => $v)
                    {
                        $output .= $k .': '. $v .'<br />';
                    }

                    $output = ( isset($_GET['mode']) && 'excerpt' == $_GET['mode'] ) ? $output : substr($output, 0, 50) . '...';

                    if( isset($data[$this->data_labels['Login']]) && $data[$this->data_labels['Login']] == $this->data_labels['Failed'] ){
                        return '<div class="login-failed">' . $output . '</div>';
                    }
                    return $output;
                }
                break;
            default:
                return $item[$column_name];
        }
    }


    function get_columns()
    {
        global $status;
        $columns = array(
            'id'            => __('#', 'sll'),
            'uid'           => __('User ID', 'sll'),
            'user_login'    => __('Username', 'sll'),
            'user_role'     => __('User Role', 'sll'),
            'name'          => __('Name', 'sll'),
            'time'          => __('Time', 'sll'),
            'ip'            => __('IP Address', 'sll'),
            'login_result'  => __('Login Result', 'sll'),
            'data'          => __('Data', 'sll'),
        );
        return $columns;
    }


    function get_sortable_columns()
    {
        $sortable_columns = array(
            //'id'    => array('id',true),     //doesn't sort correctly
            'uid'           => array('uid',false),
            'user_login'    => array('user_login', false),
            'time'          => array('time',true),
            'ip'            => array('ip', false),
        );
        return $sortable_columns;
    }


    function get_views()
    {
        //creating class="current" variables
        if( !isset($_GET['result']) ){
            $all = 'class="current"';
            $success = '';
            $failed = '';
        }else{
            $all = '';
            $success = ( '1' == $_GET['result'] ) ? 'class="current"' : '';
            $failed = ( '0' == $_GET['result'] ) ? 'class="current"' : '';
        }



        //if date filter is set, adjust views label to reflect the date
        $date_label = false;
        if( isset($_GET['datefilter']) && !empty($_GET['datefilter']) ){
            $year = substr($_GET['datefilter'], 0, 4);
            $month = substr($_GET['datefilter'], -2);
            $timestamp = mktime(0, 0, 0, $month, 1, $year);
            $date_label = date('F', $timestamp) . ' ' . $year . ' ';
        }

        //get args from the URL
        $args = wp_parse_args( parse_url($_SERVER["REQUEST_URI"], PHP_URL_QUERY) );
        //the only arguments we can pass are mode and datefilter
        $param = false;
        if( isset($args['mode']) )
            $param['mode'] = $args['mode'];

        if( isset($args['datefilter']) )
            $param['datefilter'] = $args['datefilter'];

        //creating base url for the views links
        $menu_page_url = menu_page_url('login_log', false);
        ( is_array($param) && !empty($param) ) ? $url = add_query_arg( $param, $menu_page_url) : $url = $menu_page_url;

        //definition for views array
        $views = array(
            'all' => $date_label . __('Login Results', 'sll') . ': <a ' . $all . ' href="' . $url . '">' . __('All', 'sll') . '</a>' . '(' .$this->get('allTotal') . ')',
            'success' => '<a ' . $success . ' href="' . $url . '&result=1">' . __('Successful', 'sll') . '</a> (' . $this->get('successTotal') . ')',
            'failed' => '<a ' . $failed . ' href="' . $url . '&result=0">' . __('Failed', 'sll') . '</a>' . '(' . $this->get('failedTotal') . ')',
        );

        return $views;
    }


    function prepare_items()
    {
        global $wpdb, $sll;

        //get number of successful and failed logins so we can display them in parentheces for each view

        //building a WHERE SQL query for each view
        $where = $sll->make_where_query();
        //we only need the date filter, everything else need to be unset
        if( is_array($where) && isset($where['datefilter']) ){
            $where = array( 'datefilter' =>  $where['datefilter'] );
        }else{
            $where = false;
        }

        $where3 = $where2 = $where1 = $where;
        $where2['login_result'] = "login_result = '1'";
        $where3['login_result'] = "login_result = '0'";

        if(is_array($where1) && !empty($where1)){
            $where1 = 'WHERE ' . implode(' AND ', $where1);
        }
        $where2 = 'WHERE ' . implode(' AND ', $where2);
        $where3 = 'WHERE ' . implode(' AND ', $where3);

        $sql1 = "SELECT count(*) FROM {$sll->table} {$where1}";
        $allTotal = $wpdb->get_var($sql1);
        $sql2 = "SELECT count(*) FROM {$sll->table} {$where2}";
        $successTotal = $wpdb->get_var($sql2);
        $sql3 = "SELECT count(*) FROM {$sll->table} {$where3}";
        $failedTotal = $wpdb->get_var($sql3);

        $this->set('allTotal', $allTotal);
        $this->set('successTotal', $successTotal);
        $this->set('failedTotal', $failedTotal);

        $screen = get_current_screen();

        /**
         * First, lets decide how many records per page to show
         */
        // $per_page_option = $screen->id . '_per_page';
        // $per_page = get_option($per_page_option, 20);
        // $per_page = ($per_page != false) ? $per_page : 20;
        $per_page = 20;

        $offset = $per_page * ($this->get_pagenum() - 1);

        $orderby = (isset($_REQUEST['orderby']) && !empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : false;
        $order = (isset($_REQUEST['order']) && !empty($_REQUEST['order'])) ? $_REQUEST['order'] : false;

        $this->items = $sll->log_get_data($orderby, $order, $per_page, $offset);

        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden_cols = get_user_option( 'manage' . $screen->id . 'columnshidden' );
        $hidden = ( $hidden_cols ) ? $hidden_cols : array();
        $sortable = $this->get_sortable_columns();


        /**
         * REQUIRED. Finally, we build an array to be used by the class for column
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);
        $columns = get_column_headers( $screen );



        /**
         * REQUIRED for pagination. Let's figure out what page the user is currently
         * looking at. We'll need this later, so you should always include it in
         * your own package classes.
         */
        $current_page = $this->get_pagenum();

        /**
         * REQUIRED for pagination. Let's check how many items are in our data array.
         * In real-world use, this would be the total number of items in your database,
         * without filtering. We'll need this later, so you should always include it
         * in your own package classes.
         */

        if (isset($_GET['result']) && $_GET['result'] == '1')
        {
            $total_items = $wpdb->get_var("SELECT COUNT(*) FROM $sll->table {$where2}");
        }
        else if(isset($_GET['result']) && $_GET['result'] == '0')
        {
            $total_items = $wpdb->get_var("SELECT COUNT(*) FROM $sll->table {$where3}");
        }
        else
        {
            $total_items = $wpdb->get_var("SELECT COUNT(*) FROM $sll->table");
        }


        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );

    }

}