<?php
/**
 * Hook block
 * @since 2.0
 * @developer Thanh Vu
 */
class Caia_Code_Block extends Caia_Block
{
	// protected $defaults;

	function __construct()
	{
		$this->defaults = array(
				'title' => '',
				'code' => ''
			);
		

		$id_base = 'code-block';
		$name = __( 'Code Block', 'caia' );

		$this->Caia_Block( $id_base, $name, $this->defaults );
	}

	/**
	 * Display this block base on its settings.
	 * This function override the parents function.
	 *
	 * @since 1.0
	 */
	function show()
	{
		?>

		<div id="<?php echo $this->id_base . '-' . $this->number; ?>" class="<?php $this->block_class(); ?>">
			<?php do_action('caia_code_block_do_content', $this->options['code'], $this->options['title'])?>
		</div>

		<?php
	}


	/**
	 * Add classes to news block
	 *
	 * @since  2.0.0
	 *
	 * @param array $classes The default classes
	 * @param string $id_base
	 *
	 * @return array
	 */	
	protected function block_class( $class = '' ){

		$classes = array();
		if ($this->get_field_value( 'code' )) $classes[] = 'code-' . $this->get_field_value('code');		
		if (!$this->get_field_value( 'title' )) $classes[] = 'empty-block-title';

		parent::block_class();

		$my_classes = implode( ' ', $classes );
		echo ' ' . $my_classes;
	}

	/**
	 * Display this block settings on admin screen.
	 * This function override the parents function.
	 *
	 * @since 1.0
	 */
	function form()
	{
		?>

	    <p>
	        <label>
				<?php _e( 'Title:', 'caia' ); ?><br/>
	            <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $this->get_field_value( 'title' ); ?>" />
	        </label>
	    </p>

	    <p>
			<label>
				<?php caia_dropdown_block_code(array('name' => $this->get_field_name( 'code' ),
													'selected' => $this->get_field_value( 'code' )));
				?>								
				<?php _e( 'Block code (only coder use, don\'t change!)', 'caia' ); ?><br />
			</label>
		</p>
		

		<?php
	}
}